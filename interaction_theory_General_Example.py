#%% MODULES IMPORT

# Import classes
#==============================================================================
from post_processing import *
from env import Environment
from farm import Farm
from class_body import *
from classes_farm_mechanics import *
from class_Mechanics import Mechanics
from class_free_surface import Free_surface
from layout_generation import *
#==============================================================================
# Import python tools
#==============================================================================
import numpy as n
import math as m
import pickle
from class_sim import Simulation
import scipy.io as io
import matplotlib.pyplot as plt
#==============================================================================

#%% 
###############################################################################
#
# STEP 1 : ENVIRONMENTAL CONDITIONS DEFINITION
#
###############################################################################

# Initialisation of Environmental Class
#==============================================================================
depth         = 50.0
water_density = 1025
gravity       = 9.81

omegamin = 1.0
omegamax = 3.14
omeganum = 2
#==============================================================================

frequencies = n.linspace(omegamin, omegamax, omeganum)

myenv = Environment(frequencies, depth, water_density, gravity)


#%% 
###############################################################################
#
# STEP 2 : DEFINE TYPE OF WECS PRESENT IN THE FARM
#
###############################################################################

# Definition of the body parameters
#==============================================================================
name            = 'cylinder'    # name of the body
dof_body        = 6             # number of degrees of freedom of the body
trunc_angmode   = None          # trunc_angmode = None, automatic truncation
                                # trunc_angmode = [list], user-defined truncation
trunc_depthmode = [0]*omeganum  # trunc_depthmode = [list]
                                # trunc_depthmode = None, not allowed!
cyl_rad         = 3.            # Radius of the body circumscribing cylinder [m]
cG              = [0,0,-3.]     # (x,y,z)[m] Center of Gravity
#==============================================================================

#Initialisation of Body Class
#==============================================================================
body_unique_1 = BodyUnique(name,cG,dof_body,myenv,trunc_angmode,trunc_depthmode,cyl_rad)

dict_bodies = {'cylinder': body_unique_1}

#To Save the body class:
with open('dict_bodies', 'wb') as f:
     pickle.dump(dict_bodies,f)
#     
##%% Import existing body hydrodynamic characteristics
#     
# To import an existing body dictionary:
with open(r'D:\Clement\Students\Theses\Fabregas\IT_SOLVER_FFF\dict_bodies'.replace("\\", "//"),'rb') as f:
    dict_bodies = pickle.load(f)
    
    
#%%
###############################################################################
#
# STEP 3 : DEFINE POSITION (X,Y,rotation) OF EACH OF THE WECS ON THE FARM
#
###############################################################################



# Definition of the farm parameters
#==============================================================================
d = 20                                      # body separating distance
rotation_angle  = 0.0 #(degrees)            # body rotation
name            = 'cylinder'                # name of the body

body1 = Body(name, 0, 0, rotation_angle)
body2 = Body(name, d, 0, rotation_angle)

# Comment : for more complex array layouts, see functions in layout_generation.py
#==============================================================================

layout = [body1,body2]

#Initialisation of the farm 
#==============================================================================
myfarm = Farm(layout, myenv, dict_bodies)

##%% Define a more complex array layout using function in layout_generation.py:

##Example:
#dx = 7.2
#dy = 24
#cyl_rad = 3.
#nfloaters = 60
#
#layout    = []
#
#(x,y,r) = wavestar_config_star(nfloaters, cyl_rad*2, dx, dy)
#
#plt.scatter(x,y)
#
#
#for ind in range(0,nfloaters):
#    
#    layout.append(Body(name, x[ind], y[ind], r[ind]))


#%% 
###############################################################################
#
# STEP 4 : DEFINITION OF THE CALCULATION CASES (REGULAR WAVES)
#
###############################################################################

# Initialisation of the calculation
#==============================================================================
ndir = 20                         # Number of wave directions

beta_array = n.linspace(0,90,ndir)   # Wave directions [radians]

dof_array  = n.arange(3,12,6)    # Degrees of freedom to solve for (radiation)

Hs = [0]                         # Vector of significant wave heights
                                 # Note : for regular wave cases, leave it to [0]
Tp = [0]                         # Vector of peak periods
Prob = [0]                       # Vector of probability of occurences

angular_spreading_parameter = 0  # Value of the angular spreading parameter
                                 # Note : see definition of the angular spreading
                                 # in the environment class (env.py)
main_beta_direction = 0          # Main wave propagation direction
#==============================================================================

# Initialisation of the simulation class
#==============================================================================
mySim = Simulation(beta_array, dof_array, frequencies, Hs, Tp, Prob, ndir, angular_spreading_parameter, main_beta_direction)


#%% STEP 4 : DEFINITION OF THE CALCULATION CASES (IRREGULAR WAVES)

### Initialisation of the calculation
###==============================================================================

#Hs   = [0.75, 0.75, 0.75, 1.25, 1.25, 1.25, 1.75, 1.75, 2.25, 2.25, 2.75]
#Tz   = [3.5, 4.5, 5.5, 3.5, 4.5, 5.5, 4.5, 5.5, 4.5, 5.5, 5.5]
#Prob = [19.2, 11.4, 2.21, 6.84, 13.0, 2.96, 9.58, 3.05, 3.34, 4.6, 3.89]
#      
#Hs = n.asarray(Hs)
#
#Tp = 1.17 * n.asarray(Tz)
#
#Prob = n.asarray(Prob) / 100.
#
#ndir = 5
#
#angular_spreading_parameter = 20
#
#main_beta_direction = 60 * n.pi / 180
#
#beta_array = n.linspace(-n.pi/2. + main_beta_direction, n.pi/2. + main_beta_direction, ndir) 
#
#dof_array = n.arange(3, dof_body * len(layout), 6)
#
#frequencies = myenv.frequency
###==============================================================================
##
## Initialisation of the simulation class
##==============================================================================
#mySim = Simulation(beta_array, dof_array, frequencies, Hs, Tp, Prob, ndir, angular_spreading_parameter, main_beta_direction)


#%% 
###############################################################################
#
# STEP 5 : COMPUTATION OF THE HYDRODYNAMIC COEFFICIENTS 
#
###############################################################################

# Excitation Forces
#==============================================================================
exc_forces = myfarm.gen_excitation_forces_farm(myenv,beta_array)

# Radiation Hydrodynamic Coefficients
#==============================================================================
(AM_coeffs, DAMP_coeffs) = myfarm.gen_hydrodynamic_coefficients_farm(myenv,dof_array)

## To save the matrices:
#with open('AM_coeffs', 'wb') as f:
#     pickle.dump(AM_coeffs,f)
#    
#with open('DAMP_coeffs', 'wb') as f:
#     pickle.dump(DAMP_coeffs,f)
#    
#with open('exc_forces', 'wb') as f:
#     pickle.dump(exc_forces,f)
#     
#sauvegarde en MATLAB
io.savemat('coeffs_hydro_array',{'AM':AM_coeffs,'DAMP':DAMP_coeffs,'exc':exc_forces})


#%% 
###############################################################################
#
# STEP 6 : DEFINE MECHANICAL CHARACTERISTICS
#
###############################################################################


# Mooring definition
#==============================================================================
# Diagonal components of each dof (use none if no k_mooring)
k_mooring = [0,0,0,0,0,0]    # SI units
name = 'cylinder' #name of the WEC to which the mooring will be added

# Mooring used for the WECs
Mooring_1 = Mooring(name, myfarm, k_mooring)

Mooring_layout = [Mooring_1, Mooring_1]
#==============================================================================

# PTO definition
#==============================================================================
# diag components of each freedom deg (use none if no k_pto or b_pto)
BPTO = 4 * (10**6)  # SI units

k_pto = [0, 0, 0, 0, 0, 0]                   
b_pto = [0.0, 0.0, BPTO, 0.0, 0.0, 0.0]     

b_pto_layout = [n.diag(b_pto)] * len(layout)

k_pto_layout = [n.diag(k_pto)] * len(layout)
#==============================================================================

# Initialization of the mechanical class
#------------------------------------------------------------------------------
myMechanics = Mechanics(mySim, myenv, myfarm, Mooring_layout)

# Computation excitation force individual body
ind_body = 0

exc_body = myMechanics._gen_exc_forces_indiv(mySim, myenv, myfarm, ind_body)


#%% 
###############################################################################
#
# STEP 7 : COMPUTE RESPONSE AND POWER PRODUCTION (REGULAR WAVES)
#
###############################################################################

# RAO 
#==============================================================================
ind_beta  = 0   # index of the beta vector
ind_omega = 0

RAO_bodies = myMechanics.RAO_calc(myenv, ind_omega, ind_beta, k_pto_layout, b_pto_layout)


#RAO_bodies = []
#
#for ind_omega in range(0,omeganum):
#
#    RAO_bodies.append(myMechanics.RAO_calc(myenv, ind_omega, ind_beta, k_pto_layout, b_pto_layout))
#    
#RAO_w = n.asarray(RAO_bodies)
#
#ind_body = 0
#
#plt.plot(frequencies,abs(RAO_w[:,ind_body]))
#==============================================================================

# POWER 
#==============================================================================
wave_amp = 1.0  # wave amplitude

Power_bodies = myMechanics.Power_bodies_infarm(myenv, ind_omega, ind_beta, wave_amp, k_pto_layout, b_pto_layout)
#==============================================================================

# q-factor 
#==============================================================================
ind_omega = 0   # index of the omega vector
ind_beta  = 0   # index of the beta vector
wave_amp = 1.0

q_factor_bodies = myMechanics.q_factor_calc(myenv,ind_omega,ind_beta,wave_amp,k_pto_layout,b_pto_layout)
#==============================================================================


#%% 
################################################################################
##
## STEP 7 : COMPUTE RESPONSE AND POWER PRODUCTION (IRREGULAR WAVES)
##
################################################################################


## POWER 
##==============================================================================
#ind_seastate = 0 
#
#Hs = mySim.Hs[ind_seastate] 
#Tp = mySim.Tp[ind_seastate]
#Prob = mySim.Prob[ind_seastate]
#
#wave_amplitudes_array = myenv.bretschneider_spectrum_angular_spreading(Hs, Tp, myenv.frequency, mySim.ndir, mySim.m, mySim.theta_principal)[0]
#
#power_seastate_bodies = myMechanics.Power_bodies_infarm_seastate(myenv, wave_amplitudes_array, k_pto_layout, b_pto_layout)
##==============================================================================
#
## q-factor 
##==============================================================================
#ind_seastate = 0 
#
#Hs = mySim.Hs[ind_seastate] 
#Tp = mySim.Tp[ind_seastate]
#Prob = mySim.Prob[ind_seastate]
#
#wave_amplitudes_array = myenv.bretschneider_spectrum_angular_spreading(Hs, Tp, myenv.frequency, mySim.ndir, mySim.m, mySim.theta_principal)[0]
#
#q_factor_bodies = myMechanics.q_factor_calc_seastate(myenv, wave_amplitudes_array, k_pto_layout, b_pto_layout)
##==============================================================================


#%% 
###############################################################################
#
# STEP 8 : INITIALISE FREE SURFACE ELEVATION 
#
###############################################################################

# Free surface plot parameters
#==============================================================================
# if "relative", domain limits as a function of the circumscribing cylinder
# if "absolute", domain limits in the positive and negative x and y directions
domain_reference = 'absolute'

x_upward  =  60 #in meters
x_leeward =  60
y_upper   =  60
y_lower   =  60

domain_limits    = [x_upward, x_leeward, y_upper, y_lower]

resolution = 0.5
#==============================================================================

myfree_surface = Free_surface(domain_limits, domain_reference, resolution, myfarm, myenv)


#%% 
###############################################################################
#
# STEP 8 : COMPUTE & PLOT FREE SURFACE ELEVATION (REGULAR WAVES)
#
###############################################################################

# Free surface calculation
#==============================================================================
ind_beta = 0
ind_omega = 0
wave_amp = 1.

eta_tot = myfree_surface._eta_total(myfarm, myMechanics, mySim, myenv, ind_beta, ind_omega, wave_amp, k_pto_layout, b_pto_layout)

plot_eta_total_newformat(eta_tot, myfree_surface)


#%% 
###############################################################################
#
# STEP 8 : COMPUTE & PLOT FREE SURFACE ELEVATION (IRREGULAR WAVES)
#
###############################################################################


## Free surface calculation
##==============================================================================
#Hs = mySim.Hs[0]
#Tp = mySim.Tp[0]
#
#wave_amplitudes_array = myenv.bretschneider_spectrum_angular_spreading(Hs, Tp, myenv.frequency, mySim.ndir, mySim.m, mySim.theta_principal)[0]
#
#(dist_coeff, hs_incident) = myfree_surface.disturbance_coefficient_seastate(myfarm ,myMechanics, mySim, myenv ,k_pto_layout, b_pto_layout, wave_amplitudes_array)
#    
#plot_disturbance_coefficient(dist_coeff, myfree_surface)