#IMPORT MODULES
#------------------------------------------------------------------------------
import numpy as np
import cmath as cm
import scipy.special as spec
#------------------------------------------------------------------------------
#
#CLASS DEFINING THE MAPPING FROM POLAR TO CARTESIAN
#------------------------------------------------------------------------------
class Sommerfeld_relation:
    
    # Class Initialization
    #--------------------------------------------------------------------------
    def __init__(self, N_angular_samples):
    
        self.Na       = N_angular_samples
        self.angles   = np.linspace(- np.pi/2,np.pi/2,self.Na)
    
    # Function to calculate total integral
    #--------------------------------------------------------------------------
    def complete_integral(self, x, y, k, n, gamma):
        
        Int_1 = self.first_integral(x, y, k, n, gamma)
        Int_2 = self.second_integral(x, y, k, n)
        Int_3 = self.third_integral(x, y, k, n, gamma)
        
        Total = (((-1j)**n) / np.pi) * (Int_1 + Int_2 + Int_3)
        
        return Total
    #--------------------------------------------------------------------------    
    #
    # Function to calculate first integral
    #--------------------------------------------------------------------------
    def first_integral(self, x, y, k, n, gamma):
        
        V_I = np.zeros(self.Na, dtype = complex)
        
        # Create V_I
        ti = np.linspace(-gamma-1.,-1.,self.Na)
        
        for ind in range(0, self.Na):
            
            V_I[ind] = (-1j)*((-1j)**n) * np.exp(n*(1+ti[ind])) * \
                                  np.exp(k*x*np.sinh(1+ti[ind])) * \
                                  np.exp(-1j * k * y * np.cosh(1+ti[ind]))
                                  
        # Create I_I
        Ax = gamma / (self.Na - 1.0)
        I_I = np.asarray([2.0] * self.Na)
        I_I[0] = 1.0
        I_I[self.Na - 1] = 1.0
        I_I = (Ax / 2.0) * I_I
        
        # Compute first integral
        Int_1 = np.dot(V_I, I_I)
        
        return Int_1
    #--------------------------------------------------------------------------    
    #  
    # Function to calculate second integral
    #--------------------------------------------------------------------------
    def second_integral(self, x, y, k, n):
        
        V_II = np.zeros(self.Na, dtype = complex)
        
        # Create V_I
        ti = np.linspace(-1.,1.,self.Na)
        
        for ind in range(0, self.Na):
            
            V_II[ind] = (np.pi / 2.0) * np.exp(1j * n * (np.pi / 2.) * ti[ind]) * \
                                  np.exp(1j * k * x * np.cos((np.pi / 2.) * ti[ind])) * \
                                  np.exp(1j * k * y * np.sin((np.pi / 2.0) * ti[ind]))
                                  
        # Create I_I
        Ax = 2.0 / (self.Na - 1.0)
        I_I = np.asarray([2.0] * self.Na)
        I_I[0] = 1.0
        I_I[self.Na - 1] = 1.0
        I_I = (Ax / 2.0) * I_I
        
        # Compute first integral
        Int_2 = np.dot(V_II, I_I)
        
        return Int_2
    #--------------------------------------------------------------------------
    #
    # Function to calculate third integral
    #--------------------------------------------------------------------------
    def third_integral(self, x, y, k, n, gamma):
        
        V_III = np.zeros(self.Na, dtype = complex)
        
        # Create V_I
        ti = np.linspace(1.,gamma + 1.,self.Na)
        
        for ind in range(0, self.Na):
            
            V_III[ind] = (-1j)*((1j)**n) * np.exp(- n*(1-ti[ind])) * \
                                  np.exp(k*x*np.sinh(1-ti[ind])) * \
                                  np.exp(1j * k * y * np.cosh(1-ti[ind]))
                                  
        # Create I_I
        Ax = gamma / (self.Na - 1.0)
        I_I = np.asarray([2.0] * self.Na)
        I_I[0] = 1.0
        I_I[self.Na - 1] = 1.0
        I_I = (Ax / 2.0) * I_I
        
        # Compute first integral
        Int_3 = np.dot(V_III, I_I)
        
        return Int_3
        