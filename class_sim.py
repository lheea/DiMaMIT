# Import relevant modules
#------------------------------------------------------------------------------
import numpy as n
#------------------------------------------------------------------------------

#CLASS DEFINING THE PARAMETERS OF THE SIMULATION
#------------------------------------------------------------------------------
class Simulation:  
    
    # Class Initialization
    #--------------------------------------------------------------------------
    def __init__(self, beta_array, dof_array, frequency_array, Hs, Tp, Prob, ndir, angular_spreading_parameter, main_beta_direction):
        
        self.beta_array = beta_array
        self.dof_array  = dof_array
        self.frequency_array  = frequency_array
        self.Hs = Hs
        self.Tp = Tp
        self.Prob = Prob
        self.ndir = ndir
        self.m  = angular_spreading_parameter
        self.theta_principal = main_beta_direction