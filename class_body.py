# Import relevant modules
#------------------------------------------------------------------------------
import numpy as n
import math as m
import cmath as cm
from  Nemoh_InOut import *
import scipy.special as spec
#------------------------------------------------------------------------------

#CLASS DEFINING A BODY OF THE ARRAY WITH "UNIQUE" GEOMETRY (FULL DESCRIPTION)
#------------------------------------------------------------------------------
class BodyUnique:  
    
    # Class Initialization
    #--------------------------------------------------------------------------
    def __init__(self, name, center_of_gravity, dof_body, envobj, trunc_angmode, \
                                                     trunc_depthmode, cyl_rad):
        
        self.name = name   
        self.cG = center_of_gravity     #meters, [x,y,z]
        self.dof_body = dof_body
        self.cyl_rad = cyl_rad  #radius of the circumscribing cylinder  
        
        ([self.Added_Mass_Coefficients, # (dimensional)
          self.Damping_Coefficients,    # (dimensional)
          self.Force_Transfer_Matrix,
          self.Diffraction_Transfer_Matrix,
          self.Radiation_Characteristics,
          self.trunc_angmode]) = self._computeprop(envobj, trunc_angmode, \
                                                           trunc_depthmode) 
        
        self.trunc_depthmode = trunc_depthmode
        
        (self.Stiffness, self.Inertia) = self.BodyMechanicalCharacteristics()
        
           
        
    #--------------------------------------------------------------------------
    #
    # Function to calculate the Body Operators   
    #--------------------------------------------------------------------------
    def _computeprop(self, envobj, trunc_angmode, trunc_depthmode):
		
          # Automatic Angular Mode Truncation
          #--------------------------------------------------------------------
          if (trunc_angmode == None): 
          
              ([Added_Mass_Coefficients, # (dimensional)
                Damping_Coefficients,    # (dimensional)
                Force_Transfer_Matrix,
                Diffraction_Transfer_Matrix,
                Radiation_Characteristics,
                trunc_angmode]) = self.automatic_truncation(envobj, \
                                                               trunc_depthmode) 
                                                  
          # User-Specified Angular Mode Truncation
          #--------------------------------------------------------------------
          else:                         
          
              ([Added_Mass_Coefficients, # (dimensional)
                Damping_Coefficients,    # (dimensional)
                Force_Transfer_Matrix,
                Diffraction_Transfer_Matrix,
                Radiation_Characteristics]) = \
                        self.manual_truncation(envobj,\
                                                 trunc_angmode,trunc_depthmode)
              
              trunc_angmode   = trunc_angmode
              
          # Normalisation of the Hydrodynamic Operators
          #--------------------------------------------------------------------
          (Force_Transfer_Matrix_norm,\
          Diffraction_Transfer_Matrix_norm, \
          Radiation_Characteristics_norm) = self.Normalise_operators(envobj,Force_Transfer_Matrix,\
                                                 Diffraction_Transfer_Matrix, \
                                                     Radiation_Characteristics,\
                                                     trunc_angmode,trunc_depthmode)
                                                     
          return (Added_Mass_Coefficients,Damping_Coefficients,\
                 Force_Transfer_Matrix_norm,Diffraction_Transfer_Matrix_norm,\
                 Radiation_Characteristics_norm,trunc_angmode)
                 
#          return (Added_Mass_Coefficients,Damping_Coefficients,\
#                 Force_Transfer_Matrix,Diffraction_Transfer_Matrix,\
#                 Radiation_Characteristics,trunc_angmode)
            
    #--------------------------------------------------------------------------
    #
    # Function to calculate body operators with manual truncation
    #--------------------------------------------------------------------------
    def manual_truncation(self, envobj, trunc_angmode,trunc_depthmode):

        nfreqs = len(envobj.frequency)
        
        # Create empty list of matrices with nomega number of elements
        Added_Mass_Coefficients = [None] * nfreqs
        Damping_Coefficients    = [None] * nfreqs
        Force_Transfer_Matrix = [None] * nfreqs
        Radiation_Characteristics = [None] * nfreqs
        Diffraction_Transfer_Matrix = [None] * nfreqs
        
        for ind in range(0, nfreqs):
            
            frequencies = []
            frequencies.append(envobj.frequency[ind])
            
            (A_w,B_w,Fe_w,DTM_w,R_w) = self.BodyHydroCharacteristics(envobj.depth, \
                        self.cG,frequencies,trunc_angmode[ind],trunc_depthmode[ind])
                        
            Diffraction_Transfer_Matrix[ind] = DTM_w
            Radiation_Characteristics[ind]   = R_w
            Force_Transfer_Matrix[ind]       = Fe_w
            Added_Mass_Coefficients[ind]     = A_w
            Damping_Coefficients[ind]        = B_w
       
        
        return (Added_Mass_Coefficients,Damping_Coefficients,\
                Force_Transfer_Matrix,Diffraction_Transfer_Matrix,\
                Radiation_Characteristics)
    #-------------------------------------------------------------------------- 
    #
    # Function to automatically truncate body operators
    #--------------------------------------------------------------------------
    def automatic_truncation(self, envobj, trunc_depthmode_input):
        
        nfreqs = len(envobj.frequency)
        
        # Create empty list of matrices with nomega number of elements
        Added_Mass_Coefficients = [None] * nfreqs
        Damping_Coefficients    = [None] * nfreqs
        Force_Transfer_Matrix = [None] * nfreqs
        Radiation_Characteristics = [None] * nfreqs
        Diffraction_Transfer_Matrix = [None] * nfreqs
        trunc_angmode_mat = [None] * nfreqs
        
        # First, the matrix is truncated with the angular modes
        # then, using this truncation, the final matrix with trunc_depthmodes
        # specified by the user is generated
        trunc_depthmode = 0
            
        index_freq = 0
        
        for w in envobj.frequency:
            
            frequencies = []
            frequencies.append(w)
            
            trunc_angmode = 0
            
            (A_w,B_w,Fe_w,DTM_w,R_w) = self.BodyHydroCharacteristics(envobj, \
                        self.cG,frequencies,trunc_angmode,trunc_depthmode)
                        
            # Scalar containing max value of the DTM
            prev_ref_DTM_w = n.amax(n.absolute(DTM_w))
            
            trunc_angmode = 1
            condition = True
            
            it = 0
            
            while(condition):
                
                (A_w,B_w,Fe_w,DTM_w,R_w) = self.BodyHydroCharacteristics(\
                 envobj, self.cG,frequencies,trunc_angmode,\
                                                   trunc_depthmode)
                                                               
                cur_ref_DTM_w = n.amax(n.absolute(DTM_w))
                
                condition_bigger_DTM = False
                
                # check if there is an element bigger than 10^(-6) times the 
                # biggest element of the previous truncation for the DTM
                for j in range(-trunc_angmode,trunc_angmode + 1):
                    for i in range(-trunc_angmode,trunc_angmode + 1):
                        
                        if ((abs(j) > (trunc_angmode - 1)) | (abs(i) > (trunc_angmode - 1))):
                            
                             index_i = (trunc_angmode + i) * (trunc_depthmode + 1) 
                             index_j = (trunc_angmode + j) * (trunc_depthmode + 1) 
                
                             if (abs(DTM_w[index_i,index_j,0]) > ((10**(-6)) * prev_ref_DTM_w)):
                                  condition_bigger_DTM = True
                                  break
                    
                    if (condition_bigger_DTM == True):
                       break
                
                if (condition_bigger_DTM):
                    
                    trunc_angmode = trunc_angmode + 1
                    prev_ref_DTM_w = cur_ref_DTM_w

                    DTM_w_prev = DTM_w
                    R_w_prev   = R_w
                    Fe_w_prev  = Fe_w
                    A_w_prev   = A_w
                    B_w_prev   = B_w
                    
                else:
                    
                    if (it == 0):
                        
                        trunc_angmode = trunc_angmode + 1
                        it = it + 1
                        
                    elif (it == 1):
                        
                        print('found final truncation')
                        condition = False;
                        if (trunc_depthmode_input[index_freq] == 0):
                            trunc_angmode_mat[index_freq] = trunc_angmode - 2
                            Diffraction_Transfer_Matrix[index_freq] = DTM_w_prev
                            Radiation_Characteristics[index_freq]   = R_w_prev
                            Force_Transfer_Matrix[index_freq]  = Fe_w_prev
                            Added_Mass_Coefficients[index_freq]   = A_w_prev
                            Damping_Coefficients[index_freq]   = B_w_prev
                            
                        else:
                            trunc_angmode_mat[index_freq] = trunc_angmode - 2
                            (Added_Mass_Coefficients[index_freq],\
                            Damping_Coefficients[index_freq],\
                            Force_Transfer_Matrix[index_freq],\
                            Diffraction_Transfer_Matrix[index_freq],\
                            Radiation_Characteristics[index_freq]) = \
                                      self.BodyHydroCharacteristics(\
                                      envobj, self.cG,frequencies,\
                                      trunc_angmode - 2, trunc_depthmode_input[index_freq])
     
                        
            index_freq = index_freq + 1
                        
        return (Added_Mass_Coefficients,Damping_Coefficients,\
                Force_Transfer_Matrix,Diffraction_Transfer_Matrix,\
                Radiation_Characteristics,trunc_angmode_mat)
    #--------------------------------------------------------------------------
    #
    # Normalisation of the hydrodynamic operators (SEE PAGE 52)
    #--------------------------------------------------------------------------
    def Normalise_operators(self, envobj, G, DTM, RC, trunc_angmode, trunc_depthmode): 
        
       # Number of frequencies
       num_freq = len(G)
       
       # Get radius of the body circumscribing cylinder 
       cyl_rad = self.cyl_rad       
       
       #Initialization of empty operators
       G_norm   = [None] * num_freq
       DTM_norm = [None] * num_freq
       RC_norm  = [None] * num_freq
       
       # Calculate wave numbers
       (k,error) = envobj.get_wave_number(trunc_depthmode)
       
       #Iteration through frequencies
       for ind_w in range(0,num_freq):
           
           Q = trunc_angmode[ind_w]    #incident angular mode truncation
           L = trunc_depthmode[ind_w]  #incident depth mode truncation
            
           M = Q                            #outgoing angular mode truncation
           N = L                            #outgoing depth mode trunctation
           
           dim =  2 * Q * (L + 1) + L + 1   #dimension of the operators
           
           # Normalization of the Force Transfer Matrix
           #-------------------------------------------------------------------
           G_norm[ind_w] = n.zeros((self.dof_body,dim),dtype = n.complex)
            
           for index_dof_body in range(0, self.dof_body):
                
                for index_Q in range(-Q, Q + 1):
                    for index_L in range(0, L + 1):
                        
                        glob_ind = (Q + index_Q) * (L + 1) + index_L
                        
                        #Normalisation progressive terms
                        if (index_L == 0):
                            
                            arg_nf_p_G = envobj.k0[ind_w] * cyl_rad
                            
                            norm_factor = (spec.jv(index_Q,arg_nf_p_G))**(-1)
                               
                        #Normalisation evanescent terms
                        else:
                           
                           arg_nf_ev_G = k[ind_w][index_L] * cyl_rad
                           
                           norm_factor = (spec.iv(index_Q,arg_nf_ev_G))**(-1)
                           
        
                        G_norm[ind_w][index_dof_body,glob_ind] = \
                               norm_factor * G[ind_w][index_dof_body,glob_ind,0]
                               
           # Normalization of the Diffraction Transfer Matrix
           #-------------------------------------------------------------------
           DTM_norm[ind_w] = n.zeros((dim,dim),dtype = n.complex)
           
           for index_row_M in range (- M, M + 1):
                for index_row_N in range (0, N + 1):
                    
                    for index_col_Q in range (- Q, Q + 1):
                        for index_col_L in range (0, L + 1):
                            
                            glob_ind_row = (M + index_row_M) * (N + 1) + index_row_N
                            glob_ind_col = (Q + index_col_Q) * (L + 1) + index_col_L
                            
                            # Get normalising factor norm_fact
                            if (index_row_N==0): #progressive outgoing
                                
                                if (index_col_L==0): #progressive incident
                                    
                                    arg_p = envobj.k0[ind_w] * cyl_rad
                                    func_num= spec.hankel1(index_row_M,arg_p)
                                    func_denom = spec.jv(index_col_Q,arg_p)
                                    norm_fact = func_num / func_denom
                                
                                else: #evanescent incident
                                
                                    arg_p = envobj.k0[ind_w] * cyl_rad
                                    arg_ev = k[ind_w][index_col_L] * cyl_rad
                                    func_num = spec.hankel1(index_row_M,arg_p)
                                    func_denom= spec.iv(index_col_Q,arg_ev)
                                    norm_fact = func_num/ func_denom
                                    
                            else: #evanescent outgoing
                                
                                if (index_col_L==0): #progressive incident
                                    
                                    arg_ev = k[ind_w][index_row_N] * cyl_rad
                                    func_num = spec.kn(index_row_M,arg_ev)
                                    arg_p = envobj.k0[ind_w] * cyl_rad
                                    func_denom = spec.jv(index_col_Q,arg_p)
                                    norm_fact = func_num / func_denom
                                    
                                else: #evanescent incident
                                    
                                    arg_ev_num = k[ind_w][index_row_N] * cyl_rad
                                    arg_ev_denom = k[ind_w][index_col_L] * cyl_rad
                                    func_num = spec.kn(index_row_M,arg_ev_num)
                                    func_denom = spec.iv(index_col_Q,arg_ev_denom)
                                    norm_fact = func_num / func_denom
                                    
                            DTM_norm[ind_w][glob_ind_row,glob_ind_col] = \
                            norm_fact * DTM[ind_w][glob_ind_row,glob_ind_col,0] 
                            
                            
           # Normalization of the Radiation Characteristics
           #-------------------------------------------------------------------
           RC_norm[ind_w] = n.zeros((dim,self.dof_body),dtype = n.complex)
            
           for index_dof_body in range (0, self.dof_body):
                
                for index_M in range (- M, M + 1):
                    for index_N in range (0, N + 1):
                
                        glob_ind = (M + index_M) * (N + 1) + index_N
                        
                        if (index_N==0): #progressive modes
                        
                            arg_p = envobj.k0[ind_w] * cyl_rad
                            norm_fact = spec.hankel1(index_M,arg_p)
                            
                        else: #evanescent modes
                            
                            arg_ev = k[ind_w][index_N] * cyl_rad
                            norm_fact = spec.kn(index_M,arg_ev)
                
                        RC_norm[ind_w][glob_ind,index_dof_body] = \
                               norm_fact * RC[ind_w][glob_ind,index_dof_body,0]
                              
            
       return(G_norm, DTM_norm, RC_norm) 
    #--------------------------------------------------------------------------    
    #    
    # Function to rotate the Body Operators (SEE PAGE 49)
    #--------------------------------------------------------------------------
    def rotate_operators(self, rotation_angle):
        
        # Set rotation angle gamma in radians
        gamma = rotation_angle * (m.pi / 180)
        
        # Initialization of rotated operators
        DTM_rotated = [None] * len(self.Diffraction_Transfer_Matrix)
        R_rotated   = [None] * len(self.Diffraction_Transfer_Matrix)
        G_rotated   = [None] * len(self.Diffraction_Transfer_Matrix)
        
        # Iteration through frequencies        
        for ind_w in range(0,len(self.Diffraction_Transfer_Matrix)):
            
            
            Q = self.trunc_angmode[ind_w]    #incident angular mode truncation
            L = self.trunc_depthmode[ind_w]  #incident depth mode truncation
            
            M = Q                            #outgoing angular mode truncation
            N = L                            #outgoing depth mode trunctation
            
            # Rotation of the Diffraction Transfer Matrix
            #------------------------------------------------------------------
            dim =  2 * Q * (L + 1) + L + 1
            
            DTM_rotated[ind_w] = n.zeros((dim,dim),dtype = n.complex)
            
            DTM = self.Diffraction_Transfer_Matrix[ind_w]
            
            for index_row_M in range (- M, M + 1):
                for index_row_N in range (0, N + 1):
                    
                    for index_col_Q in range (- Q, Q + 1):
                        for index_col_L in range (0, L + 1):
                            
                            glob_ind_row = (M + index_row_M) * (N + 1) + index_row_N
                            glob_ind_col = (Q + index_col_Q) * (L + 1) + index_col_L
                            
                            rot_factor =  cm.exp(1j * \
                                          (index_col_Q - index_row_M) * gamma)
                            
                            DTM_rotated[ind_w][glob_ind_row,glob_ind_col] = \
                            rot_factor * DTM[glob_ind_row,glob_ind_col]

            # Rotation of the Radiation Characteristics
            #------------------------------------------------------------------
            R = self.Radiation_Characteristics[ind_w]
            
            R_rotated[ind_w] = n.zeros((dim,self.dof_body),dtype = n.complex)
            
            for index_dof_body in range (0, self.dof_body):
                
                for index_M in range (- M, M + 1):
                    for index_N in range (0, N + 1):
                
                        glob_ind = (M + index_M) * (N + 1) + index_N
                    
                        rot_factor = cm.exp(-1j * index_M * gamma)
                
                        R_rotated[ind_w][glob_ind,index_dof_body] = \
                               rot_factor * R[glob_ind,index_dof_body]
                                                  
            # Rotation of the Force Transfer Matrix
            #------------------------------------------------------------------
            G = self.Force_Transfer_Matrix[ind_w]
            
            G_rotated[ind_w] = n.zeros((self.dof_body,dim),dtype = n.complex)
            
            for index_dof_body in range(0, self.dof_body):
                
                for index_Q in range(-Q, Q + 1):
                    for index_L in range(0, L + 1):
                        
                        glob_ind = (Q + index_Q) * (L + 1) + index_L
                        
                        rot_factor = cm.exp(1j * index_Q * gamma)
                        
                        G_rotated[ind_w][index_dof_body,glob_ind] = \
                               rot_factor * G[index_dof_body,glob_ind]
                    
            #------------------------------------------------------------------       
                    
        
        return (DTM_rotated, R_rotated, G_rotated)
    #--------------------------------------------------------------------------
    #   
    #    
    #--------------------------------------------------------------------------    
    def BodyHydroCharacteristics(self,envobj,cG,frequencies,trunc_angmode,trunc_depthmode):
    
       # Create ID file
        
        writeID()
       
       # Create Nemoh.cal file
        
        wNumMinMax          = [None] * 3
        nCylwavesAngtDeptht = [None] * 3
        
        wNumMinMax[0] = len(frequencies)
        wNumMinMax[1] = frequencies[0]
        wNumMinMax[2] = frequencies[len(frequencies) - 1]
        
        nCylwavesAngtDeptht[0] = (2 * trunc_angmode + 1) * (trunc_depthmode + 1)
        nCylwavesAngtDeptht[1] = trunc_angmode
        nCylwavesAngtDeptht[2] = trunc_depthmode
        
        FSE = [0,0,0,0]
        Pressure = 0
        Kochin = [0,0,0]
        
        writeNemohcal_cylindricalwaves(1,cG,envobj,wNumMinMax,nCylwavesAngtDeptht,FSE,Pressure,Kochin)
    
        # Nemoh - Solve BVP
    
        call_Nemoh(nCylwavesAngtDeptht)
    
        # Recuperate Body Hydrodynamic Characteristics
    
        Ma  = loadMa()
        B   = loadB()
        Fe  = loadFe()
        DTM = loadDTM()
        Rad = loadRcharacteristics()
         
        return(Ma,B,Fe,DTM,Rad)
    #--------------------------------------------------------------------------
    #   
    #    
    #--------------------------------------------------------------------------  
    def BodyMechanicalCharacteristics(self):

        KH = loadKH()
        M =  loadInertia()

        return (KH, M)     
    #--------------------------------------------------------------------------
    #   
    #    
    #--------------------------------------------------------------------------  
    
#-----------------------------------------------------------------------------
#------------------------------------------------------------------------------
#
#CLASS DEFINING A BODY OF THE ARRAY WITH "ANY" GEOMETRY (EMPTY DESCRIPTION)
#------------------------------------------------------------------------------             
class Body(BodyUnique):
    
    # Class Initialization
    #--------------------------------------------------------------------------
    def __init__(self, name, x, y, rotation_angle):
        
        self.name = name
        self.position  = (x, y)                       #meters
        self.rotation_angle  = rotation_angle         #degrees
#------------------------------------------------------------------------------                