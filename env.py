#IMPORT MODULES
#------------------------------------------------------------------------------
import numpy as n
import math as m
import nonlinear_solvers as solvers
import scipy.special as spec
#------------------------------------------------------------------------------
#
#CLASS DEFINING THE OCEAN ENVIRONMENT (CONSTANT DEPTH, FINITE)
#------------------------------------------------------------------------------
class Environment:
    
    # Class Initialization
    #--------------------------------------------------------------------------
    def __init__(self, frequency, depth, water_density, gravity):
        
        self.frequency       = frequency # matrix (if only one frequency, 1x1)
        self.depth           = depth
        self.water_density   = water_density
        self.gravity         = gravity
        self.k0              = self._gen_progressive_wavenumb()
    #--------------------------------------------------------------------------
    #
    # Function to Calculate Wave Number(s)
    #--------------------------------------------------------------------------    
    def get_wave_number(self, trunc_depthmode):
    
       # Initialize emply list of wave numbers
       k = list()
       error = list()
       
       for ind_w in range(0,len(self.frequency)):
           k.append([])
           error.append([])
       
       
       # Iteration over the frequencies
       #-----------------------------------------------------------------------
       for ind_w in range(0,len(self.frequency)):
           
         omega = self.frequency[ind_w]
         
         # Progressive Modes
         #---------------------------------------------------------------------
         initial_guess =self._JunkeGuo(self.frequency[ind_w])
         
         function = self._progressive_dispersion_eq
         
         function_derivative = self._progressive_dispersion_eq_derivative
         
         # Solve Progressive Dispersion Equation
         #---------------------------------------------------------------------
         k[ind_w].append(solvers.Newton_Raphson_alg(function, \
                        function_derivative, omega, self.depth, initial_guess)[0])
         error[ind_w].append(solvers.Newton_Raphson_alg(function, \
                        function_derivative, omega, self.depth, initial_guess)[1])
         #---------------------------------------------------------------------
         
         # Evanescent Modes
         #---------------------------------------------------------------------
         if (trunc_depthmode[ind_w] > 0):

             for ind_L in range(1, trunc_depthmode[ind_w] + 1):
               
               epsilon = n.spacing(1)
               
               initial_guess = (m.pi / (2 * self.depth)) + \
                               (ind_L - 1) * (m.pi / self.depth) + epsilon 
                               
               function = self._evanescent_dispersion_eq
           
               # Solve Evanescent Dispersion Equation
               #-------------------------------------------------------------------
               k[ind_w].append(solvers.Bisection(function, omega, self.depth, ind_L)[0])
               error[ind_w].append(solvers.Bisection(function, omega, self.depth, ind_L)[1])
               #-------------------------------------------------------------------
       
       # If error !=0, problem with the calculation of the wavenumbers                               
       return (k,error)    
    #--------------------------------------------------------------------------
    #  
    # Function that returns vector with progressive wave numbers at each frequency
    #--------------------------------------------------------------------------
    def _gen_progressive_wavenumb(self):
        
        empty_depthmode_trunc = [0] * len(self.frequency)
        
        (k,error) = self.get_wave_number(empty_depthmode_trunc)
        
        indicator = self._check_error_wavenumb(error)
        
        if (indicator == 0):
            
            k0 = [k[ind_w][0] for ind_w in range(0,len(self.frequency))]
            
        return k0
    #--------------------------------------------------------------------------
    #
    # Function to check errors in the calculation of the wave number
    #-------------------------------------------------------------------------- 
    def _check_error_wavenumb (self, error):
        
        indicator = 0
        
        for index_freq in range(0, len(self.frequency)):
            
            for index_depthmode in range(0, len(error[index_freq])):
                
                if (error[index_freq][index_depthmode] != 0):
                    
                    indicator = -1
                    
        if (indicator != 0):
            
            raise ValueError('Dispersion Equation Solution Not Found')
            
        return indicator
    #--------------------------------------------------------------------------             
    #          
    # Dispersion Equation for Progressive Modes     
    #--------------------------------------------------------------------------
    def _progressive_dispersion_eq(self, k, omega):
        
        return k * m.tanh(k * self.depth) - (omega ** 2) / self.gravity
    #--------------------------------------------------------------------------
    #
    # Derivative of the Dispersion Equation for Progressive Modes
    #--------------------------------------------------------------------------  
    def _progressive_dispersion_eq_derivative (self, k):
       
       return m.tanh(k * self.depth) + k * self.depth * \
                                           (1 - (m.tanh(k * self.depth)) ** 2)
    #--------------------------------------------------------------------------
    #
    # Dispersion Equation for the Evanescent Modes
    #--------------------------------------------------------------------------                                         
    def _evanescent_dispersion_eq(self, k, omega):
        
        return k * m.tan(k * self.depth) + (omega ** 2) / self.gravity
    #--------------------------------------------------------------------------
    #
    # Approximation of the solution of the Progressive Dispersion Equation
    # using the explicit formula by Junke Guo (2002)
    #--------------------------------------------------------------------------                 
    def _JunkeGuo(self,omega):
        
        beta = 2.4908
        x = (self.depth * omega) / m.sqrt(self.gravity * self.depth)
       
        return (1 / self.depth) * (x **2) * \
              (1 - m.exp(- (x) ** beta)) ** (- 1 / beta)
    #--------------------------------------------------------------------------  
    #         
    # Function to generate a discrete set of wave amplitudes for a monochromatic
    # normalized angular distribution spectrum
    #--------------------------------------------------------------------------
    def monochrom_norm_angulardistrib_spectrum(self, nfreq, ndir):
        
        # Initialize matrices with [Nfreqs, Ndirections] dimensions
        S = n.zeros((nfreq,ndir), dtype=float)
        Amp = n.zeros((nfreq,ndir), dtype=float)
        
        angles = n.linspace(- n.pi/2,n.pi/2, ndir)
        
        for ind_w in range(0, nfreq):
            for ind_dir in range(0, ndir):
            
                S[ind_w,ind_dir] = (2. / n.pi) * (n.cos(angles[ind_dir]))**2
            
                Amp[ind_w,ind_dir] = n.sqrt(S[ind_w,ind_dir])
            
        self.wave_amplitudes = Amp
        
    #--------------------------------------------------------------------------  
    #
    # Function to generate a discrete set of wave amplitudes for a monochromatic
    # normalized angular distribution spectrum
    #--------------------------------------------------------------------------
    def monochrom_norm_angulardistrib_spectrum_param_m(self, nfreq, ndir, m, theta_princ):
        
        # Initialize matrices with [Nfreqs, Ndirections] dimensions
        S = n.zeros((nfreq,ndir), dtype=float)
        Amp = n.zeros((nfreq,ndir), dtype=float)
        
        angles = n.linspace(- n.pi/2,n.pi/2, ndir)
        
        constant = spec.gamma(0.5*m + 1) / (spec.gamma(0.5*m + 0.5) * n.sqrt(n.pi))
        
        for ind_w in range(0, nfreq):
            for ind_dir in range(0, ndir):
            
                S[ind_w,ind_dir] = constant * (n.cos(angles[ind_dir] - theta_princ))**m
            
                Amp[ind_w,ind_dir] = n.sqrt(S[ind_w,ind_dir])
            
        self.wave_amplitudes = Amp
        #return Amp
        
    #--------------------------------------------------------------------------  
    #
    # Function to generate a discrete set of wave amplitudes for a monochromatic
    # normalized angular distribution spectrum
    #--------------------------------------------------------------------------
    def monochrom_norm_angulardistrib_spectrum_param_s(self, nfreq, ndir, s, theta_princ):
        
        # Initialize matrices with [Nfreqs, Ndirections] dimensions
        S = n.zeros((nfreq,ndir), dtype=float)
        Amp = n.zeros((nfreq,ndir), dtype=float)
        
        angles = n.linspace(- n.pi/2,n.pi/2, ndir)
        
        constant = spec.gamma(s + 1) / (spec.gamma(s + 0.5) * 2 * n.sqrt(n.pi))
        
        for ind_w in range(0, nfreq):
            for ind_dir in range(0, ndir):
            
                S[ind_w,ind_dir] = constant * (n.cos(0.5*(angles[ind_dir] - theta_princ)))**(2*s)
            
                Amp[ind_w,ind_dir] = n.sqrt(S[ind_w,ind_dir])
            
        self.wave_amplitudes = Amp
        #return Amp
        
    #-------------------------------------------------------------------------- 
    #         
    # Function to generate a discrete set of wave amplitudes all the same and  
    # unitary for each frequency and directions
    #--------------------------------------------------------------------------
    def unitary_spectrum(self, ndir):
        
        # Initialize matrices with [Nfreqs, Ndirections] dimensions
        nfreqs = len(self.frequency)
        Amp = n.zeros((nfreqs, ndir), dtype=float)
        
        for ind_freqs in range(0, nfreqs):
            
            for ind_dir in range(0, ndir):
                
                Amp[ind_freqs, ind_dir] = 1.0
                
        self.wave_amplitudes = Amp
    #--------------------------------------------------------------------------
    #
    #            
    #--------------------------------------------------------------------------
    def bretschneider_spectrum_angular_spreading(self, Hs, Tp, frequencies, ndir, m, theta_princ):
        
        # SEE PAGE 112 FOR DETAILS OF ITS DEFINITION
        
        # Spreading function is taken only between +-90 on both sides of main 
        # wave propagation direction
        
        # Initialize matrices with [Nfreqs, Ndirections] dimensions
        nfreq = len(frequencies)
        
        S = n.zeros((nfreq,ndir), dtype=float)
        Amp = n.zeros((nfreq,ndir), dtype=float)
        S_theta = n.zeros(ndir, dtype=float)
        S_f     = n.zeros(nfreq, dtype=float)
        
        Aw = (frequencies[1] - frequencies[0]) / (2 * n.pi)
        
        not_ang_frequencies = frequencies / (2 * n.pi)
        
        angles = n.linspace(- n.pi/2 + theta_princ, n.pi/2 + theta_princ, ndir)
        
        Atheta = angles[1] - angles[0]
        
        Constant = spec.gamma(0.5*m + 1) / (spec.gamma(0.5*m + 0.5) * n.sqrt(n.pi))
        
        A_param = (5. / 16.) * ((Hs ** 2) / (Tp ** 4))
                
        B_param = (5. / 4.) * (1. / (Tp ** 4))
        
            
        for ind_freqs in range(0, nfreq):
            
            for ind_dir in range(0, ndir):
                
                S[ind_freqs, ind_dir] = (A_param / (not_ang_frequencies[ind_freqs] ** 5)) * \
                                n.exp( - B_param / (not_ang_frequencies[ind_freqs] ** 4)) * Constant * (n.cos(angles[ind_dir] - theta_princ))**m
                
                Amp[ind_freqs, ind_dir] = n.sqrt(2 * S[ind_freqs, ind_dir] * Aw * Atheta)
                
                S_theta[ind_dir] = Constant * (n.cos(angles[ind_dir] - theta_princ))**m
                
                S_f[ind_freqs] = (A_param / (not_ang_frequencies[ind_freqs] ** 5)) * \
                                n.exp( - B_param / (not_ang_frequencies[ind_freqs] ** 4))
                
        return (Amp,S,S_theta,S_f)
    #--------------------------------------------------------------------------
    #
    #  
    #--------------------------------------------------------------------------
    def bretschneider_spectrum_1D(self, Hs, Tp, frequencies):
    
        
        # Initialize matrices with [Nfreqs, Ndirections] dimensions
        nfreq = len(frequencies)
        
        Amp = n.zeros(nfreq, dtype=float)
        S_f     = n.zeros(nfreq, dtype=float)
        
        Aw = (frequencies[1] - frequencies[0]) / (2 * n.pi)
        
        not_ang_frequencies = frequencies / (2 * n.pi)
        
        A_param = (5. / 16.) * ((Hs ** 2) / (Tp ** 4))
                
        B_param = (5. / 4.) * (1. / (Tp ** 4))
        
            
        for ind_freqs in range(0, nfreq):
            
            S_f[ind_freqs] = (A_param / (not_ang_frequencies[ind_freqs] ** 5)) * \
                                n.exp( - B_param / (not_ang_frequencies[ind_freqs] ** 4))
                
            Amp[ind_freqs] = n.sqrt(2 * S_f[ind_freqs] * Aw)
            
                
        return (Amp,S_f)    
        
    #--------------------------------------------------------------------------
    #
    #     
    #--------------------------------------------------------------------------
    def compute_Hs_from_2D_spectrum(self, Sftheta, frequencies, angles):
        
        
        Aw = (frequencies[1] - frequencies[0]) / (2 * n.pi)
        
        Atheta = angles[1] - angles[0]
        
        nfreq = frequencies.shape[0]
        
        ndir = angles.shape[0]
        
        first_moment = 0.0
        
        for ind_freqs in range(0, nfreq):
            
            for ind_dir in range(0, ndir):
    
                first_moment = Sftheta[ind_freqs, ind_dir]*Aw*Atheta + first_moment
                
        # TO DO: compute spectrum width parameter and adjust depending on result
        Hs = 4 * n.sqrt(first_moment) / 1.08
        
        return Hs
    #--------------------------------------------------------------------------
    #
    #            
    #--------------------------------------------------------------------------
    def compute_Hs_from_2D_spectrum_amplitudes(self, ampsftheta, nfreq, ndir):
    
        
        first_moment = 0.0
        
        for ind_freqs in range(0, nfreq):
            
            for ind_dir in range(0, ndir):
    
                first_moment = 0.5 * (ampsftheta[ind_freqs, ind_dir]**2) + first_moment
        
        # TO DO: compute spectrum width parameter and adjust depending on result
        Hs = 4 * n.sqrt(first_moment) / 1.08 #for a not narrow spectrum
        
        return Hs
    #--------------------------------------------------------------------------
    #
    #
    #--------------------------------------------------------------------------
    def compute_Hs_from_1D_spectrum(self, Sf, frequencies):
        
        Aw = (frequencies[1] - frequencies[0])/ (2 * n.pi)
        
        first_moment = 0.0
        
        nfreq = frequencies.shape[0]
        
        for ind_freqs in range(0, nfreq):
    
            first_moment = Sf[ind_freqs]*Aw + first_moment
            
        Hs = 4 * n.sqrt(first_moment)
        
        return Hs
    
#------------------------------------------------------------------------------
    