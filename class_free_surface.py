#%% MODULES IMPORT

# Import classes
#------------------------------------------------------------------------------
from env import Environment
from farm import Farm
from class_body import *
import pickle
from class_Mechanics import Mechanics

# Import python tools
#------------------------------------------------------------------------------
import numpy as n
import math as m
import scipy.special as spec
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import h5py
#------------------------------------------------------------------------------
#
#CLASS DEFINING THE FREE SURFACE
#------------------------------------------------------------------------------
class Free_surface:
    
    # Class Initialization
    #--------------------------------------------------------------------------
    def __init__(self, domain_limits, domain_reference, resolution, myfarm, myenv):
        
        # Distance from the bodies in the perimeter of the farm to the domain limits
        # as a function of the characteristic dimension of the device
        # domain limits : [x_upward,x_leeward,y_upper,y_lower]
        self.domain_limits  = domain_limits 
    
        self.domain_reference = domain_reference
        
        # Resolution of the grid (step x and y used to fill the domain_limits)
        self.resolution = resolution
        
        # Discretization of the free surface, X and Y coords of the grid points
        # in a global cartesian coordinate system
        self.X = self._mesh_for_computation(myfarm, domain_reference)[0]
        
        self.Y = self._mesh_for_computation(myfarm, domain_reference)[1]
        
        # Calculate relative polar coordinates to each body of the farm
        self.mesh_rel_global = self._mesh_relto_allbodies(myfarm)
        
        # Mask inner region to the circumscribing cylinder
        self.interior = self._mask_inner_cylinder(myfarm)
        
        # Store circumscribing cylinder radius for bodies of the farm
        self.circumscribing_cylinders = self._get_circumscribing_cylinders(myfarm)
        
        # Calculate wave numbers
        self.k = myfarm.k
        
        # Compute outgoing partial wave functions at each point of the grid
        self.outgoing_basis_func = self._gen_outgoing_basisfunctions_farm(myfarm, myenv)
        
    #--------------------------------------------------------------------------
    #
    # 
    #--------------------------------------------------------------------------
    def _mesh_for_computation(self, myfarm, domain_reference):
        
        #======================================================================
        #         generates a grid of points where the free surface elevation 
        #         will be computed
        #         output : X and Y vectors of coordinates
        #======================================================================
        
        
        if (domain_reference == 'relative'):
            
            char_dimension = self._find_max_char_dim(myfarm)

            x_min = self._find_minmax_xy(myfarm)[0] - self.domain_limits[0] * char_dimension
        
            x_max = self._find_minmax_xy(myfarm)[2] + self.domain_limits[1] * char_dimension
        
            y_min = self._find_minmax_xy(myfarm)[1] - self.domain_limits[3] * char_dimension
        
            y_max = self._find_minmax_xy(myfarm)[3] + self.domain_limits[2] * char_dimension
        
            # Resolution in x
            step_x = self.resolution
        
            # Resolution in y
            step_y = self.resolution
        
            x = n.arange(x_min, x_max + step_x, step_x)
        
            y = n.arange(y_min, y_max + step_y, step_y)
        
            X, Y = n.meshgrid(x,y)
            
        elif (domain_reference == 'absolute'):
            
            x_min = - self.domain_limits[0]
            
            x_max = self.domain_limits[1]
            
            y_min = - self.domain_limits[3]
            
            y_max = self.domain_limits[2]
            
            # Resolution in x
            step_x = self.resolution
            
            x = n.arange(x_min, x_max + step_x, step_x)
        
            y = n.arange(y_min, y_max + step_x, step_x)
        
            X, Y = n.meshgrid(x,y)
            
        elif (domain_reference == 'line_y'):
            
            x_min = self.domain_limits[0]
            
            x_max = x_min 
            
            y_min = self.domain_limits[3]
            
            y_max = self.domain_limits[2]
            
            points = self.resolution
            
            x = x_min
        
            y = n.linspace(y_min, y_max, points)
        
            X, Y = n.meshgrid(x,y)
            
        elif (domain_reference == 'circle_trans'):
            
            radius = self.domain_limits[0]
            
            angles = n.linspace(-n.pi/2.0, n.pi/2.0, self.resolution)
            
            x_final = n.zeros((self.resolution,1),dtype=float)
            y_final = n.zeros((self.resolution,1),dtype=float)
            
            x = radius * n.cos(angles)
            
            x_final[:,0] = x
            
            y = radius * n.sin(angles)
            
            y_final[:,0] = y
            
            X, Y = x_final, y_final
        
        return (X, Y)
    #--------------------------------------------------------------------------
    #
    # 
    #--------------------------------------------------------------------------         
    def _find_minmax_xy(self, myfarm):
        
        #======================================================================
        #    finds the minimum x coordinate of all the body centers in the farm
        #    output: tuple with x and y min and max coordinates
        #======================================================================
        
        x_coords = []
        y_coords = []
        
        for body_index in range(0,myfarm.farm_nbodies):
            
            x_coords.append(myfarm.layout[body_index].position[0])
            y_coords.append(myfarm.layout[body_index].position[1])

            
        return (n.min(x_coords), n.min(y_coords), \
                n.max(x_coords), n.max(y_coords))
    #--------------------------------------------------------------------------
    #
    # 
    #--------------------------------------------------------------------------                
    def _find_max_char_dim(self, myfarm):        
        
        #======================================================================
        #    finds the biggest characteristic dimension
        #    output: biggest char dimension
        #======================================================================
        
        char_dim = []
        
        for body_index in range(0, myfarm.farm_nbodies):
            
            char_dim.append(myfarm.dict_bodies[myfarm.layout[body_index].name].cyl_rad)    
            
        return n.max(char_dim)
        
    #--------------------------------------------------------------------------
    #
    # 
    #--------------------------------------------------------------------------
    def _mesh_for_computation_reltobody(self, myfarm, ind_body):
        
        #======================================================================
        #         generates a grid of points in polar coordinates where the free 
        #         surface elevation will be computed
        #         input  : ind_body: body to which the relative coords are calculated
        #         output : R and Theta vectors of relative coordinates for body 
        #                  ind_body
        #======================================================================
        
        # Leading dimension of the grid points matrix 
        dim_grid = self.X.shape[0]
        
        # Initialize empty matrix of relative coordinates
        X_rel     = n.zeros((dim_grid,dim_grid), n.float)
        Y_rel     = n.zeros((dim_grid,dim_grid), n.float)
        R_rel     = n.zeros((dim_grid,dim_grid), n.float)
        Theta_rel = n.zeros((dim_grid,dim_grid), n.float)
        
        # Coordinates of the body "ind_body" center
        x_center = myfarm.layout[ind_body].position[0]
        y_center = myfarm.layout[ind_body].position[1]
                
        # Cartesian relative coordinates
        X_rel = self.X - x_center
        Y_rel = self.Y - y_center
        
        # Polar relative coordinates
        R_rel     = n.sqrt((X_rel ** 2) + (Y_rel ** 2))
        Theta_rel = n.arctan2 (Y_rel, X_rel)
        
        return (R_rel, Theta_rel)
        
    #--------------------------------------------------------------------------
    #
    #
    #--------------------------------------------------------------------------    
    def _mesh_relto_allbodies(self, myfarm):
        
        #======================================================================
        #  generates the polar coordinates of all the grid points with 
        #  reference to each body of the farm
        #======================================================================
        
        mesh_rel_global = []
        
        for ind in range(0,myfarm.farm_nbodies):
            
            mesh_rel_global.append(self._mesh_for_computation_reltobody(myfarm,ind))
            
        return mesh_rel_global
        
    #--------------------------------------------------------------------------
    #
    #
    #--------------------------------------------------------------------------
    def _mask_inner_cylinder(self,myfarm):
        
        #======================================================================
        #   masks points of the free surface inside the circumscribing cylinder
        #======================================================================
        
        #generate an array of false
        interior = n.zeros((self.X.shape),dtype=bool) 
        
        for ind_body in range(0, myfarm.farm_nbodies):
            
            char_dimension = myfarm.dict_bodies[myfarm.layout[ind_body].name].cyl_rad
            
            center_x = myfarm.layout[ind_body].position[0]
            center_y = myfarm.layout[ind_body].position[1]
            
            condition = n.sqrt((self.X - center_x)**2 + (self.Y - center_y)**2) < char_dimension
            interior = n.logical_or(interior,condition)
        
        
        return interior
        
    #--------------------------------------------------------------------------
    #
    #
    #--------------------------------------------------------------------------
    def _get_circumscribing_cylinders(self, myfarm):
        
        cyls = list()
        
        for ind in range(0,myfarm.farm_nbodies):
            
            cyls.append(myfarm.dict_bodies[myfarm.layout[ind].name].cyl_rad)
            
        return cyls
    #--------------------------------------------------------------------------    
    #
    #
    #--------------------------------------------------------------------------        
    def _gen_outgoing_basisfunctions_farm(self, myfarm, envobj):
        
        #======================================================================
        #  generate a matrix containing the vectors of outgoing partial wave functions
        #  evaluated at each point of the computational grid, taking into account 
        #  the relative position of the points with respect to each body and the
        #  different frequencies
        
        #  output :  [nbodies][nomega, dim_vect, n_computational_points]
        #======================================================================
        
        # Initialize hdf5 file to store partial wave functions
        #----------------------------------------------------------------------
        working_dir_path = os.getcwd()
        path_hdf5_file = os.path.join(working_dir_path,'Results')
        
        name_file = os.path.join(path_hdf5_file,'outgoing_basis_functions.hdf5')
        
        f = h5py.File(name_file,'w')
        
        # wave numbers
        k = self.k
        
        
        print ('START Outgoing basis functions calculation')
        print ('=============================================================')
        
        for ind_body in range(0, myfarm.farm_nbodies):
            
            print ('index_body' + '                     ' +  str(ind_body))
            
            # Polar coordinates of the mesh relative to body "ind_body"
            #----------------------------------------------------------------------
            (R_rel,Theta_rel) = self.mesh_rel_global[ind_body]
            
            # Generate wave numbers for the frequency defined by ind_omega
            #----------------------------------------------------------------------
            for ind_omega in range(0, len(k)):
                
                print ('index_omega' + '               ' +  str(ind_omega))
            
                komega = k[ind_omega]
            
                omega = envobj.frequency[ind_omega]
            
                trunc_angmode = myfarm.dict_bodies[myfarm.layout[ind_body].name].trunc_angmode[ind_omega] 
            
                trunc_depthmode = myfarm.dict_bodies[myfarm.layout[ind_body].name].trunc_depthmode[ind_omega] 
            
                M = trunc_angmode
                N = trunc_depthmode
          
                dim_vect = 2 * M * (N + 1) + N + 1
                
                npoints_freesurface = self.X.shape[0] * self.X.shape[1]
                
                outgoing_part_waves_matrix = n.zeros((dim_vect, npoints_freesurface), dtype=complex)
                
                # evaluate scattered wave functions
                #----------------------------------------------------------------------
                for ind_i in range(0, self.X.shape[0]):
                    for ind_j in range(0, self.X.shape[1]):
                    
                        # Generate vector of evaluated scattered wave funct at (r,theta,0)
                        #--------------------------------------------------------------
                        r     = R_rel[ind_i, ind_j]
                        theta = Theta_rel[ind_i, ind_j]
                        general_index = ind_j + ind_i * self.X.shape[1]
                    
                        psi_vector = self._gen_scattered_partial_wave_vector(omega,\
                                      komega,trunc_angmode,trunc_depthmode,\
                                                          ind_body,r,theta,envobj.depth)
                                                          
                        
                        outgoing_part_waves_matrix[:, general_index] = psi_vector
                        
                f.create_dataset(str(ind_body)+'/'+str(ind_omega),data = outgoing_part_waves_matrix)
                
                
        print ('END Outgoing basis functions calculation')
        print ('=============================================================')        
        
        f.close()
                
        return None
    #--------------------------------------------------------------------------        
    #
    #                                       
    #-------------------------------------------------------------------------- 
    def _eta_diffraction(self, myfarm, envobj, mySim, ind_beta, ind_omega):
        
        #======================================================================
        #   calculates the free surface elevation for the diffraction problem
        #   associated with a propagation direction (beta) and 
        #   a frequency (omega) from all the cases computed previously
        #   indicated by the array of betas (beta_array) and frequencies
        #
        #   input  : ind_beta : propagation direction desired
        #            ind_omega : desired frequency
        #   output : eta_diffraction : free surface elevation at each grid node               
        #======================================================================
        
        # Initialization of the total scattered  and incident potential matrices
        #----------------------------------------------------------------------
        phi_S_total = n.zeros(self.X.shape[0] * self.X.shape[1],n.complex)
        phi_I = n.zeros(self.X.shape[0] * self.X.shape[1],n.complex)
        
        # Vector of scattered and incident coefficients for diffraction
        #----------------------------------------------------------------------
        scattered_coeffs = myfarm.global_solution_vector_diffraction                 
        
        # Summation of potentials for each body due to scattering
        #----------------------------------------------------------------------
        for ind_body in range(0, myfarm.farm_nbodies):
            
            print ('Body' + str(ind_body))
                                                                               
            phi_S_total = self._potential_scattered_body(myfarm, envobj,\
                          ind_beta, ind_omega, ind_body, scattered_coeffs) + \
                          phi_S_total
                          
        # Incident potential associated with a plane incident wave
        #----------------------------------------------------------------------
        beta  = mySim.beta_array[ind_beta]
        omega = envobj.frequency[ind_omega]
        
        phi_I = self._potential_incident(beta, ind_beta, omega, ind_omega, envobj)
                              
        omega = envobj.frequency[ind_omega]
        g     = envobj.gravity                
        
        eta_diffraction = (1j * omega / g) * (phi_S_total + phi_I)
        
        return eta_diffraction
    #--------------------------------------------------------------------------
    #
    #
    #-------------------------------------------------------------------------- 
    def _eta_scattered(self, myfarm, envobj, ind_beta, ind_omega):
        
        #======================================================================
        #   calculates the free surface elevation for the scattered potential 
        #   (diffraction problem) associated with a propagation direction (beta)  
        #   and a frequency (omega) from all the cases computed previously
        #   indicated by the array of betas (beta_array) and frequencies
        #
        #   input  : beta_array: array of propagating directions
        #            frequencies: array of frequencies calculated
        #            beta : propagation direction desired
        #            omega : desired frequency
        #   output : eta_scattered : free surface elevation at each grid node               
        #======================================================================
        
        
        # Initialization of the total scattered  and incident potential matrices
        #----------------------------------------------------------------------
        phi_S_total = n.zeros(self.X.shape[0] * self.X.shape[1],n.complex)
        
        # Vector of scattered and incident coefficients for diffraction
        #----------------------------------------------------------------------
        scattered_coeffs = myfarm.global_solution_vector_diffraction                 
        
        # Summation of potentials for each body due to scattering
        #----------------------------------------------------------------------
        for ind_body in range(0, myfarm.farm_nbodies):
                                                                               
            phi_S_total = self._potential_scattered_body(myfarm, envobj, \
                          ind_beta, ind_omega, ind_body, scattered_coeffs) + \
                                                                   phi_S_total
                                                      
        omega = envobj.frequency[ind_omega]
        g     = envobj.gravity                
        
        eta_scattered = (1j * omega / g) * (phi_S_total)
        
        return eta_scattered
    #--------------------------------------------------------------------------
    #
    #
    #-------------------------------------------------------------------------- 
    def _eta_incident(self, myfarm, mySim, envobj, ind_beta, ind_omega):
        
        #======================================================================
        #   calculates the free surface elevation for the incident potential
        #   associated with a propagation direction (beta) and 
        #   a frequency (omega) from all the cases computed previously
        #   indicated by the array of betas (beta_array) and frequencies
        #
        #   input  : ind_beta : propagation direction desired
        #            ind_omega : desired frequency
        #   output : eta_incident : free surface elevation at each grid node               
        #======================================================================
       
        # Initialization of the total scattered  and incident potential matrices
        #----------------------------------------------------------------------
        phi_I = n.zeros(self.X.shape[0] * self.X.shape[1], n.complex)
                          
        # Incident potential associated with a plane incident wave
        #----------------------------------------------------------------------
        beta = mySim.beta_array[ind_beta]
        
        omega = envobj.frequency[ind_omega]
        g     = envobj.gravity   
        
        phi_I = self._potential_incident(beta, ind_beta, omega, ind_omega, envobj)
        
        eta_incident = (1j * omega / g) * (phi_I)
        
        return eta_incident
    #--------------------------------------------------------------------------
    #
    #
    #--------------------------------------------------------------------------
    def _potential_scattered_body(self, myfarm, envobj, index_problem, ind_omega,\
                                            ind_body, global_solution_vector):
        
        #======================================================================
        #   - Purpose: calculation of the scattered potential due to either an 
        #              incident wave of beta indicated by "index_problem" or due  
        #              to the radiation of a farm body the degree of freedom of 
        #              which is indicated  as well as "index_problem"
        #   - Inputs : -index_problem: either index of beta or index of degree
        #                             of freedom
        #              -global_solution_vector: vector of scattered coefficients
        #               from the diffraction or the radiation problem
        #               dimension [beta_ind][ind_body][ind_omega][:]
        #               dimension [dof_ind][ind_body][ind_omega][:]
        #              -ind_body : index of the body to analyse
        #              -ind_omega : index of the omega to analyse
        #   - Outputs : - phi_S_body: vector with scattered potential at each
        #                coordinate of the mesh to plot the free surface
        #======================================================================
    
        # Get data from hdf5 file containing partial wave functions
        #----------------------------------------------------------------------
        working_dir_path = os.getcwd()
        path_hdf5_file = os.path.join(working_dir_path,'Results')
        
        name_file = os.path.join(path_hdf5_file,'outgoing_basis_functions.hdf5')
        
        f = h5py.File(name_file,'r')

    
        # Scattered coefficients vector for body ind_body
        #----------------------------------------------------------------------
        Body_A_Scattered = global_solution_vector[ind_omega][ind_body][index_problem][:]    
        
        phi_S_body = n.dot(Body_A_Scattered.transpose(), f[str(ind_body)+'/'+str(ind_omega)][:])
        
        f.close()
        
        return phi_S_body
                
    #--------------------------------------------------------------------------
    #
    # 
    #--------------------------------------------------------------------------
#    def _potential_incident_body(self, myfarm, envobj, index_problem, ind_omega,\
#                                            ind_body, global_solution_vector):   
#        
#       #======================================================================
#        #   - Purpose: calculation of the incident potential RELATIVE TO EACH BODY
#        #              due to either an incident wave of beta indicated by 
#        #              "index_problem" or due to the radiation of a farm body 
#        #              the degree of freedom of which is indicated  as well as
#        #              "index_problem".
#        #   - Inputs : -index_problem: either index of beta or index of degree
#        #                             of freedom
#        #              -global_solution_vector: vector of incident coefficients
#        #               from the diffraction or the radiation problem
#        #               dimension [beta_ind][ind_body][ind_omega][:]
#        #               dimension [dof_ind][ind_body][ind_omega][:]
#        #              -ind_body : index of the body to analyse
#        #              -ind_omega : index of the omega to analyse
#        #   - Outputs : - phi_I_body: matrix with incident potential at each
#        #                coordinate of the mesh to plot the free surface
#        #======================================================================
#        
#        # Initialize matrix with potential at each coordinate
#        #----------------------------------------------------------------------
#        phi_I_body = n.zeros((self.X.shape[0],self.X.shape[1]),n.complex)
#        
#        
#        # Scattered coefficients vector for diffraction of body ind_body
#        #----------------------------------------------------------------------
#        Body_I_Incident = global_solution_vector[ind_omega][ind_body][index_problem][:]     
#        
#        
#        # Polar coordinates of the mesh relative to body "ind_body"
#        #----------------------------------------------------------------------
#        (R_rel,Theta_rel) = self.mesh_rel_global[ind_body]
#        
#        # Generate wave numbers for the frequency defined by ind_omega
#        #----------------------------------------------------------------------
#        k = self.k
#        
#        komega = k[ind_omega]
#        
#        omega = envobj.frequency[ind_omega]
#        
#        trunc_angmode = myfarm.dict_bodies[myfarm.layout[ind_body].name].trunc_angmode[ind_omega] 
#        
#        trunc_depthmode = myfarm.dict_bodies[myfarm.layout[ind_body].name].trunc_depthmode[ind_omega] 
#        
#        # Compute potential as scalar product between a_incident_coeffs and 
#        # evaluated incident wave functions
#        #----------------------------------------------------------------------
#        for ind_i in range(0, self.X.shape[0]):
#            for ind_j in range(0, self.X.shape[1]):
#                
#                # Generate vector of evaluated scattered wave funct at (r,theta,0)
#                #--------------------------------------------------------------
#                r     = R_rel[ind_i, ind_j]
#                theta = Theta_rel[ind_i, ind_j]
#                
#                psi_vector = self._gen_incident_partial_wave_vector(omega,\
#                                  komega,trunc_angmode,trunc_depthmode,\
#                                                      ind_body,r,theta,envobj.depth)
#                                                      
#                phi_I_body[ind_i, ind_j] = n.dot(Body_I_Incident,\
#                                                 psi_vector)
#                                                 
#        return phi_I_body
#        
#    #-------------------------------------------------------------------------- 
    #
    #
    #-------------------------------------------------------------------------- 
    def _potential_incident(self, beta, ind_beta, omega, ind_omega, envobj):
       
       #=======================================================================
       # Purpose : calculation of the incident potential at each node of the free
       #           surface mesh by means of the formula of the potential of a 
       #           plane incident wave
       # Inputs  : -ind_omega :index of the omega to analyse
       #           -ind_beta  :index of the propagation direction to analyse
       # Outputs : -phi_I : vector with incident potential at each
       #                  coordinate of the mesh to plot the free surface
       #=======================================================================
       
       # Initialize matrix with potential at each coordinate
       #----------------------------------------------------------------------
       phi_I = n.zeros((self.X.shape[0],self.X.shape[1]),n.complex)
       
       # Generate wave numbers for the frequency defined by ind_omega
       #----------------------------------------------------------------------
       komega = envobj.k0[ind_omega]
       
       # Calculate offset from reference measuring point (taken as zero)
       #----------------------------------------------------------------------
       offset = 1j * komega * (self.X * n.cos(beta) + self.Y * n.sin(beta))
       
       phi_I = - (1j * envobj.gravity / omega) * n.exp(offset)
       
       phi_I_out = n.ravel(phi_I)
       
       return phi_I_out
       
    #-------------------------------------------------------------------------- 
    #
    #    
    #--------------------------------------------------------------------------
    def _gen_scattered_partial_wave_vector(self,omega,komega,trunc_angmode,\
                                             trunc_depthmode,ind_body,r,theta,depth):
        
        #======================================================================                             
        # Purpose : evaluate scattered partial waves at (r,theta,0) for a 
        #           given omega and truncation values (depth and angular modes)
        # Inputs : - omega = frequency (in rad/s)
        #          - k0omega = progressive wave number of omega
        #          - trunc_angmode = angular mode truncation
        #          - trunc_depthmode = depth mode truncation
        #          - ind_body = refers to the index of the body to which the 
        #                       wave is incident
        #          - r = radial coordinate
        #          - theta = angular coordinate
        #          - depth = domain depth
        # Outputs : - partial_waves = vector of partial waves
        #                          [dim_vect]
        #======================================================================
        
        # Generate vector dimensions and initialize vector
        #----------------------------------------------------------------------
        M = trunc_angmode
        N = trunc_depthmode
          
        dim_vect = 2 * M * (N + 1) + N + 1
          
        partial_waves_value = n.zeros((dim_vect),complex)
        
        # Get radius of body ind_body circumscribing cylinder
        #----------------------------------------------------------------------
        cyl_rad_j = self.circumscribing_cylinders[ind_body]
          
        # Calculate cylindrical partial waves values
        #----------------------------------------------------------------------  
        for ind_m in range(-M, M + 1):
              
            for ind_n in range(0, N + 1):
                  
                global_ind = (M + ind_m) * (N + 1) +  ind_n
                  
                # Progressive modes
                if (ind_n == 0):
                    
                    #Normalizing factor
                    arg_norm = komega[0] * cyl_rad_j
                    index_norm = ind_m
                    norm_f_prog = spec.hankel1(index_norm,arg_norm)
                    #-------------------------------------------------
                    
                    argument = komega[0] * r
                    
                    expression_p = spec.hankel1(ind_m,argument) * n.exp(1j * ind_m * theta)
                      
                    partial_waves_value[global_ind] = expression_p * ((norm_f_prog)**(-1))
                   
                # Evanescent modes 
                else:
                    
                    #Normalizing factor
                    arg_norm_ev = komega[ind_n] * cyl_rad_j
                    index_norm_ev = ind_m
                    norm_f_ev = spec.kn(index_norm_ev, arg_norm_ev)
                    #-------------------------------------------------
                    
                    argument = komega[ind_n] * r
                    
                    expression_ev = n.cos(komega[ind_n] * depth) * \
                                 spec.kn(ind_m, argument) * n.exp(1j * ind_m * theta)
                    
                    partial_waves_value[global_ind] = expression_ev * ((norm_f_ev)**(-1))
                    
                      
        return partial_waves_value
    #-------------------------------------------------------------------------- 
    #
    # 
    #--------------------------------------------------------------------------
    def _gen_incident_partial_wave_vector(self,omega,komega,trunc_angmode,\
                                             trunc_depthmode,ind_body,r,theta,depth):
        
        #======================================================================                             
        # Purpose : evaluate incident partial waves at (r,theta,0) for a 
        #           given omega and truncation values (depth and angular modes)
        # Inputs : - omega = frequency (in rad/s)
        #          - k0omega = progressive wave number of omega
        #          - trunc_angmode = angular mode truncation
        #          - trunc_depthmode = depth mode truncation
        #          - ind_body = refers to the index of the body to which the 
        #                       wave is incident
        #          - r = radial coordinate
        #          - theta = angular coordinate
        #          - depth = domain depth
        # Outputs : - partial_waves = vector of partial waves
        #                          [dim_vect]
        #======================================================================
        
        # Generate vector dimensions and initialize vector
        #----------------------------------------------------------------------
        M = trunc_angmode
        N = trunc_depthmode
          
        dim_vect = 2 * M * (N + 1) + N + 1
          
        partial_waves_value = n.zeros((dim_vect),complex)
        
        # Get radius of body ind_body circumscribing cylinder
        #----------------------------------------------------------------------
        cyl_rad_j = self.circumscribing_cylinders[ind_body]
          
        # Calculate cylindrical partial waves values
        #----------------------------------------------------------------------  
        for ind_m in range(-M, M + 1):
              
            for ind_n in range(0, N + 1):
                  
                global_ind = (M + ind_m) * (N + 1) +  ind_n
                  
                # Progressive modes
                if (ind_n == 0):
                    
                    #Normalizing factor
                    arg_norm = komega[0] * cyl_rad_j
                    index_norm = ind_m
                    norm_f_prog = spec.jv(index_norm,arg_norm)
                    #-------------------------------------------------
                     
                    argument = komega[0] * r
                    
                    expression_p = spec.jv(ind_m,argument) * n.exp(1j * ind_m * theta)
                      
                    partial_waves_value[global_ind] = expression_p * ((norm_f_prog)**(-1))
                   
                # Evanescent modes 
                else:
                    
                    #Normalizing factor
                    arg_norm_ev = komega[ind_n] * cyl_rad_j
                    index_norm_ev = ind_m
                    norm_f_ev = spec.iv(index_norm_ev, arg_norm_ev)
                    #-------------------------------------------------
                    
                    argument = komega[ind_n] * r
                    
                    expression_ev = n.cos(komega[ind_n] * depth) * \
                                 spec.iv(ind_m, argument) * n.exp(1j * ind_m * theta)
                    
                    partial_waves_value[global_ind] = expression_ev * ((norm_f_ev)**(-1))
                    
                      
        return partial_waves_value
    #--------------------------------------------------------------------------
    #
    # 
    #--------------------------------------------------------------------------    
    def _eta_radiation(self, myfarm, envobj, ind_dof, ind_omega):                                                                                          
        
        #======================================================================
        #   calculates the free surface elevation for the radiation problem
        #   associated with a farm degree of freedom (dof) and 
        #   a frequency (omega) from all the cases computed previously
        #   indicated by the array of dofs (myfarm.productive_dofarray) and frequencies            
        #
        #   input  : ind_dof : degree of freedom whose free surface wants to be calculated
        #            ind_omega : frequency the free surface of which wants to be calculated       
        #   output : eta_radiation : complex value free surface elevation at each grid node               
        #======================================================================
        
        
        # Initialization of the total scattered and radiation potential matrices
        #----------------------------------------------------------------------
        phi_S_total = n.zeros(self.X.shape[0] * self.X.shape[1],n.complex)
        phi_R = n.zeros(self.X.shape[0] * self.X.shape[1],n.complex)
        
        # Vector of scattered coefficients for radiation
        #----------------------------------------------------------------------
        scattered_coeffs = myfarm.global_solution_vector_radiation                 
        
        # Summation of potentials for each body due to scattering
        #----------------------------------------------------------------------
        for ind_body in range(0, myfarm.farm_nbodies):
                                                                           
            phi_S_total = self._potential_scattered_body(myfarm, envobj, \
                          ind_dof, ind_omega, ind_body, scattered_coeffs) + \
                                                                    phi_S_total
                          
        # Radiation potential associated with the motion in a ind_dof degree of 
        # freedom of the farm
        #----------------------------------------------------------------------
        phi_R = self._potential_radiated_body(myfarm, envobj, ind_dof, ind_omega)
                              
        omega = envobj.frequency[ind_omega]
        g     = envobj.gravity                
        
        eta_radiation = (1j * omega / g) * (phi_S_total + phi_R)

        return eta_radiation
    #--------------------------------------------------------------------------
    #
    # 
    #--------------------------------------------------------------------------
    def _eta_radiation_global(self, myfarm, mymechanics, envobj, ind_beta,\
                                                                    ind_omega):                                
        
        #======================================================================
        #   - Purpose : calculation of the total free surface elevation due to
        #               the radiated wave fields cause by an incident wave
        #               of beta direction and wave_amp amplitude
        #   - Inputs : -ind_beta : array of wave propagation directions
        #              -ind_omega : desired frequency
        #   - Outputs : -eta_total : vector with total free surface elevation
        #               at each coordinate of the mesh to plot the free surface
        #======================================================================
        
        # Initialize total radiation free surface elevation
        #----------------------------------------------------------------------
        eta_rad_total = n.zeros(self.X.shape[0] * self.X.shape[1],n.complex)
        
        # Generate free surface elevation due to radiation potential
        #----------------------------------------------------------------------
        # Calculate motions due to the incident wave at omega, beta, and wave_amp
        RAO = mymechanics.RAO_calc(envobj, ind_omega, ind_beta)
        
        motion =  RAO
        
        omega = envobj.frequency[ind_omega]
        
        # Calculate velocities from RAO's
        velocities = -1j * omega * motion
        
        print ('Total Free Surface Calculation')
        print ('=============================================================')
        # Scale each radiation free surface by the appropriate velocity
        for ind_dof in range(0,len(myfarm.productive_dofarray)):
            
            dof = myfarm.productive_dofarray[ind_dof]
            
            print ('dof' + str(dof))
            
            eta_rad_unitvel = self._eta_radiation(myfarm, envobj,\
                                                         ind_dof, ind_omega) 
                    
            # Scale the radiated free surface by the appropriate velocity
            eta_rad_total = eta_rad_unitvel * velocities[ind_dof] + eta_rad_total
            
        eta_total = eta_rad_total
        
        return eta_total
        
    #--------------------------------------------------------------------------
    #
    #
    #--------------------------------------------------------------------------
    def _potential_radiated_body(self, myfarm, envobj, ind_dof, ind_omega):
        
        #======================================================================
        #   - Purpose: calculation of the radiated potential due to a motion 
        #              in a degree of freedom of the farm "ind_dof" and in
        #              a frequency described by "ind_omega".
        #   - Inputs : -ind_dof : index of degree of freedoms of the farm
        #              -ind_omega : index of the omega to analyse
        #   - Outputs : - phi_R_body: matrix with radiated potential at each
        #                coordinate of the mesh to plot the free surface
        #======================================================================
        
        # Get data from hdf5 file containing partial wave functions
        #----------------------------------------------------------------------
        working_dir_path = os.getcwd()
        path_hdf5_file = os.path.join(working_dir_path,'Results')
        
        name_file = os.path.join(path_hdf5_file,'outgoing_basis_functions.hdf5')
        
        f = h5py.File(name_file,'r')
        
        # Initialize matrix with potential at each coordinate
        #----------------------------------------------------------------------
        phi_R_body = n.zeros(self.X.shape[0] * self.X.shape[1],n.complex)
        
        # Find characteristics of the body corresponding to the global dof of 
        # the farm "dof" represented by ind_dof                                  
        #----------------------------------------------------------------------
        dof=myfarm.productive_dofarray[ind_dof]                                
        ind_body  = myfarm.inv_dict_dof_farm[dof][0]
        dof_body  = myfarm.inv_dict_dof_farm[dof][1]
        body_name = myfarm.layout[ind_body].name
        rot_angle = myfarm.layout[ind_body].rotation_angle
        
        # Radiated coefficients vector for body ind_body
        #----------------------------------------------------------------------
        if (rot_angle == 0.0):
            Body_R_Radiated = myfarm.dict_bodies[(body_name)].\
                               Radiation_Characteristics[ind_omega][:,dof_body-1]
                               
        else:
            Body_R_Radiated = myfarm.dict_bodies[body_name].\
                                          rotate_operators(rot_angle)[1][ind_omega][:,dof_body-1]
        

        phi_R_body = n.dot(Body_R_Radiated, f[str(ind_body)+'/'+str(ind_omega)][:])
        
        f.close()
                                                 
        return phi_R_body
                
    #--------------------------------------------------------------------------
    #
    # 
    #--------------------------------------------------------------------------    
    def _eta_total(self, myfarm, mymechanics, mySim, envobj, ind_beta, ind_omega, wave_amp, k_pto_layout, b_pto_layout):                                
        
        #======================================================================
        #   - Purpose : calculation of the total free surface elevation due to
        #               an incident wave of frequency omega in a propagation 
        #               direction beta with an amplitude wave_amp
        #   - Inputs : -frequencies : array of frequencies calculated
        #              -beta_array : array of wave propagation directions
        #              -omega : desired frequency
        #              -beta : desired wave propagation direction
        #              -wave_amp : wave amplitude (in meters)
        #   - Outputs : -eta_total : vector with total free surface elevation
        #               at each coordinate of the mesh to plot the free surface
        #======================================================================

        
        # Initialize total free surface elevation
        #----------------------------------------------------------------------
        eta_rad_total = n.zeros(self.X.shape[0] * self.X.shape[1],n.complex)
        
        print ('Total Free Surface Calculation')
        print ('=============================================================')
        print ('Diffraction')
        print ('===========')
        # Generate free surface elevation due to incident potential + scattered
        #----------------------------------------------------------------------
        eta_diff_unitamp = self._eta_diffraction(myfarm, envobj, mySim, \
                                                          ind_beta, ind_omega)
                                                      
        eta_diff = eta_diff_unitamp * wave_amp
        
        # Generate free surface elevation due to radiation potential
        #----------------------------------------------------------------------
        # Calculate motions due to the incident wave at omega, beta, and wave_amp
        RAO = mymechanics.RAO_calc(envobj, ind_omega, ind_beta, k_pto_layout, b_pto_layout)
        
        motion = wave_amp * RAO
        
        omega = envobj.frequency[ind_omega]
        
        # Calculate velocities from RAO's
        velocities = -1j * omega * motion
        
        print ('Radiation')
        print ('===========')
        # Scale each radiation free surface by the appropriate velocity
        for ind_dof in range(0,len(myfarm.productive_dofarray)):
            
            dof = myfarm.productive_dofarray[ind_dof]
            
            print ('dof' + str(dof))

            eta_rad_unitvel = self._eta_radiation(myfarm, envobj,\
                                                         ind_dof, ind_omega) 
                   
            # Scale the radiated free surface by the appropriate velocity
            eta_rad_total = eta_rad_unitvel * velocities[ind_dof] + eta_rad_total
            
        eta_total = eta_diff + eta_rad_total
        
        return eta_total
        
    #--------------------------------------------------------------------------
    #
    # 
    #--------------------------------------------------------------------------    
    def _eta_total_seastate_indivcomponents(self, myfarm, mymechanics, mySim, envobj, k_pto_layout, b_pto_layout, wave_amplitudes): 
        
        #======================================================================
        # function to compute the free surface elevation at each mesh 
        # computational point, for each pair of wave frequency and propagation
        # direction considered
        # 
        #  outputs: - eta_total_seastate : list with each element being the 
        #                   free surface elevation for each pair of freq/theta
        #======================================================================
        
        # Initialize total free surface elevation
        #----------------------------------------------------------------------
        eta_total_seastate = []
        
        for ind_omega in range(0, wave_amplitudes.shape[0]):
            for ind_dir in range(0, wave_amplitudes.shape[1]):
                
                wave_amp = wave_amplitudes[ind_omega, ind_dir]
                
                eta_total_seastate.append(self._eta_total(myfarm, mymechanics, \
                                        mySim, envobj, ind_dir, ind_omega, wave_amp, k_pto_layout, b_pto_layout))
        
        return eta_total_seastate
    #--------------------------------------------------------------------------
    #
    #   
    #--------------------------------------------------------------------------    
    def _eta_incident_seastate_indivcomponents(self, myfarm, mymechanics, mySim, envobj, wave_amplitudes): 
        
        #======================================================================
        # function to compute the free surface elevation at each mesh 
        # computational point, for each pair of wave frequency and propagation
        # direction considered
        # 
        #  outputs: - eta_total_seastate : list with each element being the 
        #                   free surface elevation for each pair of freq/theta
        #======================================================================
        
        # Initialize total free surface elevation
        #----------------------------------------------------------------------
        eta_incident_seastate = []
        
        for ind_omega in range(0, wave_amplitudes.shape[0]):
            for ind_dir in range(0, wave_amplitudes.shape[1]):
                
                wave_amp = wave_amplitudes[ind_omega, ind_dir]

                eta_incident_seastate.append(wave_amp * self._eta_incident(myfarm, mySim,\
                                                   envobj, ind_dir, ind_omega))
        
        return eta_incident_seastate
    #--------------------------------------------------------------------------
    #
    #   
    #--------------------------------------------------------------------------
    def disturbance_coefficient_seastate(self, myfarm ,mymechanics, mySim, envobj, k_pto_layout, b_pto_layout, wave_amplitudes):
        
        #======================================================================
        #  function to compute the disturbance coefficient at each point of the 
        #  computational grid (definition of the disturbance coefficient : Hs / HsInc)
        # 
        #  outputs: - vector with disturbance coefficients     
        #======================================================================
        
        npoints_domain = self.X.shape[0] * self.X.shape[1]
        
        disturbance_coefficients_seastate = n.zeros(npoints_domain, dtype = float)
        
        Hs_incident_vect = n.zeros(npoints_domain, dtype = float)
        
        eta_total_seastate_components = self._eta_total_seastate_indivcomponents(myfarm, mymechanics, mySim, envobj,k_pto_layout, b_pto_layout, wave_amplitudes)
        
        eta_incident_seastate_components = self._eta_incident_seastate_indivcomponents(myfarm, mymechanics, mySim, envobj, wave_amplitudes)
        
        # rearrange list to have each line with the contribution of a pair of 
        # frequency and theta 
        eta_total_seastate_rearranged = n.vstack(eta_total_seastate_components)
        
        eta_incident_seastate_rearranged = n.vstack(eta_incident_seastate_components)
        
        for ind_point in range(0, npoints_domain):
            
            # Comment: while in the disturbance coefficient the factor 1.08 cancels
            #          out, it should be noted that it should be used only when
            #          the spectrum is not narrow to accurately compute the value of Hs
            
            m0_etatotal = n.sum(0.5 * (abs(eta_total_seastate_rearranged[:, ind_point]) ** 2))
            
            Hs_etatotal = 4. * n.sqrt(m0_etatotal) / 1.08
            
            m0_etaincident = n.sum(0.5 * (abs(eta_incident_seastate_rearranged[:, ind_point]) ** 2))
            
            Hs_incident = 4. * n.sqrt(m0_etaincident) / 1.08
            
            disturbance_coefficients_seastate[ind_point] = Hs_etatotal / Hs_incident
            
            Hs_incident_vect[ind_point] = Hs_incident
            
        
        return (disturbance_coefficients_seastate,Hs_incident_vect)
    #--------------------------------------------------------------------------
    #
    #        
    #--------------------------------------------------------------------------
    def _eta_total_seastate(self, myfarm ,mymechanics, mySim, envobj):
        
        #======================================================================
        #  function to compute the disturbance coefficient at each point of the 
        #  computational grid (definition of the disturbance coefficient : Hs / HsInc)
        # 
        #  outputs: - vector with disturbance coefficients     
        #======================================================================
        
        npoints_domain = self.X.shape[0] * self.X.shape[1]
        
        eta_total_seastate = n.zeros(npoints_domain, dtype = complex)
        
        eta_total_seastate_components = self._eta_total_seastate_indivcomponents(myfarm, mymechanics, mySim, envobj)
        
        # rearrange list to have each line with the contribution of a pair of 
        # frequency and theta 
        eta_total_seastate_rearranged = n.vstack(eta_total_seastate_components)
        
        for ind_point in range(0, npoints_domain):
            
            eta_total_seastate[ind_point] = n.sum(eta_total_seastate_rearranged[:,ind_point]) 
        
        return eta_total_seastate  
    #--------------------------------------------------------------------------
    #
    #
    #--------------------------------------------------------------------------
#    def nemoh_total_eta(self,nemoh_eta_abs,nemoh_eta_phase,wave_amp,\
#            ind_omega,ind_beta,myfarm,envobj,mymechanics,beta_array,frequencies,beta,omega,nbodies):
#        
#        # Initialize total radiation free surface elevation
#        #----------------------------------------------------------------------
#        eta_rad_total = n.zeros((self.X.shape[0],self.X.shape[1]),n.complex)
#        
#        eta_I = self._eta_incident(myfarm, envobj, beta_array, frequencies, beta, omega)
#        
#        # Calculate motions due to the incident wave at omega, beta, and wave_amp
#        RAO = mymechanics.RAO_calc(envobj, ind_omega, ind_beta)
#        
#        motion = [wave_amp * x for x in RAO]
#        
#        # Calculate velocities from RAO's
#        velocities = [-1j * omega * x for x in motion]
#        
#        # Unitary Nemoh scattered wave elevation
#        
#        nemoh_eta_abs_s = n.transpose(nemoh_eta_abs[:,:,0],(1,0))
#        nemoh_eta_phase_s = n.transpose(nemoh_eta_phase[:,:,0],(1,0))
#        
#        eta_S = nemoh_eta_abs_s[:,:,] * (n.cos(nemoh_eta_phase_s) + \
#                                        1j * n.sin(nemoh_eta_phase_s))
#                                          
#        eta_S = wave_amp * eta_S
#                                          
#        # Unitary Radiation potential Nemoh
#        dofs = range(1,nbodies*6 + 1)
#            
#        for ind in dofs:
#            
#            nemoh_eta_abs_r = n.transpose(nemoh_eta_abs[:,:,ind],(1,0))
#            nemoh_eta_phase_r = n.transpose(nemoh_eta_phase[:,:,ind],(1,0))
#            
#            eta_rad_unitvel = nemoh_eta_abs_r * (n.cos(nemoh_eta_phase_r) + \
#                                          1j * n.sin(nemoh_eta_phase_r))
#            
#            # Identify to which body and individual degree of freedom corresponds
#            # the global degree of freedom of the farm
#            body_numb = myfarm.inv_dict_dof_farm[ind][0]
#            body_dof  = myfarm.inv_dict_dof_farm[ind][1] - 1
#        
#            # Scale the radiated free surface by the appropriate velocity
#            eta_rad_total = eta_rad_unitvel * velocities[body_numb][body_dof] + eta_rad_total
#            
#        eta_total = eta_rad_total + eta_S + eta_I
#        
#        return eta_total
    #--------------------------------------------------------------------------
    #
    #    
    #-------------------------------------------------------------------------- 
    def case_calculation(self, envobj, myfarm, omega):
        
        frequencies = envobj.frequency
        
        char_dim = myfarm.dict_bodies[myfarm.layout[0].name].cyl_rad
        
        # Generate indexs of propagation frequency
        #----------------------------------------------------------------------
        freqs_list = n.ndarray.tolist(frequencies)
        for ind in range(0,len(freqs_list)):
            freqs_list[ind] = round(freqs_list[ind],1)
        
        ind_omega = freqs_list.index(omega)
        
        lambda_val = (2 * n.pi) / envobj.k0[ind_omega]
        
        case = lambda_val / char_dim
        
        return case
        
        
        