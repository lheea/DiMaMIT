# METHODS TO SOLVE DISPERSION EQUATIONS
#
# Import relevant modules
#------------------------------------------------------------------------------
import math as m
import numpy as n
#------------------------------------------------------------------------------
#
# Constants
#------------------------------------------------------------------------------
NMAX = 30000             # Max number of iterations

TOL  = 1e-14            # Tolerance of the result

epsilon = n.spacing(1)  # Separation from the singular point of the tangent
#------------------------------------------------------------------------------
#
# Newton Raphson Algorithm (converged if error = zero)
#------------------------------------------------------------------------------
def Newton_Raphson_alg(function, function_derivative, omega, depth, initial_guess):
    
    root = initial_guess
    
    for index in range(1,NMAX + 1):
        
        function_val = function(root,omega)
        
        # A root has been found 
        #----------------------------------------------------------------------
        if (abs(function_val) <= TOL): 
            
            error = 0
            return (root,error)
            
        function_der_val = function_derivative(root)
        
        # No more iteration are possible 
        #----------------------------------------------------------------------
        if (function_der_val == 0.0): 
            
            error = - 1
            return (root,error)
            
        
        root = root - function_val / function_der_val
        
    # Process has not converged  
    #--------------------------------------------------------------------------
    if (error != 0):                 
        
        error = -2 
        
        return (root,error)
#------------------------------------------------------------------------------ 
#
# Bisection Algorithm
#------------------------------------------------------------------------------ 
def Bisection(function, omega, depth, index_depthmode):
    
    
    # Define interval limits    
    #--------------------------------------------------------------------------
    xlow = ((m.pi) / (2 * depth)) + (index_depthmode - 1) * \
                                                      (m.pi / depth) + epsilon
                                                      
    xhigh = ((m.pi) / (2 * depth)) + index_depthmode * (m.pi / depth) - epsilon
    
    # Begin iterations
    #--------------------------------------------------------------------------
    N = 1
    
    error = -1
    
    while (N < NMAX):
        
        mid = (xlow + xhigh) / 2
        
        # Solution Found
        #----------------------------------------------------------------------
        if ((function(mid,omega) == 0) | (((xhigh - xlow) / 2) < TOL)):
            
            error = 0
            
            return (mid,error)
            
            N = NMAX
            
        # New interval
        #----------------------------------------------------------------------
        if (((function(mid,omega) > 0) & (function(xlow,omega) > 0)) | \
           ((function(mid,omega) < 0) & (function(xlow,omega) < 0))):
               
               xlow = mid
               
        else:
            
              xhigh = mid
#------------------------------------------------------------------------------               
   
    
    
    
    
    
    