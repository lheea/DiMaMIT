#%% MODULES IMPORT

# Import python tools
#------------------------------------------------------------------------------
import numpy as n
import scipy.linalg as spy
#------------------------------------------------------------------------------
#
# CLASS DEFINING THE MECHANICS (motions, power production, etc.)
#------------------------------------------------------------------------------
class Mechanics:
    
    # Class Initialization
    #--------------------------------------------------------------------------
    def __init__(self, sim, env, farm, Mooring_layout):
        
        # Farm productive dofs
        #----------------------------------------------------------------------
        self.prod_dofs     = farm.productive_dofarray                   
        self.inv_dict_dofs = farm.inv_dict_dof_farm
        
        # Mooring Characteristics
        #----------------------------------------------------------------------
        self.k_mooring_layout = []
        
        for ind in range(0, farm.farm_nbodies):
            
            self.k_mooring_layout.append(Mooring_layout[ind].K)
        #----------------------------------------------------------------------
            
        # Hydrostatic Characteristics
        #----------------------------------------------------------------------
        self.KH_layout = []
        self.M_layout  = []
        
        for ind in range(0, farm.farm_nbodies):
            
            body_name = farm.layout[ind].name
            
            self.KH_layout.append(farm.dict_bodies[body_name].Stiffness)        
            self.M_layout.append(farm.dict_bodies[body_name].Inertia)
        #----------------------------------------------------------------------
            
        # Hydrodynamic Characteristics (interactions taken into account)
        #----------------------------------------------------------------------
        self.AM   = farm.Added_Mass_farm
        self.DAMP = farm.Damping_farm
        self.Fe   = farm.Excitation_forces_farm
        #----------------------------------------------------------------------
            
        # Prepare global matrices of Hydrostatic quantities ready for calculation
        #----------------------------------------------------------------------
        self.KH = self._Assemble_mech_layout(self.KH_layout)
        self.M = self._Assemble_mech_layout(self.M_layout)
            
        # Prepare hydro coeffs for calculation
        #----------------------------------------------------------------------
        self.global_am_coeffs = self._Assemble_global_hydro_coeffs(self.AM, sim)
        self.global_damp_coeffs = self._Assemble_global_hydro_coeffs(self.DAMP, sim)
        self.global_exc_forces = self._Assemble_global_excforce_coeffs(sim)
        
        # Mooring matrix for calculations
        #----------------------------------------------------------------------
        self.KMooring = self._Assemble_mech_layout(self.k_mooring_layout)
    
        # Hydrodynamic Characteristics (no interactions taken into account)
        #----------------------------------------------------------------------
        self.AM_single   = self._collect_coeffs(sim, env, farm)[0]
        self.DAMP_single = self._collect_coeffs(sim, env, farm)[1]
        self.Fe_single   = self._collect_coeffs(sim, env, farm)[2]
        
       
            
    #--------------------------------------------------------------------------
    #
    # Assemble coefficients without interactions in a list
    #--------------------------------------------------------------------------
    def _collect_coeffs(self, sim, env, farm):
        
        AM_coeffs   = []
        DAMP_coeffs = []
        Fe_coeffs   = []
        
        for ind in range(0,farm.farm_nbodies):
            
            name = farm.layout[ind].name
            
            AM_coeffs.append(farm.dict_bodies[name].Added_Mass_Coefficients)
            DAMP_coeffs.append(farm.dict_bodies[name].Damping_Coefficients)
            
            # calculate excitation forces from force transfer matrix and 
            # plane wave incident coefficients
                                                     
            Fe_coeffs.append(self._gen_exc_forces_indiv(sim, env, farm, ind))
            
        return (AM_coeffs, DAMP_coeffs, Fe_coeffs)
    
    #--------------------------------------------------------------------------
    #
    # Generate excitation forces for an isolated body of the farm
    #--------------------------------------------------------------------------
    def _gen_exc_forces_indiv(self, sim, env, farm, ind_body):
        
        # Initialize empty exc force with dim [ndof_body,nbeta,nomega]
        name = farm.layout[ind_body].name
        
        ndofs = farm.dict_bodies[name].dof_body
        
        exc_forces_body = n.zeros((ndofs, len(sim.beta_array), len(env.frequency)), n.complex)
        
        # recuperate frequencies
        frequencies = env.frequency

        # Rotate Force Transfer Matrix if necessary
        rot_angle = farm.layout[ind_body].rotation_angle
        
        if (rot_angle == 0.0):
                
            body_FTM = farm.dict_bodies[name].Force_Transfer_Matrix[:]
                    
        else:
                    
            body_FTM = farm.dict_bodies[name].\
                                        rotate_operators(rot_angle)[2]
                                        
        # Iterate through omegas and frequencies
        for ind_omega in range(0, len(frequencies)):
            
            trunc_angmode = farm.dict_bodies[name].trunc_angmode[ind_omega]
        
            trunc_depthmode = farm.dict_bodies[name].trunc_depthmode[ind_omega]
            
            omega = frequencies[ind_omega]
            
            k0omega = env.k0[ind_omega]
            
            for ind_beta in range(0, len(sim.beta_array)):
                
                beta = sim.beta_array[ind_beta]
        
                vector_part_waves = farm.gen_cyl_coefficients(beta,omega,k0omega,\
                                       trunc_angmode, trunc_depthmode,ind_body)
                 
                # product of incident partial waves coefficients and  
                # force transfer matrix
                if (rot_angle == 0.0):
                    exc_force_isol = n.dot(body_FTM[ind_omega][:,:], vector_part_waves)
                else:
                    exc_force_isol = n.dot(body_FTM[ind_omega], vector_part_waves)
                
                exc_forces_body[:,ind_beta,ind_omega] = exc_force_isol
                
        return exc_forces_body
            
    #--------------------------------------------------------------------------
    #        
    # Calculate motion of bodies in farm for a unit wave amplitude
    #--------------------------------------------------------------------------     
    def RAO_calc(self, env, ind_omega, ind_beta, k_pto_layout, b_pto_layout):
        
        # Omega 
        #----------------------------------------------------------------------
        omega = env.frequency[ind_omega]
        
        # Generate global matrices from PTO and mooring layouts
        #----------------------------------------------------------------------
        KPTO = self._Assemble_mech_layout(k_pto_layout)
        BPTO = self._Assemble_mech_layout(b_pto_layout)
        KMooring = self.KMooring
        
        # Global matrices of Hydrostatic quantities
        #----------------------------------------------------------------------
        KH = self.KH
        M = self.M
        
        # Get global matrices of Hydrodynamic quantities for the chosen omega and beta
        #----------------------------------------------------------------------
        Fe = self.global_exc_forces[:, ind_omega, ind_beta]
        AM = self.global_am_coeffs[:, :, ind_omega]
        DAMP = self.global_damp_coeffs[:, :, ind_omega]
        
        # Calculate Impedance Matrix
        #----------------------------------------------------------------------
        left_block = (- omega**2) * (M + AM) - (1j * omega) * (DAMP + BPTO) + \
                                                         (KH + KMooring + KPTO)      
                                                                                        
        # Calculate RAOs
        #----------------------------------------------------------------------
#        inv_left_block = n.linalg.inv(left_block)
#
#        RAO_global = n.dot(inv_left_block,Fe)
                                                                                        
        RAO_global = n.linalg.solve(left_block,Fe)
        
        return RAO_global
    #--------------------------------------------------------------------------
    #
    #
    #--------------------------------------------------------------------------
    def RAO_calc_isolated(self, env, ind_omega, ind_beta, k_pto_layout, b_pto_layout):
        
        #======================================================================
        #     calculate RAO of an isolated body "ind_body" in the "ind_dof"  
        #     productive degree of freedom, for an omega, beta 
        #     Inputs: -env: environment class
        #             -ind_omega: index of the omega
        #             -ind_beta: index of propagation direction
        #     Outputs: -raos_body_arr: motion of the bodies in the productive
        #                             degrees of freedom
        #======================================================================
        
        omega = env.frequency[ind_omega]
        
        # Store productive dofs of each body
        raos_body_list = [] 
        
        for body in range(0, len(self.M_layout)):
            
            # Select hydro coeffs
            AM = self.AM_single[body][ind_omega][:,:,0]
            DAMP = self.DAMP_single[body][ind_omega][:,:,0]
            
            # Select exc forces
            Fe = self.Fe_single[body][:,ind_beta,ind_omega]
            
            # Inertia
            M = self.M_layout[body]
            
            # Hydrostatic stiffness
            KH = self.KH_layout[body]
            
            # PTO forces
            KPTO = k_pto_layout[body]
            BPTO = b_pto_layout[body]
            
            # Mooring forces
            KMooring = self.k_mooring_layout[body]
            
            #prepare diagonal matrices (off diagonal terms set
            #to zero so that no interaction between dofs is taken into account) #change for a body with multiple productive dofs
            diag_elem_AM = n.diagonal(AM)
            diag_elem_DAMP = n.diagonal(DAMP)
            
            AM_d = n.diag(diag_elem_AM)
            DAMP_d = n.diag(diag_elem_DAMP)
                        
            # Calculate Impedance Matrix
            #----------------------------------------------------------------------
            left_block = (- omega**2) * (M + AM_d) - \
                         (1j * omega) * (DAMP_d + BPTO) + (KH + KMooring + KPTO) 
                         
            # Calculate RAOs
            #----------------------------------------------------------------------
            inv_left_block = n.linalg.inv(left_block)
        
            RAO = n.dot(inv_left_block,Fe)
            
            raos_body_list.append(RAO)
        
        # Select only the productive RAOs of each body
        raos_body_arr = n.hstack(raos_body_list)[self.prod_dofs - 1] 
        
        return raos_body_arr
        
    #--------------------------------------------------------------------------
    #    
    # Create a single array from the PTO and the Mooring layout
    #--------------------------------------------------------------------------     
    def _Assemble_mech_layout(self, mech_layout):
        
        length = len(mech_layout)
        
        if (length > 1):
        
            diag_matrix = spy.block_diag(mech_layout[0], mech_layout[1])
         
            for ind in range(2, length):
         
                 diag_matrix = spy.block_diag(diag_matrix, mech_layout[ind])
                 
        else:
            
            diag_matrix = mech_layout[0]
             
        # Chop array mechanics if necessary
        #----------------------------------------------------------------------
        # Check number of dofs in farm analysed
        ndofs_an = self.AM[0].shape[1]
        
        # Number of dofs of each wec
        ndofs_single = self.Fe[0].shape[0]
        
        # If ndofs lower than total possible farms dofs, select elements of
        # added mass and damping matrices to form square matrix
        
        if (ndofs_an < (len(self.M_layout) * ndofs_single)):
            
            indexs_selected = self.prod_dofs - 1
            
            diag_matrix = diag_matrix[indexs_selected,:][:,indexs_selected]  
             
        return diag_matrix
    #--------------------------------------------------------------------------
    #
    # Assemble global exc forces for all frequencies and propagation directions
    #--------------------------------------------------------------------------
    def _Assemble_global_excforce_coeffs(self, sim):
        
        nomegas = len(sim.frequency_array)
        ndir    = len(sim.beta_array)
        ndofs   = len(sim.dof_array)
        
        global_exc_forces = n.zeros((ndofs, nomegas, ndir), dtype=complex)
        
        for ind_omega in range(0, nomegas):
            for ind_beta in range(0, ndir):
                
                global_exc_forces[:, ind_omega, ind_beta] = self._Assemble_excforce_coeffs(ind_omega, ind_beta)
                
        return global_exc_forces        
        
    #--------------------------------------------------------------------------
    #    
    # Assemble exc forces
    #--------------------------------------------------------------------------  
    def _Assemble_excforce_coeffs(self, ind_omega, ind_beta):
        
        # Empty list of excitation forces
        #----------------------------------------------------------------------
        Fe_global = []
        length    = len(self.Fe)
        
        # Store excitation forces matrices in a list 
        for ind_body in range(0, length):
            
            Fe_global.append(self.Fe[ind_body][:,ind_beta,ind_omega])
            
        # Put excitation forces matrices in a unique vector
        unique_vector = n.hstack(Fe_global)
        
        # Chop excitation exc forces vector if necessary
        #----------------------------------------------------------------------
        # Check number of dofs in farm analysed
        ndofs_an = self.AM[0].shape[1]
        
        # Number of dofs of each wec
        ndofs_single = self.Fe[0].shape[0]
        
        # If ndofs lower than total possible farms dofs, select elements of
        # added mass and damping matrices to form square matrix
        
        if (ndofs_an < (len(self.M_layout) * ndofs_single)):
            
            indexs_selected = self.prod_dofs - 1
            
            unique_vector = unique_vector[indexs_selected]  
            
        return unique_vector
        
    #--------------------------------------------------------------------------  
    #
    # Assemble global hydro coeffs
    #-------------------------------------------------------------------------- 
    def _Assemble_global_hydro_coeffs(self, hydro_coeff, sim):
        
        nomegas = len(sim.frequency_array)
        ndof    = len(sim.dof_array)
        
        global_hydro_coeffs = n.zeros((ndof, ndof, nomegas), dtype=complex)
        
        for ind_omega in range(0, nomegas):
                
            global_hydro_coeffs[:, :, ind_omega] = self._Assemble_hydro_coeffs(hydro_coeff, ind_omega)
                
        return global_hydro_coeffs    
        
    #--------------------------------------------------------------------------  
    #    
    # Assemble hydro coeffs
    #--------------------------------------------------------------------------  
    def _Assemble_hydro_coeffs(self, hydro_coeff, ind_omega):
        
        # Empty list of excitation forces
        #----------------------------------------------------------------------
        Coeff_global = []
        
        length    = len(hydro_coeff)
        
        # Store hydro coeffs matrices in a list 
        for ind_body in range(0, length):
            
            Coeff_global.append(hydro_coeff[ind_body][:,:,ind_omega])
            
        # Put hydro coeffs matrices in a unique matrix   
        unique_matrix = n.vstack(Coeff_global)     
        
        # Chop excitation hydro coeffs matrix if necessary
        #----------------------------------------------------------------------
        # Check number of dofs in farm analysed
        ndofs_an = self.AM[0].shape[1]
        
        # Number of dofs of each wec
        ndofs_single = self.Fe[0].shape[0]
        
        # If ndofs lower than total possible farms dofs, select elements of
        # added mass and damping matrices to form square matrix
        
        if (ndofs_an < (len(self.M_layout) * ndofs_single)):
            
            indexs_selected = self.prod_dofs - 1
            unique_matrix = unique_matrix[indexs_selected,:]
             
        return unique_matrix 
        
    #--------------------------------------------------------------------------
    #
    #
    #--------------------------------------------------------------------------
    def Power_bodies_infarm(self, env, ind_omega, ind_beta, wave_amp, k_pto_layout, b_pto_layout):
        
        #======================================================================
        #     calculate the absorbed power by all the productive degrees of 
        #     freedom of a body in the farm due to an incident regular wave of 
        #     frequency(ind_omega) and beta(ind_beta) of a wave amplitude(wave_amp)
        #     Inputs : - env: environment class
        #              - ind_omega: index of the omega treated
        #              - ind_beta: index of the wave direction (rad's)
        #              - wave_amp:  wave amplitude (m)
        #     Outputs: - power : power in Watts absorbed in all productive dofs
        #======================================================================
        
        # Initialize power vect
        nbodies = len(self.M_layout)
        
        power_vect = n.zeros((nbodies))
        
        # Calculate powers        
        RAO = self.RAO_calc(env, ind_omega, ind_beta, k_pto_layout, b_pto_layout)
        
        #RAO_square = [x**2 for x in RAO]
        RAO_square = RAO ** 2
        
        omega = env.frequency[ind_omega]
        
        omega_square = omega ** 2
        
        wave_amp_square = wave_amp ** 2
        
        
        for ind in range(0,len(self.prod_dofs)):
            
            index_body = self.inv_dict_dofs[self.prod_dofs[ind]][0]
            
            index_dof_body   = self.inv_dict_dofs[self.prod_dofs[ind]][1] - 1
            
            bpto = b_pto_layout[index_body][index_dof_body, index_dof_body]
            
            absRAO = abs(RAO_square[ind]) 
            
            power_vect[index_body] = (1. / 2) * bpto * omega_square * absRAO * \
                                       wave_amp_square + power_vect[index_body]
                                       
        return power_vect
    #--------------------------------------------------------------------------
    #
    # 
    #--------------------------------------------------------------------------
    def Power_bodies_infarm_seastate(self, env, wave_amplitudes, k_pto_layout, b_pto_layout):
        
        #======================================================================
        #  calculate the absorbed power by all productive degrees of freedom of 
        #  the bodies in the farm in a seastate defined by a couple (Hs, Tp)
        #  Inputs : env: environment class
        #           wave_amplitudes : [nfreqs, ndir]
        #  Ouputs : power_vect : power in Watts absorbed by each body [nbodies]
        #======================================================================
    
        # Initialize power vect
        nbodies = len(self.M_layout)
        
        # initialize vector of total computed power in the seastate
        power_seastate = n.zeros(nbodies, dtype = float)
        
        for ind_omega in range(0, wave_amplitudes.shape[0]):
            for ind_dir in range(0, wave_amplitudes.shape[1]):
                
                wave_amp = wave_amplitudes[ind_omega, ind_dir]
                
                power_seastate[:] = self.Power_bodies_infarm(env, ind_omega, ind_dir, wave_amp, k_pto_layout, b_pto_layout) + power_seastate[:]
        
        return power_seastate
    #--------------------------------------------------------------------------  
    #
    #
    #--------------------------------------------------------------------------
    def Power_bodies_isolated(self, env, ind_omega, ind_beta, wave_amp, k_pto_layout, b_pto_layout): 
        
        #======================================================================
        #     calculate the absorbed power by all the productive degrees of 
        #     freedom of an isolated body due to an incident regular wave of 
        #     frequency(ind_omega) and beta(ind_beta) of a wave amplitude(wave_amp)
        #     Inputs : - env: environment class
        #              - ind_omega: index of the omega treated
        #              - ind_beta: index of the wave direction (rad's)
        #              - wave_amp:  wave amplitude (m)
        #     Outputs: - power : power in Watts absorbed in all productive dofs
        #======================================================================
        
        # Initialize power vect
        nbodies = len(self.M_layout)
        
        power_vect = n.zeros((nbodies))
        
        # Calculate powers        
        RAO = self.RAO_calc_isolated(env, ind_omega, ind_beta, k_pto_layout, b_pto_layout)

        RAO_square = RAO ** 2
        
        omega = env.frequency[ind_omega]
        
        omega_square = omega ** 2
        
        wave_amp_square = wave_amp ** 2
        
        for index in range(0, len(self.prod_dofs)):
            
            dof = self.prod_dofs[index]
            
            index_body = self.inv_dict_dofs[dof][0]
            index_dof_body   = self.inv_dict_dofs[dof][1] - 1
            
            bpto = b_pto_layout[index_body][index_dof_body, index_dof_body]
             
            absRAO = abs(RAO_square[index])  
            
            power_vect[index_body] = (1. / 2) * bpto * omega_square * absRAO * \
                                       wave_amp_square + power_vect[index_body]
                                       
        return power_vect
    #--------------------------------------------------------------------------
    #
    #
    #--------------------------------------------------------------------------
    def Power_bodies_isolated_seastate(self, env, wave_amplitudes, k_pto_layout, b_pto_layout):
        
        #======================================================================
        #  calculate the absorbed power by all productive degrees of freedom of 
        #  the bodies isolated in a seastate defined by a couple (Hs, Tp)
        #  Inputs : env: environment class
        #           wave_amplitudes : [nfreqs, ndir]
        #  Ouputs : power_vect : power in Watts absorbed by each body [nbodies]
        #======================================================================
    
        # Initialize power vect
        nbodies = len(self.M_layout)
        
        # initialize vector of total computed power in the seastate
        power_seastate = n.zeros(nbodies, dtype = float)
        
        for ind_omega in range(0, wave_amplitudes.shape[0]):
            for ind_dir in range(0, wave_amplitudes.shape[1]):
                
                wave_amp = wave_amplitudes[ind_omega, ind_dir]
                
                power_seastate[:] = self.Power_bodies_isolated(env, ind_omega, ind_dir, wave_amp, k_pto_layout, b_pto_layout) + power_seastate[:]
        
        return power_seastate
    #--------------------------------------------------------------------------  
    #
    #
    #--------------------------------------------------------------------------
    def q_factor_calc(self,env,ind_omega,ind_beta,wave_amp,k_pto_layout,b_pto_layout):
        
        
        power_isol = self.Power_bodies_isolated(env, ind_omega, ind_beta, wave_amp, k_pto_layout, b_pto_layout)
        
        power_infarm = self.Power_bodies_infarm(env, ind_omega, ind_beta, wave_amp, k_pto_layout, b_pto_layout)
        
        q_factors = power_infarm / power_isol
        
        return q_factors
        
    #--------------------------------------------------------------------------
    #
    #
    #--------------------------------------------------------------------------
    def q_factor_calc_seastate(self, env, wave_amplitudes, k_pto_layout, b_pto_layout):
        
        
        power_isol_seastate = self.Power_bodies_isolated_seastate(env, wave_amplitudes, k_pto_layout, b_pto_layout)
        
        power_infarm_seastate = self.Power_bodies_infarm_seastate(env, wave_amplitudes, k_pto_layout, b_pto_layout)
        
        q_factors_seastate = power_infarm_seastate / power_isol_seastate
        
        return q_factors_seastate
        
    #--------------------------------------------------------------------------   
    #
    #
    #--------------------------------------------------------------------------
    def q_mod_factor_calc(self,env,sim, ind_omega,ind_beta,wave_amp,k_pto_layout,b_pto_layout):
        
        power_isol = self.Power_bodies_isolated(env,ind_omega,ind_beta,wave_amp, k_pto_layout, b_pto_layout)
        
        power_infarm = self.Power_bodies_infarm(env, ind_omega, ind_beta, wave_amp, k_pto_layout, b_pto_layout)
        
        q_mod_factors = (power_infarm - power_isol) / self.max_isol_power_calc(env, wave_amp, sim, k_pto_layout, b_pto_layout)[ind_beta,:]
        
        return q_mod_factors
    #--------------------------------------------------------------------------     
    #
    #
    #--------------------------------------------------------------------------     
    def global_q_factor_calc(self,env,sim,wave_amplitudes):
        
        # Return an array of dimensions [nbeta,nomegas,nbodies] with the 
        # q factors
        
        nbodies = len(self.M_layout)
        
        nfreqs  = len(env.frequency) 
        
        nbetas  = len(sim.beta_array)
        
        q_factors_global = n.zeros((nbetas,nfreqs,nbodies))
        
        for ind_beta in range(0,nbetas):
            for ind_omega in range(0,nfreqs):
                
                q_factors_global[ind_beta,ind_omega,:] = self.q_factor_calc(env,ind_omega,ind_beta,wave_amplitudes[ind_omega, ind_beta])
        
        return q_factors_global        
    #--------------------------------------------------------------------------                
    #
    #      
    #--------------------------------------------------------------------------     
    def global_q_mod_factor_calc(self,env,sim,wave_amplitudes):
        
        # Return an array of dimensions [nbeta,nomegas,nbodies] with the 
        # q mod factors
        
        nbodies = len(self.M_layout)
        
        nfreqs  = len(env.frequency) 
        
        nbetas  = len(sim.beta_array)
        
        q_mod_factors_global = n.zeros((nbetas,nfreqs,nbodies))
        
        for ind_beta in range(0,nbetas):
            for ind_omega in range(0,nfreqs):
                
                q_mod_factors_global[ind_beta,ind_omega,:] = self.q_mod_factor_calc(env,sim,ind_omega,ind_beta,wave_amplitudes[ind_omega, ind_beta])
        
        return q_mod_factors_global        
    #--------------------------------------------------------------------------                
    #
    # 
    #--------------------------------------------------------------------------  
    def global_RAO_calc(self,env,sim,k_pto_layout,b_pto_layout):
        
        nbodies = len(self.M_layout)
        
        nfreqs  = len(env.frequency) 
        
        nbetas  = len(sim.beta_array)
        
        rao = n.zeros((nbetas,nfreqs,nbodies),dtype=complex)
        
        for ind_beta in range(0,nbetas):
            for ind_omega in range(0,nfreqs):
                
                rao[ind_beta,ind_omega,:] = self.RAO_calc(env, ind_omega, ind_beta, k_pto_layout, b_pto_layout)
                
        return rao
    #--------------------------------------------------------------------------  
    #
    #
    #--------------------------------------------------------------------------  
    def global_RAO_isol_calc(self,env,sim,k_pto_layout,b_pto_layout):
        
        nbodies = len(self.M_layout)
        
        nfreqs  = len(env.frequency) 
        
        nbetas  = len(sim.beta_array)
        
        rao = n.zeros((nbetas,nfreqs,nbodies),dtype=complex)
        
        for ind_beta in range(0,nbetas):
            for ind_omega in range(0,nfreqs):
                
                rao[ind_beta,ind_omega,:] = self.RAO_calc_isolated(env, ind_omega, ind_beta, k_pto_layout, b_pto_layout)
                
        return rao
    #--------------------------------------------------------------------------  
    #
    #
    #--------------------------------------------------------------------------
    def power_farm_global(self,env,wave_amplitudes, sim,k_pto_layout,b_pto_layout):
        
        nbodies = len(self.M_layout)
        
        nfreqs  = len(env.frequency) 
        
        nbetas  = len(sim.beta_array)
        
        power = n.zeros((nbetas,nfreqs,nbodies))
        
        for ind_beta in range(0,nbetas):
            for ind_omega in range(0,nfreqs):
                
                power[ind_beta,ind_omega,:] = self.Power_bodies_infarm(env, ind_omega, ind_beta, wave_amplitudes[ind_omega, ind_beta],k_pto_layout,b_pto_layout)
        
        return power      
    #--------------------------------------------------------------------------  
    #
    #
    #--------------------------------------------------------------------------   
    def power_isolated_global(self,env,wave_amplitudes,sim,k_pto_layout,b_pto_layout):
        
        nbodies = len(self.M_layout)
        
        nfreqs  = len(env.frequency) 
        
        nbetas  = len(sim.beta_array)
        
        power = n.zeros((nbetas,nfreqs,nbodies))
        
        for ind_beta in range(0,nbetas):
            for ind_omega in range(0,nfreqs):
                
                 power[ind_beta,ind_omega,:] = self.Power_bodies_isolated(env, ind_omega, ind_beta, wave_amplitudes[ind_omega, ind_beta],k_pto_layout,b_pto_layout)
      
        return power    
    #--------------------------------------------------------------------------  
    #
    #
    #--------------------------------------------------------------------------  
    def max_isol_power_calc(self,env, wave_amplitudes, sim, k_pto_layout, b_pto_layout):  
        # Return max_powers which is an array of [nbeta,nbodies] elements, 
        # with the max power produced for the range of frequencies considered
        
        nbodies = len(self.M_layout)
        
        nfreqs  = len(env.frequency) 
        
        nbetas  = len(sim.beta_array)
        
        powers_isol = n.zeros((nfreqs,nbodies))
        
        max_powers  = n.zeros((nbetas,nbodies))
        
        for ind_beta in range(0,nbetas):
        
            for ind_omega in range(0,len(env.frequency)):
        
                powers_isol[ind_omega,:] = self.Power_bodies_isolated(env, ind_omega, ind_beta, wave_amplitudes[ind_omega,ind_beta],k_pto_layout,b_pto_layout)
        
            max_powers[ind_beta,:]=n.amax(powers_isol,axis=0)
        
        return max_powers
    #--------------------------------------------------------------------------    
    #
    # Compute significant motion for each body and for a particular seastate
    #--------------------------------------------------------------------------     
    def significant_motion_amps_seastate_calculation(self, env,sim,k_pto_layout,b_pto_layout, wave_amplitudes):
    
        nbodies = len(self.M_layout)
         
        nfreqs  = len(env.frequency) 
        
        nbetas  = len(sim.beta_array)
        
        rao = self.global_RAO_calc(env,sim,k_pto_layout,b_pto_layout) #(beta,freqs,bodies)
        
        signif_motion_bodies = n.zeros(nbodies, dtype=float)
        
        for ind_beta in range(0,nbetas):
            for ind_omega in range(0,nfreqs): 
        
                signif_motion_bodies[:] = 0.5 * ((abs(rao[ind_beta, ind_omega,:]) * wave_amplitudes[ind_omega, ind_beta]) ** 2) + signif_motion_bodies[:]
                
        signif_motion_bodies[:] = 2. * n.sqrt(signif_motion_bodies[:])
        
        return signif_motion_bodies
    #--------------------------------------------------------------------------    
    #
    # Compute retardation function
    #--------------------------------------------------------------------------    
    def compute_retardation_function(self, env):
        
        nfreqs  = len(env.frequency) # last frequency corresponds to infinity
        
        vect_s = 1j * env.frequency[0:nfreqs-1]
        
        A_inf = self.global_am_coeffs[:,:,len(vect_s)-1]
        
        ndofs = self.global_am_coeffs.shape[0]
        
        K_ret = n.zeros((ndofs, ndofs, nfreqs - 1), dtype = complex)
        
        for ind in range(0, len(vect_s)):
            
           K_ret[:,:,ind] = self.global_damp_coeffs[:,:,ind] + vect_s[ind] * (self.global_am_coeffs[:,:,ind] - A_inf)
        
        return K_ret
        
        
        
        