#IMPORT MODULES
#------------------------------------------------------------------------------
# Class definitions
from class_body import BodyUnique
# Python tools
import numpy as n
import math as m
import cmath as cm
import scipy.special as spec
import scipy.linalg as spy
import scipy.sparse.linalg as spla
#------------------------------------------------------------------------------
#
#CLASS DEFINING THE FARM
#------------------------------------------------------------------------------
class Farm:
    
    # Class Initialization
    #--------------------------------------------------------------------------
    def __init__(self, layout, envobj, dict_bodies):
        
        # input list with all the bodies of the farm
        self.layout  = layout 
        
        # define total number of bodies of the farm
        self.farm_nbodies = len(self.layout)

        # generate dictionary of unique bodies of the farm
        self.dict_bodies = dict_bodies
        
        # generate dictionary of farm dofs and calculate their total number
        self._gen_farm_dofs()
        
        # initialize the maximum value of the truncations in farm
        self._id_max_modetrunc(envobj)
        
        # generate list of transformation matrices of body couples in the farm
        self._gen_Farm_Transformation_Matrices_List(envobj)
        
        # generate list of transformation matrices M of body couples in the farm
        #self. _gen_Farm_Transformation_Matrices_M_list(envobj)
        
    #--------------------------------------------------------------------------
    #
    # 
    #--------------------------------------------------------------------------
    def _gen_farm_dofs(self):
        
        #======================================================================                      
        # Purpose : generate the farm dictionary of farm degrees of freedom
        # Inputs : - 
        # Outputs : - dict_dof_farm = from (body_number, body_dof) get 
        #             corresponding farm_dof associated with body_dof
        #           - inv_dict_dof_farm = inverse of dict_dof_farm
        #======================================================================

         # Total number of farm degrees of freedom
         #---------------------------------------------------------------------
         self.dof_farm = 0
         
         # Dictionary of farm degrees of freedom
         #---------------------------------------------------------------------
         self.dict_dof_farm = {}  # Entry is (body_number, body_dof)
                                  # Result is farm_dof associated with body_dof
         
         # Inverse of the dictionary of farm degrees of freedom
         #---------------------------------------------------------------------
         self.inv_dict_dof_farm = {}
         
         # body number is defined as the position in the layout list
         #---------------------------------------------------------------------
         for body_number in range(0, self.farm_nbodies):

            dof_body = self.dict_bodies[(self.layout[body_number].name)].dof_body
            
            for deg in range(1, dof_body + 1):

                self.dict_dof_farm[(body_number, deg)] = self.dof_farm + deg

            self.dof_farm = self.dict_dof_farm[(body_number, dof_body)]
                                            
         # create an inverse-dictionary
         # Entry is (farm_dof), Result is tuple with body number and its individual dof
         #---------------------------------------------------------------------
         self.inv_dict_dof_farm = dict(zip(self.dict_dof_farm.values(),\
                                           self.dict_dof_farm.keys()))
    #--------------------------------------------------------------------------                
    #                     
    # 
    #--------------------------------------------------------------------------                                       
#    def _gen_farm_bodydictionary(self, envobj):
#        
#        #======================================================================                      
#        # Purpose : generate the farm dictionary of unique geometry bodies
#        # Inputs : - envobj = environment class
#        # Outputs : - dict_bodies = dictionary containing the bodies with 
#        #             unique geometry of the farm with all their characteristic
#        #======================================================================
#        
#        self.dict_bodies = {}
#        
#        for body in self.layout:
#            
#            #Check if the body name is in the collection or not
#            #------------------------------------------------------------------
#            try:               
#       
#                self.dict_bodies[body.name]
#                
#            #If the body name is not in the collection, add it 
#            #------------------------------------------------------------------
#            except KeyError:    
#                
#                mybody = BodyUnique(body.name,body.cG,envobj,\
#                                       body.trunc_angmode,body.trunc_depthmode)

#                self.dict_bodies[body.name] = mybody
    #-------------------------------------------------------------------------- 
    #
    # 
    #-------------------------------------------------------------------------- 
    def _id_max_modetrunc(self, envobj):
        
        #======================================================================                      
        # Purpose : Identify maximum truncation for the angular modes of the 
        #           farm bodies at each frequency
        # Inputs : - envobj : environment object 
        # Outputs : - list of max angular modes truncation and depth modes
        #             truncation. Dim: [nomega]
        #======================================================================
        
        # Generate empty lists of angular and depth mode truncations
        #----------------------------------------------------------------------
        self.max_trunc_angmode     = [None] * len(envobj.frequency)
        self.max_trunc_depthmode   = [None] * len(envobj.frequency)
        
#        list_angmode_truncations = list()
#        list_depthmode_truncations = list()
        
        for ind in range(0, len(envobj.frequency)):
            
            list_angmode_truncations = list()
            list_depthmode_truncations = list()
            
            # Iterate through the keys (names) of the farm body dictionary
            #----------------------------------------------------------------------
            for key in self.dict_bodies:
            
                list_angmode_truncations.append(self.dict_bodies[key].\
                                                                 trunc_angmode[ind])
                                                     
                list_depthmode_truncations.append(self.dict_bodies[key].\
                                                               trunc_depthmode[ind])
                                                     
            self.max_trunc_angmode[ind]   =  max(list_angmode_truncations)       
            self.max_trunc_depthmode[ind] =  max(list_depthmode_truncations)                                         
    #--------------------------------------------------------------------------  
    #
    # Generate transformation matrices of all the farm body couples
    #--------------------------------------------------------------------------  
    def _gen_Farm_Transformation_Matrices_List(self, envobj):
        
        #======================================================================                      
        # Purpose : generate Transformation matrices of all the farm body 
        #           couples
        # Inputs : - envobj = environment class
        # Outputs : - list_farm_T_matrices = list containing all transformation
        #             matrices [nbodies_farm][nbodies_farm][dim,dim,nomega]
        #======================================================================
        
        
        # Initialize empty list of transformation matrices
        #----------------------------------------------------------------------
        self.list_farm_T_matrices = list()
        
        for ind in range(0, self.farm_nbodies):
            
            self.list_farm_T_matrices.append([])
        
        for ind_i in range(0, self.farm_nbodies):
            
            for ind_j in range(0, self.farm_nbodies):
                
                self.list_farm_T_matrices[ind_i].append([])
                

        # Generate wave numbers for each frequency
        #----------------------------------------------------------------------
        (k,error) = envobj.get_wave_number(self.max_trunc_depthmode)
        
        # Verify that the calculation of wave numbers is correct
        envobj._check_error_wavenumb(error)
        
        # Store wave numbers as an attribute of farm
        self.k = k
        
        # Generate transformation matrices for each couple of bodies i,j
        #----------------------------------------------------------------------
        for ind_body_i in range(0, self.farm_nbodies):
            
            for ind_body_j in range(0, self.farm_nbodies):
                    
                    self.list_farm_T_matrices[ind_body_i][ind_body_j] = \
                    self._gen_Transformation_Matrix(ind_body_i, ind_body_j, k)
                    
        return self.list_farm_T_matrices
    #--------------------------------------------------------------------------  
    #
    # See APPENDIX B (pg.179)
    #-------------------------------------------------------------------------
    def _gen_Transformation_Matrix(self, body_number_i, body_number_j, k):
        
       #=======================================================================                            
       # Purpose : generate Transformation matrix for a farm body couple i,j
       # Inputs : - body_number_i = index body i
       #          - body_number_j = index body j
       #          - k = array of wave numbers [nomega][trunc_depthmode + 1]
       # Outputs : - T_ij = transformation matrix array of dimensions
       #                    [omega][dim,dim]
       #=======================================================================
        
        # Initialize transformation matrix
        #----------------------------------------------------------------------
        nfreqs = len(k)  # Number of frequencies
        
        T_ij = [None] * nfreqs
       
        # Calculate distance and angle between bodies i and j
        #----------------------------------------------------------------------
        x_i = self.layout[body_number_i].position[0]
        y_i = self.layout[body_number_i].position[1]
        
        x_j = self.layout[body_number_j].position[0]
        y_j = self.layout[body_number_j].position[1]
        
        x = x_j - x_i
        y = y_j - y_i
        
        angle_ij = m.atan2 (y, x)
        
        L_ij = m.sqrt(x**2 + y**2)
        
        # Store circumscribing cylinder radius of bodies i and j
        #----------------------------------------------------------------------
        cyl_rad_i = self.dict_bodies[self.layout[body_number_i].name].cyl_rad
        cyl_rad_j = self.dict_bodies[self.layout[body_number_j].name].cyl_rad
        
        # Calculate elements of the transformation matrix
        #----------------------------------------------------------------------
        for ind_w in range(0, nfreqs):                      # Iterate freqs
        
            # Define dimensions of the transformation matrix for this freq
            Q = self.max_trunc_angmode[ind_w]     # Input angmode truncation
            L = self.max_trunc_depthmode[ind_w]   # Input depthmode truncation
        
            M = Q                          # Output angmode truncation 
            N = L                          # Output depthmode truncation
        
            dim =  2 * Q * (L + 1) + L + 1
            nfreqs = len(k)
        
        
            T_ij[ind_w] = n.zeros((dim,dim),dtype = n.complex)
        
    
            for index_row_m in range(- M, M + 1):           # Iterate  rows
                for index_row_n in range(0, N + 1):
                
                    for index_col_q in range(-Q, Q + 1):    # Iterate  cols
                        for index_col_l in range(0, L + 1):
                            
                            # Generation of global indices using mapping (see page 34)
                            glob_index_row = (M + index_row_m) * (N + 1) + \
                                                                  index_row_n
                            glob_index_col = (Q + index_col_q) * (L + 1) + \
                                                                  index_col_l
                            # Case number of bodies is the same
                            if (body_number_i == body_number_j):
                                
                                T_ij[ind_w][glob_index_row,glob_index_col] = \
                                                               complex(0.0,0.0)
                            # Other cases                             
                            else:                                   
                                                               
                                 # Transformation keeps the same depth-mode
                                 if (index_row_n == index_col_l):
                            
                                    # Bessel function order and argument
                                    order = index_row_m - index_col_q
                                
                                    argument = k[ind_w][index_col_l] * L_ij
                                
                                    # Progressive modes
                                    if (index_row_n == 0):
                                        
                                       #normalizing factor (see page 36)
                                       arg_norm_num = k[ind_w][index_col_l] * cyl_rad_j
                                       arg_norm_denom = k[ind_w][index_col_l] * cyl_rad_i
                                       index_norm_num = index_col_q
                                       index_norm_denom = index_row_m
                                       
                                       norm_f_num = spec.jv(index_norm_num,arg_norm_num)
                                       norm_f_denom = spec.hankel1(index_norm_denom,arg_norm_denom)
                                       
                                       norm_f_prog = norm_f_num / norm_f_denom
  
                                       T_ij[ind_w][glob_index_row,glob_index_col]\
                                        = spec.hankel1(order,argument)\
                                       * cm.exp(1j * angle_ij * (order)) * norm_f_prog
                                    
                                    # Evanescent modes    
                                    else:
                                    
                                       # normalizing factor (see page 36)
                                       arg_norm_num_ev =  k[ind_w][index_col_l] * cyl_rad_j
                                       arg_norm_denom_ev = k[ind_w][index_col_l] * cyl_rad_i
                                       index_norm_num_ev = index_col_q
                                       index_norm_denom_ev = index_row_m
                                       
                                       norm_f_num_ev = spec.iv(index_norm_num_ev, arg_norm_num_ev)
                                       norm_f_denom_ev = spec.kn(index_norm_denom_ev, arg_norm_denom_ev)
                                       
                                       norm_f_ev = norm_f_num_ev / norm_f_denom_ev
                                       
                                       T_ij[ind_w][glob_index_row,glob_index_col]\
                                        = spec.kn(order, argument) * \
                                       ((- 1) ** index_col_q) * \
                                       cm.exp(1j * angle_ij * (order)) * norm_f_ev
                                    
        return T_ij
                                    
    #-------------------------------------------------------------------------- 
    #       
    # 
    #--------------------------------------------------------------------------
    def _solve_coefficients_radiation_interaction_global(self, envobj): 
        
      #========================================================================                             
      # Purpose : Solve for radiated waves coefficients for a given vector of 
      #           farm degrees
      # Inputs : - envobj = environment class with info about the frequencies
      #          - degfreedom_array = array of farm dofs
      # Outputs : - global_solution_vector = vector containing the scattered 
      #             waves coefficients solution of the problem
      #             [nomega][nbodies][nfarm_dofs]                              #modify indexs
      #           - global_incidence_vector = vector containing the incident
      #             partial cylindrical waves coefficients to each body of the 
      #             farm  [nomega][nbodies][nfarm_dofs]                        #modify indexs
      #========================================================================
     
      # Initialize lists containing solution vectors and incident vectors
      #------------------------------------------------------------------------ 
      global_solution_vector = []
      global_incidence_vector = []
      
      # Iterate through the farm degrees of freedom
      #------------------------------------------------------------------------ 
      for ind_w in range(0,len(envobj.frequency)):                                            
         
         print ('Omega' + ' ' + str(envobj.frequency[ind_w]))                         
         
         (computed_coeff, computed_incidence, conditioning) = \
                  self._solve_coefficients_radiation_interaction(envobj,ind_w)     
            
         global_solution_vector.append(computed_coeff)
         global_incidence_vector.append(computed_incidence)
      
      # Assign global lists of interaction and incident coeffs to farm atributes
      #------------------------------------------------------------------------
      self.global_solution_vector_radiation  = global_solution_vector          
      self.global_incidence_vector_radiation = global_incidence_vector        
            
      return (global_solution_vector, global_incidence_vector)
    #--------------------------------------------------------------------------
    #
    # 
    #--------------------------------------------------------------------------                
    def _solve_coefficients_radiation_interaction(self, envobj, ind_w):    
        
      #========================================================================                             
      # Purpose : solve complex system of equations to find the scattered waves 
      #           coefficients resulting from the motion of a given farm degree
      #           of freedom(radiation problem)
      #           STRUCTURE OF THE SYSTEM OF EQUATIONS EXPLAINED IN PAGE 31
      # Inputs : - envobj = environment class with info about the frequencies
      #          - degfreedom = degree of freedom of the farm
      # Outputs : - solution_vector_split = vector containing the scattered 
      #             waves coefficients solution of the problem
      #             [nbodies][nomega]
      #           - incidence_vector_split = vector containing the incident
      #             partial cylindrical waves coefficients to each body of the 
      #             farm  [nbodies][nomega]
      #========================================================================
   
      # Initialize empty list to store transposed transformation matrices
      #------------------------------------------------------------------------ 
      list_farm_T_matrices_transposed = list()
        
      for ind in range(0, self.farm_nbodies):
            
            list_farm_T_matrices_transposed.append([])
        
      for ind_i in range(0, self.farm_nbodies):
            
            for ind_j in range(0, self.farm_nbodies):
                
                list_farm_T_matrices_transposed[ind_i].append([])

                
      # Initialize empty list to store the radiation interaction coefficients 
      #------------------------------------------------------------------------ 
      solution_vector_split = list()
      
      incidence_vector_split = list()
      
      for ind in range(0, self.farm_nbodies):
            
            solution_vector_split.append([])
            incidence_vector_split.append([])
        
      for ind_i in range(0, self.farm_nbodies):
            
            for ind_j in range(0, len(self.productive_dofarray)):           
                
                solution_vector_split[ind_i].append([])
                incidence_vector_split[ind_i].append([])
                

      # TO DO: Make matrices homogeneous in case of different truncation 
      #---------------------------------------------------------------------
          
          
      # Generate diagonal matrix with bodies diffraction transfer matrices   
      #---------------------------------------------------------------------
      list_DTM_mats = []
         
      for body in self.layout:
             
             # Rotate Diffraction Transfer Matrices if necessary
             #-----------------------------------------------------------------
             if (body.rotation_angle == 0.0):
         
                 list_DTM_mats.append(self.dict_bodies[body.name].\
                                     Diffraction_Transfer_Matrix[ind_w][:,:]) #elim 0
                                     
             else:
                 
                 rot_ang = body.rotation_angle 
                 
                 DTM_rotated = self.dict_bodies[body.name].\
                                rotate_operators(rot_ang)[0][ind_w]
                                
                 list_DTM_mats.append(DTM_rotated)
             #-----------------------------------------------------------------                         
      
      if (self.farm_nbodies == 1):
          
          diag_global_DTM_mat = list_DTM_mats[0]
          
      else:
          
          diag_global_DTM_mat = spy.block_diag(list_DTM_mats[0], \
                                                  list_DTM_mats[1])      #modify if no adimensionalization is done
             
          for ind in range(2, self.farm_nbodies):
             
                 diag_global_DTM_mat = spy.block_diag(diag_global_DTM_mat, \
                                                      list_DTM_mats[ind])#modify if no adimansionalization 
      #---------------------------------------------------------------------
         
      # Generate global transposed transformation matrix of the system
      #---------------------------------------------------------------------  
      for ind_i in range(0, self.farm_nbodies):
             for ind_j in range(0, self.farm_nbodies):
                     
                    # change indexs and transpose T blocks 
                    list_farm_T_matrices_transposed[ind_j][ind_i] = \
                    self.list_farm_T_matrices[ind_i][ind_j][ind_w][:,:].\
                                                                    transpose()
         
      # Transform list of lists of arrays to a single array
      list_mats = []
      row_mat_list = [None] * self.farm_nbodies
         
      for ind_body_i in range(0, self.farm_nbodies):
         
             for ind_body_j in range(0, self.farm_nbodies):
             
                 list_mats.append(list_farm_T_matrices_transposed[ind_body_i]\
                                                                 [ind_body_j])
                 
             row_mat_list[ind_body_i]=(n.hstack(list_mats)) 
             list_mats = []
             
      global_sys_T_mat = n.vstack(row_mat_list[:])
      #---------------------------------------------------------------------
         
      # Generate Identity Matrix of the required size
      #---------------------------------------------------------------------
      Id = n.identity(global_sys_T_mat.shape[0], complex)
      #---------------------------------------------------------------------
      
      for ind_dof in range(0,len(self.productive_dofarray)):                    
                                                                  
         degfreedom = self.productive_dofarray[ind_dof]                         
         
         print ('Dof' + ' ' + str(degfreedom))
         
         # Generate the "forcing" vector given by radiated waves from body i
         #---------------------------------------------------------------------
         # Characteristics of body i (from input degree of freedom of the farm)
         body_numb = self.inv_dict_dof_farm[(degfreedom)][0]
         dof_body  = self.inv_dict_dof_farm[(degfreedom)][1]
         
         body_name = self.layout[body_numb].name
         
         M = self.max_trunc_angmode[ind_w]
         N = self.max_trunc_depthmode[ind_w]
         
         # Dimensions of the radiation characteristics vector for one body
         dim_vect = 2 * M * (N + 1) + N + 1
         
         zeros_vect = n.zeros((dim_vect))
         
         # ind_body refers to body j (static) and body_numb to body i (moving)
         rad_forcing_vect = [None] * self.farm_nbodies
         
         # Iterate through the bodies of the farm
         for ind_body in range(0,self.farm_nbodies):
             
             if (ind_body == body_numb):
                 
                 rad_forcing_vect[ind_body] = zeros_vect
                 
             else:
                 
                 T_ij_transp = self.list_farm_T_matrices[body_numb][ind_body]\
                                                       [ind_w][:,:].transpose()
                 
                 # Rotate radiation vector if necessary
                 #-------------------------------------------------------------
                 if (self.layout[body_numb].rotation_angle == 0.0):
                     
                     Rad_vect = self.dict_bodies[(body_name)].\
                               Radiation_Characteristics[ind_w][:,dof_body-1] #elim 0
                 else:
                     
                     rot_ang = self.layout[body_numb].rotation_angle

                     Rotated_Radvect = self.dict_bodies[body_name].\
                                          rotate_operators(rot_ang)[1][ind_w]
                                          
                     Rad_vect = Rotated_Radvect[:,dof_body-1]
                 #-------------------------------------------------------------
                     
                 rad_forcing_vect[ind_body] = n.dot(T_ij_transp, Rad_vect)
                 
         global_sys_rad_forcing_vect = n.hstack(rad_forcing_vect)
         #---------------------------------------------------------------------
         
         # Generate Coefficient matrix of the system(A of AX = B)
         #---------------------------------------------------------------------
         prod_DTM_T_transp = n.dot(diag_global_DTM_mat, global_sys_T_mat)
         
         LHS_SYS_MAT = n.subtract(prod_DTM_T_transp, Id)
         #---------------------------------------------------------------------
         
         # Generate ordinate values of the system(B of AX = B)
         #---------------------------------------------------------------------
         RHS_SYS_MAT = (- 1) * n.dot(diag_global_DTM_mat, \
                                     global_sys_rad_forcing_vect)
         #---------------------------------------------------------------------
         
         # Calculate Conditioning Number of Matrix RHS_SYS_MAT
         #---------------------------------------------------------------------
         singular_values = n.linalg.svd(LHS_SYS_MAT,compute_uv=0)
         
         sing_max = n.max(singular_values)
         
         sing_min = n.min(singular_values)
         
         #conditioning = sing_max / sing_min
         conditioning = n.linalg.cond(LHS_SYS_MAT,p=1)
         #---------------------------------------------------------------------
         
         # Solve linear complex system LHS_SYS_MAT * X = RHS_SYS_MAT
         # STRUCTURE OF THE SYSTEM OF EQUATIONS EXPLAINED IN PAGE 31
         #---------------------------------------------------------------------
         solution_vector = n.linalg.solve(LHS_SYS_MAT,RHS_SYS_MAT)
         #spyspa.linalg.gmres()
         check = n.allclose(n.dot(LHS_SYS_MAT,solution_vector),RHS_SYS_MAT)
         
         print (check)
         #---------------------------------------------------------------------  
         
         # If the solution is correct, slice solution_vector and store it
         #--------------------------------------------------------------------- 
         #
         if (check == True):
             
             split_coeffs = n.split(solution_vector, self.farm_nbodies)
             
             for ind_body in range(0, self.farm_nbodies):
                 
                 solution_vector_split[ind_body][ind_dof] = \
                                                         split_coeffs[ind_body]   
                 incidence_vector_split[ind_body][ind_dof] = \
                                                     rad_forcing_vect[ind_body]   
         else:
             
             print('error in solving radiation interaction coefficients')
         
      return (solution_vector_split, incidence_vector_split, conditioning)
    #-------------------------------------------------------------------------- 
    #
    # 
    #--------------------------------------------------------------------------
    def _solve_coefficients_diffraction_interaction_global(self, envobj, beta_array):      
        
      #========================================================================                             
      # Purpose : Solve for scattered waves coefficients for a given vector of 
      #           beta propagating directions of plane incident waves
      # Inputs : - envobj = environment class with info about the frequencies
      #          - beta_array = array of wave directions (in radians)
      # Outputs : - global_solution_vector = vector containing the scattered   #modify indexs
      #             waves coefficients solution of the problem
      #             [nomega][nbodies][nbeta]
      #           - global_incidence_vector = vector containing the incident
      #             partial cylindrical waves coefficients to each body of the 
      #             farm  [nomega][nbodies][nbeta]                             #modify indexs
      #========================================================================
    
      # Initialize lists containing solution vectors and incident vectors
      #------------------------------------------------------------------------ 
      global_solution_vector = []
      global_incidence_vector = []
      
      # Iterate through wave directions
      #------------------------------------------------------------------------ 
      for ind_w in range(0, len(envobj.frequency)):                                

          print ('Omega' + ' ' + str(envobj.frequency[ind_w]))                                  
           
          (computed_coeff, computed_incidence) = \
                  self._solve_coefficients_diffraction_interaction(envobj,ind_w,beta_array) 
                                                                               
            
          global_solution_vector.append(computed_coeff)
          global_incidence_vector.append(computed_incidence)  
      
      # Assign global lists of interaction and incident coeffs to farm atributes
      #------------------------------------------------------------------------
      self.global_solution_vector_diffraction  = global_solution_vector
      self.global_incidence_vector_diffraction = global_incidence_vector
      
      return (global_solution_vector, global_incidence_vector)
    #--------------------------------------------------------------------------    
    #
    # 
    #--------------------------------------------------------------------------                
    def _solve_coefficients_diffraction_interaction(self, envobj, ind_w, beta_array):       
        
      #========================================================================                             
      # Purpose : solve complex system of equations to find the scattered waves 
      #           coefficients for a given single plane wave incidence in beta
      #           direction (diffraction problem)
      #           STRUCTURE OF THE SYSTEM OF EQUATIONS EXPLAINED IN PAGE 31
      # Inputs : - envobj = environment class with info about the frequencies
      #          - beta = direction (in radians)
      # Outputs : - solution_vector_split = vector containing the scattered 
      #             waves coefficients solution of the problem
      #             [nbodies][nomega]
      #           - incidence_vector_split = vector containing the incident
      #             partial cylindrical waves coefficients to each body of the 
      #             farm  [nbodies][nomega]
      #========================================================================
       
      # Initialize empty list to store transposed transformation matrices
      #------------------------------------------------------------------------ 
      list_farm_T_matrices_transposed = list()
        
      for ind in range(0, self.farm_nbodies):
            
            list_farm_T_matrices_transposed.append([])
        
      for ind_i in range(0, self.farm_nbodies):
            
            for ind_j in range(0, self.farm_nbodies):
                
                list_farm_T_matrices_transposed[ind_i].append([])
                
                
      # Initialize empty list to store the diffraction interaction coefficients 
      # and incident vectors
      #------------------------------------------------------------------------ 
      solution_vector_split = list()
      
      incidence_vector_split = list()
      
      for ind in range(0, self.farm_nbodies):
            
            solution_vector_split.append([])
            incidence_vector_split.append([])
        
      for ind_i in range(0, self.farm_nbodies):
            
            for ind_j in range(0, len(beta_array)):                        
                
                solution_vector_split[ind_i].append([])
                incidence_vector_split[ind_i].append([])
                          
         
      # TO DO : Make matrices homogeneous in case of different truncation 
      #---------------------------------------------------------------------
          
          
      # Generate diagonal matrix with bodies diffraction transfer matrices   
      #---------------------------------------------------------------------
      list_DTM_mats = [] 
         
      for body in self.layout:
             
             # Rotate Diffraction Transfer Matrices if necessary
             #-----------------------------------------------------------------
             if (body.rotation_angle == 0.0):
         
                 list_DTM_mats.append(self.dict_bodies[body.name].\
                                     Diffraction_Transfer_Matrix[ind_w][:,:])  #elim 0
                                  
             else:
                 
                 rot_ang = body.rotation_angle
                 
                 DTM_rotated = self.dict_bodies[body.name].\
                               rotate_operators(rot_ang)[0][ind_w]
                               
                 list_DTM_mats.append(DTM_rotated)
                 
             #-----------------------------------------------------------------
      
      if (self.farm_nbodies == 1):
          
          diag_global_DTM_mat = list_DTM_mats[0]
          
      else:
          
          diag_global_DTM_mat = spy.block_diag(list_DTM_mats[0], \
                                                  list_DTM_mats[1])
                                                                           #remove if adimensionalization included
          for ind in range(2, self.farm_nbodies):
             
                 diag_global_DTM_mat = spy.block_diag(diag_global_DTM_mat, \
                                                     list_DTM_mats[ind]) #remove if adimensionalization included
      #---------------------------------------------------------------------
                                                        
         
      # Generate global transposed transformation matrix of the system
      #---------------------------------------------------------------------
      for ind_i in range(0, self.farm_nbodies):
             for ind_j in range(0, self.farm_nbodies):
                     
                    # change indexs and transpose T blocks 
                    list_farm_T_matrices_transposed[ind_j][ind_i] = \
                    self.list_farm_T_matrices[ind_i][ind_j][ind_w][:,:].\
                                                                    transpose()
         
      # Transform list of lists of arrays to a single array
      list_mats = []
      row_mat_list = [None] * self.farm_nbodies
         
      for ind_body_i in range(0, self.farm_nbodies):
         
             for ind_body_j in range(0, self.farm_nbodies):
             
                 list_mats.append(list_farm_T_matrices_transposed[ind_body_i]\
                                                                 [ind_body_j])
                 
             row_mat_list[ind_body_i]=(n.hstack(list_mats)) 
             list_mats = []
             
      global_sys_T_mat = n.vstack(row_mat_list[:])
      #---------------------------------------------------------------------
         
      # Generate Identity Matrix of the required size
      #---------------------------------------------------------------------
      Id = n.identity(global_sys_T_mat.shape[0], complex)
      #---------------------------------------------------------------------
      
      # Truncation values
      #---------------------------------------------------------------------
      M = self.max_trunc_angmode[ind_w]
      N = self.max_trunc_depthmode[ind_w]
      
      # Generate Coefficient matrix of the system(A of AX = B)
      #---------------------------------------------------------------------
      prod_DTM_T_transp = n.dot(diag_global_DTM_mat, global_sys_T_mat)
         
      LHS_SYS_MAT = n.subtract(prod_DTM_T_transp, Id)
      #---------------------------------------------------------------------
       
      for ind_beta in range(0,len(beta_array)):                                   
         
         beta = beta_array[ind_beta]                                                  
         
         print ('Beta'+' '+ str(beta))
         
         # Generate the "forcing" vector given by plane incident waves to bodyj
         #---------------------------------------------------------------------       

         # ind_body refers to body j (body to which the wave is travelling)
         diffraction_forcing_vect = [None] * self.farm_nbodies
         
         for ind_body in range(0,self.farm_nbodies):
             
             omega = envobj.frequency[ind_w]
             
             k0omega = envobj.k0[ind_w]
             
             diffraction_forcing_vect[ind_body] = self.gen_cyl_coefficients\
                                             (beta,omega,k0omega,M,N,ind_body)
                 
         global_sys_diffraction_forcing_vect =\
                                             n.hstack(diffraction_forcing_vect)
         #---------------------------------------------------------------------

         # Generate ordinate values of the system(B of AX = B)
         #---------------------------------------------------------------------
         RHS_SYS_MAT = (- 1) * n.dot(diag_global_DTM_mat, \
                                           global_sys_diffraction_forcing_vect)
         #---------------------------------------------------------------------
                 
         # Solve linear complex system LHS_SYS_MAT * X = RHS_SYS_MAT
         # STRUCTURE OF THE SYSTEM OF EQUATIONS EXPLAINED IN PAGE 31
         #---------------------------------------------------------------------
         solution_vector = n.linalg.solve(LHS_SYS_MAT,RHS_SYS_MAT)
         #solution_vector = spla.gmres(LHS_SYS_MAT,RHS_SYS_MAT)[0]
         check = n.allclose(n.dot(LHS_SYS_MAT,solution_vector),RHS_SYS_MAT)
         
         print (check)
         #---------------------------------------------------------------------  
         
         # If the solution is correct, slice solution_vector and store it
         #--------------------------------------------------------------------- 
         #
         if (check == True):
             
             split_coeffs = n.split(solution_vector, self.farm_nbodies)
             
             for ind_body in range(0, self.farm_nbodies):
                 
                 solution_vector_split[ind_body][ind_beta] = \
                                                         split_coeffs[ind_body]     
                 
                 incidence_vector_split[ind_body][ind_beta] = \
                                             diffraction_forcing_vect[ind_body]     
             
         else:
             
             print('error in solving diffraction interaction coefficients')
         
      return (solution_vector_split,incidence_vector_split)
    #-------------------------------------------------------------------------- 
    #
    # 
    #--------------------------------------------------------------------------
    def gen_cyl_coefficients(self,beta,omega,k0omega,trunc_angmode,\
                                                     trunc_depthmode,ind_body):
        
        #======================================================================                             
        # Purpose : generate the cylindrical coefficients of a plane wave with
        #           direction beta
        #           FORMULATION GIVEN IN PAGE 27
        # Inputs : - beta = direction (in radians)
        #          - omega = frequency (in rad/s)
        #          - k0omega = progressive wave number of omega
        #          - trunc_angmode = angular mode truncation
        #          - trunc_depthmode = depth mode truncation
        #          - ind_body = refers to the index of the body to which the 
        #                       wave is incident
        # Outputs : - cyl_coeffs = vector of cylindrical coefficients
        #                          [dim_vect]
        #======================================================================
        
        # Generate vector dimensions and initialize vector
        #----------------------------------------------------------------------
        M = trunc_angmode
        N = trunc_depthmode
          
        dim_vect = 2 * M * (N + 1) + N + 1
          
        cyl_coeffs = n.zeros((dim_vect),complex)
          
        # Get global coordinates of body ind_body
        #----------------------------------------------------------------------  
        Xj = self.layout[ind_body].position[0]
        Yj = self.layout[ind_body].position[1]
        
        # Get circumscribing cylinder radius of body ind_body
        #----------------------------------------------------------------------  
        cyl_rad_j = self.dict_bodies[self.layout[ind_body].name].cyl_rad
          
        # Calculate cylindrical partial waves coefficients
        #----------------------------------------------------------------------  
        for ind_m in range(-M, M + 1):
              
            for ind_n in range(0, N + 1):
                  
                global_ind = (M + ind_m) * (N + 1) +  ind_n
                  
                # Progressive modes
                if (ind_n == 0):
                    
                    #normalizing factor (SEE PAGE 36)
                    index_f = ind_m 
                    arg_f = k0omega * cyl_rad_j
                    norm_f = spec.jv(index_f,arg_f)
                    #-----------------------------------
                      
                    phase_disp = cm.exp(1j * k0omega * (Xj * m.cos(beta) + \
                                                        Yj * m.sin(beta)))
                      
                    expression = - (1j * 9.81 / omega) * phase_disp * \
                                   (1j ** ind_m) * cm.exp(- 1j * ind_m * beta)
                     
                      
                    cyl_coeffs[global_ind] = expression * norm_f
                   
                # Evanescent modes 
                else:
                      
                    cyl_coeffs[global_ind] = complex(0.0,0.0)
                      
        return cyl_coeffs
    #--------------------------------------------------------------------------  
    #
    # 
    #--------------------------------------------------------------------------
    def gen_excitation_forces_farm(self, envobj, beta_array):                   
        
        #======================================================================                             
        # Purpose : compute excitation forces for all farm bodies
        #           for all specified incident waves 
        # Inputs : - envobj = environment class with info about frequencies
        #          - beta_array = array with wave directions [nbeta]
        # Outputs : - Excitation_forces_farm = array with excitation forces for
        #              all farm bodies and conditions
        #              [nbodies_farm][ndof_body,nbeta,nomega]
        #======================================================================
                                                 
        
        # Print start of diffraction interaction problem
        #----------------------------------------------------------------------
        print ('Diffraction interaction problem solver START')
        
        print ('==========================================')
        
        # Solve farm diffraction interaction problem
        #----------------------------------------------------------------------
        (global_solution_vector, global_incidence_vector) = \
        self._solve_coefficients_diffraction_interaction_global(envobj, beta_array)        
                                                                    
        # Print end of diffraction interaction problem
        #----------------------------------------------------------------------
        print ('Diffraction interaction problem solver END')
        
        print ('==========================================')
        
        # Print start of the excitation forces calculation
        #----------------------------------------------------------------------
        print ('Excitation forces calculation START')
        
        print ('======================================================')
        
        # Initialize list of excitation force matrices
        #----------------------------------------------------------------------
        self.Excitation_forces_farm = [None] * self.farm_nbodies
        
        # Iterate through bodies of the farm
        #----------------------------------------------------------------------
        for ind_body_j in range(0,self.farm_nbodies):
             
            # Print body index 
            print ('Body' + ' ' + str(ind_body_j))
            
            # Initialize Excitation Force Matrix for a body j
            #----------------------------------------------------------------------
            ndof_body = self.dict_bodies[(self.layout[ind_body_j].name)].dof_body
            nbeta     = len(beta_array)                                              
            nw        = len(envobj.frequency)
        
            exc_forces_j = n.zeros((ndof_body, nbeta, nw),complex)
        
            # Iterate trough directions and frequencies
            #----------------------------------------------------------------------
            for beta_ind in range(0, nbeta):
            
                for omega_ind in range(0, nw):
                
                    # Calculate excitation force for given conditions and bodyj
                    exc_forces_j[:,beta_ind,omega_ind] = \
                    self._compute_forces_diffraction_body(ind_body_j,\
                    global_solution_vector, global_incidence_vector, \
                                                   beta_ind, omega_ind)
            
            # Store excitation forces for each body_j
            self.Excitation_forces_farm[ind_body_j] = exc_forces_j
            
        # Print END of the excitation forces calculation
        #----------------------------------------------------------------------
        print ('Excitation forces calculation END')
        
        print ('======================================================')
            
        return self.Excitation_forces_farm
        
    #--------------------------------------------------------------------------
    #
    # 
    #--------------------------------------------------------------------------
    def _compute_forces_diffraction_body(self,body_j, global_solution_vector,\
                                 global_incidence_vector, beta_ind, omega_ind):
        
        #======================================================================                             
        # Purpose : compute vector of excitation forces for body j of the farm
        #           for an incident wave of w travelling with beta direction
        #           FORMULATION GIVEN IN PAGE 33
        # Inputs : - body_numb = bodyj of which the forces want to be calculated
        #          - global_solution_vector : vector containing the scattered
        #            coefficients of farm diffraction problem for all farm bodies 
        #            [nomega][nbodies_farm][nbeta][truncation_dimension]          
        #          - global_incidence_vector : vector containing the incident
        #            cylindrical waves coefficients of plane wave for all bodies
        #            [nomega]x[nbodies_farm][nbeta][truncation_dimension]           
        #          - beta_ind : index of the wave direction 
        #          - omega_ind : index of the frequency
        # Outputs : - Force_exc_j : vector of excitation forces to body j
        #             [ndofs]
        #======================================================================
        
        # Get quantities of interest of body j
        #----------------------------------------------------------------------
        name_body = self.layout[body_j].name
        
        # Rotate Force Transfer Matrix if necessary
        #----------------------------------------------------------------------
        rot_ang = self.layout[body_j].rotation_angle 
        
        if (rot_ang == 0.0):
            
            force_transfer_matrix_j = self.dict_bodies[(name_body)].\
                                        Force_Transfer_Matrix[omega_ind][:,:]  #elim 0
                                        
        else:
            
            Rotated_FTM = self.dict_bodies[(name_body)].\
                                        rotate_operators(rot_ang)[2][omega_ind]
                          
            force_transfer_matrix_j = Rotated_FTM
        #----------------------------------------------------------------------
            
        ambient_incidence_body_j = global_incidence_vector[omega_ind][body_j]\
                                                                 [beta_ind][:]  
        
        # Produce list of bodies different than body_j
        #----------------------------------------------------------------------
        indexs_all_bodies = list(range(0, self.farm_nbodies))
        
        indexs_all_bodies.pop(body_j)
        
        # Calculate incidence waves to body_j from scattering of bodies 
        # different than body_j
        #----------------------------------------------------------------------
        dim = global_incidence_vector[omega_ind][body_j][beta_ind].shape        
        
        sum_vect = n.zeros((dim), complex)
        
        for ind_it in indexs_all_bodies:
            
            mat_T_ij_transpose = self.list_farm_T_matrices[ind_it][body_j]\
                                                    [omega_ind][:,:].transpose()
            
            coeff_scattered  = global_solution_vector[omega_ind][ind_it]\
                                                                 [beta_ind][:]   
            
            sum_vect = sum_vect + n.dot(mat_T_ij_transpose, coeff_scattered)
        
        # Calculate total vector of incident waves to body_j
        #----------------------------------------------------------------------
        total_incident_waves = ambient_incidence_body_j + sum_vect
        
        # Calculate force on body_j using definition of force transfer matrix
        #----------------------------------------------------------------------
        Force_exc_j = n.dot(force_transfer_matrix_j, total_incident_waves) #modifiy when no adimensionalization
            
        return Force_exc_j    
    #--------------------------------------------------------------------------
    #
    # 
    #--------------------------------------------------------------------------
    def gen_hydrodynamic_coefficients_farm(self, envobj, degfreedom_array):
        
        #======================================================================                             
        # Purpose : compute hydrodynamic coefficients (added mass and damping)
        #           for the specified degrees of freedom of the farm
        #           FORMULATION GIVEN IN PAGE 34
        # Inputs : - envobj = environmental class with info on the frequencies
        #          - degfreedom_array = array of farm dofs to solve
        # Outputs : - Added_Mass_farm = list of AM coefficients for each
        #             body of the farm and specified farm degrees of freedom
        #             [nbodies][ndof_body,ndof_farm,nomega]
        #======================================================================
        
        # Save dof_array as a farm attribute                                      
        self.productive_dofarray = degfreedom_array
        
        # Print start of radiation interaction problem
        #----------------------------------------------------------------------
        print ('Radiation interaction problem solver START')
        
        print ('==========================================')
        
        # Solve farm radiation interaction problem
        #----------------------------------------------------------------------
        (global_solution_vector, global_incidence_vector) = \
        self._solve_coefficients_radiation_interaction_global(envobj) 
        
        # Print end of radiation interaction problem
        #----------------------------------------------------------------------                                                      
        print ('Radiation interaction problem solver END')
        
        print ('==========================================')
                                                              
        # Print start of the radiation hydrodynamic coefficients calculation
        #----------------------------------------------------------------------
        print ('Radiation hydrodynamic coefficients calculations START')
        
        print ('======================================================')
        
        # Initialize lists of hydrodynamic coefficients
        #----------------------------------------------------------------------
        self.Added_Mass_farm = [None] * self.farm_nbodies
        
        self.Damping_farm    = [None] * self.farm_nbodies
        
        # Iterate through bodies of the array
        #----------------------------------------------------------------------
        for ind_body_j in range(0,self.farm_nbodies):
            
            print ('Body' + ' ' + str(ind_body_j))
            
            # Initialize Radiation Force & Hydro Coeffs Matrices for a body_j
            #------------------------------------------------------------------
            ndof_body  = self.dict_bodies[(self.layout[ind_body_j].name)].dof_body
            ndofs_farm = len(degfreedom_array)
            nw         = len(envobj.frequency)
        
            rad_forces_j = n.zeros((ndof_body, ndofs_farm, nw),complex)        
            
            added_mass_j = n.zeros((ndof_body, ndofs_farm, nw))                
            
            damping_j    = n.zeros((ndof_body, ndofs_farm, nw))                
        
            # Iterate through specified degrees of freedom of the farm
            #------------------------------------------------------------------
            for dof_farm_ind in range(0, ndofs_farm):                          
                
                for omega_ind in range(0, nw):
                    
                    omega = envobj.frequency[omega_ind]
                  
                    # Compute radiation forces on body_j
                    rad_forces_j[:,dof_farm_ind,omega_ind] = \
                    self._compute_forces_radiation_body(envobj,ind_body_j,\
                    global_solution_vector, global_incidence_vector, \
                                                       dof_farm_ind, omega_ind)
                    
                    # Definition of added mass Coefficient                               
                    added_mass_j[:,dof_farm_ind,omega_ind] = \
                    rad_forces_j[:,dof_farm_ind,omega_ind].imag / (omega)
                    
                    # Definition of damping Coefficient             
                    damping_j[:,dof_farm_ind,omega_ind] = \
                        -rad_forces_j[:,dof_farm_ind,omega_ind].real
            
            # Store Hydrodynamic Coefficients matrices for each farm body
            #------------------------------------------------------------------
            self.Added_Mass_farm[ind_body_j] = added_mass_j
            
            self.Damping_farm[ind_body_j]    = damping_j
            
        # Print END of the radiation hydrodynamic coefficients calculation
        #----------------------------------------------------------------------
        print ('Radiation hydrodynamic coefficients calculations END')
        
        print ('======================================================')
            
        return (self.Added_Mass_farm, self.Damping_farm)
        
    #--------------------------------------------------------------------------
        
    #--------------------------------------------------------------------------
    def _compute_forces_radiation_body(self,envobj,ind_body_j,\
     global_solution_vector, global_incidence_vector, dof_farm_ind, omega_ind):
                                 
        #======================================================================                             
        # Purpose : compute radiation forces on body_j due to the motion of a
        #           body_i in the farm and for a specific frequency
        #           FORMULATION GIVEN IN PAGE 33
        # Inputs : - envobj = environmental class with info on the frequencies
        #          - ind_body_j = index of the body which to calculate the F on
        #          - global_solution_vector =vector containing the scattered
        #            coefficients of farm radiation problem for all farm bodies 
        #            [nomega][nbodies_farm][ndofs_farm][truncation_dimension]      
        #          - global_incidence_vector : vector containing the incident
        #            waves coefficients originating from the motion of a body 
        #            in the farm for all farm bodies
        #            [nomega]x[nbodies_farm][ndofs_farm][truncation_dimension]                        
        #          - dof_farm_ind = index specifying the farm degree of freedom
        #          - omega_ind    = index specifying the frequency
        # Outputs : - Force_rad_j = vector of radiation forces to body j
        #             [ndofs]
        #======================================================================
        
        # Define moving body from global degree of freedom    
        #----------------------------------------------------------------------                     
        ind_body_i = self.inv_dict_dof_farm[(self.productive_dofarray[dof_farm_ind])][0]       
        
        # Get characteristics of body j
        #----------------------------------------------------------------------  
        name_body_j = self.layout[ind_body_j].name
        
                                          
        # Rotate Force Transfer Matrix of body j if necessary
        #----------------------------------------------------------------------
        rot_ang = self.layout[ind_body_j].rotation_angle 
        
        if (rot_ang == 0.0):
            
            force_transfer_matrix_j = self.dict_bodies[(name_body_j)].\
                                        Force_Transfer_Matrix[omega_ind][:,:]  #elim 0
                                        
        else:
            
            Rotated_FTM = self.dict_bodies[(name_body_j)].\
                                        rotate_operators(rot_ang)[2][omega_ind]
                          
            force_transfer_matrix_j = Rotated_FTM
        #----------------------------------------------------------------------
        
        # Define degree of freedom of the moving body
        #----------------------------------------------------------------------                    
        k_dof_body_i = self.inv_dict_dof_farm[(self.productive_dofarray[dof_farm_ind])][1]  

        # Calculate Vector of incident waves to body j
        #---------------------------------------------------------------------- 
        # List bodies different than j        
        indexs_all_bodies = list(range(0, self.farm_nbodies))
        
        indexs_all_bodies.pop(ind_body_j)
        
        # Initialize vector of incident waves
        dim = global_incidence_vector[omega_ind][ind_body_j][dof_farm_ind].shape 
        
        sum_vect = n.zeros((dim), complex)
        
        # Iterate through bodies diff than j (incident waves due to scattering)
        for ind_it in indexs_all_bodies:
            
            mat_T_ij_transpose = self.list_farm_T_matrices[ind_it][ind_body_j]\
                                                    [omega_ind][:,:].transpose()
            
            coeff_scattered = global_solution_vector[omega_ind][ind_it]\
                                                                 [dof_farm_ind][:]   
            
            sum_vect = sum_vect + n.dot(mat_T_ij_transpose, coeff_scattered)                 
        
        # Get incident waves due to radiation
        ambient_vect = global_incidence_vector[omega_ind][ind_body_j]\
                                                                 [dof_farm_ind][:]  
        
        # Add contributions of the scattered waves and radiated to the incident
        total_ambient_vect = sum_vect + ambient_vect
        
        
        # Calculate Forces acting on body j
        #----------------------------------------------------------------------
        # Two contributions to the force: radiation from body j and scattering
        # received from the other bodies i
        if (ind_body_i == ind_body_j):
            
            omega = envobj.frequency[omega_ind]
            
            added_mass = self.dict_bodies[name_body_j].Added_Mass_Coefficients\
                                                                [omega_ind][:,:,0]
            damping    = self.dict_bodies[name_body_j].Damping_Coefficients\
                                                                [omega_ind][:,:,0]
            
            column_addedmass =  added_mass[:, k_dof_body_i - 1]
            
            column_damping   =  damping[:, k_dof_body_i - 1]
            
            force_comp_other_bodies = n.dot(force_transfer_matrix_j, \
                                                            total_ambient_vect) #modify when no adimensionalizaiton
            
            Force_rad_j =  (1j * omega) * column_addedmass - \
                                      column_damping + force_comp_other_bodies
                            
        # Only one contribution to the force: scattering from other bodies                   
        else:
            
            Force_rad_j = n.dot(force_transfer_matrix_j, total_ambient_vect) #modify when no adimensionalization
            
        return Force_rad_j
          
    #--------------------------------------------------------------------------
    #
    # Generation of transformation matrices M
    #--------------------------------------------------------------------------  
    def _gen_Farm_Transformation_Matrices_M_list(self, envobj):
        
        #======================================================================                      
        # Purpose : generate Transformation matrices [M] (different than T!!)
        #           of all the farm bodies
        # Inputs : - envobj = environment class
        # Outputs : - list_farm_M_matrices = list containing all transformation
        #             matrices [nbodies_farm][nbodies_farm][dim,dim,nomega]
        #======================================================================
        
        
        # Initialize empty list of transformation matrices
        #----------------------------------------------------------------------
        self.list_farm_M_matrices = list()
        
        for ind in range(0, self.farm_nbodies):
            
            self.list_farm_M_matrices.append([])
        
        for ind_i in range(0, self.farm_nbodies):
            
            for ind_j in range(0, self.farm_nbodies):
                
                self.list_farm_M_matrices[ind_i].append([])
                
        # Generate wave numbers for each frequency
        #----------------------------------------------------------------------
        (k,error) = envobj.get_wave_number(self.max_trunc_depthmode)
        
        # Verify that the calculation of wave numbers is correct
        envobj._check_error_wavenumb(error)
        
        # Store wave numbers as an attribute of farm
        self.k = k
        
        # Generate transformation matrices for each couple of bodies i,j
        #----------------------------------------------------------------------
        for ind_body_i in range(0, self.farm_nbodies):
            
            for ind_body_j in range(0, self.farm_nbodies):
                    
                    self.list_farm_M_matrices[ind_body_i][ind_body_j] = \
                    self._gen_Transformation_Matrix_M(ind_body_i, ind_body_j, k)
                    
        return self.list_farm_M_matrices
    #--------------------------------------------------------------------------  
    #
    # 
    #-------------------------------------------------------------------------
    def _gen_Transformation_Matrix_M(self, body_number_i, body_number_j, k):
        
       #=======================================================================                            
       # Purpose : generate Transformation matrix M for a farm body couple i,j
       #           FORMULATION GIVEN IN PAGE 78
       # Inputs : - body_number_i = index body i
       #          - body_number_j = index body j
       #          - k = array of wave numbers [nomega][trunc_depthmode + 1]
       # Outputs : - M_ij = transformation matrix array of dimensions
       #                    [omega][dim,dim]
       #=======================================================================
        
        # Initialize transformation matrix
        #----------------------------------------------------------------------
        nfreqs = len(k)  # Number of frequencies
        
        M_ij = [None] * nfreqs
       
        # Calculate distance and angle between bodies i and j
        #----------------------------------------------------------------------
        x_i = self.layout[body_number_i].position[0]
        y_i = self.layout[body_number_i].position[1]
        
        x_j = self.layout[body_number_j].position[0]
        y_j = self.layout[body_number_j].position[1]
        
        x = x_j - x_i
        y = y_j - y_i
        
        angle_ij = m.atan2 (y, x)
        
        L_ij = m.sqrt(x**2 + y**2)
        
        # Calculate elements of the transformation matrix
        #----------------------------------------------------------------------
        for ind_w in range(0, nfreqs):                      # Iterate freqs
        
            # Define dimensions of the transformation matrix for this freq
            Q = self.max_trunc_angmode[ind_w]     # Input angmode truncation
            L = self.max_trunc_depthmode[ind_w]   # Input depthmode truncation
        
            M = Q                          # Output angmode truncation 
            N = L                          # Output depthmode truncation
        
            dim =  2 * Q * (L + 1) + L + 1
            nfreqs = len(k)
        
        
            M_ij[ind_w] = n.zeros((dim,dim),dtype = n.complex)
        
    
            for index_row_m in range(- M, M + 1):           # Iterate  rows
                for index_row_n in range(0, N + 1):
                
                    for index_col_q in range(-Q, Q + 1):    # Iterate  cols
                        for index_col_l in range(0, L + 1):
                            
                            # Generation of global indices using mapping
                            glob_index_row = (M + index_row_m) * (N + 1) + \
                                                                  index_row_n
                            glob_index_col = (Q + index_col_q) * (L + 1) + \
                                                                  index_col_l
                            # Case number of bodies is the same
                            if (body_number_i == body_number_j):
                                
                                # Transformation keeps the same depth-mode
                                if (glob_index_row == glob_index_col):
                                
                                    M_ij[ind_w][glob_index_row,glob_index_col] = \
                                                               complex(1.0,0.0)
                                                               
                                else:
                                    
                                    M_ij[ind_w][glob_index_row,glob_index_col] = \
                                                               complex(0.0,0.0)
                                    
                            # Other cases                             
                            else:                                   
                                                               
                                 # Transformation keeps the same depth-mode
                                 if (index_row_n == index_col_l):
                            
                                    # Bessel function order and argument
                                    order = index_row_m - index_col_q
                                
                                    argument = k[ind_w][index_col_l] * L_ij
                                
                                    # Progressive modes
                                    if (index_row_n == 0):
  
                                       M_ij[ind_w][glob_index_row,glob_index_col]\
                                        = spec.jv(order,argument)\
                                       * cm.exp(1j * angle_ij * (order))
                                    
                                    # Evanescent modes    
                                    else:
                                    
                                       M_ij[ind_w][glob_index_row,glob_index_col]\
                                        = spec.iv(order, argument) * \
                                       ((- 1) ** order) * \
                                       cm.exp(1j * angle_ij * (order))
                                    
        return M_ij
                                    
    #--------------------------------------------------------------------------                                
    #
    # Undo normalisation on outgoing wave coefficients
    #-------------------------------------------------------------------
    def undo_normalization_outgoing_coeffs(self, envobj, body_number, M, N, ind_w, vect):
       
        trunc_depthmode = self.dict_bodies[self.layout[body_number].name].trunc_depthmode
        
        # Calculate wave numbers
        (k,error) = envobj.get_wave_number(trunc_depthmode)
       
        cyl_rad = self.dict_bodies[self.layout[body_number].name].cyl_rad
        
        dim =  2 * M * (N + 1) + N + 1
        
        A_unnorm = n.zeros(dim, dtype = n.complex)
            
        for index_M in range (- M, M + 1):
            for index_N in range (0, N + 1):
        
                glob_ind = (M + index_M) * (N + 1) + index_N
                
                if (index_N==0): #progressive modes
                
                    arg_p = envobj.k0[ind_w] * cyl_rad
                    norm_fact = spec.hankel1(index_M,arg_p)
                    
                else: #evanescent modes
                    
                    arg_ev = k[ind_w][index_N] * cyl_rad
                    norm_fact = spec.kn(index_M,arg_ev)
        
                A_unnorm[glob_ind] = ((norm_fact)**(-1.)) * vect[glob_ind]
                
        return A_unnorm
    #-------------------------------------------------------------------
    #
    # Undo normalisation on incident wave coefficients
    #-------------------------------------------------------------------
    def undo_normalization_incident_coeffs(self, envobj, body_number, M, N, ind_w, vect):
       
        trunc_depthmode = self.dict_bodies[self.layout[body_number].name].trunc_depthmode
        
        # Calculate wave numbers
        (k,error) = envobj.get_wave_number(trunc_depthmode)
       
        cyl_rad = self.dict_bodies[self.layout[body_number].name].cyl_rad
        
        dim =  2 * M * (N + 1) + N + 1
        
        A_unnorm = n.zeros(dim, dtype = n.complex)
            
        for index_M in range (- M, M + 1):
            for index_N in range (0, N + 1):
        
                glob_ind = (M + index_M) * (N + 1) + index_N
                
                if (index_N==0): #progressive modes
                
                    arg_p = envobj.k0[ind_w] * cyl_rad
                    norm_fact = spec.jv(index_M,arg_p)
                    
                else: #evanescent modes
                    
                    arg_ev = k[ind_w][index_N] * cyl_rad
                    norm_fact = spec.iv(index_M,arg_ev)
        
                A_unnorm[glob_ind] = ((norm_fact)**(-1.)) * vect[glob_ind]
                
        return A_unnorm
    #-------------------------------------------------------------------
    #
    # Undo normalisation on incident wave coefficients
    #-------------------------------------------------------------------
    def undo_normalization_FTM(self, envobj, body_number, M, N, ind_w, vect):
       
        trunc_depthmode = self.dict_bodies[self.layout[body_number].name].trunc_depthmode
        
        # Calculate wave numbers
        (k,error) = envobj.get_wave_number(trunc_depthmode)
       
        cyl_rad = self.dict_bodies[self.layout[body_number].name].cyl_rad
        
        dim =  2 * M * (N + 1) + N + 1
        
        A_unnorm = n.zeros(dim, dtype = n.complex)
            
        for index_M in range (- M, M + 1):
            for index_N in range (0, N + 1):
        
                glob_ind = (M + index_M) * (N + 1) + index_N
                
                if (index_N==0): #progressive modes
                
                    arg_p = envobj.k0[ind_w] * cyl_rad
                    norm_fact = spec.jv(index_M,arg_p)
                    
                else: #evanescent modes
                    
                    arg_ev = k[ind_w][index_N] * cyl_rad
                    norm_fact = spec.iv(index_M,arg_ev)
        
                A_unnorm[glob_ind] = ((norm_fact)) * vect[glob_ind]
                
        return A_unnorm