# FUNCTIONS TO WRITE INPUT TO NEMOH AND READ OUTPUT FROM NEMOH 
#------------------------------------------------------------------------------

import os
import numpy as n

# TO DO: functions to get the path of the mesh, the solver and so on

###############################################################################
# GET PATHS FUNCTIONS
###############################################################################

def get_path_nemohcal():
    
    CurrDir = os.path.dirname(os.path.abspath('farm.py'))
    path_nemohcal = os.path.join(CurrDir,'Nemoh.cal')
    
    return path_nemohcal
    
def get_path_meshfolder():
    
    CurrDir = os.path.dirname(os.path.abspath('farm.py'))
    path_meshfolder = os.path.join(CurrDir,'Mesh')
    
    return path_meshfolder
    
def get_paths_Nemoh_executables():
    
    CurrDir = os.path.dirname(os.path.abspath('farm.py'))
    PathNemohFolder = os.path.join(CurrDir,'Nemoh_Cylindrical_Waves_Executables')
    
    PathPreProcessor = os.path.join(PathNemohFolder,'preProc.exe')
    PathSolver = os.path.join(PathNemohFolder,'solver.exe')
    PathPostProcessor = os.path.join(PathNemohFolder,'postproc.exe')
    
    return PathPreProcessor,PathSolver,PathPostProcessor
    
def get_paths_Nemoh_hydrocoeffs():
    
    CurrDir = os.path.dirname(os.path.abspath('farm.py'))
    PathResultsFolder = os.path.join(CurrDir,'Results')
    
    PathMa = os.path.join(PathResultsFolder,'RadiationCoefficients.tec')
    PathB = os.path.join(PathResultsFolder,'RadiationCoefficients.tec')
    PathFe = os.path.join(PathResultsFolder,'ExcitationForce.tec')
    
    return PathMa,PathB,PathFe
    
def get_paths_Nemoh_DTM_and_RC():
    
    CurrDir = os.path.dirname(os.path.abspath('farm.py'))
    PathResultsFolder = os.path.join(CurrDir,'Results')
    
    PathDTM = os.path.join(PathResultsFolder,'DiffractionTransferMatrix.tec')
    PathRC = os.path.join(PathResultsFolder,'RadiationVectorsMatrix.tec')
    
    return PathDTM,PathRC
    
def get_paths_KH_M_Matrices():
    
    CurrDir = os.path.dirname(os.path.abspath('farm.py'))
    PathResultsFolder = os.path.join(CurrDir,'Mechanics')
    
    PathKH = os.path.join(PathResultsFolder,'KH.dat')
    PathM = os.path.join(PathResultsFolder,'Inertia.dat')
    
    return PathKH,PathM
    
def get_path_ID():
    
    CurrDir = os.path.dirname(os.path.abspath('farm.py'))
    PathID = os.path.join(CurrDir,'ID.dat')
    
    return PathID
    
###############################################################################
# LOAD DATA FUNCTIONS
###############################################################################
    
def writeID():
    
    fid = open(get_path_ID(),'w')
    fid.write(str(1)+'\n')
    fid.write("'.'")
    fid.close()
    
    return
    
def loadnNodesPanels(NumMesh):
    
    NewPathMesh = os.path.join(get_path_meshfolder(),'Mesh'+str(NumMesh)+'.dat')
    fid = open(NewPathMesh,'r') # Mesh files, that means, it is NOT always for the full body
    fid.readline().rstrip()
    nNodes = 0
    FirstValue = 1
    while FirstValue != 0:
        ligne = fid.readline().split()
        FirstValue = int(ligne[0])
        if FirstValue != 0:
            nNodes = nNodes+1
    FirstValue = 1
    nPanels = 0
    while FirstValue != 0:
        ligne = fid.readline().split()
        FirstValue = int(ligne[0])
        if FirstValue != 0:
            nPanels = nPanels+1
    fid.close()

    return nNodes,nPanels
    
def loadnBodies():
    
    fid = open(get_path_nemohcal(),'r')
    
    for ind in range(0,6):
    
        fid.readline().rstrip()    
        
    nbodies = int(fid.readline().split()[0])
    
    return nbodies
    
def loadnw():
    
    nBodies = loadnBodies()
    fid = open(get_path_nemohcal(),'r')
    for i in range(0,7): 
        fid.readline().rstrip()
    for i in range(0,18*nBodies):
        fid.readline().rstrip()
    fid.readline().rstrip()
    nw = int(fid.readline().split()[0]) 
    fid.close()

    return nw
    
def loadnpartialwaves():
    
    nBodies = loadnBodies()
    fid = open(get_path_nemohcal(),'r')
    for i in range(0,7): # Delete the 6 first lines
        fid.readline().rstrip()
    for i in range(0,18*nBodies):
        fid.readline().rstrip()
    fid.readline().rstrip()
    fid.readline().rstrip()
    npartialwaves = int(fid.readline().split()[0]) # Number of partial waves
    fid.close()

    return npartialwaves
    
def loadnDOF():
    
    nBodies = loadnBodies()
    nDOF = n.zeros((nBodies),int)
    fid = open(get_path_nemohcal(),'r')
    for i in range(0,7): # Delete the 7 first lines
        fid.readline().rstrip()
    for i in range(0,nBodies):
        fid.readline().rstrip()
        fid.readline().rstrip()
        fid.readline().rstrip()
        nDOF[i] = int(fid.readline().split()[0])
        for i in range(0,14):
            fid.readline().rstrip()
    fid.close()

    return nDOF
    
def loadMa():

    nBodies = loadnBodies()
    nw = loadnw()
    Ma = n.zeros((6*nBodies,6*nBodies,nw))
    
    fid = open(get_paths_Nemoh_hydrocoeffs()[0], 'r')
    fid.readline().rstrip()
    
    for i in range(0,6*nBodies):
        fid.readline().rstrip()
    for i in range(0,nBodies):
        for j in range(0,6):
            fid.readline().rstrip()
            for k in range(0,nw):
                ligne = fid.readline().split()
                q = 0
                for p in range(1,2*6*nBodies):
                    if p%2 == 1:
                        Ma[6*i+j,q,k] = float(ligne[p])
                        q = q+1

    fid.close()
    
    return Ma
    
def loadB():

    nBodies = loadnBodies()
    nw = loadnw()
    B = n.zeros((6*nBodies,6*nBodies,nw))
    
    fid = open(get_paths_Nemoh_hydrocoeffs()[1], 'r')
    fid.readline().rstrip()
    
    for i in range(0,6*nBodies):
        fid.readline().rstrip()
    for i in range(0,nBodies):
        for j in range(0,6):
            fid.readline().rstrip()
            for k in range(0,nw):
                ligne = fid.readline().split()
                q = 0
                for p in range(1,2*6*nBodies):
                    if p%2 == 0:
                        B[6*i+j,q,k] = float(ligne[p])
                        q = q+1

    fid.close()

    return B
    
def loadFe():
    
    nw = loadnw()
    npartialwaves = loadnpartialwaves()
    nBodies = loadnBodies()
    nDOF = loadnDOF()
    
    FeAmp = n.zeros((nDOF[0]*nBodies,npartialwaves,nw),dtype = float)      
    FePhi = n.zeros((nDOF[0]*nBodies,npartialwaves,nw),dtype = float)
    Fe    = n.zeros((nDOF[0]*nBodies,npartialwaves,nw),complex)
    
    fid = open(get_paths_Nemoh_hydrocoeffs()[2],'r')    
    fid.readline().rstrip()
    for i in range(0,nBodies*6):
        fid.readline().rstrip()
        
    for i in range(0,npartialwaves):
        fid.readline().rstrip()
        for j in range(0,nw):
            ligne = fid.readline().split()
            p = 0
            for q in range(1,2*6*nBodies+1):
                if q%2 == 1:
                    FeAmp[p,i,j] = float(ligne[q])           
                else:
                    FePhi[p,i,j] = float(ligne[q])           
                    p = p+1
                    
    Fe = FeAmp * (n.cos(FePhi) + 1j * n.sin(FePhi))

    fid.close()
    
    return Fe
    
def loadDTM():
    
    nw = loadnw()
    npartialwaves = loadnpartialwaves()
    
    DTMreal = n.zeros((npartialwaves, npartialwaves,nw))
    DTMim   = n.zeros((npartialwaves, npartialwaves,nw))
    DTM     = n.zeros((npartialwaves, npartialwaves,nw),complex)
    
    fid = open(get_paths_Nemoh_DTM_and_RC()[0],'r')
    fid.readline().rstrip()
    for i in range(0,npartialwaves):
        fid.readline().rstrip()
        for j in range(0,nw):
            ligne = fid.readline().split()
            p = 0
            for q in range(1,2*npartialwaves + 1):
                if q%2 == 1:
                    DTMreal[p,i,j] = float(ligne[q])
                else:
                    DTMim  [p,i,j] = float(ligne[q])
                    p = p+1
                    
    DTM = DTMreal + 1j * DTMim
    
    fid.close()
    
    return DTM
    
def loadRcharacteristics():
    
    nw = loadnw()
    npartialwaves = loadnpartialwaves()
    nDOF = loadnDOF()
    
    if (len(nDOF) > 1):
        nDOF = n.int(n.sum(nDOF))
    else:
        nDOF = nDOF[0]
    
    Radreal = n.zeros((npartialwaves,nDOF,nw))
    Radim   = n.zeros((npartialwaves,nDOF,nw))
    Rad     = n.zeros((npartialwaves,nDOF,nw),complex)
    
    fid = open(get_paths_Nemoh_DTM_and_RC()[1],'r')
    fid.readline().rstrip()
    for i in range(0,nDOF):
        fid.readline().rstrip()
        for j in range(0,nw):
            ligne = fid.readline().split()
            p = 0
            for q in range(1,2*npartialwaves + 1):
                if q%2 == 1:
                    Radreal[p,i,j] = float(ligne[q])
                else:
                    Radim  [p,i,j] = float(ligne[q])
                    p = p+1
                    
    Rad = Radreal + 1j * Radim
    
    fid.close()
    
    return Rad
    
def loadKH(): 
    
    KH = n.zeros((6,6))
    fid = open(get_paths_KH_M_Matrices()[0],'r')
    for i in range(0,6):
        ligne = fid.readline().split()
        for j in range(0,6):
            KH[i,j] = float(ligne[j])
    fid.close()
    
    return KH
    
def loadInertia():
    
    Inertia = n.zeros((6,6))
    fid = open(get_paths_KH_M_Matrices()[1],'r')
    for i in range(0,6):
        ligne = fid.readline().split()
        for j in range(0,6):
            Inertia[i,j] = float(ligne[j])
    fid.close()
    
    return Inertia
    
###############################################################################
# WRITE INPUT FUNCTIONS
###############################################################################
    
def writeNemohcal_cylindricalwaves(nBodies,CG,envobj,wNumMinMax,nCylwavesAngtDeptht,FSE,Pressure,Kochin):
    
    Depth = envobj.depth
    rho   = envobj.water_density
    g     = envobj.gravity

    if Depth < 0:
        print ("\nERROR Nemoh.cal: Water depth has to be positive or null if infinite. Please correct your value.\n")
    else:
        with open(get_path_nemohcal(),'w') as fid:
            
            fid.write('--- Environment ------------------------------------------------------------------------------------------------------------------ \n')
            fid.write('%.1f				! RHO 		! KG/M**3 	! Fluid specific volume \n' % rho)
            fid.write('%.2f				! G			! M/S**2	! Gravity \n' % g)
            fid.write('%.2f				! DEPTH		! M		! Water depth\n' % Depth)
            fid.write('0.	0.		       	! XEFF YEFF		! M		! Wave measurement point\n')
            fid.write('--- Description of floating bodies -----------------------------------------------------------------------------------------------\n')
            fid.write('%i				! Number of bodies\n' % nBodies)
            for p in range(0,nBodies):
                NumMesh = p+1
                MeshName = "mesh"+str(NumMesh)+".dat"
                nNodes,nPanels = loadnNodesPanels(NumMesh)
                fid.write('--- Body %i -----------------------------------------------------------------------------------------------------------------------\n' % NumMesh)
                fid.write(os.path.join(get_path_meshfolder(),MeshName)+'      ! Name of mesh file\n')
                fid.write('%i   %i			! Number of points and number of panels 	\n' % (nNodes,nPanels))
                fid.write('6				! Number of degrees of freedom\n')
                fid.write('1 1. 0.	0. 0. 0. 0.		! Surge\n')
                fid.write('1 0. 1.	0. 0. 0. 0.		! Sway\n')
                fid.write('1 0. 0. 1. 0. 0. 0.		! Heave\n')
                if p == 0:
                    fid.write('2 1. 0. 0. %.2f %.2f %.2f		! Roll about a point\n' % (CG[0],CG[1],CG[2]))
                    fid.write('2 0. 1. 0. %.2f %.2f %.2f		! Pitch about a point\n' % (CG[0],CG[1],CG[2]))
                    fid.write('2 0. 0. 1. %.2f %.2f %.2f		! Yaw about a point\n' % (CG[0],CG[1],CG[2]))
                else:
                    fid.write('2 1. 0. 0. %.2f %.2f %.2f		! Roll about a point\n' % (CG[0],CG[1],CG[2]))
                    fid.write('2 0. 1. 0. %.2f %.2f %.2f		! Pitch about a point\n' % (CG[0],CG[1],CG[2]))
                    fid.write('2 0. 0. 1. %.2f %.2f %.2f		! Yaw about a point\n' % (CG[0],CG[1],CG[2]))
                fid.write('6				! Number of resulting generalised forces\n')
                fid.write('1 1. 0.	0. 0. 0. 0.		! Force in x direction\n')
                fid.write('1 0. 1.	0. 0. 0. 0.		! Force in y direction\n')
                fid.write('1 0. 0. 1. 0. 0. 0.		! Force in z direction\n')
                if p == 0:
                    fid.write('2 1. 0. 0. %.2f %.2f %.2f		! Moment force in x direction about a point\n' % (CG[0],CG[1],CG[2]))
                    fid.write('2 0. 1. 0. %.2f %.2f %.2f		! Moment force in y direction about a point\n' % (CG[0],CG[1],CG[2]))
                    fid.write('2 0. 0. 1. %.2f %.2f %.2f		! Moment force in z direction about a point\n' % (CG[0],CG[1],CG[2]))
                else:
                    fid.write('2 1. 0. 0. %.2f %.2f %.2f		! Moment force in x direction about a point\n' % (CG[0],CG[1],CG[2]))
                    fid.write('2 0. 1. 0. %.2f %.2f %.2f		! Moment force in y direction about a point\n' % (CG[0],CG[1],CG[2]))
                    fid.write('2 0. 0. 1. %.2f %.2f %.2f		! Moment force in z direction about a point\n' % (CG[0],CG[1],CG[2]))
                fid.write('0				! Number of lines of additional information \n')
            fid.write('--- Load cases to be solved -------------------------------------------------------------------------------------------------------\n');
            fid.write('%i	%.6f	%.6f		! Number of wave frequencies, Min, and Max (rad/s)\n' % (wNumMinMax[0],wNumMinMax[1],wNumMinMax[2]));
            fid.write('%i	%.2f	%.2f		! Number of partial waves, trunc_angmode and trunc_depthmode\n' % (nCylwavesAngtDeptht[0],nCylwavesAngtDeptht[1],nCylwavesAngtDeptht[2]));
            fid.write('--- Post processing ---------------------------------------------------------------------------------------------------------------\n');
            fid.write('0	0.1	10.		! IRF 				! IRF calculation (0 for no calculation), time step and duration\n');
            fid.write('%i				! Show pressure\n' % Pressure);
            fid.write('%i	%.2f	%.2f		! Kochin function 		! Number of directions of calculation (0 for no calculations), Min and Max (degrees)\n' % (Kochin[0],Kochin[1],Kochin[2]));
            fid.write('%i	%i	%.2f	%.2f	! Free surface elevation 	! Number of points in x direction (0 for no calcutions) and y direction and dimensions of domain in x and y direction\n' % (FSE[0],FSE[1],FSE[2],FSE[3]));	
            fid.write('---')
    fid.close()
    return
    
    
###############################################################################
# RUN NEMOH FUNCTION
###############################################################################
    
def call_Nemoh(nCylwavesAngtDeptht):
    
    print ("\n========================================================================")
    print ("                            Nemoh")
    print ("========================================================================")
    
    
    #print '\nFrequency (rad/s) = {}'.format(Currw)
    print ('\nNumber of Partial Waves = {}'.format(nCylwavesAngtDeptht[0]))
    print ('\nNumber of Angular Modes = {}'.format(nCylwavesAngtDeptht[1]))
    print ('\nNumber of Depth Modes   = {}'.format(nCylwavesAngtDeptht[2]))
    
    
    
    print ('\n------ Preprocessor ------ \n')
    os.system(get_paths_Nemoh_executables()[0])
    print ('------ Solver ------ \n')
    os.system(get_paths_Nemoh_executables()[1])
    print ('------ Postprocessor ------ \n')
    os.system(get_paths_Nemoh_executables()[2])
    
     
    print ("\n========================================================================")
    print ("                            End of Nemoh")
    print ("========================================================================"  )