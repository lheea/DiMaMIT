from env import Environment	
from farm import Farm
import numpy as n	
import math as m

import matplotlib.pyplot as plt 
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.collections import PatchCollection
from matplotlib import ticker
import matplotlib as mpl
import matplotlib.gridspec as gridspec
from matplotlib import rcParams


############################### POSTPROC ######################################
def plot_excitation_force_sep_distance(results_vect,envobj,char_dimension,body_numb,dof_body,x_min,x_max):
    
    # Pepare vectors to plot (x and y axis)
    distances = n.linspace(x_min, x_max, len(results_vect))
    
    results_to_plot = n.zeros(len(results_vect))
    
    for ind in range(0,len(results_vect)):
        
        results_to_plot[ind] =  abs(results_vect[ind][body_numb][dof_body - 1,0,0]) 
        
    plt.plot(distances, results_to_plot, 's')
    
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5), ncol=1, numpoints=1, fontsize=20)
    #plt.show()
    
    
def plot_excitation_force_sep_distance_Nemoh(results_vect,envobj,char_dimension,body_numb,dof_body,x_min,x_max):    

    distances = n.linspace(x_min, x_max, len(results_vect))
    
    results_to_plot = n.zeros(len(results_vect))
    
    for ind in range(0,len(results_vect)):
        
        results_to_plot[ind] =  results_vect[ind][0][0,6*body_numb + dof_body - 1,0]
 
    plt.plot(distances, results_to_plot, 's')
    
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5), ncol=1, numpoints=1, fontsize=20)
    
    #plt.show()

def compare_excitation_force_separation(results_vect_IT,results_vect_Nemoh,envobj,char_dimension,body_numb,dof_body,x_min,x_max,case): 

    distances = n.linspace(x_min, x_max, len(results_vect_IT))
    
    results_to_plot_IT = n.zeros(len(results_vect_IT))
    results_to_plot_Nemoh = n.zeros(len(results_vect_Nemoh))
    
    for ind in range(0,len(results_vect_IT)):
        
        results_to_plot_IT[ind] =  abs(results_vect_IT[ind][body_numb][dof_body - 1,0,0]) 
        
    for ind in range(0,len(results_vect_Nemoh)):
        
        results_to_plot_Nemoh[ind] =  results_vect_Nemoh[ind][0][0,6*body_numb + dof_body - 1,0]
    
    forces = ['Fx','Fy','Fz','Mx','My','Mz']    
    
    fig1 = plt.figure()
    plt.title(r'$\lambda / a = $' + str(case), fontsize=25)
    plt.xlabel('d/a', fontsize=20)
    if ((dof_body - 1) <= 2):
         plt.ylabel(forces[dof_body - 1]+' '+'Body'+' '+str(body_numb + 1)+' '+'(N)',fontsize=25)
    else:
         plt.ylabel(forces[dof_body - 1]+' '+'Body'+' '+str(body_numb + 1)+' '+'(N$\cdot$m)',fontsize=25)
    plt.tick_params(axis='x', labelsize=20)
    plt.tick_params(axis='y', labelsize=20)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(distances, results_to_plot_IT, '-o', label='IT (L=12)')
    plt.plot(distances, results_to_plot_Nemoh, '-o', label='Nemoh')
    
    plt.legend(loc='best', fontsize=20, frameon=False)
    
    plt.savefig(forces[dof_body - 1]+' '+'Body'+' '+str(body_numb)+'.eps',bbox_inches='tight',pad_inches=3)
    
    
def compare_excitation_force_separation_evmodes(results_vect_IT,results_vect_IT_ev1,results_vect_IT_ev2,results_vect_IT_ev3,results_vect_Nemoh,\
                                                envobj,char_dimension,body_numb,dof_body,x_min,x_max,case,ev_modes_numb1,ev_modes_numb2,ev_modes_numb3): 

    distances = n.linspace(x_min, x_max, len(results_vect_IT))
    
    results_to_plot_IT = n.zeros(len(results_vect_IT))
    results_to_plot_IT_ev1 = n.zeros(len(results_vect_IT_ev1))
    results_to_plot_IT_ev2 = n.zeros(len(results_vect_IT_ev2))
    results_to_plot_IT_ev3 = n.zeros(len(results_vect_IT_ev3))
    results_to_plot_Nemoh = n.zeros(len(results_vect_Nemoh))
    
    for ind in range(0,len(results_vect_IT)):
        
        results_to_plot_IT[ind] =  abs(results_vect_IT[ind][body_numb][dof_body - 1,0,0]) 
        results_to_plot_IT_ev1[ind] =  abs(results_vect_IT_ev1[ind][body_numb][dof_body - 1,0,0]) 
        results_to_plot_IT_ev2[ind] =  abs(results_vect_IT_ev2[ind][body_numb][dof_body - 1,0,0])
        results_to_plot_IT_ev3[ind] =  abs(results_vect_IT_ev3[ind][body_numb][dof_body - 1,0,0])
        
    for ind in range(0,len(results_vect_Nemoh)):
        
        results_to_plot_Nemoh[ind] =  results_vect_Nemoh[ind][0][0,6*body_numb + dof_body - 1,0]
    
    
    forces = ['Fx','Fy','Fz','Mx','My','Mz']    
    
    fig1 = plt.figure()
    plt.title(r'$\lambda / a = $' + str(case), fontsize=20)
    plt.xlabel('d/a', fontsize=20)
    if ((dof_body - 1) <= 2):
        plt.ylabel(forces[dof_body - 1]+' '+'Body'+' '+str(body_numb + 1) + ' ' + '(N)',fontsize=20)
    else:
        plt.ylabel(forces[dof_body - 1]+' '+'Body'+' '+str(body_numb + 1) + ' ' + '(N$\cdot$m)',fontsize=20) 
    plt.tick_params(axis='x', labelsize=20)
    plt.tick_params(axis='y', labelsize=20)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(distances, results_to_plot_IT, '-bo', label='IT' + ' ' + 'L=0')
    plt.plot(distances, results_to_plot_IT_ev1, '-kv', label='IT' +' '+ 'L='+str(ev_modes_numb1))
    plt.plot(distances, results_to_plot_IT_ev2, '-c^', label='IT' +' '+ 'L='+str(ev_modes_numb2))
    plt.plot(distances, results_to_plot_IT_ev3, '-rd', label='IT' +' '+ 'L='+str(ev_modes_numb3))
    plt.plot(distances, results_to_plot_Nemoh, '-gs', label='Nemoh')

    plt.legend(loc='best', fontsize=20, frameon=True)
    
    plt.savefig(forces[dof_body - 1]+' '+'Body'+' '+str(body_numb)+'.eps',bbox_inches='tight',pad_inches=3)
    

def plot_hydro_coeffs_sep_distance(results_vect,envobj,char_dimension,body,ind_i,ind_j,ind_freq,x_min,x_max):  

    # Pepare vectors to plot (x and y axis)
    distances = n.linspace(x_min, x_max, len(results_vect))
    
    results_AM_to_plot = n.zeros(len(results_vect))
    results_DAMP_to_plot = n.zeros(len(results_vect))
    
    for ind in range(0,len(results_vect)):
        
        results_AM_to_plot[ind] =  (results_vect[ind][0][body][ind_i - 1,ind_j - 1,ind_freq]) 
        results_DAMP_to_plot[ind] =  (results_vect[ind][1][body][ind_i - 1,ind_j - 1,ind_freq]) 
    
    fig1 = plt.figure()        
    plt.plot(distances, results_AM_to_plot, 's')
    plt.title('Added mass ij')

def compare_hydro_coeffs_separation(results_vect_IT,results_vect_AM_Nemoh,results_vect_DAMP_Nemoh,envobj,char_dimension,body,ind_i,ind_j,x_min,x_max,case):
    
    distances = n.linspace(x_min, x_max, len(results_vect_IT))
    
    results_AM_to_plot_IT = n.zeros(len(results_vect_IT))
    results_AM_to_plot_Nemoh = n.zeros(len(results_vect_AM_Nemoh))
    results_DAMP_to_plot_IT = n.zeros(len(results_vect_IT))
    results_DAMP_to_plot_Nemoh = n.zeros(len(results_vect_DAMP_Nemoh))
    
    
    for ind in range(0,len(results_vect_IT)):
        
        results_AM_to_plot_IT[ind] =  (results_vect_IT[ind][0][body][ind_i - body*6 - 1,ind_j - 1,0]) 
        results_AM_to_plot_Nemoh[ind] = (results_vect_AM_Nemoh[ind][ind_i - 1,ind_j - 1,0]) 
        results_DAMP_to_plot_IT[ind] =  (results_vect_IT[ind][1][body][ind_i - body*6 - 1,ind_j - 1,0]) 
        results_DAMP_to_plot_Nemoh[ind] = (results_vect_DAMP_Nemoh[ind][ind_i - 1,ind_j - 1,0]) 
      
        
        
    fig1 = plt.figure()
    plt.title(r'$\lambda / a = $' + str(case), fontsize=20)
    plt.xlabel('d/a', fontsize=20)
    plt.ylabel('Added Mass'+' '+ str(ind_i) + ',' + str(ind_j) +' '+'(Kg)',fontsize=20)
    plt.tick_params(axis='x', labelsize=20)
    plt.tick_params(axis='y', labelsize=20)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(distances, results_AM_to_plot_IT, '-bo', label='IT' + ' ' + 'L=9')
    plt.plot(distances, results_AM_to_plot_Nemoh, '-go', label='Nemoh')

    plt.legend(loc='best', fontsize=20,frameon=False)
    plt.savefig('AM'+' '+ str(ind_i) + ',' + str(ind_j)+'.eps',bbox_inches='tight',pad_inches=3)
    
    fig2 = plt.figure()
    plt.title(r'$\lambda / a = $' + str(case), fontsize=20)
    plt.xlabel('d/a', fontsize=20)
    plt.ylabel('Damping'+' '+ str(ind_i) +','+ str(ind_j)+' '+'(N/m$\cdot$s)',fontsize=20)
    plt.tick_params(axis='x', labelsize=20)
    plt.tick_params(axis='y', labelsize=20)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(distances, results_DAMP_to_plot_IT, '-bo', label='IT' + ' ' + 'L=9')
    plt.plot(distances, results_DAMP_to_plot_Nemoh, '-go', label='Nemoh')

    plt.legend(loc='best', fontsize=20,frameon=False)
    plt.savefig('DAMP'+' '+ str(ind_i) +',' + str(ind_j)+'.eps',bbox_inches='tight',pad_inches=3)
    
def compare_hydro_coeffs_separation_evmodes(results_vect_IT,results_vect_IT_ev,results_vect_AM_Nemoh,results_vect_DAMP_Nemoh,envobj,char_dimension,body,ind_i,ind_j,x_min,x_max,case,ev_modes_number):
    
    distances = n.linspace(x_min, x_max, len(results_vect_IT))
    
    results_AM_to_plot_IT = n.zeros(len(results_vect_IT))
    results_AM_to_plot_IT_ev = n.zeros(len(results_vect_IT_ev))
    results_AM_to_plot_Nemoh = n.zeros(len(results_vect_IT))
    results_DAMP_to_plot_IT = n.zeros(len(results_vect_IT))
    results_DAMP_to_plot_IT_ev = n.zeros(len(results_vect_IT))
    results_DAMP_to_plot_Nemoh = n.zeros(len(results_vect_IT))
    
    
    for ind in range(0,len(results_vect_IT)):
        
        results_AM_to_plot_IT[ind] =  (results_vect_IT[ind][0][body][ind_i - body*6 - 1,ind_j - 1,0]) 
        results_AM_to_plot_IT_ev[ind] =  (results_vect_IT_ev[ind][0][body][ind_i - body*6 - 1,ind_j - 1,0])
        results_AM_to_plot_Nemoh[ind] = (results_vect_AM_Nemoh[ind][ind_i - 1,ind_j - 1,0]) 
        results_DAMP_to_plot_IT[ind] =  (results_vect_IT[ind][1][body][ind_i - body*6 - 1,ind_j - 1,0]) 
        results_DAMP_to_plot_IT_ev[ind] =  (results_vect_IT_ev[ind][1][body][ind_i - body*6 - 1,ind_j - 1,0]) 
        results_DAMP_to_plot_Nemoh[ind] = (results_vect_DAMP_Nemoh[ind][ind_i - 1,ind_j - 1,0]) 
      
        
        
    fig1 = plt.figure()
    plt.title(r'$\lambda / a = $' + str(case), fontsize=25)
    plt.xlabel('d/a', fontsize=25)
    plt.ylabel('Added Mass'+' '+'(Kg)'+' '+ str(ind_i) +','+ str(ind_j),fontsize=25)
    plt.tick_params(axis='x', labelsize=20)
    plt.tick_params(axis='y', labelsize=20)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(distances, results_AM_to_plot_IT, '-bo', label='IT' + ' ' + 'L=0')
    plt.plot(distances, results_AM_to_plot_IT_ev, '-ro', label='IT' +' '+ 'L=' + str(ev_modes_number))
    plt.plot(distances, results_AM_to_plot_Nemoh, '-go', label='Nemoh')

    plt.legend(loc='best', fontsize=20,frameon=False)
    
    plt.savefig('AM'+' '+ str(ind_i) + str(ind_j)+'.eps',bbox_inches='tight',pad_inches=3)
    
    fig2 = plt.figure()
    plt.title(r'$\lambda / a = $' + str(case), fontsize=25)
    plt.xlabel('d/a', fontsize=25)
    plt.ylabel('Damping (N/m$\cdot$s)'+' '+ str(ind_i) +','+ str(ind_j),fontsize=25)
    plt.tick_params(axis='x', labelsize=20)
    plt.tick_params(axis='y', labelsize=20)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(distances, results_DAMP_to_plot_IT, '-bo', label='IT' + ' ' + 'L=0')
    plt.plot(distances, results_DAMP_to_plot_IT_ev, '-ro', label='IT' +' '+ 'L=' + str(ev_modes_number))
    plt.plot(distances, results_DAMP_to_plot_Nemoh, '-go', label='Nemoh')

    plt.legend(loc='best', fontsize=20,frameon=False)
    
    plt.savefig('DAMP'+' '+ str(ind_i) + str(ind_j)+'.eps',bbox_inches='tight',pad_inches=3)
    
def compare_largedistancevalues(freqmin, freqmax,results_indivcyl_exc,results_indivcyl_AM,results_indivcyl_DAMP,results_IT_exc,results_IT_AM,results_IT_DAMP, index_force, index_motion,body,beta):
    
    #values plotted for body zero
    dim =  results_indivcyl_AM.shape[2]

    frequencies = n.linspace(freqmin, freqmax, dim)
     
    results_AM_to_plot_IT = n.zeros(dim)
    results_AM_to_plot_Nemoh = n.zeros(dim)
    results_DAMP_to_plot_IT = n.zeros(dim)
    results_DAMP_to_plot_Nemoh = n.zeros(dim)
    results_EXC_to_plot_IT = n.zeros(dim)
    results_EXC_to_plot_Nemoh = n.zeros(dim)
    
    for ind in range(0,dim):
        
         results_AM_to_plot_IT[ind]      = results_IT_AM[body][index_force,index_motion+6*body,ind]
         results_DAMP_to_plot_IT[ind]    = results_IT_DAMP[body][index_force,index_motion+6*body,ind]
         results_EXC_to_plot_IT[ind]     = abs(results_IT_exc[body][index_force,0,ind])
         
         results_AM_to_plot_Nemoh[ind]   = results_indivcyl_AM[index_force,index_motion,ind]
         results_DAMP_to_plot_Nemoh[ind] = results_indivcyl_DAMP[index_force,index_motion,ind]
         results_EXC_to_plot_Nemoh[ind]  = results_indivcyl_exc[0][ind,index_force,0]
    
    
    fig1 = plt.figure()
    #plt.title('Added_Mass' + ' ' + str(index_force + 1) + ',' + str(index_motion + 1) + '(Kg)', fontsize=25)
    plt.xlabel('$\omega$ (rad/s)', fontsize=20)
    plt.ylabel('Added Mass'+' '+ str(index_force + 6*body + 1) + ',' + str(index_motion + 6*body + 1) + '(Kg)',fontsize=20)
    plt.tick_params(axis='x', labelsize=20)
    plt.tick_params(axis='y', labelsize=20)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(frequencies, results_AM_to_plot_IT, '-bo', label='IT' + ' ' + 'L=0')
    plt.plot(frequencies, results_AM_to_plot_Nemoh, '-gs', label='Nemoh')
    
    plt.show()

    plt.legend(loc='best', fontsize=20, frameon=False)
    plt.savefig('Added_Mass' + str(index_force +6*body + 1) + ',' + str(index_motion + 6*body + 1) +'.eps',bbox_inches='tight',pad_inches=3)
    
    
    fig2 = plt.figure()
    #plt.title('Damping' + ' ' + str(index_force + 1) + str(index_motion + 1) + '(N/ms)', fontsize=25)
    plt.xlabel('$\omega$ (rad/s)', fontsize=20)
    plt.ylabel('Damping'+' '+ str(index_force + 6*body + 1) + ',' + str(index_motion + 6*body + 1) + '(N/m$\cdot$s)',fontsize=20)
    plt.tick_params(axis='x', labelsize=20)
    plt.tick_params(axis='y', labelsize=20)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(frequencies, results_DAMP_to_plot_IT, '-bo', label='IT' + ' ' + 'L=0')
    plt.plot(frequencies, results_DAMP_to_plot_Nemoh, '-gs', label='Nemoh')
    
    plt.show()

    plt.legend(loc='best', fontsize=20, frameon=False)
    plt.savefig('Damping' + str(index_force + 6*body + 1) + ',' + str(index_motion + 6*body + 1) +'.eps',bbox_inches='tight',pad_inches=3)
    
    indexs = ['Fx','Fy','Fz','Mx','My','Mz']
    fig3 = plt.figure()
    plt.title(r'$\beta$ ='+str(beta)+' '+'(deg)', fontsize=20)
    #plt.title(r'$\beta$', fontsize=20)
    plt.xlabel('$\omega$ (rad/s)', fontsize=20)
    if (index_force <= 2):
        plt.ylabel(indexs[index_force] + ' ' + 'body' + ' ' + str(body+1) + ' ' + '(N)',fontsize=20)
    else:
        plt.ylabel(indexs[index_force] + ' ' + 'body' + ' ' + str(body+1) + ' ' + '(N$\cdot$m)',fontsize=20)
    plt.tick_params(axis='x', labelsize=20)
    plt.tick_params(axis='y', labelsize=20)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(frequencies, results_EXC_to_plot_IT, '-bo', label='IT' + ' ' + 'L=0')
    plt.plot(frequencies, results_EXC_to_plot_Nemoh, '-gs', label='Nemoh')
    
    plt.show()

    plt.legend(loc='best', fontsize=20, frameon=False)
    plt.savefig(indexs[index_force] + 'body' + str(body+1) +'.eps',bbox_inches='tight',pad_inches=3)
    

    fig1 = plt.figure()
    ax = fig1.add_subplot(111)
    #plt.title('Added_Mass' + ' ' + str(index_force + 1) + ',' + str(index_motion + 1) + '(Kg)', fontsize=25)
    lns1 = ax.plot(frequencies, results_AM_to_plot_IT, '-bo', label='AM' + ' ' + 'IT' + ' ' + '(L=0)')
    lns2 = ax.plot(frequencies, results_AM_to_plot_Nemoh, '-gs', label='AM' + ' ' + 'Nemoh')
    ax.set_xlabel('$\omega$ (rad/s)', fontsize=20)
    ax.set_ylabel('Added Mass'+' '+ str(index_force + 1) + ',' + str(index_motion + 1) + '(Kg)',fontsize=20)
    ax2 = ax.twinx()
    lns3 = ax2.plot(frequencies, results_DAMP_to_plot_IT, '-r^', label='DAMP' + ' ' + 'IT' + ' ' + '(L=0)')
    lns4 = ax2.plot(frequencies, results_DAMP_to_plot_Nemoh, '-kd', label='DAMP' + ' ' + 'Nemoh')
    ax2.set_ylabel('Damping'+' '+ str(index_force + 1) + ',' + str(index_motion + 1) + '(N/m$\cdot$s)',fontsize=20)
    ax.tick_params(axis='x', labelsize=20)
    ax.tick_params(axis='y', labelsize=20)
    ax2.tick_params(axis='y', labelsize=20)
    ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    
    ax.yaxis.get_major_formatter().set_powerlimits((0, 1))
    ax2.yaxis.get_major_formatter().set_powerlimits((0, 1))
    plt.show()
    
    lns = lns1 + lns2 + lns3 + lns4
    labs = [l.get_label() for l in lns]
    #ax.legend(lns, labs,loc='best', fontsize=18, frameon=False)
    ax.legend(lns, labs,loc=(0.05,0.15), fontsize=18, frameon=False)
    #ax2.legend(loc='', fontsize=20, frameon=False)
    plt.savefig('Added_Mass_and_Damping' + str(index_force) + str(index_motion) +'.eps',bbox_inches='tight',pad_inches=3)
    
def compare_largedistancevalues_twobodies(freqmin, freqmax,results_twocyl_exc,results_twocyl_AM,results_twocyl_DAMP,results_IT_exc,results_IT_AM,results_IT_DAMP, index_force, index_motion,body,beta):  
    
    #values plotted for body zero
    dim =  results_twocyl_AM.shape[2]

    frequencies = n.linspace(freqmin, freqmax, dim)
     
    results_AM_to_plot_IT = n.zeros(dim)
    results_AM_to_plot_Nemoh = n.zeros(dim)
    results_DAMP_to_plot_IT = n.zeros(dim)
    results_DAMP_to_plot_Nemoh = n.zeros(dim)
    results_EXC_to_plot_IT = n.zeros(dim)
    results_EXC_to_plot_Nemoh = n.zeros(dim)
    
    for ind in range(0,dim):
        
         results_AM_to_plot_IT[ind]      = results_IT_AM[body][index_force,index_motion,ind]
         results_DAMP_to_plot_IT[ind]    = results_IT_DAMP[body][index_force,index_motion,ind]
         results_EXC_to_plot_IT[ind]     = abs(results_IT_exc[body][index_force,0,ind])
         
         results_AM_to_plot_Nemoh[ind]   = results_twocyl_AM[index_force+body*6,index_motion,ind]
         results_DAMP_to_plot_Nemoh[ind] = results_twocyl_DAMP[index_force+body*6,index_motion,ind]
         results_EXC_to_plot_Nemoh[ind]  = results_twocyl_exc[0][ind,index_force+body*6,0]
    
    
    fig1 = plt.figure()
    #plt.title('Added_Mass' + ' ' + str(index_force + 1) + ',' + str(index_motion + 1) + '(Kg)', fontsize=25)
    plt.xlabel('$\omega$ (rad/s)', fontsize=20)
    plt.ylabel('Added Mass'+' '+ str(index_force + 6*body + 1) + ',' + str(index_motion + 1) + '(Kg)',fontsize=20)
    plt.tick_params(axis='x', labelsize=20)
    plt.tick_params(axis='y', labelsize=20)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(frequencies, results_AM_to_plot_IT, '-bo', label='IT' + ' ' + 'L=0')
    plt.plot(frequencies, results_AM_to_plot_Nemoh, '-gs', label='Nemoh')
    
    plt.show()

    plt.legend(loc='best', fontsize=20, frameon=False)
    plt.savefig('Added_Mass' + str(index_force + 6*body + 1) + ',' + str(index_motion + 1) +'.eps',bbox_inches='tight',pad_inches=3)
    
    
    fig2 = plt.figure()
    #plt.title('Damping' + ' ' + str(index_force + 1) + str(index_motion + 1) + '(N/ms)', fontsize=25)
    plt.xlabel('$\omega$ (rad/s)', fontsize=20)
    plt.ylabel('Damping'+' '+ str(index_force + 6*body + 1) + ',' + str(index_motion + 1) + '(N/m$\cdot$s)',fontsize=20)
    plt.tick_params(axis='x', labelsize=20)
    plt.tick_params(axis='y', labelsize=20)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(frequencies, results_DAMP_to_plot_IT, '-bo', label='IT' + ' ' + 'L=0')
    plt.plot(frequencies, results_DAMP_to_plot_Nemoh, '-gs', label='Nemoh')
    
    plt.show()

    plt.legend(loc='best', fontsize=20, frameon=False)
    plt.savefig('Damping' + str(index_force + 6*body + 1) + ',' + str(index_motion + 1) +'.eps',bbox_inches='tight',pad_inches=3)
    
    indexs = ['Fx','Fy','Fz','Mx','My','Mz']
    fig3 = plt.figure()
    plt.title(r'$\beta$ ='+str(beta)+' '+'(deg)', fontsize=20)
    #plt.title(r'$\beta$', fontsize=20)
    plt.xlabel('$\omega$ (rad/s)', fontsize=20)
    if (index_force <= 2):
        plt.ylabel(indexs[index_force] + ' ' + 'body' + ' ' + str(body+1) + ' ' + '(N)',fontsize=20)
    else:
        plt.ylabel(indexs[index_force] + ' ' + 'body' + ' ' + str(body+1) + ' ' + '(N$\cdot$m)',fontsize=20)
    plt.tick_params(axis='x', labelsize=20)
    plt.tick_params(axis='y', labelsize=20)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(frequencies, results_EXC_to_plot_IT, '-bo', label='IT' + ' ' + 'L=0')
    plt.plot(frequencies, results_EXC_to_plot_Nemoh, '-gs', label='Nemoh')
    
    plt.show()

    plt.legend(loc='best', fontsize=20, frameon=False)
    plt.savefig(indexs[index_force] + 'body' + str(body+1) +'.eps',bbox_inches='tight',pad_inches=3)
   
def compare_largedistancevalues_twobodies_analytical(freqmin, freqmax,results_am_analytical,results_damp_analytical,\
                                                                   results_twocyl_AM_coarse,results_twocyl_DAMP_coarse,\
                                                                   results_twocyl_AM_fine,results_twocyl_DAMP_fine, \
                                                                    results_IT_AM_coarse,results_IT_DAMP_coarse, \
                                                                    results_IT_AM_fine, results_IT_DAMP_fine, \
                                                                    index_force, index_motion,body,beta):  
    
    #values plotted for body zero
    dim =  results_twocyl_AM_coarse.shape[2]

    frequencies = n.linspace(freqmin, freqmax, dim)
     
    results_AM_to_plot_IT_coarse = n.zeros(dim)
    results_AM_to_plot_IT_fine = n.zeros(dim)
    results_AM_to_plot_Nemoh_coarse = n.zeros(dim)
    results_AM_to_plot_Nemoh_fine = n.zeros(dim)
    results_DAMP_to_plot_IT_coarse = n.zeros(dim)
    results_DAMP_to_plot_IT_fine = n.zeros(dim)
    results_DAMP_to_plot_Nemoh_coarse = n.zeros(dim)
    results_DAMP_to_plot_Nemoh_fine = n.zeros(dim)
    results_AM_to_plot_analytical = n.array(results_am_analytical)
    results_DAMP_to_plot_analytical = n.array(results_damp_analytical)
    
    for ind in range(0,dim):
        
         results_AM_to_plot_IT_coarse[ind]      = results_IT_AM_coarse[body][index_force-body*6-1,index_motion-1,ind]
         results_DAMP_to_plot_IT_coarse[ind]    = results_IT_DAMP_coarse[body][index_force-body*6-1,index_motion-1,ind]
         
         results_AM_to_plot_IT_fine[ind]      = results_IT_AM_fine[body][index_force-body*6-1,index_motion-1,ind]
         results_DAMP_to_plot_IT_fine[ind]    = results_IT_DAMP_fine[body][index_force-body*6-1,index_motion-1,ind]
         
         results_AM_to_plot_Nemoh_coarse[ind]   = results_twocyl_AM_coarse[index_force-body*6-1,index_motion-1,ind]
         results_DAMP_to_plot_Nemoh_coarse[ind] = results_twocyl_DAMP_coarse[index_force-body*6-1,index_motion-1,ind]
         
         results_AM_to_plot_Nemoh_fine[ind]   = results_twocyl_AM_fine[index_force-1,index_motion-1,ind]
         results_DAMP_to_plot_Nemoh_fine[ind] = results_twocyl_DAMP_fine[index_force-1,index_motion-1,ind]
    
    
    fig1 = plt.figure()
    #plt.title('Added_Mass' + ' ' + str(index_force + 1) + ',' + str(index_motion + 1) + '(Kg)', fontsize=25)
    plt.xlabel('$\omega$ (rad/s)', fontsize=20)
    plt.ylabel('Added Mass'+' '+ str(index_force) + ',' + str(index_motion) + '(Kg)',fontsize=20)
    plt.tick_params(axis='x', labelsize=20)
    plt.tick_params(axis='y', labelsize=20)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(frequencies, results_AM_to_plot_IT_coarse, '-bo', label='IT' + ' ' + '(L=0, 361 panels)')
    plt.plot(frequencies, results_AM_to_plot_IT_fine, ':b^', label='IT' + ' ' + '(L=0, 1521 panels)')
    plt.plot(frequencies, results_AM_to_plot_analytical[:,body,index_force-body*6-1,index_motion-1], '-rd', label='Zeng et al. (2013, L=50)')
    plt.plot(frequencies, results_AM_to_plot_Nemoh_coarse, '-gs', label='Nemoh (361 panels)')
    plt.plot(frequencies, results_AM_to_plot_Nemoh_fine, ':gd', label='Nemoh (1521 panels)')

    
    plt.show()

    plt.legend(loc='best', fontsize=18, frameon=False,numpoints=1)
    plt.savefig('Added_Mass' + str(index_force) + ',' + str(index_motion) +'.eps',bbox_inches='tight',pad_inches=3)
    
    
    fig2 = plt.figure()
    #plt.title('Damping' + ' ' + str(index_force + 1) + str(index_motion + 1) + '(N/ms)', fontsize=25)
    plt.xlabel('$\omega$ (rad/s)', fontsize=20)
    plt.ylabel('Damping'+' '+ str(index_force) + ',' + str(index_motion) + '(N/m$\cdot$s)',fontsize=20)
    plt.tick_params(axis='x', labelsize=20)
    plt.tick_params(axis='y', labelsize=20)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(frequencies, results_DAMP_to_plot_IT_coarse, '-bo', label='IT' + ' ' + '(L=0, 361 panels)')
    plt.plot(frequencies, results_DAMP_to_plot_IT_fine, ':b^', label='IT' + ' ' + '(L=0, 1521 panels)')
    plt.plot(frequencies, results_DAMP_to_plot_analytical[:,body,index_force-body*6-1,index_motion-1], '-rd', label='Zeng et al. (2013, L=50)')
    plt.plot(frequencies, results_DAMP_to_plot_Nemoh_coarse, '-gs', label='Nemoh (361 panels)')
    plt.plot(frequencies, results_DAMP_to_plot_Nemoh_fine, ':gd', label='Nemoh (1521 panels)')
    
    plt.show()

    plt.legend(loc='best', fontsize=18, frameon=False, numpoints=1)
    plt.savefig('Damping' + str(index_force) + ',' + str(index_motion) +'.eps',bbox_inches='tight',pad_inches=3)
    
#    indexs = ['Fx','Fy','Fz','Mx','My','Mz']
#    fig3 = plt.figure()
#    plt.title(r'$\beta$ ='+str(beta)+' '+'(deg)', fontsize=20)
#    #plt.title(r'$\beta$', fontsize=20)
#    plt.xlabel('$\omega$ (rad/s)', fontsize=20)
#    if (index_force <= 2):
#        plt.ylabel(indexs[index_force] + ' ' + 'body' + ' ' + str(body+1) + ' ' + '(N)',fontsize=20)
#    else:
#        plt.ylabel(indexs[index_force] + ' ' + 'body' + ' ' + str(body+1) + ' ' + '(N$\cdot$m)',fontsize=20)
#    plt.tick_params(axis='x', labelsize=20)
#    plt.tick_params(axis='y', labelsize=20)
#    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
#    plt.plot(frequencies, results_EXC_to_plot_IT, '-bo', label='IT' + ' ' + 'L=0')
#    plt.plot(frequencies, results_EXC_to_plot_Nemoh, '-gs', label='Nemoh')
#    
#    plt.show()
#
#    plt.legend(loc='best', fontsize=20, frameon=False)
#    plt.savefig(indexs[index_force] + 'body' + str(body+1) +'.eps',bbox_inches='tight',pad_inches=3)
   
   
   
def compare_largedistancevalues_twobodies_evmodes(freqmin, freqmax,results_indivcyl_exc,results_indivcyl_AM,results_indivcyl_DAMP, \
                                                 results_twocyl_exc,results_twocyl_AM,results_twocyl_DAMP,results_IT_exc,results_IT_AM,results_IT_DAMP,\
                                                 results_IT_exc_3ev,results_IT_AM_3ev,results_IT_DAMP_3ev,\
                                                 results_IT_exc_6ev,results_IT_AM_6ev,results_IT_DAMP_6ev, index_force, index_motion,body,beta):  
    
    #values plotted for body zero
    dim =  results_twocyl_AM.shape[2]

    frequencies = n.linspace(freqmin, freqmax, dim)
     
    results_AM_to_plot_IT = n.zeros(dim)
    results_AM_to_plot_IT_3ev = n.zeros(dim)
    results_AM_to_plot_IT_6ev = n.zeros(dim)
    results_AM_to_plot_Nemoh = n.zeros(dim)
    results_AM_to_plot_Nemoh_indiv = n.zeros(dim)
    results_DAMP_to_plot_IT = n.zeros(dim)
    results_DAMP_to_plot_IT_3ev = n.zeros(dim)
    results_DAMP_to_plot_IT_6ev = n.zeros(dim)
    results_DAMP_to_plot_Nemoh = n.zeros(dim)
    results_DAMP_to_plot_Nemoh_indiv = n.zeros(dim)
    results_EXC_to_plot_IT = n.zeros(dim)
    results_EXC_to_plot_IT_3ev = n.zeros(dim)
    results_EXC_to_plot_IT_6ev = n.zeros(dim)
    results_EXC_to_plot_Nemoh = n.zeros(dim)
    results_EXC_to_plot_Nemoh_indiv = n.zeros(dim)
    
    for ind in range(0,dim):
        
         results_AM_to_plot_IT[ind]      = results_IT_AM[body][index_force,index_motion,ind]
         results_AM_to_plot_IT_3ev[ind]      = results_IT_AM_3ev[body][index_force,index_motion,ind]
         results_AM_to_plot_IT_6ev[ind]      = results_IT_AM_6ev[body][index_force,index_motion,ind]
         results_DAMP_to_plot_IT[ind]    = results_IT_DAMP[body][index_force,index_motion,ind]
         results_DAMP_to_plot_IT_3ev[ind]    = results_IT_DAMP_3ev[body][index_force,index_motion,ind]
         results_DAMP_to_plot_IT_6ev[ind]    = results_IT_DAMP_6ev[body][index_force,index_motion,ind]
         results_EXC_to_plot_IT[ind]     = abs(results_IT_exc[body][index_force,0,ind])
         results_EXC_to_plot_IT_3ev[ind]     = abs(results_IT_exc_3ev[body][index_force,0,ind])
         results_EXC_to_plot_IT_6ev[ind]     = abs(results_IT_exc_6ev[body][index_force,0,ind])
         
         results_AM_to_plot_Nemoh[ind]   = results_twocyl_AM[index_force+body*6,index_motion,ind]
         if (index_motion == (index_force+6*body)):
             if (index_motion > 5):
                 results_AM_to_plot_Nemoh_indiv[ind]   = results_indivcyl_AM[index_force,index_motion-6,ind]
                 results_DAMP_to_plot_Nemoh_indiv[ind]   = results_indivcyl_DAMP[index_force,index_motion-6,ind]
             else:
                 results_AM_to_plot_Nemoh_indiv[ind]   = results_indivcyl_AM[index_force,index_motion-6,ind]
                 results_DAMP_to_plot_Nemoh_indiv[ind]   = results_indivcyl_DAMP[index_force,index_motion-6,ind]
         results_DAMP_to_plot_Nemoh[ind] = results_twocyl_DAMP[index_force+body*6,index_motion,ind]
         results_EXC_to_plot_Nemoh[ind]  = results_twocyl_exc[0][ind,index_force+body*6,0]
         if ((index_force+body*6) > 5):
             results_EXC_to_plot_Nemoh_indiv[ind]  = results_indivcyl_exc[0][ind,index_force-body*6,0]
         else:
             results_EXC_to_plot_Nemoh_indiv[ind]  = results_indivcyl_exc[0][ind,index_force,0]
    
    
    fig1 = plt.figure()
    #plt.title('Added_Mass' + ' ' + str(index_force + 1) + ',' + str(index_motion + 1) + '(Kg)', fontsize=25)
    plt.xlabel('$\omega$ (rad/s)', fontsize=20)
    plt.ylabel('Added Mass'+' '+ str(index_force + 6*body + 1) + ',' + str(index_motion + 1) + '(Kg)',fontsize=20)
    plt.tick_params(axis='x', labelsize=20)
    plt.tick_params(axis='y', labelsize=20)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(frequencies, results_AM_to_plot_IT, '-bo', label='IT' + ' ' + 'L=0')
    plt.plot(frequencies, results_AM_to_plot_IT_3ev, '-r^', label='IT' + ' ' + 'L=3')
    #plt.plot(frequencies, results_AM_to_plot_IT_6ev, '-kd', label='IT' + ' ' + 'L=6')
    plt.plot(frequencies, results_AM_to_plot_Nemoh, '-gs', label='Nemoh')
    if (index_motion == (index_force+6*body)):
        plt.plot(frequencies, results_AM_to_plot_Nemoh_indiv, ':k', label='Nemoh (isolated)',linewidth=2)
    
    plt.show()

    plt.legend(loc='best', fontsize=20, frameon=False)
    plt.savefig('Added_Mass' + str(index_force + 6*body + 1) + ',' + str(index_motion + 1) +'.eps',bbox_inches='tight',pad_inches=3)
    
    
    fig2 = plt.figure()
    #plt.title('Damping' + ' ' + str(index_force + 1) + str(index_motion + 1) + '(N/ms)', fontsize=25)
    plt.xlabel('$\omega$ (rad/s)', fontsize=20)
    plt.ylabel('Damping'+' '+ str(index_force + 6*body + 1) + ',' + str(index_motion + 1) + '(N/m$\cdot$s)',fontsize=20)
    plt.tick_params(axis='x', labelsize=20)
    plt.tick_params(axis='y', labelsize=20)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(frequencies, results_DAMP_to_plot_IT, '-bo', label='IT' + ' ' + 'L=0')
    plt.plot(frequencies, results_DAMP_to_plot_IT_3ev, '-r^', label='IT' + ' ' + 'L=3')
    #plt.plot(frequencies, results_DAMP_to_plot_IT_6ev, '-kd', label='IT' + ' ' + 'L=6')
    plt.plot(frequencies, results_DAMP_to_plot_Nemoh, '-gs', label='Nemoh')
    if (index_motion == (index_force+6*body)):
        plt.plot(frequencies, results_DAMP_to_plot_Nemoh_indiv, ':k', label='Nemoh (isolated)',linewidth=2)
    
    plt.show()

    plt.legend(loc='best', fontsize=20, frameon=False)
    plt.savefig('Damping' + str(index_force + 6*body + 1) + ',' + str(index_motion + 1) +'.eps',bbox_inches='tight',pad_inches=3)
    
    indexs = ['Fx','Fy','Fz','Mx','My','Mz']
    fig3 = plt.figure()
    plt.title(r'$\beta$ ='+str(beta)+' '+'(deg)', fontsize=20)
    #plt.title(r'$\beta$', fontsize=20)
    plt.xlabel('$\omega$ (rad/s)', fontsize=20)
    if (index_force <= 2):
        plt.ylabel(indexs[index_force] + ' ' + 'body' + ' ' + str(body+1) + ' ' + '(N)',fontsize=20)
    else:
        plt.ylabel(indexs[index_force] + ' ' + 'body' + ' ' + str(body+1) + ' ' + '(N$\cdot$m)',fontsize=20)
    plt.tick_params(axis='x', labelsize=20)
    plt.tick_params(axis='y', labelsize=20)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(frequencies, results_EXC_to_plot_IT, '-bo', label='IT' + ' ' + 'L=0')
    plt.plot(frequencies, results_EXC_to_plot_IT_3ev, '-r^', label='IT' + ' ' + 'L=3')
    plt.plot(frequencies, results_EXC_to_plot_Nemoh, '-gs', label='Nemoh')
    plt.plot(frequencies, results_EXC_to_plot_Nemoh_indiv, ':k', label='Nemoh (isolated)',linewidth=2)
    
    plt.show()

    plt.legend(loc='best', fontsize=20, frameon=False)
    plt.savefig(indexs[index_force] + 'body' + str(body+1) +'.eps',bbox_inches='tight',pad_inches=3)
   
def compare_hydro_coeffs_separation_evmodes_several(results_vect_IT,results_vect_IT_ev1,results_vect_IT_ev2,results_vect_IT_ev3,results_vect_IT_ev4,results_vect_AM_Nemoh,results_vect_DAMP_Nemoh,envobj,char_dimension,body,ind_i,ind_j,x_min,x_max,case,ev_numb1, ev_numb2,ev_numb3,ev_numb4):
    
    distances = n.linspace(x_min, x_max, len(results_vect_IT))
    
    results_AM_to_plot_IT = n.zeros(len(results_vect_IT))
    results_AM_to_plot_IT_ev1 = n.zeros(len(results_vect_IT_ev1))
    results_AM_to_plot_IT_ev2 = n.zeros(len(results_vect_IT_ev2))
    results_AM_to_plot_IT_ev3 = n.zeros(len(results_vect_IT_ev3))
    results_AM_to_plot_IT_ev4 = n.zeros(len(results_vect_IT_ev4))
    results_AM_to_plot_Nemoh = n.zeros(len(results_vect_IT))
    results_DAMP_to_plot_IT = n.zeros(len(results_vect_IT))
    results_DAMP_to_plot_IT_ev1 = n.zeros(len(results_vect_IT))
    results_DAMP_to_plot_IT_ev2 = n.zeros(len(results_vect_IT))
    results_DAMP_to_plot_IT_ev3 = n.zeros(len(results_vect_IT))
    results_DAMP_to_plot_IT_ev4 = n.zeros(len(results_vect_IT))
    results_DAMP_to_plot_Nemoh = n.zeros(len(results_vect_IT))
    
    
    for ind in range(0,len(results_vect_IT)):
        
        results_AM_to_plot_IT[ind] =  (results_vect_IT[ind][0][body][ind_i - body*6 - 1,ind_j - 1,0]) 
        results_AM_to_plot_IT_ev1[ind] =  (results_vect_IT_ev1[ind][0][body][ind_i - body*6 - 1,ind_j - 1,0])
        results_AM_to_plot_IT_ev2[ind] =  (results_vect_IT_ev2[ind][0][body][ind_i - body*6 - 1,ind_j - 1,0])
        results_AM_to_plot_IT_ev3[ind] =  (results_vect_IT_ev3[ind][0][body][ind_i - body*6 - 1,ind_j - 1,0])
        results_AM_to_plot_IT_ev4[ind] =  (results_vect_IT_ev4[ind][0][body][ind_i - body*6 - 1,ind_j - 1,0])
        results_AM_to_plot_Nemoh[ind] = (results_vect_AM_Nemoh[ind][ind_i - 1,ind_j - 1,0]) 
        results_DAMP_to_plot_IT[ind] =  (results_vect_IT[ind][1][body][ind_i - body*6 - 1,ind_j - 1,0]) 
        results_DAMP_to_plot_IT_ev1[ind] =  (results_vect_IT_ev1[ind][1][body][ind_i - body*6 - 1,ind_j - 1,0]) 
        results_DAMP_to_plot_IT_ev2[ind] =  (results_vect_IT_ev2[ind][1][body][ind_i - body*6 - 1,ind_j - 1,0]) 
        results_DAMP_to_plot_IT_ev3[ind] =  (results_vect_IT_ev3[ind][1][body][ind_i - body*6 - 1,ind_j - 1,0]) 
        results_DAMP_to_plot_IT_ev4[ind] =  (results_vect_IT_ev4[ind][1][body][ind_i - body*6 - 1,ind_j - 1,0]) 
        results_DAMP_to_plot_Nemoh[ind] = (results_vect_DAMP_Nemoh[ind][ind_i - 1,ind_j - 1,0]) 
      
        
        
    fig1 = plt.figure()
    plt.title(r'$\lambda / a = $' + str(case), fontsize=20)
    plt.xlabel('d/a', fontsize=20)
    plt.ylabel('Added Mass'+' '+ str(ind_i) + ',' + str(ind_j) + ' ' + '(Kg)',fontsize=20)
    plt.tick_params(axis='x', labelsize=20)
    plt.tick_params(axis='y', labelsize=20)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(distances, results_AM_to_plot_IT, '-bo', label='IT' + ' ' + 'L=0')
    plt.plot(distances, results_AM_to_plot_IT_ev1, '-kv', label='IT' +' '+ 'L=' + str(ev_numb1))
    plt.plot(distances, results_AM_to_plot_IT_ev2, '-c^', label='IT' +' '+ 'L=' + str(ev_numb2))
    plt.plot(distances, results_AM_to_plot_IT_ev3, '-m*', label='IT' +' '+ 'L=' + str(ev_numb3))
    plt.plot(distances, results_AM_to_plot_IT_ev4, '-rs', label='IT' +' '+ 'L=' + str(ev_numb4))
    plt.plot(distances, results_AM_to_plot_Nemoh, '-gD', label='Nemoh')

    plt.legend(loc='best', fontsize=20, frameon=False)
    
    plt.savefig('AM'+' '+ str(ind_i) + ',' + str(ind_j)+'.eps',bbox_inches='tight',pad_inches=3)
    
    fig2 = plt.figure()
    plt.title(r'$\lambda / a = $' + str(case), fontsize=25)
    plt.xlabel('d/a', fontsize=25)
    plt.ylabel('Damping'+' '+ str(ind_i) + ',' + str(ind_j) + ' ' + '(N/m$\cdot$s)',fontsize=20)
    plt.tick_params(axis='x', labelsize=20)
    plt.tick_params(axis='y', labelsize=20)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(distances, results_DAMP_to_plot_IT, '-bo', label='IT' + ' ' + 'L=0')
    plt.plot(distances, results_DAMP_to_plot_IT_ev1, '-kv', label='IT' +' '+ 'L=' + str(ev_numb1))
    plt.plot(distances, results_DAMP_to_plot_IT_ev2, '-c^', label='IT' +' '+ 'L=' + str(ev_numb2))
    plt.plot(distances, results_DAMP_to_plot_IT_ev3, '-m*', label='IT' +' '+ 'L=' + str(ev_numb3))
    plt.plot(distances, results_DAMP_to_plot_IT_ev4, '-rs', label='IT' +' '+ 'L=' + str(ev_numb4))
    plt.plot(distances, results_DAMP_to_plot_Nemoh, '-gD', label='Nemoh')

    plt.legend(loc='best', fontsize=20, frameon=False)
    
    plt.savefig('DAMP'+' '+ str(ind_i) + ',' + str(ind_j)+'.eps',bbox_inches='tight',pad_inches=3)   
    
    
def compare_conditioning_one_freq(case,distances,cond_0_ev_modes,cond_3_ev_modes,\
                                  cond_6_ev_modes,cond_9_ev_modes):
    
    fig1 = plt.figure()
    ax = plt.subplot(111)
    plt.title(r'$\lambda / a = $' + str(case), fontsize=20)
    plt.xlabel('d/a', fontsize=20)
    plt.ylabel('Matrix Conditioning',fontsize=20)
    #ax.set_ylim([0, 400])
    plt.tick_params(axis='x', labelsize=20)
    plt.tick_params(axis='y', labelsize=20)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.semilogy(distances, cond_0_ev_modes, '-bo', label='L=0')
    plt.semilogy(distances, cond_3_ev_modes, '-ro', label='L=3')
    plt.semilogy(distances, cond_6_ev_modes, '-go', label='L=6')
    plt.semilogy(distances, cond_9_ev_modes, '-ko', label='L=9')

    plt.legend(loc='best', fontsize=20,frameon=False)
    
    plt.savefig('conditioning'+' ' + 'case' + str(case) +'.eps',bbox_inches='tight',pad_inches=3)
    
def compare_hydro_coeffs_separation_analytical(results_vect_IT_coarse,results_vect_IT_fine, \
                                                 results_vect_AM_Nemoh_coarse,results_vect_DAMP_Nemoh_coarse,\
                                                 results_vect_AM_Nemoh_fine,results_vect_DAMP_Nemoh_fine, \
                                                results_analytical_AM, results_analytical_DAMP,envobj,\
                                                char_dimension,body,ind_i,ind_j,x_min,x_max,case,ev_numb):
    
    distances = n.linspace(x_min, x_max, len(results_vect_IT_coarse))
    
    results_AM_to_plot_IT_coarse = n.zeros(len(results_vect_IT_coarse))
    results_AM_to_plot_IT_fine = n.zeros(len(results_vect_IT_fine))
    results_AM_to_plot_Nemoh_coarse = n.zeros(len(results_vect_IT_coarse))
    results_AM_to_plot_Nemoh_fine = n.zeros(len(results_vect_IT_coarse))
    results_DAMP_to_plot_IT_coarse = n.zeros(len(results_vect_IT_coarse))
    results_DAMP_to_plot_IT_fine = n.zeros(len(results_vect_IT_fine))
    results_DAMP_to_plot_Nemoh_coarse = n.zeros(len(results_vect_IT_coarse))
    results_DAMP_to_plot_Nemoh_fine= n.zeros(len(results_vect_IT_coarse))
    results_AM_analytical_to_plot = n.array(results_analytical_AM)
    results_DAMP_analytical_to_plot = n.array(results_analytical_DAMP)
    
    
    for ind in range(0,len(results_vect_IT_coarse)):
        
        results_AM_to_plot_IT_coarse[ind] =  (results_vect_IT_coarse[ind][0][body][ind_i - body*6 - 1,ind_j - 1,0]) 
        results_AM_to_plot_IT_fine[ind] =  (results_vect_IT_fine[ind][0][body][ind_i - body*6 - 1,ind_j - 1,0]) 
        results_AM_to_plot_Nemoh_coarse[ind] = (results_vect_AM_Nemoh_coarse[ind][ind_i - 1,ind_j - 1,0]) 
        results_AM_to_plot_Nemoh_fine[ind] = (results_vect_AM_Nemoh_fine[ind][ind_i - 1,ind_j - 1,0]) 
        results_DAMP_to_plot_IT_coarse[ind] =  (results_vect_IT_coarse[ind][1][body][ind_i - body*6 - 1,ind_j - 1,0]) 
        results_DAMP_to_plot_IT_fine[ind] =  (results_vect_IT_fine[ind][1][body][ind_i - body*6 - 1,ind_j - 1,0]) 
        results_DAMP_to_plot_Nemoh_coarse[ind] = (results_vect_DAMP_Nemoh_coarse[ind][ind_i - 1,ind_j - 1,0]) 
        results_DAMP_to_plot_Nemoh_fine[ind] = (results_vect_DAMP_Nemoh_fine[ind][ind_i - 1,ind_j - 1,0]) 
      
        
        
    fig1 = plt.figure()
    plt.title(r'$\lambda / a = $' + str(case), fontsize=20)
    plt.xlabel('d/a', fontsize=20)
    plt.ylabel('Added Mass'+' '+ str(ind_i) + ',' + str(ind_j) + ' ' + '(Kg)',fontsize=20)
    plt.tick_params(axis='x', labelsize=20)
    plt.tick_params(axis='y', labelsize=20)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(distances, results_AM_to_plot_IT_coarse, '-bo', label='IT' + ' ' + '(L=' + str(ev_numb)+','+' '+'361 panels)')
    plt.plot(distances, results_AM_to_plot_IT_fine, ':b^', label='IT' + ' ' + 'L=' + str(ev_numb)+','+' '+'1521 panels)')
    plt.plot(distances, results_AM_analytical_to_plot[:,body,ind_i - body*6 - 1, ind_j -1],'-rs',label='Zeng et al.(2013, L=50)')
    plt.plot(distances, results_AM_to_plot_Nemoh_coarse, '-gs', label='Nemoh (361 panels)')
    plt.plot(distances, results_AM_to_plot_Nemoh_fine, ':gd', label='Nemoh (1521 panels)')


    plt.legend(loc='best', fontsize=18, frameon=False, numpoints=1)
    
    plt.savefig('AM'+' '+ str(ind_i) + ',' + str(ind_j)+'.eps',bbox_inches='tight',pad_inches=3)
    
    fig2 = plt.figure()
    plt.title(r'$\lambda / a = $' + str(case), fontsize=20)
    plt.xlabel('d/a', fontsize=25)
    plt.ylabel('Damping'+' '+ str(ind_i) + ',' + str(ind_j) + ' ' + '(N/m$\cdot$s)',fontsize=20)
    plt.tick_params(axis='x', labelsize=20)
    plt.tick_params(axis='y', labelsize=20)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(distances, results_DAMP_to_plot_IT_coarse, '-bo', label='IT' + ' ' + '(L=' + str(ev_numb)+','+' '+'361 panels)')
    plt.plot(distances, results_DAMP_to_plot_IT_fine, ':b^', label='IT' + ' ' + '(L=' + str(ev_numb)+','+' '+'1521 panels)')
    plt.plot(distances, results_DAMP_analytical_to_plot[:,body,ind_i - body*6 - 1, ind_j -1],'-rs', label='Zeng et al.(2013, L=50)')
    plt.plot(distances, results_DAMP_to_plot_Nemoh_coarse, '-gs', label='Nemoh (361 panels)')
    plt.plot(distances, results_DAMP_to_plot_Nemoh_fine, ':gd', label='Nemoh (1521 panels)')

    plt.legend(loc='best', fontsize=18, frameon=False, numpoints=1)
    
    plt.savefig('DAMP'+' '+ str(ind_i) + ',' + str(ind_j)+'.eps',bbox_inches='tight',pad_inches=3)  
     
 
def plot_AM_DAMP_ensemble(k0,cyl_rad,AM_Nemoh_isol,DAMP_Nemoh_isol,IT_AM,IT_DAMP):
    
    
    fig, ax1 = plt.subplots()
    
    lns1 = ax1.plot(k0*cyl_rad, AM_Nemoh_isol, ':k',label='AM Nemoh',linewidth=2)
    lns2 = ax1.plot(k0*cyl_rad, IT_AM, '-g',label='AM IT (L=0)',linewidth=2)
    ax1.set_xlabel('$ka$', fontsize=20)
    ax1.set_ylabel('Added Mass 1,1 (kg)', fontsize=18)
    ax1.set_xlim([0, 2.5])
    ax1.get_legend()
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    #plt.legend(loc='center right', fontsize=16, frameon=False)
    
    ax2 = ax1.twinx()
    lns3 = ax2.plot(k0*cyl_rad, DAMP_Nemoh_isol, '-.k', label='DAMP Nemoh',linewidth=2)
    lns4 = ax2.plot(k0*cyl_rad, IT_DAMP, '-r', label='DAMP IT (L=0)',linewidth=2)
    ax2.set_ylabel('Damping 1,1 (N/ms)', fontsize=18)
    ax2.set_xlim([0, 2.5])
    
    lns = lns1 + lns2 + lns3 + lns4
    labs = [l.get_label() for l in lns]
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.legend(lns,labs,loc='upper right', fontsize=14, frameon=False)
    #fig.legend(loc='best', fontsize=16, frameon=False)
    plt.show()
    
def plot_AM_DAMP_separate(k0,cyl_rad,AM_Nemoh_isol,AM_Nemoh_5a,DAMP_Nemoh_isol,DAMP_Nemoh_5a,IT_AM_5a,IT_DAMP_5a):
    
    
    fig1= plt.figure()
    
    plt.plot(k0*cyl_rad, AM_Nemoh_isol, ':k',label='Nemoh (isolated)',linewidth=2)
    plt.plot(k0*cyl_rad, AM_Nemoh_5a, '-.g',label='Nemoh',linewidth=2)
    plt.plot(k0*cyl_rad, IT_AM_5a, '--b',label='IT (L=0)',linewidth=2)
    plt.xlabel('$ka$', fontsize=20)
    plt.ylabel('Added Mass 1,1 (kg)', fontsize=18)
    plt.xlim([0, 2.5])
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.legend(loc='best', fontsize=14, frameon=False)
    plt.show()
    
    fig2 = plt.figure()
    
    plt.plot(k0*cyl_rad, DAMP_Nemoh_isol, ':k',label='Nemoh (isolated)',linewidth=2)
    plt.plot(k0*cyl_rad, DAMP_Nemoh_5a, '-.g', label='Nemoh',linewidth=2)
    plt.plot(k0*cyl_rad, IT_DAMP_5a, '--b', label='IT (L=0)',linewidth=2)
    plt.xlabel('$ka$', fontsize=20)
    plt.ylabel('Damping 1,1 (N/ms)', fontsize=18)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.xlim([0, 2.5])
    plt.legend(loc='best', fontsize=14, frameon=False)
    plt.show()
    
############################################################################### 
############################################################################### 
############################FREE SURFACE PLOT##################################
############################################################################### 
############################################################################### 
    
def plot_eta_radiation(eta, freesurf_class, dof, omega):  
    
        # Calculate absolute valule of free surface elevation
        #----------------------------------------------------------------------
        Z = abs(eta)
        
        # Plot
        #----------------------------------------------------------------------
        # Set ranges of plot
        #------------------------------
        n_levels = 10
        
        levels = []
    
        step = n.nanmax(Z) / n_levels
        
        for ind in range(0, n_levels+1):
            levels.append(ind*step)
            
        # Set plot properties
        #------------------------------    
        cmap = plt.cm.get_cmap("jet")
        plt.figure()
        CS = plt.contourf(freesurf_class.X, freesurf_class.Y, Z, levels, cmap=cmap)
        plt.title('Radiation' + ' ' + '($dof=$' + str(dof) + ' '+ ',' + ' ' + r'$\omega= $'  + str(round(omega,1)) + '$rad/s$' + ')', fontsize=20)
        plt.xlabel('$x$' + ' ' + '$(m)$', fontsize=18)
        plt.ylabel('$y$' + ' ' + '$(m)$', fontsize=18)
        #plt.tick_params(axis='x', labelsize=20)
        #plt.tick_params(axis='y', labelsize=20)
        plt.gca().set_aspect('equal', adjustable='box')
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("right", "5%", pad="3%")

        cbar = plt.colorbar(CS, cax=cax, format='%1.1f')
        cbar.ax.set_ylabel('$|\eta / A|$', fontsize=18)
        
        plt.show()
        
        plt.savefig('eta_radiation'+'_'+ 'dof' + str(dof) + 'omega' + str(omega) +'.eps',bbox_inches='tight',pad_inches=3)  
        
        
def plot_eta_diffraction(eta, freesurf_class, beta, omega):       
    
        # Calculate absolute valule of free surface elevation
        #----------------------------------------------------------------------
        Z = abs(eta)
        
        # Plot
        #----------------------------------------------------------------------
        # Set ranges of plot
        #------------------------------
        n_levels = 10
        
        levels = []
    
        step = n.nanmax(Z) / n_levels
        
        for ind in range(0, n_levels+1):
            levels.append(ind*step)
            
        # Set plot properties
        #------------------------------    
        cmap = plt.cm.get_cmap("jet")
        plt.figure()
        CS = plt.contourf(freesurf_class.X, freesurf_class.Y, Z, levels, cmap=cmap)
        plt.title('Diffraction' + ' ' + r'$(\beta= $' + str(round(m.degrees(beta),1)) + '$^\circ$' + ',' + ' ' + r'$\omega= $' + str(round(omega,1)) + '$rad/s$' + '$)$', fontsize=20)
        plt.xlabel('$x$' + ' ' + '$(m)$', fontsize=18)
        plt.ylabel('$y$' + ' ' + '$(m)$', fontsize=18)
        #plt.tick_params(axis='x', labelsize=20)
        #plt.tick_params(axis='y', labelsize=20)
        plt.gca().set_aspect('equal', adjustable='box')
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("right", "5%", pad="3%")

        cbar = plt.colorbar(CS, cax=cax, format='%1.1f')
        cbar.ax.set_ylabel('$|\eta / A|$', fontsize=20)
        
        plt.show()
        
        plt.savefig('eta_diffraction'+'_'+ 'beta' + str(round(m.degrees(beta),1))  + 'omega' + str(omega) +'.eps',bbox_inches='tight',pad_inches=3)  
        
        
        
def plot_eta_total(eta, freesurf_class, beta, case, wave_amp):       
    
        # Calculate absolute valule of free surface elevation
        #----------------------------------------------------------------------
        Z = abs(eta) / wave_amp
        
        # Plot
        #----------------------------------------------------------------------
        # Set ranges of plot
        #------------------------------
        n_levels = 10
        
        levels = []
    
        step = n.nanmax(Z) / n_levels
        
        for ind in range(0, n_levels+1):
            levels.append(ind*step)
            
        # Set plot properties
        #------------------------------    
        cmap = plt.cm.get_cmap("jet")
        plt.figure()
        CS = plt.contourf(freesurf_class.X, freesurf_class.Y, Z, levels, cmap=cmap)
        #plt.title(r'$(\beta= $' + str(round(m.degrees(beta),1)) + '$^\circ$' + ',' + ' ' + r'$\lambda / a = $' + str(round(case,1)) + ')', fontsize=20)
        plt.xlabel('$x$' + ' ' + '$(m)$', fontsize=18)
        plt.ylabel('$y$' + ' ' + '$(m)$', fontsize=18)
        #plt.tick_params(axis='x', labelsize=20)
        #plt.tick_params(axis='y', labelsize=20)
        plt.gca().set_aspect('equal', adjustable='box')
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("right", "5%", pad="3%")

        cbar = plt.colorbar(CS, cax=cax, format='%1.1f')
        cbar.ax.set_ylabel('$|\eta / A|$', fontsize=20)
        
        plt.show(block=True)
        
        plt.savefig('eta_total'+'_'+'beta' + str(round(m.degrees(beta),1))  + 'case' + str(round(case,1)) +'.eps',bbox_inches='tight',pad_inches=3)  
        
        
def plot_eta_total_newformat(eta, freesurf_class):       
    
        # Calculate absolute valule of free surface elevation
        #----------------------------------------------------------------------
        Z = n.vstack(n.split(abs(eta),freesurf_class.X.shape[0]))
        
        Z[freesurf_class.interior] = n.nan
        
        # Plot
        #----------------------------------------------------------------------
        # Set ranges of plot
        #------------------------------
        n_levels = 10
        
        levels = []
    
        step = n.nanmax(Z) / n_levels
        
        for ind in range(0, n_levels+1):
            levels.append(ind*step)
            
        # Set plot properties
        #------------------------------    
        cmap = plt.cm.get_cmap("jet")
        plt.figure()
        CS = plt.contourf(freesurf_class.X, freesurf_class.Y, Z, levels, cmap=cmap)
        #plt.title(r'$(\beta= $' + str(round(m.degrees(beta),1)) + '$^\circ$' + ',' + ' ' + r'$\lambda / a = $' + str(round(case,1)) + ')', fontsize=20)
        plt.xlabel('$x$' + ' ' + '$(m)$', fontsize=18)
        plt.ylabel('$y$' + ' ' + '$(m)$', fontsize=18)
        #plt.tick_params(axis='x', labelsize=20)
        #plt.tick_params(axis='y', labelsize=20)
        plt.gca().set_aspect('equal', adjustable='box')
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("right", "5%", pad="3%")

        cbar = plt.colorbar(CS, cax=cax, format='%1.1f')
        cbar.ax.set_ylabel('$|\eta / A|$', fontsize=20)
        
        plt.show(block=True)
        
        plt.savefig('eta_total'+'.eps',bbox_inches='tight',pad_inches=3)
        
        
def plot_disturbance_coefficient(eta, freesurf_class):       
    
        # Calculate absolute valule of free surface elevation
        #----------------------------------------------------------------------
        Z = n.vstack(n.split(abs(eta),freesurf_class.X.shape[0]))
        
        Z[freesurf_class.interior] = n.nan
        
        # Plot
        #----------------------------------------------------------------------
        # Set ranges of plot
        #------------------------------
        n_levels = 10
        
        levels = []
    
        step = (n.nanmax(Z) - n.nanmin(Z))/ n_levels
        
        for ind in range(0, n_levels+1):
            levels.append(n.nanmin(Z) + ind*step)
            
        # Set plot properties
        #------------------------------    
        cmap = plt.cm.get_cmap("jet")
        plt.figure()
        CS = plt.contourf(freesurf_class.X, freesurf_class.Y, Z, levels, cmap=cmap)
        #plt.title(r'$(\beta= $' + str(round(m.degrees(beta),1)) + '$^\circ$' + ',' + ' ' + r'$\lambda / a = $' + str(round(case,1)) + ')', fontsize=20)
        plt.xlabel('$x$' + ' ' + '$(m)$', fontsize=18)
        plt.ylabel('$y$' + ' ' + '$(m)$', fontsize=18)
        plt.locator_params(axis = 'x', nbins = 6)
        plt.locator_params(axis = 'y', nbins = 6)
        #plt.tick_params(axis='x', labelsize=20)
        #plt.tick_params(axis='y', labelsize=20)
        plt.gca().set_aspect('equal', adjustable='box')
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("right", "5%", pad="3%")

        cbar = plt.colorbar(CS, cax=cax, format='%1.3f')
        cbar.ax.set_ylabel('$|H_s / H_s^I|$', fontsize=20)
        
        plt.show(block=True)
        
#        fig = plt.gcf()
#        fig.set_size_inches(10,10)
#        fig.savefig('test.eps', dpi=800)
#        fig.set_size_inches(10.0, 10.0, forward=True)
        
        plt.savefig('eta_total'+'.eps',bbox_inches='tight',pad_inches=3)
        
        
        
def plot_eta_total_name(eta, name, freesurf_class, beta, case, wave_amp):       
    
        # Calculate absolute valule of free surface elevation
        #----------------------------------------------------------------------
        Z = abs(eta) / wave_amp
        
#        if (name=='nemoh'):
#            for ind in range(0,len(freesurf_class.interior)):
#                Z[freesurf_class.interior[ind]] = n.nan
        for ind in range(0,len(freesurf_class.interior)):
                Z[freesurf_class.interior] = n.nan
        
        # Plot
        #----------------------------------------------------------------------
        # Set ranges of plot
        #------------------------------
        n_levels = 10
        
        levels = []
    
        step = n.nanmax(Z) / n_levels
        
        for ind in range(0, n_levels+1):
            levels.append(ind*step)
            
        # Set plot properties
        #------------------------------    
        cmap = plt.cm.get_cmap("jet")
        plt.figure()
        CS = plt.contourf(freesurf_class.X, freesurf_class.Y, Z, levels, cmap=cmap)
        #plt.title(r'$(\beta= $' + str(round(m.degrees(beta),1)) + '$^\circ$' + ',' + ' ' + r'$\lambda / a = $' + str(round(case,1)) + ')', fontsize=20)
        plt.xlabel('$x$' + ' ' + '$(m)$', fontsize=18)
        plt.ylabel('$y$' + ' ' + '$(m)$', fontsize=18)
        #plt.tick_params(axis='x', labelsize=20)
        #plt.tick_params(axis='y', labelsize=20)
        plt.gca().set_aspect('equal', adjustable='box')
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("right", "5%", pad="3%")

        cbar = plt.colorbar(CS, cax=cax, format='%1.1f')
        cbar.ax.set_ylabel('$|\eta / A|$', fontsize=20)
        
        plt.show(block=True)
        
        plt.savefig('eta_total'+'_'+name+'beta' + str(round(m.degrees(beta),1))  + 'case' + str(round(case,1)) +'.eps',bbox_inches='tight',pad_inches=3)  
        
        
def plot_eta_total_comparison(eta_it, eta_nemoh, freesurf_class, beta,case, wave_amp):
    
        # Calculate absolute valule of free surface elevation
        #----------------------------------------------------------------------
        Z_it    = eta_it 
        Z_nemoh = eta_nemoh 
        
        Z = (abs(Z_nemoh - Z_it) / wave_amp) * 100
        #Z = ((abs(Z_nemoh) - abs(Z_it)) / wave_amp) * 100
        
        for ind in range(0,len(freesurf_class.interior)):
                Z[freesurf_class.interior] = n.nan
        
        # Plot
        #----------------------------------------------------------------------
        # Set ranges of plot
        #------------------------------
        n_levels = 10
        
        levels = []
    
        step = n.nanmax(Z) / n_levels
        
        for ind in range(0, n_levels+1):
            levels.append(ind*step)
            
        # Set plot properties
        #------------------------------    
        cmap = plt.cm.get_cmap("jet")
        plt.figure()
        CS = plt.contourf(freesurf_class.X, freesurf_class.Y, Z, levels, cmap=cmap)
        #plt.title(r'$(\beta= $' + str(round(m.degrees(beta),1)) + '$^\circ$' + ',' + ' ' + r'$\lambda / a = $' + str(round(case,1)) + ')', fontsize=20)
        plt.xlabel('$x$' + ' ' + '$(m)$', fontsize=18)
        plt.ylabel('$y$' + ' ' + '$(m)$', fontsize=18)
        #plt.tick_params(axis='x', labelsize=20)
        #plt.tick_params(axis='y', labelsize=20)
        plt.gca().set_aspect('equal', adjustable='box')
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("right", "5%", pad="3%")

        cbar = plt.colorbar(CS, cax=cax, format='%1.1f')
        cbar.ax.set_ylabel('$|\eta^{N} - \eta^{IT}| / A\,(\%)$', fontsize=20)
        
        plt.show(block=True)
        
        plt.savefig('eta_total_comparison'+'_'+ 'beta' + str(round(m.degrees(beta),1))  + 'case' + str(round(case,1)) +'.eps',bbox_inches='tight',pad_inches=3)  
        
def plot_eta_total_comparison_cartopol(eta_it, eta_nemoh, freesurf_class, gamma, wave_amp, max_level):
    
        # Calculate absolute valule of free surface elevation
        #----------------------------------------------------------------------
        Z_it    = eta_it 
        Z_nemoh = eta_nemoh 
        
        Z = (abs(Z_nemoh - Z_it) / wave_amp) * 100
        #Z = ((abs(Z_nemoh) - abs(Z_it)) / wave_amp) * 100
        
        for ind in range(0,len(freesurf_class.interior)):
                Z[freesurf_class.interior] = n.nan
        
        # Plot
        #----------------------------------------------------------------------
        # Set ranges of plot
        #------------------------------
        n_levels = 10
        
        levels = []
    
        step = max_level / n_levels
        
        for ind in range(0, n_levels+1):
            levels.append(ind*step)
            
        # Set plot properties
        #------------------------------    
        cmap = plt.cm.get_cmap("jet")
        #cmap.set_over('jet')
        plt.figure()
        CS = plt.contourf(freesurf_class.X, freesurf_class.Y, Z, levels, cmap=cmap, extend="both")
        
        #plt.title(r'$(\beta= $' + str(round(m.degrees(beta),1)) + '$^\circ$' + ',' + ' ' + r'$\lambda / a = $' + str(round(case,1)) + ')', fontsize=20)
        plt.xlabel('$x$' + ' ' + '$(m)$', fontsize=24)
        plt.ylabel('$y$' + ' ' + '$(m)$', fontsize=24)
        plt.tick_params(axis='x', labelsize=20)
        plt.tick_params(axis='y', labelsize=20)
        plt.gca().set_aspect('equal', adjustable='box')
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("right", "5%", pad="3%")

        cbar = plt.colorbar(CS, cax=cax, format='%1.1f')
        cbar.ax.set_ylabel('$|\eta^{PC} - \eta^{IT}|/\eta^{IT} \,(\%)$', fontsize=24)
        cbar.ax.tick_params(labelsize=20)
        
        plt.show(block=True)
        
        plt.savefig('eta_total_comparison'+'_'+ 'gamma' + str(gamma) + '.eps',bbox_inches='tight')  
        
        
def plot_eta_total_name_cartopol_real(eta, name, freesurf_class, wave_amp, gamma):       
    
        # Calculate absolute valule of free surface elevation
        #----------------------------------------------------------------------
        Z = abs(eta) / wave_amp
        
#        if (name=='nemoh'):
#            for ind in range(0,len(freesurf_class.interior)):
#                Z[freesurf_class.interior[ind]] = n.nan
        for ind in range(0,len(freesurf_class.interior)):
                Z[freesurf_class.interior] = n.nan
        
        # Plot
        #----------------------------------------------------------------------
        # Set ranges of plot
        #------------------------------
        n_levels = 10
        
        levels = []
    
        step = n.nanmax(Z) / n_levels
        
        for ind in range(0, n_levels+1):
            levels.append(ind*step)
            
        # Set plot properties
        #------------------------------    
        cmap = plt.cm.get_cmap("jet")
        plt.figure()
        CS = plt.contourf(freesurf_class.X, freesurf_class.Y, Z, levels, cmap=cmap)
        #plt.title(r'$(\beta= $' + str(round(m.degrees(beta),1)) + '$^\circ$' + ',' + ' ' + r'$\lambda / a = $' + str(round(case,1)) + ')', fontsize=20)
        plt.xlabel('$x$' + ' ' + '$(m)$', fontsize=18)
        plt.ylabel('$y$' + ' ' + '$(m)$', fontsize=18)
        #plt.tick_params(axis='x', labelsize=20)
        #plt.tick_params(axis='y', labelsize=20)
        plt.gca().set_aspect('equal', adjustable='box')
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("right", "5%", pad="3%")

        cbar = plt.colorbar(CS, cax=cax, format='%1.1f')
        cbar.ax.set_ylabel('$Re\{|\eta / A|\}$', fontsize=20)
        
        plt.show(block=True)
        
        plt.savefig('eta_scattered'+'_'+name +'.eps',bbox_inches='tight',pad_inches=3) 
        
        
def plot_eta_total_name_cartopol_imag(eta, name, freesurf_class, wave_amp, gamma):       
    
        # Calculate absolute valule of free surface elevation
        #----------------------------------------------------------------------
        Z = abs(eta) / wave_amp
        
#        if (name=='nemoh'):
#            for ind in range(0,len(freesurf_class.interior)):
#                Z[freesurf_class.interior[ind]] = n.nan
        for ind in range(0,len(freesurf_class.interior)):
                Z[freesurf_class.interior] = n.nan
        
        # Plot
        #----------------------------------------------------------------------
        # Set ranges of plot
        #------------------------------
        n_levels = 10
        
        levels = []
    
        step = n.nanmax(Z) / n_levels
        
        for ind in range(0, n_levels+1):
            levels.append(ind*step)
            
        # Set plot properties
        #------------------------------    
        cmap = plt.cm.get_cmap("jet")
        plt.figure()
        CS = plt.contourf(freesurf_class.X, freesurf_class.Y, Z, levels, cmap=cmap)
        #plt.title(r'$(\beta= $' + str(round(m.degrees(beta),1)) + '$^\circ$' + ',' + ' ' + r'$\lambda / a = $' + str(round(case,1)) + ')', fontsize=20)
        plt.xlabel('$x$' + ' ' + '$(m)$', fontsize=18)
        plt.ylabel('$y$' + ' ' + '$(m)$', fontsize=18)
        #plt.tick_params(axis='x', labelsize=20)
        #plt.tick_params(axis='y', labelsize=20)
        plt.gca().set_aspect('equal', adjustable='box')
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("right", "5%", pad="3%")

        cbar = plt.colorbar(CS, cax=cax, format='%1.1f')
        cbar.ax.set_ylabel('$Imag\{|\eta / A|\}$', fontsize=20)
        
        plt.show(block=True)
        
        plt.savefig('eta_scattered'+'_'+name +'.eps',bbox_inches='tight',pad_inches=3) 
        
        
def plot_eta_total_name_cartopol_abs(eta, name, freesurf_class, wave_amp, gamma):       
    
        # Calculate absolute valule of free surface elevation
        #----------------------------------------------------------------------
        Z = abs(eta) / wave_amp
        
#        if (name=='nemoh'):
#            for ind in range(0,len(freesurf_class.interior)):
#                Z[freesurf_class.interior[ind]] = n.nan
        for ind in range(0,len(freesurf_class.interior)):
                Z[freesurf_class.interior] = n.nan
        
        # Plot
        #----------------------------------------------------------------------
        # Set ranges of plot
        #------------------------------
        n_levels = 10
        
        levels = []
    
        step = n.nanmax(Z) / n_levels
        
        for ind in range(0, n_levels+1):
            levels.append(ind*step)
            
        # Set plot properties
        #------------------------------    
        cmap = plt.cm.get_cmap("jet")
        plt.figure()
        CS = plt.contourf(freesurf_class.X, freesurf_class.Y, Z, levels, cmap=cmap)
        #plt.title(r'$(\beta= $' + str(round(m.degrees(beta),1)) + '$^\circ$' + ',' + ' ' + r'$\lambda / a = $' + str(round(case,1)) + ')', fontsize=20)
        plt.xlabel('$x$' + ' ' + '$(m)$', fontsize=24)
        plt.ylabel('$y$' + ' ' + '$(m)$', fontsize=24)
        plt.tick_params(axis='x', labelsize=20)
        plt.tick_params(axis='y', labelsize=20)
        plt.gca().set_aspect('equal', adjustable='box')
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("right", "5%", pad="3%")

        cbar = plt.colorbar(CS, cax=cax, format='%1.2f')
        cbar.ax.set_ylabel('$|\eta|$', fontsize=24)
        cbar.ax.tick_params(labelsize=20)
        
        plt.show(block=True)
        
        plt.savefig('eta_scattered'+'_'+name +'.eps',bbox_inches='tight') 
        
    
def create_2d_variable(Tp_base, Hs_base, Tp_values, Hs_values, z_var_values):
    
    (Tp,Hs) = n.meshgrid(Tp_base,Hs_base)
    
    P = n.zeros((Tp.shape[0], Tp.shape[1]), dtype = float)

    for ind in range(0, len(Tp_values)):
        
        Tp_val = Tp_values[ind]
        Hs_val = Hs_values[ind]
        
        for ind_Tp in range(0, Tp.shape[0]):
            for ind_Hs in range(0, Tp.shape[1]):
            
                if ((Tp_val == Tp[ind_Tp, ind_Hs]) & (Hs_val == Hs[ind_Tp, ind_Hs])):
                    
                    P[ind_Tp, ind_Hs] = z_var_values[ind]

    return P
    
def plot_scatter_plot_wave_climate(x, y, z):       
    
        # Set ranges of plot
        #------------------------------
        n_levels = 10
        
        levels = []
    
        step = n.nanmax(z/100) / n_levels
        
        for ind in range(0, n_levels+1):
            levels.append(ind*step)
            
        # Set plot properties
        #------------------------------    
        cmap = plt.cm.get_cmap("jet")
        plt.figure()

        CS = plt.contourf(x, y, z/100, levels, cmap=cmap)
        #plt.title(r'$(\beta= $' + str(round(m.degrees(beta),1)) + '$^\circ$' + ',' + ' ' + r'$\lambda / a = $' + str(round(case,1)) + ')', fontsize=20)
        
        plt.xlabel('$Tp$' + ' ' + '$(s)$', fontsize=18)
        plt.ylabel('$Hs$' + ' ' + '$(m)$', fontsize=18)
        #plt.tick_params(axis='x', labelsize=20)
        #plt.tick_params(axis='y', labelsize=20)
        #plt.gca().set_aspect('equal', adjustable='box')
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("right", "5%", pad="3%")

        cbar = plt.colorbar(CS, cax=cax, format='%1.3f')
        cbar.ax.set_ylabel('$C(H_s, T_p)$', fontsize=20)
        
        plt.tight_layout()
        
        plt.show(block=True)
        
def plot_scatter_plot_powerseastates(x, y, z):       
    
        # Set ranges of plot
        #------------------------------
        n_levels = 10
        
        levels = []
    
        step = n.nanmax(z/1000) / n_levels
        
        for ind in range(0, n_levels+1):
            levels.append(ind*step)
            
        # Set plot properties
        #------------------------------    
        cmap = plt.cm.get_cmap("jet")
        plt.figure()

        CS = plt.contourf(x, y, z/1000, levels, cmap=cmap)
        #plt.title(r'$(\beta= $' + str(round(m.degrees(beta),1)) + '$^\circ$' + ',' + ' ' + r'$\lambda / a = $' + str(round(case,1)) + ')', fontsize=20)
        
        plt.xlabel('$Tp$' + ' ' + '$(s)$', fontsize=18)
        plt.ylabel('$Hs$' + ' ' + '$(m)$', fontsize=18)
        #plt.tick_params(axis='x', labelsize=20)
        #plt.tick_params(axis='y', labelsize=20)
        #plt.gca().set_aspect('equal', adjustable='box')
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("right", "5%", pad="3%")

        cbar = plt.colorbar(CS, cax=cax, format='%.0e')
        cbar.ax.set_ylabel('$P$' + ' ' + '$(KW)$', fontsize=20)
        
        plt.tight_layout()
        
        plt.show(block=True)
        
def plot_scatter_plot_bpto(x, y, z):       
    
        # Set ranges of plot
        #------------------------------
        n_levels = 10
        
        levels = []
    
        step = n.nanmax(z/1000) / n_levels
        
        for ind in range(0, n_levels+1):
            levels.append(ind*step)
            
        # Set plot properties
        #------------------------------    
        cmap = plt.cm.get_cmap("jet")
        plt.figure()

        CS = plt.contourf(x, y, z/1000, levels, cmap=cmap)
        #plt.title(r'$(\beta= $' + str(round(m.degrees(beta),1)) + '$^\circ$' + ',' + ' ' + r'$\lambda / a = $' + str(round(case,1)) + ')', fontsize=20)
        
        plt.xlabel('$Tp$' + ' ' + '$(s)$', fontsize=18)
        plt.ylabel('$Hs$' + ' ' + '$(m)$', fontsize=18)
        #plt.tick_params(axis='x', labelsize=20)
        #plt.tick_params(axis='y', labelsize=20)
        #plt.gca().set_aspect('equal', adjustable='box')
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("right", "5%", pad="3%")

        cbar = plt.colorbar(CS, cax=cax, format='%.0e')
        cbar.ax.set_ylabel('$Bpto$' + ' ' + '$(KN / m \cdot s)$', fontsize=20)
        
        plt.tight_layout()
        
        plt.show(block=True)



def plot_trans_coeff(trans_coeff, Ng, Na):
    
        angles = n.linspace(-n.pi/2.0, n.pi/2.0, Na)
        
        fig = plt.figure()
        ax  = fig.add_subplot(111)
        if (Ng == 0):
            ax.plot(angles, abs(trans_coeff)**2, linewidth=2)
        else:
            ax.plot(angles, abs(trans_coeff[Ng - 1 : Ng - 1 + Na])**2, linewidth=2)
        
        x_tick = n.linspace(-n.pi/2., n.pi/2., 3)

        x_label = [r"$-\frac{\pi}{2}$", r"$0$", r"$+\frac{\pi}{2}$"]
        ax.set_xticks(x_tick)
        ax.set_xticklabels(x_label, fontsize=24)
        
        plt.xlabel('$\chi \, (rad)$', fontsize=24)
        plt.ylabel('$|A^{T}(\chi)|^{2}$', fontsize=24)
        plt.tick_params(axis='x', labelsize=20)
        plt.tick_params(axis='y', labelsize=20)
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        plt.gcf().subplots_adjust(bottom=0.15)
        #plt.ylim((0,1.5))
        
        #plt.legend(loc='best', fontsize=18, frameon=False, ncol = 2)
        
        plt.savefig('amp_scatt'+'.eps',bbox_inches='tight') 
        

def plot_ref_coeff(trans_coeff, Ng, Na):
    
        angles = n.linspace(-n.pi/2.0, n.pi/2.0, Na)
        
        fig = plt.figure()
        ax  = fig.add_subplot(111)
        if (Ng == 0):
            ax.plot(angles, abs(trans_coeff)**2, linewidth=2)
        else:
            ax.plot(angles, abs(trans_coeff[Ng - 1 : Ng - 1 + Na])**2, linewidth=2)
        
        x_tick = n.linspace(-n.pi/2., n.pi/2., 3)

        x_label = [r"$-\frac{\pi}{2}$", r"$0$", r"$+\frac{\pi}{2}$"]
        ax.set_xticks(x_tick)
        ax.set_xticklabels(x_label, fontsize=24)
        
        plt.xlabel('$\chi \, (rad)$', fontsize=24)
        plt.ylabel('$|A^{R}(\chi)|^{2}$', fontsize=24)
        plt.tick_params(axis='x', labelsize=20)
        plt.tick_params(axis='y', labelsize=20)
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        plt.gcf().subplots_adjust(bottom=0.15)
        
        plt.savefig('amp_ref'+'.eps',bbox_inches='tight')
        
        
def plot_trans_coeff_conv(trunc_vect, nfreqs, info_data, data):
    
        plt.figure()
        for ind in range(0, nfreqs):
            
            plt.plot(trunc_vect, data[:,ind], label = info_data[ind], linewidth=2)
        
        
        plt.xlabel('$N_t$', fontsize=20)
        plt.ylabel('$T$', fontsize=20)
        plt.tick_params(axis='x', labelsize=18)
        plt.tick_params(axis='y', labelsize=18)
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        plt.ylim((0,1.5))
        
        plt.legend(loc='best', fontsize=18, frameon=False, ncol = 2)
        
def plot_ref_coeff_conv(trunc_vect, nfreqs, info_data, data):
    
        plt.figure()
        for ind in range(0, nfreqs):
            
            plt.plot(trunc_vect, data[:,ind], label = info_data[ind], linewidth=2)
        
        plt.xlabel('$N_t$', fontsize=20)
        plt.ylabel('$R$', fontsize=20)
        plt.tick_params(axis='x', labelsize=18)
        plt.tick_params(axis='y', labelsize=18)
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        plt.ylim((0,1.5))
        
        plt.legend(loc='best', fontsize=18, frameon=False, ncol = 2)

        
def plot_floaters_wavestar_lin(cyl_rad,nfloaters,x,y,dy):

        circles = []

        for ind in range(0,x.shape[0]):
            circles.append(plt.Circle([x[ind], y[ind]], cyl_rad)) 
            
        col = PatchCollection(circles)

        col.set(array=n.array(x), cmap='jet')

        fig, ax = plt.subplots()

        ax.add_collection(col)
        
        ymax = max(y)
        xmax = max(x)
        xmin = min(x)
        #ax.autoscale()
        plt.ylim((-(ymax +2*dy),ymax+2*dy))
        plt.xlim((-(xmin + dy*2),xmax + dy*2))
        plt.xlabel('$x$' + ' ' + '$(m)$', fontsize=15)
        plt.ylabel('$y$' + ' ' + '$(m)$', fontsize=15)
        plt.locator_params(axis = 'y', nbins = 6)
        plt.locator_params(axis = 'x', nbins = 8)
        
        v = n.linspace(xmin, xmax, 10, endpoint=True)
        #ax.axis('equal')
        plt.gca().set_aspect('equal', adjustable='box')
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("top", "10%", pad="10%")
        #cbaxes = fig.add_axes([0.8, 0.1, 0.03, 0.8])   
        cbar=fig.colorbar(col,orientation="horizontal",cax=cax,format='%1.1f',ticklocation='top',ticks=v)
        cbar.ax.set_xlabel('$x location (m)$', fontsize=15)
        plt.tight_layout()
        plt.show()
        
def plot_floaters_wavestar_star(cyl_rad,nfloaters,x,y,dy):

        circles = []

        for ind in range(0,x.shape[0]):
            circles.append(plt.Circle([x[ind], y[ind]], cyl_rad)) 
            
        col = PatchCollection(circles)

        col.set(array=n.array(x), cmap='jet')

        fig, ax = plt.subplots()

        ax.add_collection(col)
        
        ymax = max(y)
        xmax = max(x)
        xmin = min(x)
        #ax.autoscale()
        plt.ylim((-(ymax +2*dy),ymax+2*dy))
        plt.xlim((-(xmin + dy*2),xmax + dy*2))
        plt.xlabel('$x$' + ' ' + '$(m)$', fontsize=15)
        plt.ylabel('$y$' + ' ' + '$(m)$', fontsize=15)
        plt.locator_params(axis = 'x', nbins = 6)
        plt.locator_params(axis = 'y', nbins = 6)
        #plt.axis('off')
        
        v = n.linspace(xmin, xmax, 10, endpoint=True)
        #ax.axis('equal')
        plt.gca().set_aspect('equal', adjustable='box')
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("right", "5%", pad="3%")
        #cbaxes = fig.add_axes([0.8, 0.1, 0.03, 0.8])   
        cbar=fig.colorbar(col,cax=cax,format='%1.1f',ticks=v)
        cbar.ax.set_ylabel('$x location (m)$', fontsize=15)
        plt.tight_layout()
        plt.show()
        
        plt.savefig('wavestar_star'+'.eps',bbox_inches='tight',pad_inches=3) 
        
        
def plot_floaters_wavestar_star_aggregation(cyl_rad,nfloaters,x,y,dy):

        circles = []

        for ind in range(0,x.shape[0]):
            circles.append(plt.Circle([x[ind], y[ind]], cyl_rad)) 
            
        col = PatchCollection(circles)

        col.set(array=n.array(x), cmap='jet')

        fig, ax = plt.subplots()

        ax.add_collection(col)
        
        ymax = max(y)
        xmax = max(x)
        xmin = min(x)
        ymin = min(y)
        #ax.autoscale()
        plt.ylim((ymin - 2*dy,ymax+2*dy))
        plt.xlim((-(xmin + dy*2),xmax + dy*2))
        plt.xlabel('$x$' + ' ' + '$(m)$', fontsize=15)
        plt.ylabel('$y$' + ' ' + '$(m)$', fontsize=15)
        plt.locator_params(axis = 'x', nbins = 6)
        plt.locator_params(axis = 'y', nbins = 6)
        #plt.axis('off')
        
        v = n.linspace(xmin, xmax, 10, endpoint=True)
        #ax.axis('equal')
        plt.gca().set_aspect('equal', adjustable='box')
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("right", "5%", pad="3%")
        #cbaxes = fig.add_axes([0.8, 0.1, 0.03, 0.8])   
        cbar=fig.colorbar(col,cax=cax,format='%1.1f',ticks=v)
        cbar.ax.set_ylabel('$x location (m)$', fontsize=15)
        plt.tight_layout()
        plt.show()
        
        plt.savefig('wavestar_star'+'.eps',bbox_inches='tight',pad_inches=3) 
        
        
def plot_qfactors_wavestar_star(cyl_rad,nfloaters,x,y,q,dy,beta):

        circles = []

        for ind in range(0,x.shape[0]):
            circles.append(plt.Circle([x[ind], y[ind]], cyl_rad)) 
            
        col = PatchCollection(circles)

        col.set(array=n.array(q), cmap='jet')

        fig, ax = plt.subplots()

        ax.add_collection(col)
        
        ymax = max(y)
        xmax = max(x)
        xmin = min(x)
        #ax.autoscale()
        plt.ylim((-(ymax +5*dy/(8.)),ymax+5*dy/8.))
        plt.xlim((-(xmin + dy*5/8.),xmax + dy*5/8.))
        plt.xlabel('$x$' + ' ' + '$(m)$', fontsize=18)
        plt.ylabel('$y$' + ' ' + '$(m)$', fontsize=18)
        plt.locator_params(axis = 'x', nbins = 6)
        plt.locator_params(axis = 'y', nbins = 6)
        #plt.axis('off')
        
        qmax = max(q)
        qmin = min(q)
        
        v = n.linspace(qmin, qmax, 10, endpoint=True)
        #ax.axis('equal')
        plt.gca().set_aspect('equal', adjustable='box')
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("right", "5%", pad="3%")
        #cbaxes = fig.add_axes([0.8, 0.1, 0.03, 0.8])   
        cbar=fig.colorbar(col,cax=cax,format='%1.1f',ticks=v)
        cbar.ax.set_ylabel('$q factor$', fontsize=15)
        plt.tight_layout()
        
        plt.show()
        
#        fig = plt.gcf()
#        fig.set_size_inches(10,10)
#        fig.savefig('test.eps', dpi=800)
#        fig.set_size_inches(10.0, 10.0, forward=True)
        
        plt.savefig('wavestar_star'+'beta'+str(beta)+'.eps',bbox_inches='tight',pad_inches=3) 


        
def plot_qfactors_staggered_grid(cyl_rad,nfloaters,x,y,q):

        circles = []

        for ind in range(0,x.shape[0]):
            circles.append(plt.Circle([x[ind], y[ind]], cyl_rad)) 
            
        col = PatchCollection(circles)

        col.set(array=n.array(q), cmap='jet')

        fig, ax = plt.subplots()

        ax.add_collection(col)
        
        ymax = max(y)
        xmax = max(x)
        xmin = min(x)
        #ax.autoscale()
        plt.ylim((0 - 2 * cyl_rad),ymax+2*cyl_rad)
        plt.xlim(((xmin - 2*cyl_rad),xmax + 2*cyl_rad))
        plt.xlabel('$x$' + ' ' + '$(m)$', fontsize=18)
        plt.ylabel('$y$' + ' ' + '$(m)$', fontsize=18)
        plt.locator_params(axis = 'x', nbins = 6)
        plt.locator_params(axis = 'y', nbins = 6)
        #plt.axis('off')
        
        qmax = max(q)
        qmin = min(q)
        
        v = n.linspace(qmin, qmax, 10, endpoint=True)
        #ax.axis('equal')
        plt.gca().set_aspect('equal', adjustable='box')
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("right", "5%", pad="3%")
        #cbaxes = fig.add_axes([0.8, 0.1, 0.03, 0.8])   
        cbar=fig.colorbar(col,cax=cax,format='%1.1f',ticks=v)
        cbar.ax.set_ylabel('$q factor$', fontsize=15)
        plt.tight_layout()
        
        plt.show()        
        
def test_automatic_4subplot_figure(cyl_rad,nfloaters,x,y,q1,q2,dy,beta, eta1, freesurf_class1, eta2, freesurf_class2):
        
        
        fig, big_axes = plt.subplots(nrows=2, ncols=1, sharey=True) 
        
        
        titles = [r'$\beta=0$',r'$\beta=0$',r'$\beta=\pi/3$']
        
        for row, big_ax in enumerate(big_axes, start=1):
            
            plt.text(0.5, 1.08, titles[row],
                     horizontalalignment='center',
                     fontsize=16,
                     transform = big_ax.transAxes)
            
            #big_ax.set_title(titles[row], fontsize=20, position=(1.5, 1.5))

            # Turn off axis lines and ticks of the big subplot 
            # obs alpha is 0 in RGBA string!
            big_ax.tick_params(labelcolor=(1.,1.,1., 0.0), top='off', bottom='off', left='off', right='off')
            # removes the white frame
            big_ax._frameon = False
        
       
######### First Subplot

        ax1 = fig.add_subplot(2,2,1)        
        circles = []

        for ind in range(0,x.shape[0]):
            circles.append(plt.Circle([x[ind], y[ind]], cyl_rad)) 
            
        col = PatchCollection(circles)

        col.set(array=n.array(q1), cmap='jet')
        
        ax1.add_collection(col)
        ymax = max(y)
        xmax = max(x)
        xmin = min(x)
        #ax.autoscale()
        ax1.set_ylim((-(ymax +5*dy/(8.)),ymax+5*dy/8.))
        ax1.set_xlim((-(xmin + dy*5/8.),xmax + dy*5/8.))
        ax1.set_xlabel('$x$' + ' ' + '$(m)$', fontsize=10)
        ax1.set_ylabel('$y$' + ' ' + '$(m)$', fontsize=10)
        plt.locator_params(axis = 'x', nbins = 6)
        plt.locator_params(axis = 'y', nbins = 6)
        #plt.axis('off')
        
        qmax = max(q1)
        qmin = min(q1)
        
        v = n.linspace(qmin, qmax, 10, endpoint=True)
        #ax.axis('equal')
        ax1.set_aspect('equal', adjustable='datalim')
        
        divider1 = make_axes_locatable(ax1)
        cax1 = divider1.append_axes("right", "5%", pad="3%")
        #cbaxes = fig.add_axes([0.8, 0.1, 0.03, 0.8])
        cbar1=plt.colorbar(col,cax=cax1,format='%1.1f',ticks=v)
        cbar1.set_label('$q$' + ' ' + '$factor$', fontsize=14)
        cbar1.ax.tick_params(labelsize=10)

        ax1.xaxis.set_visible(False)
        ax1.yaxis.set_visible(False)
        
######### Second subplot

        ax2 = fig.add_subplot(2,2,2)  

        # Calculate absolute valule of free surface elevation
        #----------------------------------------------------------------------
        Z = n.vstack(n.split(abs(eta1),freesurf_class1.X.shape[0]))
        
        Z[freesurf_class1.interior] = n.nan
        
        # Plot
        #----------------------------------------------------------------------
        # Set ranges of plot
        #------------------------------
        n_levels = 10
        
        levels = []
    
        step = (n.nanmax(Z) - n.nanmin(Z))/ n_levels
        
        for ind in range(0, n_levels+1):
            levels.append(n.nanmin(Z) + ind*step)
            
        # Set plot properties
        #------------------------------    
        cmap = plt.cm.get_cmap("jet")

        CS = ax2.contourf(freesurf_class1.X, freesurf_class1.Y, Z, levels, cmap=cmap)
        #plt.title(r'$(\beta= $' + str(round(m.degrees(beta),1)) + '$^\circ$' + ',' + ' ' + r'$\lambda / a = $' + str(round(case,1)) + ')', fontsize=20)
        ax2.set_xlabel('$x$' + ' ' + '$(m)$', fontsize=12)
        ax2.set_ylabel('$y$' + ' ' + '$(m)$', fontsize=12)
#        plt.locator_params(axis = 'x', nbins = 6)
#        plt.locator_params(axis = 'y', nbins = 6)
        #plt.tick_params(axis='x', labelsize=20)
        #plt.tick_params(axis='y', labelsize=20)
        ax2.set_aspect('equal', adjustable='datalim')
        divider2 = make_axes_locatable(ax2)
        cax2 = divider2.append_axes("right", "5%", pad="3%")

        cbar2 = plt.colorbar(CS, cax=cax2, format='%1.2f')
        cbar2.set_label('$|H_s / H_s^I|$', fontsize=14)
        cbar2.ax.tick_params(labelsize=10)
        
        ax2.xaxis.set_visible(False)
        ax2.yaxis.set_visible(False)
        
        plt.show(block=True)
    
        
######### Third Subplot

        ax3 = fig.add_subplot(2,2,3)          
        circles = []

        for ind in range(0,x.shape[0]):
            circles.append(plt.Circle([x[ind], y[ind]], cyl_rad)) 
            
        col = PatchCollection(circles)

        col.set(array=n.array(q2), cmap='jet')
        
        ax3.add_collection(col)
        ymax = max(y)
        xmax = max(x)
        xmin = min(x)
        #ax.autoscale()
        ax3.set_ylim((-(ymax +5*dy/(8.)),ymax+5*dy/8.))
        ax3.set_xlim((-(xmin + dy*5/8.),xmax + dy*5/8.))
        ax3.set_xlabel('$x$' + ' ' + '$(m)$', fontsize=10)
        ax3.set_ylabel('$y$' + ' ' + '$(m)$', fontsize=10)
#        plt.locator_params(axis = 'x', nbins = 6)
#        plt.locator_params(axis = 'y', nbins = 6)
        #plt.axis('off')
        
        qmax = max(q2)
        qmin = min(q2)
        
        v = n.linspace(qmin, qmax, 10, endpoint=True)
        #ax.axis('equal')
        ax3.set_aspect('equal', adjustable='datalim')
        ax3.tick_params(axis='x', labelsize=10)
        ax3.tick_params(axis='y', labelsize=10)
        
        divider3 = make_axes_locatable(ax3)
        cax3 = divider3.append_axes("right", "5%", pad="3%")
        #cbaxes = fig.add_axes([0.8, 0.1, 0.03, 0.8])
        cbar3=plt.colorbar(col,cax=cax3,format='%1.1f',ticks=v)
        cbar3.set_label('$q$' + ' ' + '$factor$', fontsize=14)
        cbar3.ax.tick_params(labelsize=10)
        
        plt.setp(ax3, xticks=[0, 50, 100], xticklabels=['0', '50', '100'],
        yticks=[-50, 0, 50])
        
#        ax3.set_xticklabels([0, 50, 100])
#        ax3.set_yticklabels([-50, 0, 50])
        
######### Forth subplot

        ax4 = fig.add_subplot(2,2,4)  
        # Calculate absolute valule of free surface elevation
        #----------------------------------------------------------------------
        Z = n.vstack(n.split(abs(eta2),freesurf_class2.X.shape[0]))
        
        Z[freesurf_class2.interior] = n.nan
        
        # Plot
        #----------------------------------------------------------------------
        # Set ranges of plot
        #------------------------------
        n_levels = 10
        
        levels = []
    
        step = (n.nanmax(Z) - n.nanmin(Z))/ n_levels
        
        for ind in range(0, n_levels+1):
            levels.append(n.nanmin(Z) + ind*step)
            
        # Set plot properties
        #------------------------------    
        cmap = plt.cm.get_cmap("jet")

        CS = ax4.contourf(freesurf_class2.X, freesurf_class2.Y, Z, levels, cmap=cmap)
        #plt.title(r'$(\beta= $' + str(round(m.degrees(beta),1)) + '$^\circ$' + ',' + ' ' + r'$\lambda / a = $' + str(round(case,1)) + ')', fontsize=20)
        ax4.set_xlabel('$x$' + ' ' + '$(m)$', fontsize=12)
        ax4.set_ylabel('$y$' + ' ' + '$(m)$', fontsize=12)
        
#        plt.locator_params(axis = 'x', nbins = 6)
#        plt.locator_params(axis = 'y', nbins = 6)
        #plt.tick_params(axis='x', labelsize=20)
        #plt.tick_params(axis='y', labelsize=20)
        ax4.set_aspect('equal', adjustable='datalim')
        divider4 = make_axes_locatable(ax4)
        cax4 = divider4.append_axes("right", "5%", pad="3%")

        cbar4 = plt.colorbar(CS, cax=cax4, format='%1.2f')
        cbar4.set_label('$|H_s / H_s^I|$', fontsize=14)
        cbar4.ax.tick_params(labelsize=10)
        
        ax4.xaxis.set_visible(False)
        ax4.yaxis.set_visible(False)
        
        fig.subplots_adjust(hspace=0.25, wspace=.3)
        
       # plt.subplots_adjust(bottom=0.0, right=0., top=0.0,  left=0.0)
        
        #fig.tight_layout()
        
        fig.set_figheight(8)
        fig.set_figwidth(8)

        plt.show(block=True)
    
    
#        ax3.scatter(x, 2 * y ** 2 - 1, color='r')
#        ax4.plot(x, 2 * y ** 2 - 1, color='r')
        plt.savefig('wavestar_star'+'test'+'.eps', bbox_inches='tight') 
        
        
def test_automatic_4subplot_figure_diff_floats(cyl_rad,nfloaters60,x60,y60,nfloaters36,x36,y36,dy,q_60_beta_0,q_36_beta_0, q_60_beta_60, q_36_beta_60, beta):
        
        
        fig, big_axes = plt.subplots(nrows=2, ncols=1, sharey=True) 
        
        
        titles = [r'$\beta=0$',r'$\beta=0$',r'$\beta=\pi/3$']
        
        for row, big_ax in enumerate(big_axes, start=1):
            
            plt.text(0.5, 1.08, titles[row],
                     horizontalalignment='center',
                     fontsize=16,
                     transform = big_ax.transAxes)
            
            #big_ax.set_title(titles[row], fontsize=20, position=(1.5, 1.5))

            # Turn off axis lines and ticks of the big subplot 
            # obs alpha is 0 in RGBA string!
            big_ax.tick_params(labelcolor=(1.,1.,1., 0.0), top='off', bottom='off', left='off', right='off')
            # removes the white frame
            big_ax._frameon = False
        
       
######### First Subplot

        ax1 = fig.add_subplot(2,2,1)        
        circles = []

        for ind in range(0,x60.shape[0]):
            circles.append(plt.Circle([x60[ind], y60[ind]], cyl_rad)) 
            
        col = PatchCollection(circles)

        col.set(array=n.array(q_60_beta_0), cmap='jet')
        
        ax1.add_collection(col)
        ymax = max(y60)
        xmax = max(x60)
        xmin = min(x60)
        #ax.autoscale()
        ax1.set_ylim((-(ymax +5*dy/(8.)),ymax+5*dy/8.))
        ax1.set_xlim((-(xmin + dy*5/8.),xmax + dy*5/8.))
        ax1.set_xlabel('$x$' + ' ' + '$(m)$', fontsize=10)
        ax1.set_ylabel('$y$' + ' ' + '$(m)$', fontsize=10)
        plt.locator_params(axis = 'x', nbins = 6)
        plt.locator_params(axis = 'y', nbins = 6)
        #plt.axis('off')
        
        qmax = max(q_60_beta_0)
        qmin = min(q_60_beta_0)
        
        v = n.linspace(qmin, qmax, 5, endpoint=True)
        #ax.axis('equal')
        ax1.set_aspect('equal', adjustable='datalim')
        
        divider1 = make_axes_locatable(ax1)
        cax1 = divider1.append_axes("right", "5%", pad="3%")
        #cbaxes = fig.add_axes([0.8, 0.1, 0.03, 0.8])
        cbar1=plt.colorbar(col,cax=cax1,format='%1.2f',ticks=v)
        cbar1.set_label('$\gamma_s$' + ' ' + '$(rad)$', fontsize=14)
        cbar1.ax.tick_params(labelsize=10)

        ax1.xaxis.set_visible(False)
        ax1.yaxis.set_visible(False)
        
######### Second subplot

        ax2 = fig.add_subplot(2,2,2)  
        circles = []

        for ind in range(0,x36.shape[0]):
            circles.append(plt.Circle([x36[ind], y36[ind]], cyl_rad)) 
            
        col = PatchCollection(circles)

        col.set(array=n.array(q_36_beta_0), cmap='jet')
        
        ax2.add_collection(col)
        ymax = max(y36)
        xmax = max(x36)
        xmin = min(x36)
        #ax.autoscale()
        ax2.set_ylim((-(ymax +5*dy/(8.)),ymax+5*dy/8.))
        ax2.set_xlim((-(xmin + dy*5/8.),xmax + dy*5/8.))
        ax2.set_xlabel('$x$' + ' ' + '$(m)$', fontsize=10)
        ax2.set_ylabel('$y$' + ' ' + '$(m)$', fontsize=10)
        plt.locator_params(axis = 'x', nbins = 6)
        plt.locator_params(axis = 'y', nbins = 6)
        #plt.axis('off')
        
        qmax = max(q_36_beta_0)
        qmin = min(q_36_beta_0)
        
        v = n.linspace(qmin, qmax, 5, endpoint=True)
        #ax.axis('equal')
        ax2.set_aspect('equal', adjustable='datalim')
        
        divider2 = make_axes_locatable(ax2)
        cax2 = divider2.append_axes("right", "5%", pad="3%")
        #cbaxes = fig.add_axes([0.8, 0.1, 0.03, 0.8])
        cbar2=plt.colorbar(col,cax=cax2,format='%1.2f',ticks=v)
        cbar2.set_label('$\gamma_s$' + ' ' + '$(rad)$', fontsize=14)
        cbar2.ax.tick_params(labelsize=10)

        ax2.xaxis.set_visible(False)
        ax2.yaxis.set_visible(False)
    
        
######### Third Subplot

        
        ax3 = fig.add_subplot(2,2,3)  
        circles = []

        for ind in range(0,x60.shape[0]):
            circles.append(plt.Circle([x60[ind], y60[ind]], cyl_rad)) 
            
        col = PatchCollection(circles)

        col.set(array=n.array(q_60_beta_60), cmap='jet')
        
        ax3.add_collection(col)
        ymax = max(y60)
        xmax = max(x60)
        xmin = min(x60)
        #ax.autoscale()
        ax3.set_ylim((-(ymax +5*dy/(8.)),ymax+5*dy/8.))
        ax3.set_xlim((-(xmin + dy*5/8.),xmax + dy*5/8.))
        ax3.set_xlabel('$x$' + ' ' + '$(m)$', fontsize=10)
        ax3.set_ylabel('$y$' + ' ' + '$(m)$', fontsize=10)
        plt.locator_params(axis = 'x', nbins = 6)
        plt.locator_params(axis = 'y', nbins = 6)
        #plt.axis('off')
        
        qmax = max(q_60_beta_60)
        qmin = min(q_60_beta_60)
        
        v = n.linspace(qmin, qmax, 5, endpoint=True)
        #ax.axis('equal')
        ax3.set_aspect('equal', adjustable='datalim')
        
        divider3 = make_axes_locatable(ax3)
        cax3 = divider3.append_axes("right", "5%", pad="3%")
        #cbaxes = fig.add_axes([0.8, 0.1, 0.03, 0.8])
        cbar3=plt.colorbar(col,cax=cax3,format='%1.2f',ticks=v)
        cbar3.set_label('$\gamma_s$' + ' ' + '$(rad)$', fontsize=14)
        cbar3.ax.tick_params(labelsize=10)

        plt.setp(ax3, xticks=[0, 50, 100], xticklabels=['0', '50', '100'],
        yticks=[-50, 0, 50])
        
        ax3.xaxis.set_visible(True)
        ax3.yaxis.set_visible(True)
        
######### Forth subplot

       
        ax4 = fig.add_subplot(2,2,4)  
        circles = []

        for ind in range(0,x36.shape[0]):
            circles.append(plt.Circle([x36[ind], y36[ind]], cyl_rad)) 
            
        col = PatchCollection(circles)

        col.set(array=n.array(q_36_beta_60), cmap='jet')
        
        ax4.add_collection(col)
        ymax = max(y36)
        xmax = max(x36)
        xmin = min(x36)
        #ax.autoscale()
        ax4.set_ylim((-(ymax +5*dy/(8.)),ymax+5*dy/8.))
        ax4.set_xlim((-(xmin + dy*5/8.),xmax + dy*5/8.))
        ax4.set_xlabel('$x$' + ' ' + '$(m)$', fontsize=10)
        ax4.set_ylabel('$y$' + ' ' + '$(m)$', fontsize=10)
        plt.locator_params(axis = 'x', nbins = 6)
        plt.locator_params(axis = 'y', nbins = 6)
        #plt.axis('off')
        
        qmax = max(q_36_beta_60)
        qmin = min(q_36_beta_60)
        
        v = n.linspace(qmin, qmax, 5, endpoint=True)
        #ax.axis('equal')
        ax4.set_aspect('equal', adjustable='datalim')
        
        divider4 = make_axes_locatable(ax4)
        cax4 = divider4.append_axes("right", "5%", pad="3%")
        #cbaxes = fig.add_axes([0.8, 0.1, 0.03, 0.8])
        cbar4=plt.colorbar(col,cax=cax4,format='%1.2f',ticks=v)
        cbar4.set_label('$\gamma_s$' + ' ' + '$(rad)$', fontsize=14)
        cbar4.ax.tick_params(labelsize=10)

        ax4.xaxis.set_visible(False)
        ax4.yaxis.set_visible(False)
        
        fig.subplots_adjust(hspace=0.25, wspace=.3)
        
       # plt.subplots_adjust(bottom=0.0, right=0., top=0.0,  left=0.0)
        
        #fig.tight_layout()
        
        fig.set_figheight(8)
        fig.set_figwidth(8)

        plt.show(block=True)
    
    
#        ax3.scatter(x, 2 * y ** 2 - 1, color='r')
#        ax4.plot(x, 2 * y ** 2 - 1, color='r')
        plt.savefig('wavestar_star'+'test'+'.eps', bbox_inches='tight') 


def test_automatic_4subplot_figure_diff_floats_subtitles_column(cyl_rad,nfloaters60,x60,y60,nfloaters36,x36,y36,dy,q_60_beta_0,q_36_beta_0, q_60_beta_60, q_36_beta_60, beta):
        
        
        fig, big_axes = plt.subplots(nrows=2, ncols=1, sharey=True) 
        
        
        titles = [r'$\beta=0$',r'$\beta=0$',r'$\beta=\pi/3$']
        
        for row, big_ax in enumerate(big_axes, start=1):
            
            plt.text(0.5, 1.08, titles[row],
                     horizontalalignment='center',
                     fontsize=16,
                     transform = big_ax.transAxes)
            
            #big_ax.set_title(titles[row], fontsize=20, position=(1.5, 1.5))

            # Turn off axis lines and ticks of the big subplot 
            # obs alpha is 0 in RGBA string!
            big_ax.tick_params(labelcolor=(1.,1.,1., 0.0), top='off', bottom='off', left='off', right='off')
            # removes the white frame
            big_ax._frameon = False
        
       
######### First Subplot

        ax1 = fig.add_subplot(2,2,1)        
        circles = []

        for ind in range(0,x60.shape[0]):
            circles.append(plt.Circle([x60[ind], y60[ind]], cyl_rad)) 
            
        col = PatchCollection(circles)

        col.set(array=n.array(q_60_beta_0), cmap='jet')
        
        ax1.add_collection(col)
        ymax = max(y60)
        xmax = max(x60)
        xmin = min(x60)
        #ax.autoscale()
        plt.text(0.5, 1.08, r'$B_{pto,60}^{r=3m}$',
                 horizontalalignment='center',
                 fontsize=18,
                 transform = ax1.transAxes)
        #ax1.set_title(r'$B_{pto,60}^{r=3m}$')
        ax1.set_ylim((-(ymax +5*dy/(8.)),ymax+5*dy/8.))
        ax1.set_xlim((-(xmin + dy*5/8.),xmax + dy*5/8.))
        ax1.set_xlabel('$x$' + ' ' + '$(m)$', fontsize=10)
        ax1.set_ylabel('$y$' + ' ' + '$(m)$', fontsize=10)
        plt.locator_params(axis = 'x', nbins = 6)
        plt.locator_params(axis = 'y', nbins = 6)
        #plt.axis('off')
        
        qmax = max(q_60_beta_0)
        qmin = min(q_60_beta_0)
        
        v = n.linspace(qmin, qmax, 5, endpoint=True)
        #ax.axis('equal')
        ax1.set_aspect('equal', adjustable='datalim')
        
        divider1 = make_axes_locatable(ax1)
        cax1 = divider1.append_axes("right", "5%", pad="3%")
        #cbaxes = fig.add_axes([0.8, 0.1, 0.03, 0.8])
        cbar1=plt.colorbar(col,cax=cax1,format='%1.2f',ticks=v)
        cbar1.set_label('$\gamma_s$' + ' ' + '$(rad)$', fontsize=14)
        cbar1.ax.tick_params(labelsize=10)

        ax1.xaxis.set_visible(False)
        ax1.yaxis.set_visible(False)
        
######### Second subplot

        ax2 = fig.add_subplot(2,2,2)  
        circles = []

        for ind in range(0,x36.shape[0]):
            circles.append(plt.Circle([x36[ind], y36[ind]], cyl_rad)) 
            
        col = PatchCollection(circles)

        col.set(array=n.array(q_36_beta_0), cmap='jet')
        
        ax2.add_collection(col)
        ymax = max(y36)
        xmax = max(x36)
        xmin = min(x36)
        #ax.autoscale()
        plt.text(0.5, 1.08, r'$B_{pto,36}^{r=4.5m}$',
                 horizontalalignment='center',
                 fontsize=18,
                 transform = ax2.transAxes)
        #ax2.set_title(r'$B_{pto,36}^{r=4.5m}$')
        ax2.set_ylim((-(ymax +5*dy/(8.)),ymax+5*dy/8.))
        ax2.set_xlim((-(xmin + dy*5/8.),xmax + dy*5/8.))
        ax2.set_xlabel('$x$' + ' ' + '$(m)$', fontsize=10)
        ax2.set_ylabel('$y$' + ' ' + '$(m)$', fontsize=10)
        plt.locator_params(axis = 'x', nbins = 6)
        plt.locator_params(axis = 'y', nbins = 6)
        #plt.axis('off')
        
        qmax = max(q_36_beta_0)
        qmin = min(q_36_beta_0)
        
        v = n.linspace(qmin, qmax, 5, endpoint=True)
        #ax.axis('equal')
        ax2.set_aspect('equal', adjustable='datalim')
        
        divider2 = make_axes_locatable(ax2)
        cax2 = divider2.append_axes("right", "5%", pad="3%")
        #cbaxes = fig.add_axes([0.8, 0.1, 0.03, 0.8])
        cbar2=plt.colorbar(col,cax=cax2,format='%1.2f',ticks=v)
        cbar2.set_label('$\gamma_s$' + ' ' + '$(rad)$', fontsize=14)
        cbar2.ax.tick_params(labelsize=10)

        ax2.xaxis.set_visible(False)
        ax2.yaxis.set_visible(False)
    
        
######### Third Subplot

        
        ax3 = fig.add_subplot(2,2,3)  
        circles = []

        for ind in range(0,x60.shape[0]):
            circles.append(plt.Circle([x60[ind], y60[ind]], cyl_rad)) 
            
        col = PatchCollection(circles)

        col.set(array=n.array(q_60_beta_60), cmap='jet')
        
        ax3.add_collection(col)
        ymax = max(y60)
        xmax = max(x60)
        xmin = min(x60)
        #ax.autoscale()
        ax3.set_ylim((-(ymax +5*dy/(8.)),ymax+5*dy/8.))
        ax3.set_xlim((-(xmin + dy*5/8.),xmax + dy*5/8.))
        ax3.set_xlabel('$x$' + ' ' + '$(m)$', fontsize=10)
        ax3.set_ylabel('$y$' + ' ' + '$(m)$', fontsize=10)
        plt.locator_params(axis = 'x', nbins = 6)
        plt.locator_params(axis = 'y', nbins = 6)
        #plt.axis('off')
        
        qmax = max(q_60_beta_60)
        qmin = min(q_60_beta_60)
        
        v = n.linspace(qmin, qmax, 5, endpoint=True)
        #ax.axis('equal')
        ax3.set_aspect('equal', adjustable='datalim')
        
        divider3 = make_axes_locatable(ax3)
        cax3 = divider3.append_axes("right", "5%", pad="3%")
        #cbaxes = fig.add_axes([0.8, 0.1, 0.03, 0.8])
        cbar3=plt.colorbar(col,cax=cax3,format='%1.2f',ticks=v)
        cbar3.set_label('$\gamma_s$' + ' ' + '$(rad)$', fontsize=14)
        cbar3.ax.tick_params(labelsize=10)

        plt.setp(ax3, xticks=[0, 50, 100], xticklabels=['0', '50', '100'],
        yticks=[-50, 0, 50])
        
        ax3.xaxis.set_visible(True)
        ax3.yaxis.set_visible(True)
        
######### Forth subplot

       
        ax4 = fig.add_subplot(2,2,4)  
        circles = []

        for ind in range(0,x36.shape[0]):
            circles.append(plt.Circle([x36[ind], y36[ind]], cyl_rad)) 
            
        col = PatchCollection(circles)

        col.set(array=n.array(q_36_beta_60), cmap='jet')
        
        ax4.add_collection(col)
        ymax = max(y36)
        xmax = max(x36)
        xmin = min(x36)
        #ax.autoscale()
        ax4.set_ylim((-(ymax +5*dy/(8.)),ymax+5*dy/8.))
        ax4.set_xlim((-(xmin + dy*5/8.),xmax + dy*5/8.))
        ax4.set_xlabel('$x$' + ' ' + '$(m)$', fontsize=10)
        ax4.set_ylabel('$y$' + ' ' + '$(m)$', fontsize=10)
        plt.locator_params(axis = 'x', nbins = 6)
        plt.locator_params(axis = 'y', nbins = 6)
        #plt.axis('off')
        
        qmax = max(q_36_beta_60)
        qmin = min(q_36_beta_60)
        
        v = n.linspace(qmin, qmax, 5, endpoint=True)
        #ax.axis('equal')
        ax4.set_aspect('equal', adjustable='datalim')
        
        divider4 = make_axes_locatable(ax4)
        cax4 = divider4.append_axes("right", "5%", pad="3%")
        #cbaxes = fig.add_axes([0.8, 0.1, 0.03, 0.8])
        cbar4=plt.colorbar(col,cax=cax4,format='%1.2f',ticks=v)
        cbar4.set_label('$\gamma_s$' + ' ' + '$(rad)$', fontsize=14)
        cbar4.ax.tick_params(labelsize=10)

        ax4.xaxis.set_visible(False)
        ax4.yaxis.set_visible(False)
        
        fig.subplots_adjust(hspace=0.25, wspace=.3)
        
       # plt.subplots_adjust(bottom=0.0, right=0., top=0.0,  left=0.0)
        
        #fig.tight_layout()
        
        fig.set_figheight(8)
        fig.set_figwidth(8)

        plt.show(block=True)
    
    
#        ax3.scatter(x, 2 * y ** 2 - 1, color='r')
#        ax4.plot(x, 2 * y ** 2 - 1, color='r')
        plt.savefig('wavestar_star'+'test'+'.eps', bbox_inches='tight') 
        
        
        
def automatic_4subplot_figure_scatter(body_number, bodies, Powers_0, Power_60_0, dx_0, dx_max_0, dx_min_0, Powers_60, Power_60_60, dx_60, dx_max_60, dx_min_60):
    
#        fig, big_axes = plt.subplots(nrows=2, ncols=1, sharey=True) 
#        
#        
#        titles = [r'$\beta=0$',r'$\beta=0$',r'$\beta=\pi/3$']
#        
#        for row, big_ax in enumerate(big_axes, start=1):
#            
#            plt.text(0.5, 1.08, titles[row],
#                     horizontalalignment='center',
#                     fontsize=16,
#                     transform = big_ax.transAxes)
#            
#            #big_ax.set_title(titles[row], fontsize=20, position=(1.5, 1.5))
#
#            # Turn off axis lines and ticks of the big subplot 
#            # obs alpha is 0 in RGBA string!
#            big_ax.tick_params(labelcolor=(1.,1.,1., 0.0), top='off', bottom='off', left='off', right='off')
#            # removes the white frame
#            big_ax._frameon = False
            
            fig = plt.figure()
            
########### First Subplot
            ax1 = fig.add_subplot(2,2,1)
            ax1.set_title(r'$\beta=0$')
            #ax1.set_xlabel('$N \, floats$', fontsize=14)
            ax1.set_ylabel('$P_y(kW)$', fontsize=14)
            ax1.plot(body_number,n.asarray(Powers_0)/1000, 'sb',label=r'$P_y$')
            ax1.plot(bodies, n.asarray(Power_60_0)/1000,'-.k', linewidth=2, label=r'$P_y^{60}$')
            #ax1.plot([30,30],[150,Powers_0[3]/1000], color ='blue', linewidth=1, linestyle="--")
            
            plt.setp(ax1.get_xticklabels(),visible=False)
            plt.setp(ax1, yticks=[0, 100, 200, 300, 400], yticklabels=['0', '100', '200', '300', '400'])
            ax1.yaxis.set_visible(True)
            ax1.set_ylim([0, 400])
            
 ########### Second Subplot
            ax3 = fig.add_subplot(2,2,2)
            ax3.set_title(r'$\beta=\pi/3$')
            #ax3.set_xlabel('$N \, floats$', fontsize=14)
            #ax3.set_ylabel('$P_y(kW)$', fontsize=14)
            ax3.plot(body_number,n.asarray(Powers_60)/1000, 'sb',label=r'$P_y$')
            ax3.plot(bodies, n.asarray(Power_60_60)/1000,'-.k', linewidth=2, label=r'$P_y^{60}$')
            #ax3.plot([30,30],[150,Powers_60[3]/1000], color ='blue', linewidth=1, linestyle="--")
            ax3.legend(frameon=False,fontsize=14, numpoints=1, loc='lower right')
            ax3.set_ylim([0, 400])
            
#            ax3.yaxis.set_visible(False)
#            ax3.xaxis.set_visible(False)
            
            plt.setp(ax3.get_xticklabels(),visible=False)
            plt.setp(ax3.get_yticklabels(),visible=False)
      

########### Third Subplot
            ax2 = fig.add_subplot(2,2,3)

            ax2.set_xlabel('$N \, floats$', fontsize=14)
            ax2.set_ylabel('$dx \, (m)$', fontsize=14)
            ax2.plot(body_number,dx_0, 'ob', label='dx opt')
            ax2.plot(body_number,dx_max_0, 'sr', label='dx max')
            ax2.plot(body_number,dx_min_0, '^g', label='dx min')
            #ax2.legend(frameon=False,fontsize=18, numpoints=1)
            
            ax2.xaxis.set_visible(True)
            ax2.yaxis.set_visible(True)
            
            plt.setp(ax2, yticks=[10, 20, 30, 40, 50], yticklabels=['10', '20', '30', '40', '50'])
            

########### Forth Subplot
            ax4 = fig.add_subplot(2,2,4)
            #ax4.set_xlabel('$N \, floats$', fontsize=14)
            #ax4.set_ylabel('$dx(m)$', fontsize=14)
            ax4.plot(body_number,dx_60, 'ob', label='dx opt')
            ax4.plot(body_number,dx_max_60, 'sr', label='dx max')
            ax4.plot(body_number,dx_min_60, '^g', label='dx min')
            ax4.legend(frameon=False,fontsize=12, numpoints=1)
            
            plt.setp(ax4, yticks=[10, 20, 30, 40, 50], yticklabels=['10', '20', '30', '40', '50'])
            
            plt.setp(ax4.get_xticklabels(),visible=False)
            plt.setp(ax4.get_yticklabels(),visible=False)
            
            fig.subplots_adjust(hspace=0.15, wspace=.15)
            
            fig.set_figheight(8)
            fig.set_figwidth(8)

            plt.show(block=True)
            
            plt.savefig('wavestar_star'+'power_nfloaters'+'.eps', bbox_inches='tight') 
            
            
def automatic_4subplot_figure_scatter_sizesensitivity(anual_power_radius_beta0, anual_power_radius_opt_beta0, power_beta0_36_3mrad,bpto_vals_opt_beta0, bpto_ref_list_beta0, anual_power_radius_beta60, anual_power_radius_opt_beta60, power_beta60_36_3mrad, bpto_vals_opt_beta60, bpto_ref_list_beta60):
    
#        fig, big_axes = plt.subplots(nrows=2, ncols=1, sharey=True) 
#        
#        
#        titles = [r'$\beta=0$',r'$\beta=0$',r'$\beta=\pi/3$']
#        
#        for row, big_ax in enumerate(big_axes, start=1):
#            
#            plt.text(0.5, 1.08, titles[row],
#                     horizontalalignment='center',
#                     fontsize=16,
#                     transform = big_ax.transAxes)
#            
#            #big_ax.set_title(titles[row], fontsize=20, position=(1.5, 1.5))
#
#            # Turn off axis lines and ticks of the big subplot 
#            # obs alpha is 0 in RGBA string!
#            big_ax.tick_params(labelcolor=(1.,1.,1., 0.0), top='off', bottom='off', left='off', right='off')
#            # removes the white frame
#            big_ax._frameon = False
    
            radius = n.arange(2.0, 6.0, 0.5)
            rads_vals_toplot = n.linspace(2.0, 5.5, 50)
            
            fig = plt.figure()
            
########### First Subplot
            ax1 = fig.add_subplot(2,2,1)
            ax1.set_title(r'$\beta=0$')
            #ax1.set_xlabel('$N \, floats$', fontsize=14)
            ax1.set_ylabel('$P_y(kW)$', fontsize=14)
            ax1.plot(radius, n.asarray(anual_power_radius_beta0)/1000, 's', label='_nolegend_')
            ax1.plot(radius, n.asarray(anual_power_radius_opt_beta0)/1000, 'or', label='_nolegend_')
            ax1.plot(rads_vals_toplot, n.asarray(power_beta0_36_3mrad)/1000,'-.k', linewidth=2, label=r'$P_{y}^{36, r=3m}\,(B_{pto, 60}^{r=3m})$')
            ax1.legend(frameon=False,loc='best',fontsize=14, numpoints=1)
            
            plt.setp(ax1.get_xticklabels(),visible=False)
            ax1.yaxis.set_visible(True)

            
 ########### Second Subplot
            ax2 = fig.add_subplot(2,2,2)
            ax2.set_title(r'$\beta=\pi/3$')
            #ax3.set_xlabel('$N \, floats$', fontsize=14)
            #ax3.set_ylabel('$P_y(kW)$', fontsize=14)
            ax2.plot(radius, n.asarray(anual_power_radius_opt_beta60)/1000, 'or', label='$P_y^{36 }\,(B_{pto, 36 }^{r})$')
            ax2.plot(radius, n.asarray(anual_power_radius_beta60)/1000, 's', label='$P_y^{36 }\,(B_{pto, 60 }^{r=3m})$')
            ax2.plot(rads_vals_toplot, n.asarray(power_beta60_36_3mrad)/1000,'-.k', linewidth=2, label='_nolegend_')
            ax2.legend(frameon=False,fontsize=14, numpoints=1, loc='best')
            
#            ax3.yaxis.set_visible(False)
#            ax3.xaxis.set_visible(False)
            
            plt.setp(ax2.get_xticklabels(),visible=False)
            plt.setp(ax2.get_yticklabels(),visible=False)

########### Third Subplot
            ax3 = fig.add_subplot(2,2,3)

            ax3.set_xlabel('$r \, (m)$', fontsize=14)
            ax3.set_ylabel('$B_{pto} \, (N \, m \, s)$', fontsize=14)
            ax3.plot(radius, n.asarray(bpto_vals_opt_beta0), '^g', label=r'$B_{pto, 36}^{r}$')
            ax3.plot(rads_vals_toplot, n.asarray(bpto_ref_list_beta0),'--k', linewidth=2, label=r'$B_{pto, 60}^{r=3m}$')
            #ax2.legend(frameon=False,loc='best',fontsize=18, numpoints=1)
            #ax2.legend(frameon=False,fontsize=18, numpoints=1)

            
            ax3.xaxis.set_visible(True)
            ax3.yaxis.set_visible(True)
            
            #plt.setp(ax2, yticks=[10, 20, 30, 40, 50], yticklabels=['10', '20', '30', '40', '50'])
            

########### Forth Subplot
            ax4 = fig.add_subplot(2,2,4)
            #ax4.set_xlabel('$N \, floats$', fontsize=14)
            #ax4.set_ylabel('$dx(m)$', fontsize=14)
            ax4.plot(radius, n.asarray(bpto_vals_opt_beta60), '^g', label=r'$B_{pto, 36}^{r}$')
            ax4.plot(rads_vals_toplot, n.asarray(bpto_ref_list_beta60),'--k', linewidth=2, label=r'$B_{pto, 60}^{r=3m}$')
            ax4.legend(frameon=False,loc='best',fontsize=14, numpoints=1)
            
            #plt.setp(ax4, yticks=[10, 20, 30, 40, 50], yticklabels=['10', '20', '30', '40', '50'])
            
            plt.setp(ax4.get_xticklabels(),visible=False)
            plt.setp(ax4.get_yticklabels(),visible=False)
            
            fig.subplots_adjust(hspace=0.15, wspace=.15)
            
            fig.set_figheight(8)
            fig.set_figwidth(8)

            plt.show(block=True)
            
            plt.savefig('wavestar_star'+'power_nfloaters'+'.eps', bbox_inches='tight') 
    
    
def plot_signifmotion_wavestar_star(cyl_rad,nfloaters,x,y,q,dy,beta):

        circles = []

        for ind in range(0,x.shape[0]):
            circles.append(plt.Circle([x[ind], y[ind]], cyl_rad)) 
            
        col = PatchCollection(circles)

        col.set(array=n.array(q), cmap='jet')

        fig, ax = plt.subplots()

        ax.add_collection(col)
        
        ymax = max(y)
        xmax = max(x)
        xmin = min(x)
        #ax.autoscale()
        plt.ylim((-(ymax +5*dy/8.),ymax+5*dy/8.))
        plt.xlim((-(xmin + dy*5/8.),xmax + dy*5/8.))
        plt.xlabel('$x$' + ' ' + '$(m)$', fontsize=18)
        plt.ylabel('$y$' + ' ' + '$(m)$', fontsize=18)
        plt.locator_params(axis = 'x', nbins = 6)
        plt.locator_params(axis = 'y', nbins = 6)
        #plt.axis('off')
        
        qmax = max(q)
        qmin = min(q)
        
        v = n.linspace(qmin, qmax, 10, endpoint=True)
        #ax.axis('equal')
        plt.gca().set_aspect('equal', adjustable='box')
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("right", "5%", pad="3%")
        #cbaxes = fig.add_axes([0.8, 0.1, 0.03, 0.8])   
        cbar=fig.colorbar(col,cax=cax,format='%1.3f',ticks=v)
        cbar.ax.set_ylabel(r'$\theta_s \, (rad)$', fontsize=15)
        plt.tight_layout()
        plt.show()
        
        plt.savefig('wavestar_star'+'beta'+str(beta)+'.eps',bbox_inches='tight',pad_inches=3) 
        
        
def plot_floaters_black(cyl_rad,nfloaters,x,y,dy):

        circles = []

        for ind in range(0,x.shape[0]):
            circles.append(plt.Circle([x[ind], y[ind]], cyl_rad)) 
            
        col = PatchCollection(circles)

        col.set(array=n.array(x), color='k')

        fig, ax = plt.subplots()

        ax.add_collection(col)
        
        ymin = min(y)
        ymax = max(y)
        xmax = max(x)
        xmin = min(x)
        #ax.autoscale()
        plt.ylim((-(ymin +10*cyl_rad),ymax+10*cyl_rad))
        plt.xlim((-(xmin + 10*cyl_rad),xmax + 10*cyl_rad))
        plt.xlabel('$x$' + ' ' + '$(m)$', fontsize=15)
        plt.ylabel('$y$' + ' ' + '$(m)$', fontsize=15)
#        plt.locator_params(axis = 'x', nbins = 6)
#        plt.locator_params(axis = 'y', nbins = 6)
        #plt.axis('off')
#        
#        v = n.linspace(xmin, xmax, 10, endpoint=True)
        #ax.axis('equal')
        plt.gca().set_aspect('equal', adjustable='box')
#        divider = make_axes_locatable(plt.gca())
#        cax = divider.append_axes("right", "5%", pad="3%")
        #cbaxes = fig.add_axes([0.8, 0.1, 0.03, 0.8])   
#        cbar=fig.colorbar(col,cax=cax,format='%1.1f',ticks=v)
#        cbar.ax.set_ylabel('$x location (m)$', fontsize=15)
        #plt.tight_layout()
        fig.tight_layout()   
        plt.show()
        
        plt.savefig('layout'+'.eps',bbox_inches='tight',pad_inches=3)         
        
        
def plot_eta_total_boxes(eta, name, freesurf_class, beta, case, wave_amp):       
    
        # Calculate absolute valule of free surface elevation
        #----------------------------------------------------------------------
        Z = abs(eta) / wave_amp
        
#        if (name=='nemoh'):
#            for ind in range(0,len(freesurf_class.interior)):
#                Z[freesurf_class.interior[ind]] = n.nan
        for ind in range(0,len(freesurf_class.interior)):
                Z[freesurf_class.interior[ind]] = n.nan
                
        # Plot
        #----------------------------------------------------------------------
        # Set ranges of plot
        #------------------------------
        n_levels = 10
        
        levels = []
    
        step = n.nanmax(Z) / n_levels
        
        for ind in range(0, n_levels+1):
            levels.append(ind*step)
            
        # Set plot properties
        #------------------------------    
        cmap = plt.cm.get_cmap("jet")
        fig = plt.figure()
        CS = plt.contourf(freesurf_class.X, freesurf_class.Y, Z, levels, cmap=cmap)
        #plt.title(r'$(\beta= $' + str(round(m.degrees(beta),1)) + '$^\circ$' + ',' + ' ' + r'$\lambda / a = $' + str(round(case,1)) + ')', fontsize=20)
        plt.xlabel('$x$' + ' ' + '$(m)$', fontsize=18)
        plt.ylabel('$y$' + ' ' + '$(m)$', fontsize=18)
        #plt.tick_params(axis='x', labelsize=20)
        #plt.tick_params(axis='y', labelsize=20)
        plt.gca().set_aspect('equal', adjustable='box')
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("right", "5%", pad="3%")

        cbar = plt.colorbar(CS, cax=cax, format='%1.1f')
        cbar.ax.set_ylabel('$|\eta / A|$', fontsize=20)
        
        plt.show(block=True)
        
        ax = fig.add_subplot(111)
        rect1 = mpl.patches.Rectangle((-3,-3), 6, 6, color='black')
        rect2 = mpl.patches.Rectangle((-3,-3+12), 6, 6, color='black')
        romb1 = mpl.patches.Polygon([[-n.sqrt(18)+12,0], [12,-n.sqrt(18)],[12+n.sqrt(18),0], [12,n.sqrt(18)]], closed='True', color='black')
        romb2 = mpl.patches.Polygon([[-n.sqrt(18)+12,12], [12,-n.sqrt(18)+12],[12+n.sqrt(18),12], [12,n.sqrt(18)+12]], closed='True', color='black')
        
        ax.add_patch(rect1)
        ax.add_patch(rect2)
        ax.add_patch(romb1)
        ax.add_patch(romb2)
        
        plt.savefig('eta_total'+'name'+name+'beta' + str(round(m.degrees(beta),1))  + 'case' + str(round(case,1)) +'.eps',bbox_inches='tight',pad_inches=3)  
        
    
def plot_eta_total_comparison_boxes(eta_it, eta_nemoh, freesurf_class, beta,case, wave_amp):
    
        # Calculate absolute valule of free surface elevation
        #----------------------------------------------------------------------
        Z_it    = eta_it 
        Z_nemoh = eta_nemoh 
        
        Z = (abs(Z_nemoh - Z_it) / wave_amp) * 100
        #Z = ((abs(Z_nemoh) - abs(Z_it)) / wave_amp) * 100
        
        for ind in range(0,len(freesurf_class.interior)):
                Z[freesurf_class.interior[ind]] = n.nan
        
        # Plot
        #----------------------------------------------------------------------
        # Set ranges of plot
        #------------------------------
        n_levels = 10
        
        levels = []
    
        step = n.nanmax(Z) / n_levels
        
        for ind in range(0, n_levels+1):
            levels.append(ind*step)
            
        # Set plot properties
        #------------------------------    
        cmap = plt.cm.get_cmap("jet")
        fig = plt.figure()
        CS = plt.contourf(freesurf_class.X, freesurf_class.Y, Z, levels, cmap=cmap)
        #plt.title(r'$(\beta= $' + str(round(m.degrees(beta),1)) + '$^\circ$' + ',' + ' ' + r'$\lambda / a = $' + str(round(case,1)) + ')', fontsize=20)
        plt.xlabel('$x$' + ' ' + '$(m)$', fontsize=18)
        plt.ylabel('$y$' + ' ' + '$(m)$', fontsize=18)
        #plt.tick_params(axis='x', labelsize=20)
        #plt.tick_params(axis='y', labelsize=20)
        plt.gca().set_aspect('equal', adjustable='box')
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("right", "5%", pad="3%")

        cbar = plt.colorbar(CS, cax=cax, format='%1.1f')
        cbar.ax.set_ylabel('$|\eta^{N} - \eta^{IT}| / A\,(\%)$', fontsize=20)
        
        plt.show(block=True)
        
        ax = fig.add_subplot(111)
        rect1 = mpl.patches.Rectangle((-3,-3), 6, 6, color='black')
        rect2 = mpl.patches.Rectangle((-3,-3+12), 6, 6, color='black')
        romb1 = mpl.patches.Polygon([[-n.sqrt(18)+12,0], [12,-n.sqrt(18)],[12+n.sqrt(18),0], [12,n.sqrt(18)]], closed='True', color='black')
        romb2 = mpl.patches.Polygon([[-n.sqrt(18)+12,12], [12,-n.sqrt(18)+12],[12+n.sqrt(18),12], [12,n.sqrt(18)+12]], closed='True', color='black')
        
        ax.add_patch(rect1)
        ax.add_patch(rect2)
        ax.add_patch(romb1)
        ax.add_patch(romb2)
        
        plt.savefig('eta_total_comparison'+'_'+ 'beta' + str(round(m.degrees(beta),1))  + 'case' + str(round(case,1)) +'.eps',bbox_inches='tight',pad_inches=3)
        
def plot_mean_error(list_mean_err, list_ev_modes):
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    #ax.yaxis.set_major_formatter(ticker.FormatStrFormatter('%.3e'))
    plt.ticklabel_format(style = 'sci', useOffset=False)
    plt.xlabel('L', fontsize=20)
    plt.ylabel('Mean Error (%)',fontsize=20)
    plt.plot(list_ev_modes, list_mean_err, ':D')
    plt.show
    

def plot_conditioning(cond_0ev,cond_6ev,cond_18ev,distances,case):
    
    plt.title('$\lambda/a=$'+str(case), fontsize=25)
    plt.xlabel('d/a', fontsize=20)
    plt.ylabel('Condition Number',fontsize=20)
    plt.yscale('log', nonposy='clip')
    plt.xticks(fontsize=18)
    plt.yticks(fontsize=18)
    plt.plot(distances,cond_0ev, 'r',label='L=0')
    plt.plot(distances,cond_6ev, 'b',label='L=6')
    plt.plot(distances,cond_18ev, 'g',label='L=18')
    
    plt.legend(loc='best', fontsize=20, frameon=False)
    plt.savefig('conditioning'+str(case) +'.eps',bbox_inches='tight',pad_inches=3)
    
    
def plot_damping_isol_directindirect(x_it, damping_total_direct, damping_total_indirect, index):
    
    plt.figure()
    plt.xlabel('$ka$', fontsize=20)
    plt.ylabel(r'$ D / \rho g \pi a^2 (d-h) / (g/a)^{0.5}$',fontsize=20)
    plt.xlim((0,3.0))
    plt.plot(x_it,damping_total_direct, '-r',label='IT direct', linewidth=2)
    plt.plot(x_it,damping_total_indirect, '-.b',label='IT indirect', linewidth=2)
    
    plt.legend(loc='best', fontsize=16, frameon=False)
    plt.savefig('damping'+'body_i' + str(index) +  '.eps',bbox_inches='tight',pad_inches=3)
    
    
def plot_validation_analytical_nemoh(x, damping_nemoh, damping_analytical, index):
    
    plt.figure()
    plt.xlabel('$ka$', fontsize=20)
    plt.ylabel(r'$ D / \rho g \pi a^2 (d-h) / (g/a)^{0.5}$',fontsize=20)
    plt.xlim((0,3.0))
    plt.plot(x,damping_nemoh, '-.b',label='Nemoh', linewidth=2)
    plt.plot(x,damping_analytical, '-r',label='Zeng et al. (2013)', linewidth=2)
    
    plt.legend(loc='best', fontsize=16, frameon=False)
    plt.savefig('damping'+'mode' + str(index) +  '.eps',bbox_inches='tight',pad_inches=3)
    
    
def plot_relationship_G_R_surge(x,G_direct_plus_1,G_direct_zero,G_direct_minus_1, G_haskind_plus_1, G_haskind_zero, G_haskind_minus_1, index):
    
    # real part
    fig = plt.figure()
    plt.xlabel('$ka$', fontsize=20)
    plt.ylabel(r'$Re \{G_m^k \} / \rho g \pi a (d - h)$',fontsize=20)
    plt.xlim((0,3.0))
    ax = fig.add_subplot(111)

                
    ax.annotate('m = 1', xy=(1, 0.02), xytext=(1.25, 0.025),
            arrowprops=dict(arrowstyle="->",))
    
    ax.plot(x,n.real(G_direct_plus_1), '-r',label='direct',linewidth=2)
    ax.plot(x,n.real(G_haskind_plus_1), '-.b',label='indirect', linewidth=2)
    
    ax.annotate('m = 0', xy=(2.25, 0.), xytext=(2.5, 0.005),
           arrowprops=dict(arrowstyle="->",))
            
    ax.plot(x,n.real(G_direct_zero), '-r',linewidth=2)
    ax.plot(x,n.real(G_haskind_zero), '-.b',linewidth=2)
    
    ax.annotate('m = -1', xy=(1, -0.02), xytext=(1.25, -0.025),
           arrowprops=dict(arrowstyle="->",))
            
    ax.plot(x,n.real(G_direct_minus_1), '-r',linewidth=2)
    ax.plot(x,n.real(G_haskind_minus_1), '-.b', linewidth=2)
    
    plt.legend(loc='upper right', fontsize=16, frameon=False)
    plt.savefig('real force transfer matrix' + 'index' + '.eps',bbox_inches='tight',pad_inches=3)
    
    
    # imaginary part
    fig = plt.figure()
    plt.xlabel('$ka$', fontsize=20)
    plt.ylabel(r'$Im \{G_m^k \} / \rho g \pi a (d - h)$',fontsize=20)
    plt.xlim((0,3.0))
    ax = fig.add_subplot(111)

                
    ax.annotate('m = -1', xy=(0.3, 0.02), xytext=(0.6, 0.027),
            arrowprops=dict(arrowstyle="->",))
    
    ax.plot(x,n.imag(G_direct_plus_1), '-r',label='direct',linewidth=2)
    ax.plot(x,n.imag(G_haskind_plus_1), '-.b',label='indirect', linewidth=2)
    
    ax.annotate('m = 0', xy=(2.25, 0.), xytext=(2.5, 0.005),
           arrowprops=dict(arrowstyle="->",))
            
    ax.plot(x,n.imag(G_direct_zero), '-r',linewidth=2)
    ax.plot(x,n.imag(G_haskind_zero), '-.b',linewidth=2)
    
    ax.annotate('m = 1', xy=(0.3, -0.02), xytext=(0.6, -0.027),
           arrowprops=dict(arrowstyle="->",))
            
    ax.plot(x,n.imag(G_direct_minus_1), '-r',linewidth=2)
    ax.plot(x,n.imag(G_haskind_minus_1), '-.b', linewidth=2)
    
    plt.legend(loc='upper right', fontsize=16, frameon=False)
    plt.savefig('imag force transfer matrix' + 'index' + '.eps',bbox_inches='tight',pad_inches=3)
    
def plot_relationship_G_R_roll(x,G_direct_plus_1,G_direct_zero,G_direct_minus_1, G_haskind_plus_1, G_haskind_zero, G_haskind_minus_1, index):
    
    # real part
    fig = plt.figure()
    plt.xlabel('$ka$', fontsize=20)
    plt.ylabel(r'$Re \{G_m^k \} / \rho g \pi a (d - h)$',fontsize=20)
    plt.xlim((0,3.0))
    ax = fig.add_subplot(111)

                
    ax.annotate('m = 1, m = -1', xy=(0.75, -0.04), xytext=(1.0, -0.03),
            arrowprops=dict(arrowstyle="->",))
    
    ax.plot(x,n.real(G_direct_plus_1), '-r',label='direct',linewidth=2)
    ax.plot(x,n.real(G_haskind_plus_1), '-.b',label='indirect', linewidth=2)
    
    ax.annotate('m = 0', xy=(2.25, 0.), xytext=(2.5, 0.005),
           arrowprops=dict(arrowstyle="->",))
            
    ax.plot(x,n.real(G_direct_zero), '-r',linewidth=2)
    ax.plot(x,n.real(G_haskind_zero), '-.b',linewidth=2)
    
            
    ax.plot(x,n.real(G_direct_minus_1), '-r',linewidth=2)
    ax.plot(x,n.real(G_haskind_minus_1), '-.b', linewidth=2)
    
    plt.legend(loc='upper right', fontsize=16, frameon=False)
    plt.savefig('real force transfer matrix' + 'index' + '.eps',bbox_inches='tight',pad_inches=3)
    
    
    # imaginary part
    fig = plt.figure()
    plt.xlabel('$ka$', fontsize=20)
    plt.ylabel(r'$Im \{G_m^k \} / \rho g \pi a (d - h)$',fontsize=20)
    plt.xlim((0,3.0))
    ax = fig.add_subplot(111)

                
    ax.annotate('m = 1, m = -1', xy=(2.25, 0.02), xytext=(1.5, 0.025),
            arrowprops=dict(arrowstyle="->",))
    
    ax.plot(x,n.imag(G_direct_plus_1), '-r',label='direct',linewidth=2)
    ax.plot(x,n.imag(G_haskind_plus_1), '-.b',label='indirect', linewidth=2)
    
    ax.annotate('m = 0', xy=(2.25, 0.), xytext=(2.5, 0.005),
           arrowprops=dict(arrowstyle="->",))
            
    ax.plot(x,n.imag(G_direct_zero), '-r',linewidth=2)
    ax.plot(x,n.imag(G_haskind_zero), '-.b',linewidth=2)
    
    
    ax.plot(x,n.imag(G_direct_minus_1), '-r',linewidth=2)
    ax.plot(x,n.imag(G_haskind_minus_1), '-.b', linewidth=2)
    
    plt.legend(loc='upper left', fontsize=16, frameon=False)
    plt.savefig('imag force transfer matrix' + 'index' + '.eps',bbox_inches='tight',pad_inches=3)
    
    
def plot_conditioning_siddorn(x_it,cond_it,x_siddorn,cond_siddorn):
    
    plt.xlabel('$ka$', fontsize=20)
    plt.ylabel('Condition Number',fontsize=20)
    plt.plot(x_siddorn,cond_siddorn, '--r',label='Siddorn et al. (2008)',linewidth=2)
    plt.plot(x_it,cond_it, '-.b',label='IT', linewidth=2)
    
    plt.legend(loc='best', fontsize=18, frameon=False)
    plt.savefig('conditioning' +'.eps',bbox_inches='tight',pad_inches=3)
    
def plot_excforces_siddorn(x_it,exc_it,x_siddorn,exc_siddorn,body):
    
    plt.figure()
    plt.xlabel('$ka$', fontsize=20)
    plt.ylabel(r'$ | F | / A \rho g \pi a (d-h)$',fontsize=20)
    plt.plot(x_siddorn,exc_siddorn, '--r',label='Siddorn et al. (2008)',linewidth=2)
    plt.plot(x_it,exc_it, '-.b',label='IT', linewidth=2)
    
    plt.legend(loc='best', fontsize=18, frameon=False)
    plt.savefig('exc_force'+'body'+str(body) +'.eps',bbox_inches='tight',pad_inches=3)
    
def plot_excforces_siddorn_real_imag(x_it, exc_it_real, exc_it_imag, exc_hask_real, exc_hask_imag, body, mode):
    
    plt.figure()
    plt.xlabel('$ka$', fontsize=20)
    plt.ylabel(r'$ Re \{ F \} / A \rho g \pi a (d-h)$',fontsize=20)
    plt.plot(x_it,exc_it_real, '-r',label='IT direct', linewidth=2)
    plt.plot(x_it,exc_hask_real, '-.b',label='IT indirect', linewidth=2)
    
    plt.legend(loc='best', fontsize=18, frameon=False)
    plt.savefig('exc_force_real'+'body'+str(body) + 'mode' + str(mode) + '.eps',bbox_inches='tight',pad_inches=3)
    
    plt.figure()
    plt.xlabel('$ka$', fontsize=20)
    plt.ylabel(r'$ Im \{ F \} / A \rho g \pi a (d-h)$',fontsize=20)
    plt.plot(x_it,exc_it_imag, '-r',label='IT direct', linewidth=2)
    plt.plot(x_it,exc_hask_imag, '-.b',label='IT indirect', linewidth=2)
    
    plt.legend(loc='best', fontsize=18, frameon=False)
    plt.savefig('exc_force_imag'+'body'+str(body) + 'mode' + str(mode) + '.eps',bbox_inches='tight',pad_inches=3)
    
def plot_excforces_siddorn_abs_isol_array(x_it, x_siddorn, exc_total_direct_abs,exc_total_abs, exc_body_abs, exc_neighbours_abs, exc_siddorn, body, mode):
    
    plt.figure()
    plt.xlabel('$ka$', fontsize=20)
    plt.ylabel(r'$ | F | / A \rho g \pi a (d-h)$',fontsize=20)
    plt.plot(x_it,exc_total_direct_abs, '-.g',label='IT direct (total)', linewidth=2)
    plt.plot(x_it,exc_total_abs, '--b',label='IT indirect (total)', linewidth=2)
    plt.plot(x_siddorn, exc_siddorn, '-r',label='Siddorn et al. (2008)', linewidth=2)
    plt.plot(x_it,exc_body_abs, '-.k',label='IT indirect (isolated body)', linewidth=2)
    plt.plot(x_it,exc_neighbours_abs, ':k',label='IT indirect (interaction)', linewidth=2)
    
    plt.legend(loc='best', fontsize=16, frameon=False)
    plt.savefig('exc_force_real'+'body'+str(body) + 'mode' + str(mode) + '.eps',bbox_inches='tight',pad_inches=3)
    
def plot_damping_siddorn_nondiag(x_it, x_siddorn, damping_total_direct, damping_total_indirect, damping_siddorn, body_i, body_j,mode_i, mode_j):
    
    plt.figure()
    plt.xlabel('$ka$', fontsize=20)
    plt.ylabel(r'$ D / \rho g \pi a^2 (d-h) / (g/a)^{0.5}$',fontsize=20)
    plt.plot(x_it,damping_total_direct, '-.g',label='IT direct', linewidth=2)
    plt.plot(x_it,damping_total_indirect, '--b',label='IT indirect', linewidth=2)
    plt.plot(x_siddorn, damping_siddorn, '-r',label='Siddorn et al. (2008)', linewidth=2)
    
    plt.legend(loc='best', fontsize=16, frameon=False)
    plt.savefig('damping'+'body_i'+str(body_i) + 'body_j'+str(body_j)+'mode_i' + str(mode_i) +'mode_j' + str(mode_j)+ '.eps',bbox_inches='tight',pad_inches=3)
    
def plot_damping_siddorn_abs_isol_array(x_it, x_siddorn, damping_total_direct,damping_total_indirect, damping_isol_indirect, damping_interact_indirect, damp_siddorn, body_i, body_j,mode_i, mode_j):
    
    plt.figure()
    plt.xlabel('$ka$', fontsize=20)
    plt.ylabel(r'$ D / \rho g \pi a^2 (d-h) / (g/a)^{0.5}$',fontsize=20)
    plt.plot(x_it,damping_total_direct, '-.g',label='IT direct (total)', linewidth=2)
    plt.plot(x_it,damping_total_indirect, '--b',label='IT indirect (total)', linewidth=2)
    plt.plot(x_siddorn, damp_siddorn, '-r',label='Siddorn et al. (2008)', linewidth=2)
    plt.plot(x_it,damping_isol_indirect, '-.k',label='IT indirect (isolated body)', linewidth=2)
    plt.plot(x_it,damping_interact_indirect, ':k',label='IT indirect (interaction)', linewidth=2)
    
    plt.legend(loc='best', fontsize=16, frameon=False)
    plt.savefig('damping'+'body_i'+str(body_i) + 'body_j'+str(body_j)+'mode_i' + str(mode_i) +'mode_j' + str(mode_j)+ '.eps',bbox_inches='tight',pad_inches=3)
    
def plot_conditioning_siddorn_limitedaxisval(x_it,cond_it,x_siddorn,cond_siddorn):
    
    plt.figure()
    plt.xlabel('$ka$', fontsize=20)
    plt.ylabel('Condition Number',fontsize=20)
    plt.plot(x_siddorn,cond_siddorn, '--r',label='Siddorn et al. (2008)',linewidth=2)
    plt.plot(x_it,cond_it, '-.b',label='IT', linewidth=2)
    plt.xlim((0,2.5))
    plt.ylim((0,100))
    
    
    plt.legend(loc='best', fontsize=18, frameon=False)
    plt.savefig('conditioning' +'.eps',bbox_inches='tight',pad_inches=3)
    
    
def plot_conditioning_1evmode(cond_0ev,frequencies):
    
    plt.xlabel(r'$\omega$ (rad/s)', fontsize=20)
    plt.ylabel('Matrix Conditioning',fontsize=20)
    #plt.yscale('log', nonposy='clip')
    plt.plot(frequencies,cond_0ev, 'r',label='L=0')
    
    plt.legend(loc='best', fontsize=20, frameon=False)
    plt.savefig('conditioning'+'longdistance'+'.eps',bbox_inches='tight',pad_inches=3)
    
def plot_q_factors_global_comparison(q_factors_global,non_dim_distances,beta_list,non_dim_wavelengths,ind_wavelength):
    
    numbeta = len(beta_list)
    
    plt.figure()
    plt.title('$\lambda/a=$'+str(non_dim_wavelengths[ind_wavelength]), fontsize=25)
    plt.xlabel('d/a', fontsize=20)
    plt.ylabel('$q_T$',fontsize=20)
    for ind in range(0,numbeta):
        plt.plot(non_dim_distances,q_factors_global[ind_wavelength][ind],label=r'$\beta=$'+str(beta_list[ind]))
    
    plt.legend(loc='best', fontsize=18, frameon=False)
    plt.show()
    plt.savefig('qfactors_globalcomparison_lambdaa'+str(non_dim_wavelengths[ind_wavelength]) +'.eps',bbox_inches='tight',pad_inches=3)