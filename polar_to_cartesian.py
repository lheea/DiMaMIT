#IMPORT MODULES
#------------------------------------------------------------------------------
import numpy as n
import cmath as cm
import scipy.special as spec
import sys
#------------------------------------------------------------------------------
#
#CLASS DEFINING THE MAPPING FROM POLAR TO CARTESIAN
#------------------------------------------------------------------------------
class Polar_to_Cartesian:
    
    # Class Initialization
    #--------------------------------------------------------------------------
    def __init__(self, reflection_x_coord, transmitted_x_coord, N_angular_samples, N_complex_samples, gamma):
        
        self.x0       = reflection_x_coord
        self.x1       = transmitted_x_coord
        self.Na       = N_angular_samples
        self.angles   = n.linspace(- n.pi/2,n.pi/2,self.Na)
        self.gamma    = gamma
        self.Ng       = int(gamma * N_complex_samples)
        self.xi_vect  = self._get_xi_vector()
        
    #--------------------------------------------------------------------------  
    # 
    # Function to compute xi vector from truncation values
    #--------------------------------------------------------------------------
    def _get_xi_vector(self):
        
        imag_b1 = n.linspace(self.gamma, 0., self.Ng)
        b1 = -n.pi / 2.0 + imag_b1 * (1j)
        
        b2 = self.angles
        
        imag_b3 = n.linspace(0., - self.gamma, self.Ng)
        b3 = n.pi / 2.0 + imag_b3 * (1j)
        
        xi_vector = n.concatenate((b1[0 : self.Ng - 1], b2, b3[1 :self.Ng]))
        
        return xi_vector
    #--------------------------------------------------------------------------  
#==============================================================================
#     *
#     *     MONOCHROMATIC DIRECTIONAL SPECTRUM    
#     *
#==============================================================================
    #==========================================================================
    #     DIFFRACTION PROBLEM
    #==========================================================================
        
    # Function to calculate free surface elevation from scattered transmitted potential
    #--------------------------------------------------------------------------
    def eta_transmitted_scattered(self, farmobj, envobj, ind_w, x, y):
        
        #z = n.zeros((x.shape[0],x.shape[1]))
        
        eta_trans = self.transmitted_scattered_potential(farmobj, envobj, ind_w, x, y)
        
        return eta_trans
    #--------------------------------------------------------------------------
    #  
    # Function to calculate free surface elevation from transmitted potential
    #--------------------------------------------------------------------------
    def eta_transmitted(self, farmobj, envobj, ind_w, x, y):
        
        #z = n.zeros((x.shape[0],x.shape[1]))
        
        eta_trans = self.transmitted_potential(farmobj, envobj, ind_w, x, y)
        
        return eta_trans
    #--------------------------------------------------------------------------
    #
    # Mask cylinders areas
    #--------------------------------------------------------------------------
    def _mask_inner_cylinder(self,myfarm,x,y):
        
        
        #generate an array of false
        interior = n.zeros((x.shape),dtype=bool) 
        
        for ind_body in range(0, myfarm.farm_nbodies):
            
            char_dimension = myfarm.dict_bodies[myfarm.layout[ind_body].name].cyl_rad
            
            center_x = myfarm.layout[ind_body].position[0]
            center_y = myfarm.layout[ind_body].position[1]
            
            condition = n.sqrt((x - center_x)**2 + (y - center_y)**2) < char_dimension
            interior = n.logical_or(interior,condition)
        
        
        return interior
    #--------------------------------------------------------------------------
    #
    # Function to compute transmitted potential
    #--------------------------------------------------------------------------
    def transmitted_potential(self, farmobj, envobj, ind_w, x, y):
        
        # Compute transmitted amplitude spectra
        #----------------------------------------------------------------------
        A_T_S = self.transmitted_amplitude_spectra(farmobj, envobj, ind_w)[0]
        
        A_T_Inc = self.transmitted_amplitude_spectra(farmobj, envobj, ind_w)[1]
        
        A_T = A_T_S + A_T_Inc
        
        # Initialize output variable
        #----------------------------------------------------------------------
        pot = n.zeros((x.shape[0],x.shape[1]), dtype=complex)
        
        for ind_x in range(0,x.shape[0]):
            for ind_y in range(0,x.shape[1]):
        
                # Compute FIRST INTEGRAL
                #----------------------------------------------------------------------
                pot_1 = self._compute_trans_potential_first_integral(A_T, envobj, ind_w, x[ind_x,ind_y], y[ind_x, ind_y])
                
                # Compute SECOND INTEGRAL
                #----------------------------------------------------------------------
                pot_2 = self._compute_trans_potential_second_integral(A_T, envobj, ind_w, x[ind_x,ind_y], y[ind_x,ind_y])
                
                # Compute THIRD INTEGRAL
                #----------------------------------------------------------------------
                pot_3 = self._compute_trans_potential_third_integral(A_T, envobj, ind_w, x[ind_x,ind_y], y[ind_x,ind_y])
                
                # Total potential
                #----------------------------------------------------------------------
                pot[ind_x,ind_y] = (pot_1 + pot_2 + pot_3)
        
        return pot
    #--------------------------------------------------------------------------
    #
    # Function to compute transmitted scattered only potential
    #--------------------------------------------------------------------------
    def transmitted_scattered_potential(self, farmobj, envobj, ind_w, x, y):
        
        # Compute transmitted amplitude spectra
        #----------------------------------------------------------------------
        A_T_S = self.transmitted_amplitude_spectra(farmobj, envobj, ind_w)[0]
        
        A_T = A_T_S
        
        # Initialize output variable
        #----------------------------------------------------------------------
        pot = n.zeros((x.shape[0],x.shape[1]), dtype=complex)
        
        for ind_x in range(0,x.shape[0]):
            for ind_y in range(0,x.shape[1]):
        
                # Compute FIRST INTEGRAL
                #----------------------------------------------------------------------
                pot_1 = self._compute_trans_potential_first_integral(A_T, envobj, ind_w, x[ind_x,ind_y], y[ind_x, ind_y])
                
                # Compute SECOND INTEGRAL
                #----------------------------------------------------------------------
                pot_2 = self._compute_trans_potential_second_integral(A_T, envobj, ind_w, x[ind_x,ind_y], y[ind_x,ind_y])
                
                # Compute THIRD INTEGRAL
                #----------------------------------------------------------------------
                pot_3 = self._compute_trans_potential_third_integral(A_T, envobj, ind_w, x[ind_x,ind_y], y[ind_x,ind_y])
                
                # Total potential
                #----------------------------------------------------------------------
                pot[ind_x,ind_y] = (pot_1 + pot_2 + pot_3)
        
        return pot
    #--------------------------------------------------------------------------
    #
    # Function to compute transmitted potential first integral
    #--------------------------------------------------------------------------    
    def _compute_trans_potential_first_integral(self, A_T, envobj, ind_w, x, y):
        
        # 1. Compute phase lag term
        #----------------------------------------------------------------------
        front_vect = n.zeros(self.Ng, dtype=complex)
        
        A_b1 = A_T[0 : self.Ng]
        
        for ind_xi in range(0, self.Ng):
            
            xi_ang = self.xi_vect[ind_xi]
            
            k = envobj.k0[ind_w]
            
            phase = k * ((x - self.x1) * n.cos(xi_ang) + y * n.sin(xi_ang))
            
            front_vect[ind_xi] = n.exp(1j * phase)
            
        # 2. Compute trapezoidal integration matrix
        #----------------------------------------------------------------------
        Ax = (- self.gamma * 1j) / (self.Ng - 1.0)
        I = self._Integration_matrix_calc_imagb(Ax)
            
        # 3. Assign weight of integration matrix to transmitted spectra
        #----------------------------------------------------------------------
        weighted_trans_amp = n.dot(I, A_b1)
        
        # 4. Compute integral
        #----------------------------------------------------------------------
        pot_1 = n.dot(front_vect, weighted_trans_amp)
        
        return pot_1
    #--------------------------------------------------------------------------
    #
    # Function to compute transmitted potential second integral
    #--------------------------------------------------------------------------
    def _compute_trans_potential_second_integral(self, A_T, envobj, ind_w, x, y):
        
        # 1. Compute phase lag term
        #----------------------------------------------------------------------
        phase_vect = n.zeros(self.Na, dtype=complex)
        
        if (self.Ng == 0.):
            
            A_b2 = A_T
            
        else:
            
            A_b2 = A_T[self.Ng - 1 : self.Ng - 1 + self.Na]
            
        
        for ind_xi in range(self.Ng - 1, self.Ng - 1 + self.Na):
            
            xi_ang = self.xi_vect[ind_xi]
            
            k = envobj.k0[ind_w]
            phase = k * ((x - self.x1) * n.cos(xi_ang) + y * n.sin(xi_ang))
          
            phase_vect[ind_xi - (self.Ng - 1)] = n.exp(1j * phase)
            
        # 2. Compute trapezoidal integration matrix
        #----------------------------------------------------------------------
        Ax = n.pi / (self.Na - 1.0)
        I = self._Integration_matrix_calc(Ax)
            
        # 3. Assign weight of integration matrix to transmitted spectra
        #----------------------------------------------------------------------
        weighted_trans_amp = n.dot(I, A_b2)
        
        # 4. Compute integral
        #----------------------------------------------------------------------
        pot_2 = n.dot(phase_vect, weighted_trans_amp)
        
        return pot_2
    #--------------------------------------------------------------------------
    #
    # Function to compute transmitted potential third integral
    #--------------------------------------------------------------------------    
    def _compute_trans_potential_third_integral(self, A_T, envobj, ind_w, x, y):
        
        # 1. Compute phase lag term
        #----------------------------------------------------------------------
        front_vect = n.zeros(self.Ng, dtype=complex)
        
        A_b3 = A_T[self.Ng - 1 + self.Na - 1 : self.Ng - 1 + self.Na - 1 + self.Ng]
        
        for ind_xi in range(self.Ng - 1 + self.Na - 1, self.Ng - 1 + self.Na - 1 + self.Ng):
            
            xi_ang = self.xi_vect[ind_xi]
            
            k = envobj.k0[ind_w]
            
            phase = k * ((x - self.x1) * n.cos(xi_ang) + y * n.sin(xi_ang))
            
            front_vect[ind_xi - (self.Ng - 1 + self.Na - 1)] = n.exp(1j * phase)
            
        # 2. Compute trapezoidal integration matrix
        #----------------------------------------------------------------------
        Ax = ( - self.gamma * 1j) / (self.Ng - 1.0)
        I = self._Integration_matrix_calc_imagb(Ax)
            
        # 3. Assign weight of integration matrix to transmitted spectra
        #----------------------------------------------------------------------
        weighted_trans_amp = n.dot(I, A_b3)
        
        # 4. Compute integral
        #----------------------------------------------------------------------
        pot_3 = n.dot(front_vect, weighted_trans_amp)
        
        return pot_3
    #--------------------------------------------------------------------------  
    #
    #
    # Function to compute transmitted and reflected amplitude spectra
    # with impulse response function at beta specified
    #-------------------------------------------------------------------------- 
    def transmitted_reflected_impulse_amplitude_spectra(self, farmobj, envobj, ind_w, ind_beta):  
        
        # Angular directions discretization
        beta_array = self.angles
        
        # Compute matrix DVIN of the transmition kernel (2N+1)J x Na
        #----------------------------------------------------------------------
        DVIN = self._DVIN_matrix_calc(farmobj, envobj, ind_w, beta_array) /  (-1j * envobj.gravity / envobj.frequency[ind_w]) 

        # test convert velocity amplitudes into amplitudes
                                                                      
        # Populate matrix VT with dimensions Na x (2N+1)J
        #----------------------------------------------------------------------
        VT = self._VT_trans_matrix_calc(farmobj, envobj, ind_w, beta_array)
        
        # Populate matrix VR with dimensions Na x (2N+1)J
        #----------------------------------------------------------------------
        VR = self._VT_ref_matrix_calc(farmobj, envobj, ind_w, beta_array)
        
        # Compute discretized version of the transmition kernel ()
        #----------------------------------------------------------------------
        M_mat_trans = n.dot(VT,DVIN)
        
        # Compute discretized version of the reflection kernel ()
        #----------------------------------------------------------------------
        M_mat_ref = n.dot(VR,DVIN)
        
        # Compute Trapezoidal Integration matrix 
        #----------------------------------------------------------------------
        Ax = n.pi / (self.Na - 1.)
        I = self._Integration_matrix_calc(Ax)
        
        # Assign weight of integration to the incident spectrum amplitudes
        #----------------------------------------------------------------------
        weighted_AIN = n.dot(I, envobj.wave_amplitudes[ind_w,:])
        
        # Compute contribution from the scattered potential on the transmitted
        #----------------------------------------------------------------------
        A_scatt_trans = n.dot(M_mat_trans, weighted_AIN)
        
         # Compute contribution from the scattered potential on the reflected
        #----------------------------------------------------------------------
        A_scatt_ref = n.dot(M_mat_ref, weighted_AIN)
        
        # Compute contribution from the incident potential on the transmitted
        #----------------------------------------------------------------------
        phase_lag = self._phase_lag_incident_transmitted(envobj, ind_w)
        A_inc_trans = n.dot(phase_lag, envobj.wave_amplitudes[ind_w,:])
        if (self.Ng == 0.):
            A_inc_trans_comp = A_inc_trans
        else:
            comp_vect = n.zeros(self.Ng - 1, dtype = float)
            A_inc_trans_comp = n.concatenate((comp_vect, A_inc_trans, comp_vect))
        
        # Compute impulse response for the transmitted
        #----------------------------------------------------------------------
        Imp_phase_lag = self._phase_lag_incident_transmitted_unidirectional(envobj, ind_w)
        Imp_M_trans  = n.dot(M_mat_trans, I) + Imp_phase_lag
        Imp_M_ref    = n.dot(M_mat_ref, I)
        
        A_scatt_trans_Imp = Imp_M_trans[:, ind_beta]
        A_scatt_ref_Imp   = Imp_M_ref[:, ind_beta]
        A_inc_amb_Imp     = Imp_phase_lag[:, ind_beta]
        
                                                 
        return (A_scatt_trans, A_inc_trans_comp, A_scatt_ref, A_scatt_trans_Imp, A_scatt_ref_Imp, A_inc_amb_Imp)
    #--------------------------------------------------------------------------
    #
    # Function to compute transmitted amplitude spectra
    #--------------------------------------------------------------------------
    def transmitted_amplitude_spectra(self, farmobj, envobj, ind_w):
        
        # Angular directions discretization
        beta_array = self.angles
        
        # Compute matrix DVIN of the transmition kernel (2N+1)J x Na
        #----------------------------------------------------------------------
        DVIN = self._DVIN_matrix_calc(farmobj, envobj, ind_w, beta_array) /  (-1j * envobj.gravity / envobj.frequency[ind_w]) 

        # test convert velocity amplitudes into amplitudes
                                                                      
        # Populate matrix VT with dimensions Na x (2N+1)J
        #----------------------------------------------------------------------
        VT = self._VT_trans_matrix_calc(farmobj, envobj, ind_w, beta_array)
        
        # Compute discretized version of the transmition kernel ()
        #----------------------------------------------------------------------
        M_mat = n.dot(VT,DVIN)
        
        # Compute Trapezoidal Integration matrix 
        #----------------------------------------------------------------------
        Ax = n.pi / (self.Na - 1.)
        I = self._Integration_matrix_calc(Ax)
        
        # Assign weight of integration to the incident spectrum amplitudes
        #----------------------------------------------------------------------
        weighted_AIN = n.dot(I, envobj.wave_amplitudes[ind_w,:])
        
        # Compute contribution from the scattered potential on the transmitted
        #----------------------------------------------------------------------
        A_scatt_trans = n.dot(M_mat, weighted_AIN)
        
        # Compute contribution from the incident potential on the transmitted
        #----------------------------------------------------------------------
        phase_lag = self._phase_lag_incident_transmitted(envobj, ind_w)
        A_inc_trans = n.dot(phase_lag, envobj.wave_amplitudes[ind_w,:])
        if (self.Ng == 0.):
            A_inc_trans_comp = A_inc_trans
        else:
            comp_vect = n.zeros(self.Ng - 1, dtype = float)
            A_inc_trans_comp = n.concatenate((comp_vect, A_inc_trans, comp_vect))
        
        # Compute amplitudes of transmitted spectrum
        #----------------------------------------------------------------------
        A_trans = A_scatt_trans  
        
                                                 
        return (A_trans, A_inc_trans_comp)
    #--------------------------------------------------------------------------
    #
    # Function to compute DVIN matrix of the transmission kernel 
    #--------------------------------------------------------------------------
    def _DVIN_matrix_calc(self, farmobj, envobj, ind_w, beta_array):
        
        # Initialize square matrix of the transmission kernel
        nbodies = farmobj.farm_nbodies
        dim = 2 * farmobj.max_trunc_angmode[ind_w] + 1 
         
        DVIN = n.zeros((nbodies * dim, self.Na), dtype=complex)
        
        # Truncation angular modes
        M = farmobj.max_trunc_angmode[ind_w]
               
        # Get partial waves scattered coefficients due to an incident angular 
        # spectrum with unitary amplitude [ind_body][ind_beta](2N+1)
        #----------------------------------------------------------------------
        (solution_vector, incidence_vector) = \
             farmobj._solve_coefficients_diffraction_interaction(envobj, \
                                                            ind_w, beta_array)
                                                            
        # Treatment in case there are evanescent modes
        #--------------------------------------------------------
        if (farmobj.max_trunc_depthmode[ind_w] != 0):
            
            N = farmobj.max_trunc_depthmode[ind_w]
        
            # Reorder solution vector (as DVIN) to have dimensions (2N+1)J x Na  
            #----------------------------------------------------------------------
            for ind_angle in range(0,self.Na):  
            
                for ind_body in range(0,nbodies):
                    
                    for ind_M in range(-M, M + 1):
                        
                        local_index_vect = (M + ind_M) * (N + 1)
                        
                        global_index_vect = ind_body * dim + M + ind_M
                        
                        DVIN[global_index_vect, ind_angle] = solution_vector[ind_body][ind_angle][local_index_vect]
                        
                        
        # Treatment in case no evanescent modes are used      
        #--------------------------------------------------------                
        else:
            
            # Reorder solution vector (as DVIN) to have dimensions (2N+1)J x Na  
            #----------------------------------------------------------------------
            for ind_angle in range(0,self.Na):  
            
                for ind_body in range(0,nbodies):
                    
                    for ind_M in range(-M, M + 1):
                        
                        local_index_vect = M + ind_M
                        
                        global_index_vect = ind_body * dim + M + ind_M
                        
                        DVIN[global_index_vect, ind_angle] = solution_vector[ind_body][ind_angle][local_index_vect]
                        
        
        return DVIN
    #-------------------------------------------------------------------------- 
    #    
    # Function to populate matrix VT of the transmission kernel 
    #--------------------------------------------------------------------------    
    def _VT_trans_matrix_calc(self, farmobj, envobj, ind_w, beta_array):
        
        # Initialize size output vector VT
        nbodies = farmobj.farm_nbodies
        dim = 2 * farmobj.max_trunc_angmode[ind_w] + 1 
        if (self.Ng == 0):
            Nxi = self.Na
        else:
            Nxi = self.Na + 2 * self.Ng - 2
            
        VT   = n.zeros((Nxi, nbodies * dim), dtype=complex)
        
        # Truncation angular modes
        M = farmobj.max_trunc_angmode[ind_w]
        
        # Wave number
        k = envobj.k0[ind_w]
            
        for ind_angle in range(0, Nxi):
            
            for ind_body in range(0,nbodies):
                
                x_j = farmobj.layout[ind_body].position[0]
                y_j = farmobj.layout[ind_body].position[1]
                a_j = farmobj.dict_bodies[farmobj.layout[ind_body].name].cyl_rad
                
                for ind_M in range(-M, M + 1):
                    
                    global_index_vect = ind_body * dim + M + ind_M
                    
                    term1 = ((-1j) ** ind_M) / n.pi
                    term2 = cm.exp(1j * ind_M * self.xi_vect[ind_angle])
                    phase = k * ((x_j - self.x1) * n.cos(self.xi_vect[ind_angle]) + y_j * n.sin(self.xi_vect[ind_angle]))
                    term3 = cm.exp( - 1j * phase)
                    term4 = (spec.hankel1(ind_M, k * a_j))**(-1.)
                    
                    VT[ind_angle, global_index_vect] =  term1 * term2 * term3 * term4
            
        return VT
    #--------------------------------------------------------------------------   
    #
    # Function to compute trapezoidal integration matrix
    #--------------------------------------------------------------------------
    def _Integration_matrix_calc(self, Ax):
        
        # List with the diagonal elements
        diagonal = [2.0] * self.Na
        diagonal_arr = n.asarray(diagonal)
        diagonal_arr[0] = 1.0
        diagonal_arr[len(diagonal) - 1] = 1.0
    
        
        # Create diagonal matrix with the previous elements
        I = n.diag(diagonal_arr)
        
        I = I * (Ax/2.)
        
        return I
    #-------------------------------------------------------------------------- 
    #
    # Function to compute trapezoidal integration matrix for imaginary branches
    #--------------------------------------------------------------------------
    def _Integration_matrix_calc_imagb(self, Ax):
        
        # List with the diagonal elements
        if (self.Ng == 0.):
            
            I = 0.0
            
        else:
        
            diagonal = [2.0] * self.Ng
            diagonal_arr = n.asarray(diagonal)
            diagonal_arr[0] = 1.0
            diagonal_arr[len(diagonal) - 1] = 1.0
        
            
            # Create diagonal matrix with the previous elements
            I = n.diag(diagonal_arr)
            
            I = I * (Ax/2.)
        
        return I
    #-------------------------------------------------------------------------- 
    #    
    # Function to compute phase lag matrix of the incident potential when transmitted
    #--------------------------------------------------------------------------
    def _phase_lag_incident_transmitted(self, envobj, ind_w):
        
        phase_diag = n.zeros(self.Na, dtype=complex)
        
        for ind_dir in range(0, self.Na):
            
            phase_diag[ind_dir] = cm.exp(1j * envobj.k0[ind_w] * self.x1 * n.cos(self.angles[ind_dir]))
            
        phase_diag_mat = n.diag(phase_diag)
        
        return phase_diag_mat
    #--------------------------------------------------------------------------    
    #     
    # Function to compute reflected amplitude spectra
    #--------------------------------------------------------------------------
    def reflected_amplitude_spectra(self, farmobj, envobj, ind_w ):
        
        # Angular directions discretization
        beta_array = self.angles
        
        # Compute matrix DVIN of the reflection kernel (2N+1)J x Na
        #----------------------------------------------------------------------
        DVIN = self._DVIN_matrix_calc(farmobj, envobj, ind_w, beta_array) /  (-1j * envobj.gravity / envobj.frequency[ind_w])                                
                                       
        # Populate matrix VT with dimensions Na x (2N+1)J
        #----------------------------------------------------------------------
        VT = self._VT_ref_matrix_calc(farmobj, envobj, ind_w, beta_array)
        
        # Compute discretized version of the transmition kernel ()
        #----------------------------------------------------------------------
        M = n.dot(VT, DVIN)
        
        # Compute Trapezoidal Integration matrix 
        #----------------------------------------------------------------------
        Ax = n.pi / (self.Na - 1.)
        I = self._Integration_matrix_calc(Ax)
        
        # Assign weight of integration to the incident spectrum amplitudes
        #----------------------------------------------------------------------
        weighted_AIN = n.dot(I, envobj.wave_amplitudes[ind_w,:])
        
        # Compute contribution from the scattered potential on the transmitted
        #----------------------------------------------------------------------
        A_scatt_ref = n.dot(M, weighted_AIN)
        
        # Compute amplitudes of transmitted spectrum
        #----------------------------------------------------------------------
        A_ref = A_scatt_ref
                                                 
        return A_ref
    #--------------------------------------------------------------------------
    #
    # Function to populate matrix VT of the reflected kernel 
    #--------------------------------------------------------------------------    
    def _VT_ref_matrix_calc(self, farmobj, envobj, ind_w, beta_array):
        
        # Initialize size output vector VT
        nbodies = farmobj.farm_nbodies
        dim = 2 * farmobj.max_trunc_angmode[ind_w] + 1 
        if (self.Ng == 0.):
            Nxi = self.Na
        else:
            Nxi = self.Na + 2 * self.Ng - 2
            
        VT   = n.zeros((Nxi, nbodies * dim), dtype=complex)
                
        # Truncation angular modes
        M = farmobj.max_trunc_angmode[ind_w]
        
        # Wave number
        k = envobj.k0[ind_w]
           
        for ind_angle in range(0, Nxi):
            
            for ind_body in range(0,nbodies):
                
                x_j = farmobj.layout[ind_body].position[0]
                y_j = farmobj.layout[ind_body].position[1]
                a_j = farmobj.dict_bodies[farmobj.layout[ind_body].name].cyl_rad
                
                for ind_M in range(-M, M + 1):
                    
                    global_index_vect = ind_body * dim + M + ind_M
                    
                    term1 = ((1j) ** ind_M) / n.pi
                    term2 = cm.exp(- 1j * ind_M * self.xi_vect[ind_angle])
                    phase = k * ((x_j - self.x0) * n.cos(self.xi_vect[ind_angle]) - y_j * n.sin(self.xi_vect[ind_angle]))
                    term3 = cm.exp( 1j * phase)
                    term4 = (spec.hankel1(ind_M, k * a_j))**(-1.)
                    
                    VT[ind_angle, global_index_vect] =  term1 * term2 * term3 * term4
            
        return VT
    #--------------------------------------------------------------------------
    #
    # Function to calculate free surface elevation from reflected potential
    #--------------------------------------------------------------------------
    def eta_reflected(self, farmobj, envobj, ind_w, x, y):
        
        #z = n.zeros((x.shape[0],x.shape[1]))
        
        eta_ref = self.reflected_potential(farmobj, envobj, ind_w, x, y)
        
        return eta_ref
    #--------------------------------------------------------------------------
    #
    # Function to compute reflected potential
    #--------------------------------------------------------------------------
    def reflected_potential(self, farmobj, envobj, ind_w, x, y):
        
        # Compute reflected amplitude spectra
        #----------------------------------------------------------------------
        A_R_S = self.reflected_amplitude_spectra(farmobj, envobj, ind_w)    
        
        A_R = A_R_S
        
        # Initialize output variable
        #----------------------------------------------------------------------
        pot = n.zeros((x.shape[0],x.shape[1]), dtype=complex)
        
        for ind_x in range(0,x.shape[0]):
            for ind_y in range(0,x.shape[1]):
        
                # Compute FIRST INTEGRAL
                #----------------------------------------------------------------------
                pot_1 = self._compute_ref_potential_first_integral(A_R, envobj, ind_w, x[ind_x,ind_y], y[ind_x, ind_y])
                
                # Compute SECOND INTEGRAL
                #----------------------------------------------------------------------
                pot_2 = self._compute_ref_potential_second_integral(A_R, envobj, ind_w, x[ind_x,ind_y], y[ind_x,ind_y])
                
                # Compute THIRD INTEGRAL
                #----------------------------------------------------------------------
                pot_3 = self._compute_ref_potential_third_integral(A_R, envobj, ind_w, x[ind_x,ind_y], y[ind_x,ind_y])
                
                # Total potential
                #----------------------------------------------------------------------
                pot[ind_x,ind_y] = (pot_1 + pot_2 + pot_3)
        
        return pot
    #--------------------------------------------------------------------------
    #  
    # Function to compute reflected potential first integral
    #--------------------------------------------------------------------------    
    def _compute_ref_potential_first_integral(self, A_R, envobj, ind_w, x, y):
        
        # 1. Compute phase lag term
        #----------------------------------------------------------------------
        front_vect = n.zeros(self.Ng, dtype=complex)
        
        A_b1 = A_R[0 : self.Ng]
        
        for ind_xi in range(0, self.Ng):
            
            xi_ang = self.xi_vect[ind_xi]
            
            k = envobj.k0[ind_w]
            
            phase = k * (- (x - self.x0) * n.cos(xi_ang) + y * n.sin(xi_ang))
            
            front_vect[ind_xi] = n.exp(1j * phase)
            
        # 2. Compute trapezoidal integration matrix
        #----------------------------------------------------------------------
        Ax = (- self.gamma * 1j) / (self.Ng - 1.0)
        I = self._Integration_matrix_calc_imagb(Ax)
            
        # 3. Assign weight of integration matrix to reflected spectra
        #----------------------------------------------------------------------
        weighted_ref_amp = n.dot(I, A_b1)
        
        # 4. Compute integral
        #----------------------------------------------------------------------
        pot_1 = n.dot(front_vect, weighted_ref_amp)
        
        return pot_1
    #--------------------------------------------------------------------------  
    #    
    # Function to compute reflected potential second integral
    #--------------------------------------------------------------------------
    def _compute_ref_potential_second_integral(self, A_R, envobj, ind_w, x, y):
        
        # 1. Compute phase lag term
        #----------------------------------------------------------------------
        phase_vect = n.zeros(self.Na, dtype=complex)
        
        if (self.Ng == 0.):
            
            A_b2 = A_R
            
        else:
            
            A_b2 = A_R[self.Ng - 1 : self.Ng - 1 + self.Na]
        
        for ind_xi in range(self.Ng - 1, self.Ng - 1 + self.Na):
            
            xi_ang = self.xi_vect[ind_xi]
            
            k = envobj.k0[ind_w]
            phase = k * (- (x - self.x0) * n.cos(xi_ang) + y * n.sin(xi_ang))
          
            phase_vect[ind_xi - (self.Ng - 1)] = n.exp(1j * phase)
            
        # 2. Compute trapezoidal integration matrix
        #----------------------------------------------------------------------
        Ax = n.pi / (self.Na - 1.0)
        I = self._Integration_matrix_calc(Ax)
            
        # 3. Assign weight of integration matrix to transmitted spectra
        #----------------------------------------------------------------------
        weighted_ref_amp = n.dot(I, A_b2)
        
        # 4. Compute integral
        #----------------------------------------------------------------------
        pot_2 = n.dot(phase_vect, weighted_ref_amp)
        
        return pot_2
    #--------------------------------------------------------------------------    
    #
    # Function to compute reflected potential third integral
    #--------------------------------------------------------------------------    
    def _compute_ref_potential_third_integral(self, A_R, envobj, ind_w, x, y):
        
        # 1. Compute phase lag term
        #----------------------------------------------------------------------
        front_vect = n.zeros(self.Ng, dtype=complex)
        
        A_b3 = A_R[self.Ng - 1 + self.Na - 1 : self.Ng - 1 + self.Na - 1 + self.Ng]
        
        for ind_xi in range(self.Ng - 1 + self.Na - 1, self.Ng - 1 + self.Na - 1 + self.Ng):
            
            xi_ang = self.xi_vect[ind_xi]
            
            k = envobj.k0[ind_w]
            
            phase = k * (- (x - self.x0) * n.cos(xi_ang) + y * n.sin(xi_ang))
            
            front_vect[ind_xi - (self.Ng - 1 + self.Na - 1)] = n.exp(1j * phase)
            
        # 2. Compute trapezoidal integration matrix
        #----------------------------------------------------------------------
        Ax = (- self.gamma * 1j) / (self.Ng - 1.0)
        I = self._Integration_matrix_calc_imagb(Ax)
            
        # 3. Assign weight of integration matrix to transmitted spectra
        #----------------------------------------------------------------------
        weighted_ref_amp = n.dot(I, A_b3)
        
        # 4. Compute integral
        #----------------------------------------------------------------------
        pot_3 = n.dot(front_vect, weighted_ref_amp)
        
        return pot_3
    #--------------------------------------------------------------------------
    #
    # Energy conservation test (diffraction only)
    #-------------------------------------------------------------------------- 
    def energy_conservation_diffraction(self, farmobj, envobj, ind_w):
        
        A_T_S = self.transmitted_amplitude_spectra(farmobj, envobj, ind_w )[0]
        
        A_T_Inc = self.transmitted_amplitude_spectra(farmobj, envobj, ind_w )[1]
        
        # select only amplitudes related to real values of the xi angles
        if (self.Ng == 0.):
            
            A_T = (A_T_S + A_T_Inc)
            
            A_R = self.reflected_amplitude_spectra(farmobj, envobj, ind_w)
            
        else:
            
            A_T = (A_T_S + A_T_Inc)[self.Ng - 1 : self.Ng - 1 + self.Na]
    
            A_R = self.reflected_amplitude_spectra(farmobj, envobj, ind_w)[self.Ng - 1 : self.Ng - 1 + self.Na]
        
        Ax = n.pi / (self.Na - 1.)
        I = self._Integration_matrix_calc(Ax)
        
        E = n.sum(n.dot(I,(abs(A_T)**2))) + n.sum(n.dot(I,(abs(A_R)**2))) 
        
        Initial_E = n.sum(n.dot(I, (abs(envobj.wave_amplitudes[ind_w,:]))**2))
        
        return (E, Initial_E)
    #--------------------------------------------------------------------------    
    #    
    # Reflected and transmitted coefficients
    #-------------------------------------------------------------------------- 
    def reflected_trans_coefficient(self, farmobj, envobj, ind_w):
        
        A_T_S = self.transmitted_amplitude_spectra(farmobj, envobj, ind_w )[0]
        
        A_T_Inc = self.transmitted_amplitude_spectra(farmobj, envobj, ind_w )[1]
        
        # select only amplitudes related to real values of the xi angles
        if (self.Ng == 0.):
            
            A_T = (A_T_S + A_T_Inc)
            
            A_R = self.reflected_amplitude_spectra(farmobj, envobj, ind_w)
            
        else:
            
            A_T = (A_T_S + A_T_Inc)[self.Ng - 1 : self.Ng - 1 + self.Na]
    
            A_R = self.reflected_amplitude_spectra(farmobj, envobj, ind_w)[self.Ng - 1 : self.Ng - 1 + self.Na]
        
        Ax = n.pi / (self.Na - 1.)
        I = self._Integration_matrix_calc(Ax)
        
        R_coeff =  n.sqrt(n.sum(n.dot(I,(abs(A_R)**2))) / n.sum(n.dot(I, (abs(envobj.wave_amplitudes[ind_w,:]))**2)))
        
        T_coeff =  n.sqrt(n.sum(n.dot(I,(abs(A_T)**2))) / n.sum(n.dot(I, (abs(envobj.wave_amplitudes[ind_w,:]))**2)))
        
        return (R_coeff, T_coeff)
        
    #==========================================================================
    #     RADIATION PROBLEM
    #==========================================================================  
        
    # Function to calculate free surface elevation from scattered transmitted potential
    #--------------------------------------------------------------------------
    def eta_transmitted_radiated(self, farmobj, envobj, mechobj, ind_w, x, y):
        
        #z = n.zeros((x.shape[0],x.shape[1]))
        
        eta_trans = self.transmitted_radiated_potential(farmobj, envobj, mechobj, ind_w, x, y)
        
        return eta_trans
    #--------------------------------------------------------------------------
    #
    # Function to calculate free surface elevation from scattered transmitted potential
    #--------------------------------------------------------------------------
    def eta_reflected_radiated(self, farmobj, envobj, mechobj, ind_w, x, y):
        
        #z = n.zeros((x.shape[0],x.shape[1]))
        
        eta_ref = self.reflected_radiated_potential(farmobj, envobj, mechobj, ind_w, x, y)
        
        return eta_ref
    #--------------------------------------------------------------------------    
    #
    # Function to compute reflected potential
    #--------------------------------------------------------------------------
    def reflected_radiated_potential(self, farmobj, envobj, mechobj, ind_w, x, y):
        
        # Compute reflected amplitude spectra
        #----------------------------------------------------------------------
        A_R_S = self.reflected_amplitude_spectra_radiation(farmobj, envobj, mechobj, ind_w)    
        
        A_R = A_R_S
        
        # Initialize output variable
        #----------------------------------------------------------------------
        pot = n.zeros((x.shape[0],x.shape[1]), dtype=complex)
        
        for ind_x in range(0,x.shape[0]):
            for ind_y in range(0,x.shape[1]):
        
                # Compute FIRST INTEGRAL
                #----------------------------------------------------------------------
                pot_1 = self._compute_ref_potential_first_integral(A_R, envobj, ind_w, x[ind_x,ind_y], y[ind_x, ind_y])
                
                # Compute SECOND INTEGRAL
                #----------------------------------------------------------------------
                pot_2 = self._compute_ref_potential_second_integral(A_R, envobj, ind_w, x[ind_x,ind_y], y[ind_x,ind_y])
                
                # Compute THIRD INTEGRAL
                #----------------------------------------------------------------------
                pot_3 = self._compute_ref_potential_third_integral(A_R, envobj, ind_w, x[ind_x,ind_y], y[ind_x,ind_y])
                
                # Total potential
                #----------------------------------------------------------------------
                pot[ind_x,ind_y] = (pot_1 + pot_2 + pot_3)
        
        return pot
    #--------------------------------------------------------------------------
    # 
    # Function to compute transmitted scattered only potential
    #--------------------------------------------------------------------------
    def transmitted_radiated_potential(self, farmobj, envobj, mechobj, ind_w, x, y):
        
        # Compute transmitted amplitude spectra
        #----------------------------------------------------------------------
        A_T_S = self.transmitted_amplitude_spectra_radiation(farmobj, envobj, mechobj, ind_w)
        
        A_T = A_T_S
        
        # Initialize output variable
        #----------------------------------------------------------------------
        pot = n.zeros((x.shape[0],x.shape[1]), dtype=complex)
        
        for ind_x in range(0,x.shape[0]):
            for ind_y in range(0,x.shape[1]):
        
                # Compute FIRST INTEGRAL
                #----------------------------------------------------------------------
                pot_1 = self._compute_trans_potential_first_integral(A_T, envobj, ind_w, x[ind_x,ind_y], y[ind_x, ind_y])
                
                # Compute SECOND INTEGRAL
                #----------------------------------------------------------------------
                pot_2 = self._compute_trans_potential_second_integral(A_T, envobj, ind_w, x[ind_x,ind_y], y[ind_x,ind_y])
                
                # Compute THIRD INTEGRAL
                #----------------------------------------------------------------------
                pot_3 = self._compute_trans_potential_third_integral(A_T, envobj, ind_w, x[ind_x,ind_y], y[ind_x,ind_y])
                
                # Total potential
                #----------------------------------------------------------------------
                pot[ind_x,ind_y] = (pot_1 + pot_2 + pot_3)
        
        return pot
    #--------------------------------------------------------------------------
    #
    # Function to compute transmitted, reflected, and impulse radiated spectra
    #--------------------------------------------------------------------------
    def transmitted_reflected_impulse_spectra_radiation(self, farmobj, envobj, mechobj, ind_w, ind_beta):
        
        # Angular directions discretization
        beta_array = self.angles
        
        # Compute matrix DVIN of the transmition kernel (2N+1)J x Na
        #----------------------------------------------------------------------
        DVIN_rad = self._DVIN_matrix_calc_radiation(farmobj, envobj, mechobj, ind_w) /  (-1j * envobj.gravity / envobj.frequency[ind_w]) 
                                                                      
        # Populate matrix VT with dimensions Na x (2N+1)J
        #----------------------------------------------------------------------
        VT = self._VT_trans_matrix_calc(farmobj, envobj, ind_w, beta_array)
        
        # Populate matrix VR with dimensions Na x (2N+1)J
        #----------------------------------------------------------------------
        VR = self._VT_ref_matrix_calc(farmobj, envobj, ind_w, beta_array)
        
        # Compute discretized version of the transmition kernel ()
        #----------------------------------------------------------------------
        M_mat_trans = n.dot(VT,DVIN_rad)
        
         # Compute discretized version of the transmition kernel ()
        #----------------------------------------------------------------------
        M_mat_ref = n.dot(VR,DVIN_rad)
        
        # Compute Trapezoidal Integration matrix 
        #----------------------------------------------------------------------
        Ax = n.pi / (self.Na - 1.)
        I = self._Integration_matrix_calc(Ax)
        
        # Assign weight of integration to the incident spectrum amplitudes
        #----------------------------------------------------------------------
        weighted_AIN = n.dot(I, envobj.wave_amplitudes[ind_w,:])
        
        # Compute contribution from the scattered potential on the transmitted
        #----------------------------------------------------------------------
        A_rad_trans = n.dot(M_mat_trans, weighted_AIN)
        
        # Compute contribution from the scattered potential on the reflected
        #----------------------------------------------------------------------
        A_rad_ref = n.dot(M_mat_ref, weighted_AIN)
        
        # Compute impuse response for an ind_beta
        #----------------------------------------------------------------------
        Imp_M_mat_trans = n.dot(M_mat_trans, I)
        Imp_M_mat_ref   = n.dot(M_mat_ref, I)
        
        A_rad_trans_Imp = Imp_M_mat_trans[:, ind_beta]
        A_rad_ref_Imp   = Imp_M_mat_ref[:, ind_beta]
        
        
        return (A_rad_trans, A_rad_ref, A_rad_trans_Imp, A_rad_ref_Imp)
    #--------------------------------------------------------------------------    
    #
    # Function to compute transmitted radiation amplitude spectra
    #--------------------------------------------------------------------------
    def transmitted_amplitude_spectra_radiation(self, farmobj, envobj, mechobj, ind_w):
        
        # Angular directions discretization
        beta_array = self.angles
        
        # Compute matrix DVIN of the transmition kernel (2N+1)J x Na
        #----------------------------------------------------------------------
        DVIN_rad = self._DVIN_matrix_calc_radiation(farmobj, envobj, mechobj, ind_w) /  (-1j * envobj.gravity / envobj.frequency[ind_w]) 
                                                                      
        # Populate matrix VT with dimensions Na x (2N+1)J
        #----------------------------------------------------------------------
        VT = self._VT_trans_matrix_calc(farmobj, envobj, ind_w, beta_array)
        
        # Compute discretized version of the transmition kernel ()
        #----------------------------------------------------------------------
        M_mat = n.dot(VT,DVIN_rad)
        
        # Compute Trapezoidal Integration matrix 
        #----------------------------------------------------------------------
        Ax = n.pi / (self.Na - 1.)
        I = self._Integration_matrix_calc(Ax)
        
        # Assign weight of integration to the incident spectrum amplitudes
        #----------------------------------------------------------------------
        weighted_AIN = n.dot(I, envobj.wave_amplitudes[ind_w,:])
        
        # Compute contribution from the scattered potential on the transmitted
        #----------------------------------------------------------------------
        A_rad_trans = n.dot(M_mat, weighted_AIN)
        
        
        return A_rad_trans
    #--------------------------------------------------------------------------
    #
    # Function to compute reflected amplitude spectra
    #--------------------------------------------------------------------------
    def reflected_amplitude_spectra_radiation(self, farmobj, envobj, mechobj, ind_w ):
        
        # Angular directions discretization
        beta_array = self.angles
        
        # Compute matrix DVIN of the reflection kernel (2N+1)J x Na
        #----------------------------------------------------------------------
        DVIN_rad = self._DVIN_matrix_calc_radiation(farmobj, envobj, mechobj, ind_w) /  (-1j * envobj.gravity / envobj.frequency[ind_w])                                
                                       
        # Populate matrix VT with dimensions Na x (2N+1)J
        #----------------------------------------------------------------------
        VT = self._VT_ref_matrix_calc(farmobj, envobj, ind_w, beta_array)
        
        # Compute discretized version of the transmition kernel ()
        #----------------------------------------------------------------------
        M = n.dot(VT, DVIN_rad)
        
        # Compute Trapezoidal Integration matrix 
        #----------------------------------------------------------------------
        Ax = n.pi / (self.Na - 1.)
        I = self._Integration_matrix_calc(Ax)
        
        # Assign weight of integration to the incident spectrum amplitudes
        #----------------------------------------------------------------------
        weighted_AIN = n.dot(I, envobj.wave_amplitudes[ind_w,:])
        
        # Compute contribution from the scattered potential on the transmitted
        #----------------------------------------------------------------------
        A_scatt_ref = n.dot(M, weighted_AIN)
        
        # Compute amplitudes of transmitted spectrum
        #----------------------------------------------------------------------
        A_ref = A_scatt_ref
                                                 
        return A_ref
    #--------------------------------------------------------------------------
    #
    # Function to compute DVIN matrix of the transmission kernel radiation
    #--------------------------------------------------------------------------
    def _DVIN_matrix_calc_radiation(self, farmobj, envobj, mechobj, ind_w):
        
        # Initialize square matrix of the transmission kernel
        nbodies = farmobj.farm_nbodies
        dim = 2 * farmobj.max_trunc_angmode[ind_w] + 1 
         
        DVIN_rad = n.zeros((nbodies * dim, self.Na), dtype=complex)
         
        # Number of degrees of freedom
        ndofs = len(farmobj.productive_dofarray)
        
        # Initialize velocities
        velocities = n.zeros((self.Na, ndofs), dtype=complex)
        
        # Basis radiation matrix calculation
        #----------------------------------------------------------------------
        basis_rad_mat = self._basis_matrix_calc_radiation(farmobj, envobj, ind_w)
               
        # For each beta direction , get RAO and scale R_vectors accordingly
        #----------------------------------------------------------------------
        for ind_angle in range(0, self.Na):
            
            RAO = mechobj.RAO_calc(envobj, ind_w, ind_angle)

            velocities[ind_angle,:] = -1j * envobj.frequency[ind_w] * RAO
            
            for ind_dof in range(0, ndofs):
                DVIN_rad[:, ind_angle] = velocities[ind_angle,ind_dof] * basis_rad_mat[:, ind_dof] + DVIN_rad[:, ind_angle]
                
        
        return DVIN_rad
    #--------------------------------------------------------------------------     
    #
    # Function to compute basis radiation matrix (2N+1)J x ndofs
    #--------------------------------------------------------------------------
    def _basis_matrix_calc_radiation(self, farmobj, envobj, ind_w):
        
        # Initialize basis radiation matrices (S: scattered radiation, R: radiation)
        nbodies = farmobj.farm_nbodies
        dim = 2 * farmobj.max_trunc_angmode[ind_w] + 1 
        
        ndofs = len(farmobj.productive_dofarray)
         
        basis_mat_rad_S     = n.zeros((nbodies * dim, ndofs), dtype=complex)
        basis_mat_rad_R     = n.zeros((nbodies * dim, ndofs), dtype=complex)
        basis_mat_rad_total = n.zeros((nbodies * dim, ndofs), dtype=complex)
        
        
        # Initialize temporary list retaining scattered coeffs
        temp_list_S = []
        for ind_dof in range(0, ndofs):
            temp_list_S.append([])
            
        # Initialize temporary list retaining radiated coeffs
        temp_list_R = []
        for ind_dof in range(0, ndofs):
            temp_list_R.append(n.split(n.zeros((nbodies * dim), dtype=complex), nbodies))
        
        # Populate matrix
        # Get scattered coefficients for rad problems[ind_w][ind_body][ind_dof]
        scatt_coeffs = farmobj.global_solution_vector_radiation
        
        # Treatment in case there are evanescent modes
        #--------------------------------------------------------
        if (farmobj.max_trunc_depthmode[ind_w] != 0):
            
            # list of progressive indices
            prog_list = []
            M = farmobj.max_trunc_angmode[ind_w]
            N = farmobj.max_trunc_depthmode[ind_w]
            for ind_m in range(-M, M + 1):
                prog_list.append( (M + ind_m) * (N + 1))
                
            # basis mat with scattered components 
            for ind_dof in range(0, ndofs):
               for ind_body in range(0, nbodies):
                   
                   temp_list_S[ind_dof].append(scatt_coeffs[ind_w][ind_body][ind_dof][prog_list])
                   
            for ind_dof in range(0, ndofs):
               
               basis_mat_rad_S[:, ind_dof] = n.concatenate(temp_list_S[ind_dof])
               
            # basis mat with radiated components
            for ind_dof in range(0, ndofs):
               
               degfreedom = farmobj.productive_dofarray[ind_dof]
               
               body_numb = farmobj.inv_dict_dof_farm[(degfreedom)][0]
               dof_body  = farmobj.inv_dict_dof_farm[(degfreedom)][1] 
               body_name = farmobj.layout[body_numb].name
               
               # Rotate radiation vector if necessary
               #-------------------------------------------------------------
               if (farmobj.layout[body_numb].rotation_angle == 0.0):
                     
                     Rad_vect = farmobj.dict_bodies[(body_name)].\
                               Radiation_Characteristics[ind_w][:,dof_body-1][prog_list] 
               else:
                     
                     rot_ang = farmobj.layout[body_numb].rotation_angle

                     Rotated_Radvect = farmobj.dict_bodies[body_name].\
                                          rotate_operators(rot_ang)[1][ind_w]
                                          
                     Rad_vect = Rotated_Radvect[:,dof_body-1][prog_list]
               #-------------------------------------------------------------
        
               temp_list_R[ind_dof][body_numb] = Rad_vect
               
            for ind_dof in range(0, ndofs):
               
               basis_mat_rad_R[:, ind_dof] = n.concatenate(temp_list_R[ind_dof])
                
            
        # Treatment for no evanescent modes
        #--------------------------------------------------------
        else:
           
           # basis mat with scattered components 
           for ind_dof in range(0, ndofs):
               for ind_body in range(0, nbodies):
                   
                   temp_list_S[ind_dof].append(scatt_coeffs[ind_w][ind_body][ind_dof])
                   
           for ind_dof in range(0, ndofs):
               
               basis_mat_rad_S[:, ind_dof] = n.concatenate(temp_list_S[ind_dof])
               
           # basis mat with radiated components
           for ind_dof in range(0, ndofs):
               
               degfreedom = farmobj.productive_dofarray[ind_dof]
               
               body_numb = farmobj.inv_dict_dof_farm[(degfreedom)][0]
               dof_body  = farmobj.inv_dict_dof_farm[(degfreedom)][1] 
               body_name = farmobj.layout[body_numb].name
               
               # Rotate radiation vector if necessary
               #-------------------------------------------------------------
               if (farmobj.layout[body_numb].rotation_angle == 0.0):
                     
                     Rad_vect = farmobj.dict_bodies[(body_name)].\
                               Radiation_Characteristics[ind_w][:,dof_body-1] 
               else:
                     
                     rot_ang = farmobj.layout[body_numb].rotation_angle

                     Rotated_Radvect = farmobj.dict_bodies[body_name].\
                                          rotate_operators(rot_ang)[1][ind_w]
                                          
                     Rad_vect = Rotated_Radvect[:,dof_body-1]
               #-------------------------------------------------------------
        
               temp_list_R[ind_dof][body_numb] = Rad_vect
               
           for ind_dof in range(0, ndofs):
               
               basis_mat_rad_R[:, ind_dof] = n.concatenate(temp_list_R[ind_dof])
    
        # Sum of two contributions
        basis_mat_rad_total = basis_mat_rad_S + basis_mat_rad_R
        
        return basis_mat_rad_total
    #--------------------------------------------------------------------------     
    #
    # Energy conservation test (diffraction and radiation)
    #-------------------------------------------------------------------------- 
    def energy_conservation_total(self, farmobj, envobj, mechobj, ind_w):
        
        A_T_S = self.transmitted_amplitude_spectra(farmobj, envobj, ind_w )[0]
        
        A_T_Inc = self.transmitted_amplitude_spectra(farmobj, envobj, ind_w )[1]
        
        # select only amplitudes related to real values of the xi angles
        if (self.Ng == 0.):
            
            A_T_diff = (A_T_S + A_T_Inc)
            
            A_R_diff = self.reflected_amplitude_spectra(farmobj, envobj, ind_w)
            
            A_T_Rad =  self.transmitted_amplitude_spectra_radiation(farmobj, envobj, mechobj, ind_w)
            
            A_R_Rad =  self.reflected_amplitude_spectra_radiation(farmobj, envobj, mechobj, ind_w)
            
            A_T = A_T_diff + A_T_Rad
            
            A_R = A_R_diff + A_R_Rad
            
        else:
            
            A_T_diff = (A_T_S + A_T_Inc)[self.Ng - 1 : self.Ng - 1 + self.Na]
    
            A_R_diff = self.reflected_amplitude_spectra(farmobj, envobj, ind_w)[self.Ng - 1 : self.Ng - 1 + self.Na]
            
            A_T_Rad =  self.transmitted_amplitude_spectra_radiation(farmobj, envobj, mechobj, ind_w)[self.Ng - 1 : self.Ng - 1 + self.Na]
            
            A_R_Rad =  self.reflected_amplitude_spectra_radiation(farmobj, envobj, mechobj, ind_w)[self.Ng - 1 : self.Ng - 1 + self.Na]
            
            A_T = A_T_diff + A_T_Rad
            
            A_R = A_R_diff + A_R_Rad
        
        Ax = n.pi / (self.Na - 1.)
        I = self._Integration_matrix_calc(Ax)
        
        E = n.sum(n.dot(I,(abs(A_T)**2))) + n.sum(n.dot(I,(abs(A_R)**2))) 
        
        Initial_E = n.sum(n.dot(I, (abs(envobj.wave_amplitudes[ind_w,:]))**2))
        
        return (E, Initial_E)
    #--------------------------------------------------------------------------
    #    
    # Energy conservation test (diffraction and radiation impulse)
    #-------------------------------------------------------------------------- 
    def energy_conservation_total_impulse(self, farmobj, envobj, mechobj, ind_w, ind_beta_unidir):
        
        (A_S_T, A_I_trans, A_S_R, A_S_T_imp, A_S_R_imp, A_I_amb_imp) = self.transmitted_reflected_impulse_amplitude_spectra(farmobj, envobj, ind_w, ind_beta_unidir)
        (A_R_T, A_R_R, A_R_T_imp, A_R_R_imp) = self.transmitted_reflected_impulse_spectra_radiation(farmobj, envobj, mechobj, ind_w, ind_beta_unidir)
        
        
        # select only amplitudes related to real values of the xi angles
        if (self.Ng == 0.):
            
            A_T_diff = A_S_T + A_I_trans
            A_T_diff_imp = A_S_T_imp #+ A_I_amb_imp
            
            A_R_diff = A_S_R
            A_R_diff_imp = A_S_R_imp
            
            A_T_Rad =  A_R_T
            A_T_Rad_imp = A_R_T_imp
            
            A_R_Rad =  A_R_R
            A_R_Rad_imp = A_R_R_imp
            
            A_T = A_T_diff + A_T_Rad
            A_T_imp = A_T_diff_imp + A_T_Rad_imp
            
            A_R = A_R_diff + A_R_Rad
            A_R_imp = A_R_diff_imp + A_R_Rad_imp
            
        else:
            
            A_T_diff = (A_S_T + A_I_trans)[self.Ng - 1 : self.Ng - 1 + self.Na]
            A_T_diff_imp = (A_S_T_imp + A_I_amb_imp)[self.Ng - 1 : self.Ng - 1 + self.Na]
    
            A_R_diff = A_S_R[self.Ng - 1 : self.Ng - 1 + self.Na]
            A_R_diff_imp = A_S_R_imp[self.Ng - 1 : self.Ng - 1 + self.Na]
            
            A_T_Rad =  A_R_T[self.Ng - 1 : self.Ng - 1 + self.Na]
            A_T_Rad_imp =  A_R_T_imp[self.Ng - 1 : self.Ng - 1 + self.Na]
            
            A_R_Rad =  A_R_R[self.Ng - 1 : self.Ng - 1 + self.Na]
            A_R_Rad_imp =  A_R_R_imp[self.Ng - 1 : self.Ng - 1 + self.Na]
            
            A_T = A_T_diff + A_T_Rad
            A_T_imp = A_T_diff_imp + A_T_Rad_imp
            
            A_R = A_R_diff + A_R_Rad
            A_R_imp = A_R_diff_imp + A_R_Rad_imp
        
        Ax = n.pi / (self.Na - 1.)
        I = self._Integration_matrix_calc(Ax)
        
        E = n.sum(n.dot(I,(abs(A_T)**2))) + n.sum(n.dot(I,(abs(A_R)**2))) 
        #E_imp = n.sum(n.dot(I,(abs(A_T_imp)**2))) + n.sum(n.dot(I,(abs(A_R_imp)**2))) 
        E_imp = n.sum((abs(A_T_imp)**2)) + n.sum((abs(A_R_imp)**2))
        
        Initial_E = n.sum(n.dot(I, (abs(envobj.wave_amplitudes[ind_w,:]))**2))
        #Initial_E_imp = n.sum(n.dot(I, abs(A_I_amb_imp[:])**2))
        Initial_E_imp = n.sum(abs(A_I_amb_imp[:])**2)
        
        return (E, E_imp, Initial_E, Initial_E_imp)
    #-------------------------------------------------------------------------- 
    #    
    # Energy conservation test (diffraction and radiation impulse)
    #-------------------------------------------------------------------------- 
    def energy_conservation_total_impulse_opt(self, farmobj, envobj, mechobj, ind_w, ind_beta_unidir, \
                      A_S_T, A_I_trans, A_S_R, A_S_T_imp, A_S_R_imp, A_I_amb_imp,\
                      A_R_T, A_R_R, A_R_T_imp, A_R_R_imp):
        
        #(A_S_T, A_I_trans, A_S_R, A_S_T_imp, A_S_R_imp, A_I_amb_imp) = self.transmitted_reflected_impulse_amplitude_spectra(farmobj, envobj, ind_w, ind_beta_unidir)
        #(A_R_T, A_R_R, A_R_T_imp, A_R_R_imp) = self.transmitted_reflected_impulse_spectra_radiation(farmobj, envobj, mechobj, ind_w, ind_beta_unidir)
        
        
        # select only amplitudes related to real values of the xi angles
        if (self.Ng == 0.):
            
            A_T_diff = A_S_T + A_I_trans
            A_T_diff_imp = A_S_T_imp + A_I_amb_imp
            
            A_R_diff = A_S_R
            A_R_diff_imp = A_S_R_imp
            
            A_T_Rad =  A_R_T
            A_T_Rad_imp = A_R_T_imp
            
            A_R_Rad =  A_R_R
            A_R_Rad_imp = A_R_R_imp
            
            A_T = A_T_diff + A_T_Rad
            A_T_imp = A_T_diff_imp + A_T_Rad_imp
            
            A_R = A_R_diff + A_R_Rad
            A_R_imp = A_R_diff_imp + A_R_Rad_imp
            
        else:
            
            A_T_diff = (A_S_T + A_I_trans)[self.Ng - 1 : self.Ng - 1 + self.Na]
            A_T_diff_imp = (A_S_T_imp + A_I_amb_imp)[self.Ng - 1 : self.Ng - 1 + self.Na]
    
            A_R_diff = A_S_R[self.Ng - 1 : self.Ng - 1 + self.Na]
            A_R_diff_imp = A_S_R_imp[self.Ng - 1 : self.Ng - 1 + self.Na]
            
            A_T_Rad =  A_R_T[self.Ng - 1 : self.Ng - 1 + self.Na]
            A_T_Rad_imp =  A_R_T_imp[self.Ng - 1 : self.Ng - 1 + self.Na]
            
            A_R_Rad =  A_R_R[self.Ng - 1 : self.Ng - 1 + self.Na]
            A_R_Rad_imp =  A_R_R_imp[self.Ng - 1 : self.Ng - 1 + self.Na]
            
            A_T = A_T_diff + A_T_Rad
            A_T_imp = A_T_diff_imp + A_T_Rad_imp
            
            A_R = A_R_diff + A_R_Rad
            A_R_imp = A_R_diff_imp + A_R_Rad_imp
        
        Ax = n.pi / (self.Na - 1.)
        I = self._Integration_matrix_calc(Ax)
        
        E = n.sum(n.dot(I,(abs(A_T)**2))) + n.sum(n.dot(I,(abs(A_R)**2))) 
        E_imp = n.sum(n.dot(I,(abs(A_T_imp)**2))) + n.sum(n.dot(I,(abs(A_R_imp)**2))) 
        
        Initial_E = n.sum(n.dot(I, (abs(envobj.wave_amplitudes[ind_w,:]))**2))
        Initial_E_imp = n.sum(n.dot(I, abs(A_I_amb_imp[:])**2))
        
        return (E, E_imp, Initial_E, Initial_E_imp)
    #-------------------------------------------------------------------------- 
#==============================================================================
#     *
#     *     MONOCHROMATIC UNIDIRECTIONAL SPECTRUM    
#     *
#==============================================================================    
    #==========================================================================
    #     DIFFRACTION PROBLEM
    #==========================================================================
    # Function to compute transmitted amplitude spectra
    #--------------------------------------------------------------------------
    def transmitted_amplitude_spectra_unidirectional(self, farmobj, envobj, ind_w, ind_beta):
        
         # Angular directions discretization
        beta_array = self.angles
        
        # Compute matrix DVIN of the transmition kernel (2N+1)J x Na
        #----------------------------------------------------------------------
        DVIN = self._DVIN_matrix_calc(farmobj, envobj, ind_w, beta_array) /  (-1j * envobj.gravity / envobj.frequency[ind_w]) 
                                                                      
        # Populate matrix VT with dimensions Na x (2N+1)J
        #----------------------------------------------------------------------
        VT = self._VT_trans_matrix_calc(farmobj, envobj, ind_w, beta_array)
        
        # Compute discretized version of the transmition kernel ()
        #----------------------------------------------------------------------
        M_mat = n.dot(VT,DVIN)
        
        # Compute Trapezoidal Integration matrix 
        #----------------------------------------------------------------------
        Ax = n.pi / (self.Na - 1.)
        I = self._Integration_matrix_calc(Ax)
        
        M_mat_int = n.dot(M_mat, I)
        
        # Compute contribution from the incident potential on the transmitted
        #----------------------------------------------------------------------
        phase_lag = self._phase_lag_incident_transmitted_unidirectional(envobj, ind_w)
                
        A_scatt_trans = phase_lag + M_mat_int
        
                                                         
        return A_scatt_trans[:, ind_beta]
    #--------------------------------------------------------------------------
    #
    #
    #--------------------------------------------------------------------------
    def _phase_lag_incident_transmitted_unidirectional(self, envobj, ind_w):
        
        phase_diag = n.zeros(self.Na, dtype=complex)
        
        
        for ind_dir in range(0, self.Na):
            
            phase_diag[ind_dir] = cm.exp(1j * envobj.k0[ind_w] * self.x1 * n.cos(self.angles[ind_dir]))
            
        phase_diag_mat = n.diag(phase_diag)
        
        if (self.Ng == 0):
            
            phase_mat = phase_diag_mat
            
        else:
            
            aux = n.zeros((self.Ng - 1, self.Na), dtype = complex)
            phase_mat = n.concatenate((aux, phase_diag_mat, aux))
        
        return phase_mat
    #--------------------------------------------------------------------------
    #
    #    
    # Function to compute reflected amplitude spectra
    #--------------------------------------------------------------------------
    def reflected_amplitude_spectra_unidirectional(self, farmobj, envobj, ind_w, ind_beta ):
        
        # Angular directions discretization
        beta_array = self.angles
        
        # Compute matrix DVIN of the reflection kernel (2N+1)J x Na
        #----------------------------------------------------------------------
        DVIN = self._DVIN_matrix_calc(farmobj, envobj, ind_w, beta_array) /  (-1j * envobj.gravity / envobj.frequency[ind_w])                                
                                       
        # Populate matrix VT with dimensions Na x (2N+1)J
        #----------------------------------------------------------------------
        VT = self._VT_ref_matrix_calc(farmobj, envobj, ind_w, beta_array)
        
        # Compute discretized version of the transmition kernel ()
        #----------------------------------------------------------------------
        M = n.dot(VT, DVIN)
        
        # Compute Trapezoidal Integration matrix 
        #----------------------------------------------------------------------
        Ax = n.pi / (self.Na - 1.)
        I = self._Integration_matrix_calc(Ax)
        
        M_mat_int = n.dot(M, I )
        
        # Compute contribution from the scattered potential on the transmitted
        #----------------------------------------------------------------------
        A_scatt_ref = M_mat_int[:, ind_beta]
        
                                                 
        return A_scatt_ref
    #--------------------------------------------------------------------------
    #
    # Energy conservation test (diffraction only)
    #-------------------------------------------------------------------------- 
    def energy_conservation_diffraction_unidir(self, farmobj, envobj, ind_w, ind_beta):
        
        # select only amplitudes related to real values of the xi angles
        if (self.Ng == 0.):
            
            A_T = self.transmitted_amplitude_spectra_unidirectional(farmobj, envobj, ind_w, ind_beta)
            
            A_R = self.reflected_amplitude_spectra_unidirectional(farmobj, envobj, ind_w, ind_beta)
            
        else:
            
            A_T = self.transmitted_amplitude_spectra_unidirectional(farmobj, envobj, ind_w, ind_beta)[self.Ng - 1 : self.Ng - 1 + self.Na]
    
            A_R = self.reflected_amplitude_spectra_unidirectional(farmobj, envobj, ind_w, ind_beta)(farmobj, envobj, ind_w)[self.Ng - 1 : self.Ng - 1 + self.Na]
        
        Ax = n.pi / (self.Na - 1.)
        I = self._Integration_matrix_calc(Ax)
        
        E = n.sum((abs(A_T)**2)) + n.sum((abs(A_R)**2)) 
        #E = n.sum(n.dot(I,(abs(A_T)**2))) + n.sum(n.dot(I,(abs(A_R)**2))) 
        
        Initial_E = n.sum(n.dot(I, (abs(envobj.wave_amplitudes[ind_w,:]))**2))
        
        return (E, Initial_E)
    #--------------------------------------------------------------------------
    #
    # Compute fraction of absorbed energy using formula of power in cylindrical coordinates
    #--------------------------------------------------------------------------
    def absorbed_energy_impulse(self, farmobj, envobj, mechobj, ind_w, ind_beta, beta):  
        
        # Initialize fraction of energy absorbed by each body
        nbodies = farmobj.farm_nbodies
        E_frac = n.zeros(nbodies,dtype=float)
        
        dim = 2 * farmobj.max_trunc_angmode[ind_w] + 1 
        
        # Truncation angular modes
        M = farmobj.max_trunc_angmode[ind_w]
         
        # Angular directions discretization
        beta_array = self.angles
        
        # Compute matrix DVIN with dimensions (2N+1)J x Na (remove normalization factor Hankel function)
        #----------------------------------------------------------------------
        DVIN = self._DVIN_matrix_calc(farmobj, envobj, ind_w, beta_array) /  (-1j * envobj.gravity / envobj.frequency[ind_w]) 
        
        for ind_angle in range(0,self.Na):  
            
                for ind_body in range(0,nbodies):
                    
                    for ind_M in range(-M, M + 1):
                        
                        global_index_vect = ind_body * dim + M + ind_M
                        
                        order = ind_M
                        name  = farmobj.layout[ind_body].name
                        arg   = farmobj.dict_bodies[name].cyl_rad * envobj.k0[ind_w]
                        
                        DVIN[global_index_vect, ind_angle] =  DVIN[global_index_vect, ind_angle] / spec.hankel1(order, arg)
        
        
        # Compute matrix DVIN with dimensions (2N+1)J x Na (remove normalization factor Hankel function)
        #----------------------------------------------------------------------
        DVIN_rad = self._DVIN_matrix_calc_radiation(farmobj, envobj, mechobj, ind_w) /  (-1j * envobj.gravity / envobj.frequency[ind_w]) 
        
        for ind_angle in range(0,self.Na):  
            
                for ind_body in range(0,nbodies):
                    
                    for ind_M in range(-M, M + 1):
                        
                        global_index_vect = ind_body * dim + M + ind_M
                        
                        order = ind_M
                        name  = farmobj.layout[ind_body].name
                        arg   = farmobj.dict_bodies[name].cyl_rad * envobj.k0[ind_w]
                        
                        DVIN_rad[global_index_vect, ind_angle] =  DVIN_rad[global_index_vect, ind_angle] / spec.hankel1(order, arg)
        
        
        # Generate incident vector with dimensions (2N+1)J
        #----------------------------------------------------------------------
        a_incident = self.incident_vector_coefficients(farmobj, envobj, beta, ind_w)
        
        # Compute dm vector with dimensions (2N+1)J
        #----------------------------------------------------------------------
        dm_vector = 0.5 * a_incident + DVIN[:, ind_beta] + DVIN_rad[:, ind_beta]
        
        dm_vector_abs_sq = (abs(dm_vector)) ** 2
        
        # Compute diffraction only vector with dimensions (2N+1)J
        #----------------------------------------------------------------------
        diff_vect = 0.5 * a_incident + DVIN[:, ind_beta]
        #diff_vect = 0.5 * a_incident 
        
        diff_vect_abs_sq = (abs(diff_vect)) ** 2
        
        # Compute numerator and denomminator vectors
        #----------------------------------------------------------------------
        num_vect = diff_vect_abs_sq - dm_vector_abs_sq
        den_vect = diff_vect_abs_sq
#        
        E_total = n.sum(num_vect) / n.sum(den_vect)
        
#        num_vect_split = n.split(num_vect, nbodies)
#        den_vect_split = n.split(den_vect, nbodies)
        
        #E_total = n.sum(num_vect_split[1]) / n.sum(den_vect_split[1]) 
        
        # Compute fraction of total energy absorbed by each body
        #----------------------------------------------------------------------
#        for ind_body in range(0, nbodies):
#        
#            E_frac[ind_body] = n.sum(num_vect_split[ind_body]) / n.sum(den_vect_split[ind_body])
#            
#        # Total fraction
#        #----------------------------------------------------------------------
#        E_total = n.sum(E_frac)
        
        return E_total
    #--------------------------------------------------------------------------
    #
    # Incident power (computed only with incident cylindrical waves)
    #--------------------------------------------------------------------------
    def incident_power(self, farmobj, envobj, beta, ind_w):
        
        # Initialize vector of incident coefficients
        dim = 2 * farmobj.max_trunc_angmode[ind_w] + 1 
        
        a_in_vect = n.zeros(dim, dtype=complex) 
        
        # Truncation angular modes
        M = farmobj.max_trunc_angmode[ind_w]
        
        # Assign values
        for ind_M in range(-M, M + 1):
                        
            local_index_vect =  M + ind_M 
            
            a_in_vect[local_index_vect] = ((1j)**ind_M) * n.exp(- (1j) * ind_M * beta) * \
                                          (- 1j * envobj.gravity / envobj.frequency[ind_w])
        
        a_in_square = n.sum((abs(a_in_vect))**2)
        
        cg = 0.5 * (envobj.frequency[ind_w] / envobj.k0[ind_w]) * (1 + (2 * envobj.k0[ind_w] * envobj.depth)/ (n.sinh(2 * envobj.k0[ind_w] * envobj.depth)))
        
        Pin = 0.5 * envobj.water_density * envobj.gravity * cg / envobj.k0[ind_w] * a_in_square
        
        return Pin
    #--------------------------------------------------------------------------
    #
    # Initialize incident vector cylindrical coefficients for a unidirectional wave
    #-------------------------------------------------------------------------- 
    def incident_vector_coefficients(self, farmobj, envobj, beta, ind_w): 
        
        # Initialize vector of incident coefficients
        nbodies = farmobj.farm_nbodies
        dim = 2 * farmobj.max_trunc_angmode[ind_w] + 1 
        
        a_in_vect = n.zeros(dim * nbodies, dtype=complex) 
        
        # Truncation angular modes
        M = farmobj.max_trunc_angmode[ind_w]
        
        # Assign values
        for ind_body in range(0, nbodies):
            for ind_M in range(-M, M + 1):
                        
                local_index_vect = ind_body * (2 * M + 1) + M + ind_M 
                
                Xj = farmobj.layout[ind_body].position[0]
                Yj = farmobj.layout[ind_body].position[1]
                
                phase_term = Xj * n.cos(beta) + Yj * n.sin(beta)
            
                a_in_vect[local_index_vect] = ((1j)**ind_M) * n.exp(- (1j) * ind_M * beta) * \
                                            n.exp(1j * envobj.k0[ind_w] * phase_term) 
        
        return a_in_vect
    #-------------------------------------------------------------------------  
        
#    # Function to compute transmitted amplitude spectra
#    #--------------------------------------------------------------------------
#    def transmitted_amplitude_spectra_unidirectional(self, farmobj, envobj, ind_w, beta_array):
#        
#        
#        # Compute matrix DVIN of the transmition kernel (2N+1)J x Na
#        #----------------------------------------------------------------------
#        DVIN = self._DVIN_matrix_calc_unidirectional(farmobj, envobj, ind_w, beta_array) /  (-1j * envobj.gravity / envobj.frequency[ind_w]) 
#                                                                      
#        # Populate matrix VT with dimensions Na x (2N+1)J
#        #----------------------------------------------------------------------
#        VT = self._VT_trans_matrix_calc(farmobj, envobj, ind_w, beta_array)
#        
#        # Compute discretized version of the transmition kernel ()
#        #----------------------------------------------------------------------
#        M_mat = n.dot(VT,DVIN)
#        
#        # Compute contribution from the scattered potential on the transmitted
#        #----------------------------------------------------------------------
#        A_scatt_trans = n.dot(M_mat, weighted_AIN)
#        
#        # Compute contribution from the incident potential on the transmitted
#        #----------------------------------------------------------------------
#        phase_lag = self._phase_lag_incident_transmitted(envobj, ind_w)
#        A_inc_trans = n.dot(phase_lag, envobj.wave_amplitudes[ind_w,:])
#        if (self.Ng == 0.):
#            A_inc_trans_comp = A_inc_trans
#        else:
#            comp_vect = n.zeros(self.Ng - 1, dtype = float)
#            A_inc_trans_comp = n.concatenate((comp_vect, A_inc_trans, comp_vect))
#        
#        # Compute amplitudes of transmitted spectrum
#        #----------------------------------------------------------------------
#        A_trans = A_scatt_trans  
#        
#                                                 
#        return 
#    #--------------------------------------------------------------------------
#    #
#    # Function to compute DVIN matrix of the transmission kernel 
#    #--------------------------------------------------------------------------
#    def _DVIN_matrix_calc_unidirectional(self, farmobj, envobj, ind_w, beta_array):
#        
#        # Initialize square matrix of the transmission kernel
#        nbodies = farmobj.farm_nbodies
#        dim = 2 * farmobj.max_trunc_angmode[ind_w] + 1 
#         
#        DVIN = n.zeros((nbodies * dim, 1), dtype=complex)
#        
#        # Truncation angular modes
#        M = farmobj.max_trunc_angmode[ind_w]
#               
#        # Get partial waves scattered coefficients due to an incident angular 
#        # spectrum with unitary amplitude [ind_body][ind_beta](2N+1)
#        #----------------------------------------------------------------------
#        (solution_vector, incidence_vector) = \
#             farmobj._solve_coefficients_diffraction_interaction(envobj, \
#                                                            ind_w, beta_array)
#        
#        # Reorder solution vector (as DVIN) to have dimensions (2N+1)J x 1 
#        #----------------------------------------------------------------------
#        for ind_angle in range(0, 1):  
#        
#            for ind_body in range(0,nbodies):
#                
#                for ind_M in range(-M, M + 1):
#                    
#                    local_index_vect = M + ind_M
#                    
#                    global_index_vect = ind_body * dim + M + ind_M
#                    
#                    DVIN[global_index_vect, ind_angle] = solution_vector[ind_body][ind_angle][local_index_vect]
#        
#        return DVIN
#    #-------------------------------------------------------------------------- 
#    #
#    # Function to compute  vector of the unidirectional incident potential when transmitted
#    #--------------------------------------------------------------------------
#    def _incident_transmitted_unidirectional(self, envobj, ind_w, beta_array):
#        
#        #select real angles of the xi vector
#        if (self.Ng == 0):
#            xi_vect_real = self.xi_vect
#        else:
#            xi_vect_real = self.xi_vect[self.Ng - 1 : self.Ng - 1 + self.Na - 1]
#        
#        # initialize empty phase vector and assign to the incident angle index 
#        # the phase lag value
#        phase = n.zeros(self.Na, dtype=complex)
#        
#        step = n.pi / (self.Na - 1.)
#        ind_dir = 0
#        error = 0
#        
#        while(abs(xi_vect_real[ind_dir] - beta_array[0]) > step):
#            
#            if (ind_dir < self.Na):
#            
#                ind_dir = ind_dir + 1
#            
#            else:
#            
#                print ('value not found')
#                error = 1
#                
#        if (error == 0):
#           phase[ind_dir] = n.exp(1j * envobj.k0[ind_w] * self.x1 * n.cos(beta_array[0])) * envobj.wave_amplitudes[ind_w,0]
#           
#        else:
#
#           sys.exit("check search, value not found")
#        
#        return phase
#    #-------------------------------------------------------------------------- 
#    #     
#    # Function to compute reflected amplitude spectra
#    #--------------------------------------------------------------------------
#    def reflected_amplitude_spectra_unidirectional(self, farmobj, envobj, ind_w, beta_array):
#        
#        
#        # Compute matrix DVIN of the reflection kernel (2N+1)J x Na
#        #----------------------------------------------------------------------
#        DVIN = self._DVIN_matrix_calc_unidirectional(farmobj, envobj, ind_w, beta_array) /  (-1j * envobj.gravity / envobj.frequency[ind_w])                                
#                                       
#        # Populate matrix VT with dimensions Na x (2N+1)J
#        #----------------------------------------------------------------------
#        VT = self._VT_ref_matrix_calc(farmobj, envobj, ind_w, beta_array)
#        
#        # Compute discretized version of the transmition kernel ()
#        #----------------------------------------------------------------------
#        A_scatt_ref = n.dot(VT, DVIN)
#        
#        # Compute amplitudes of transmitted spectrum
#        #----------------------------------------------------------------------
#        A_ref = A_scatt_ref
#                                                 
#        return A_ref
#    #--------------------------------------------------------------------------
#    #    
#    # Energy conservation test (diffraction only)
#    #-------------------------------------------------------------------------- 
#    def energy_conservation_diffraction_unidirectional(self, farmobj, envobj, ind_w, beta_array):
#        
#        A_T_S = self.transmitted_amplitude_spectra_unidirectional(farmobj, envobj, ind_w, beta_array)[0]
#        
#        A_T_Inc = self.transmitted_amplitude_spectra_unidirectional(farmobj, envobj, ind_w, beta_array)[1]
#        
#        # select only amplitudes related to real values of the xi angles
#        if (self.Ng == 0.):
#            
#            A_T = (A_T_S + A_T_Inc)
#            
#            A_R = self.reflected_amplitude_spectra_unidirectional(farmobj, envobj, ind_w, beta_array)
#            
#        else:
#            
#            A_T = (A_T_S + A_T_Inc)[self.Ng - 1 : self.Ng - 1 + self.Na]
#    
#            A_R = self.reflected_amplitude_spectra_unidirectional(farmobj, envobj, ind_w, beta_array)[self.Ng - 1 : self.Ng - 1 + self.Na]
#        
#        Ax = n.pi / (self.Na - 1.)
#        I = self._Integration_matrix_calc(Ax)
#        
#        E = n.sum(n.dot(I,(abs(A_T)**2))) + n.sum(n.dot(I,(abs(A_R)**2))) 
#        
#        Initial_E = abs(envobj.wave_amplitudes[ind_w,:])**2
#        
#        return (E, Initial_E)
#    #--------------------------------------------------------------------------
    