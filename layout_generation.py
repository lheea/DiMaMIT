# Import python tools
#------------------------------------------------------------------------------
import numpy as n
import math as m
#------------------------------------------------------------------------------
def square_grid_generation(nrows, ncols, dx, dy):
    
    #======================================================================
    # Purpose:generation of the vectors with x and y coordinates of the body 
    #         centers in the array
    # Input : nrows (number of rows)
    #         ncols (number of columns)
    #         dx (horizontal spacing)
    #         dy (vertical spacing)
    # Note:   origin of coordinates (first body) at (0,0)
    # Output: vectors x_coords (with x-coordinates) and 
    #                 y_coords (with y-coordinates)
    #======================================================================
    
    bodynumber = 0  #body index
    
    x_coords = n.zeros((nrows * ncols))
    y_coords = n.zeros((nrows * ncols))
    
    for i in range(0, nrows):
        
        for j in range(0, ncols):
            
            x = j * dx
            y = i * dy
            
            x_coords[bodynumber] = x
            y_coords[bodynumber] = y
            
            bodynumber = bodynumber + 1
            
    return (x_coords, y_coords)
#------------------------------------------------------------------------------
#
#    
#------------------------------------------------------------------------------    
def staggered_square_grid_generation(nrows, ncols, dx, dy):
    
    #======================================================================
    # Purpose:generation of the vectors with x and y coordinates of the body 
    #         centers in the array
    # Input : nrows (number of rows)
    #         ncols (number of columns)
    #         dx (horizontal spacing)
    #         dy (vertical spacing) -- staggering related to y separation
    # Note:   origin of coordinates (first body) at (0,0)
    # Output: vectors x_coords (with x-coordinates) and 
    #                 y_coords (with y-coordinates)
    #======================================================================
    
    bodynumber = 0  #body index
    
    x_coords = n.zeros((nrows * ncols))
    y_coords = n.zeros((nrows * ncols))
    
    for j in range(0, ncols):
        
        for i in range(0, nrows):
            
            x = j * dx
            y = i * dy + (1 - (-1)**j) * 0.5 * dy / 2.
            
            x_coords[bodynumber] = x
            y_coords[bodynumber] = y
            
            bodynumber = bodynumber + 1
            
    return (x_coords, y_coords)
    
    
#------------------------------------------------------------------------------        
#
#    
#------------------------------------------------------------------------------    
def uniform_random_position(x, y, number_random_samples, perturbation_distance):
    
    epsilon = n.random.uniform(-1, 1, number_random_samples)
    
    x_tilde = x + perturbation_distance * epsilon
    y_tilde = y + perturbation_distance * epsilon
    
    return (x_tilde, y_tilde)
#------------------------------------------------------------------------------
#
#        
#------------------------------------------------------------------------------
def wavestar_config_lin(nfloaters, dx, dy):
    
    #======================================================================
    # Purpose:generation of the vectors with x and y coordinates of the body 
    #         centers in the array
    # Input : nfloaters (number of floaters)
    #         dx (horizontal spacing)
    #         dy (vertical spacing)
    # Note:   origin of coordinates (first body) at (0,0)
    # Output: vectors x_coords (with x-coordinates) and 
    #                 y_coords (with y-coordinates)
    #======================================================================
    
    bodynumber = 0  #body index
    
    ncols = int(nfloaters / 2)
    nrows = 2
    
    x_coords   = n.zeros((nrows * ncols))
    y_coords   = n.zeros((nrows * ncols))
    rot_coords = n.zeros((nrows * ncols))
    
    for i in range(0, nrows):
        
        for j in range(0, ncols):
            
            x = j * dx
            y = i * dy
            
            x_coords[bodynumber] = x
            y_coords[bodynumber] = y
            
            bodynumber = bodynumber + 1
        
            if (i == 0):
            
                rot_coords[(i+1) * j] = 180.0
            
            elif (i == 1):
            
                rot_coords[(i) * j + ncols] = 0.0
            
    y_coords = y_coords - dy/2
            
    return (x_coords, y_coords, rot_coords)
#------------------------------------------------------------------------------
#
#    
#------------------------------------------------------------------------------
def wavestar_config_star(nfloaters, dim_floater, dx, dy):
    
    #======================================================================
    # Purpose:generation of the vectors with x and y coordinates of the body 
    #         centers in the array for a wavestar with 3 branches
    # Input : nfloaters (number of floaters)
    #         characteristic dimension of the floater (diameter)
    #         dx (horizontal spacing)
    #         dy (vertical spacing)
    # Note:   origin of coordinates (first body) at (0,0)
    # Output: vectors x_coords (with x-coordinates) and 
    #                 y_coords (with y-coordinates)
    #======================================================================
    
    nfloaters_1branch = int(nfloaters / 3)
    
    rot_coords_2_ext = n.zeros((nfloaters_1branch)/2)
    rot_coords_2_int = n.zeros((nfloaters_1branch)/2)
    rot_coords_3_ext = n.zeros((nfloaters_1branch)/2)
    rot_coords_3_int = n.zeros((nfloaters_1branch)/2)
    
    # first branch coordinates
    (x,y,r) = wavestar_config_lin(nfloaters_1branch, dx, dy)
    
    # define angle between branches
    gamma = 120 * n.pi / 180
    
    hgamma = gamma / 2
    
    # create second branch (pointing downwards)
        # 1 rotate first branch
    (x_rot_1, y_rot_1) = rotation(x,y+dy/2,-hgamma)
        # translate
    tx_1 = x[int(nfloaters_1branch/2) - 1] + dx*n.sin(hgamma)
    #ty_1 = - dx*n.cos(hgamma) - dy/2
    ty_1 =  - dx/(2*n.cos(m.radians(30))) * n.sin(m.radians(60)) - dy/2
    (x_br_2, y_br_2) = translation_xy(x_rot_1,y_rot_1,tx_1,ty_1)
    
    for ind in range(0,nfloaters_1branch/2):
        rot_coords_2_ext[ind] = 120
        rot_coords_2_int[ind] = 300
    
    
     # create third branch (pointing upwards)
        # 1 rotate first branch
    (x_rot_2, y_rot_2) = rotation(x,y+dy/2,hgamma)
        # translate
    tx_2 = x[int(nfloaters_1branch/2) - 1] + dx*n.sin(hgamma) + dy*n.sin(hgamma)
    ty_2 = dy + (dx / (2 * n.cos(m.radians(30)))) * n.sin(m.radians(60)) - dy * n.cos(m.radians(60)) - dy/2
    (x_br_3, y_br_3) = translation_xy(x_rot_2,y_rot_2,tx_2,ty_2)
    
    for ind in range(0,nfloaters_1branch/2):
        rot_coords_3_ext[ind] = 60
        rot_coords_3_int[ind] = 240
    
    x_total = n.concatenate((x,x_br_2,x_br_3))  
    y_total = n.concatenate((y,y_br_2,y_br_3)) 
    
    r_total = n.concatenate((r,rot_coords_2_ext,rot_coords_2_int,rot_coords_3_int,rot_coords_3_ext))
    
    return(x_total, y_total, r_total)
#------------------------------------------------------------------------------
#
#    
#------------------------------------------------------------------------------    
def translation_xy(x,y,tx,ty):

    #======================================================================
    # Purpose:translate x y coordinates with tx and ty distances 
    # Input : x y  (coordinate vectors)
    #         tx (horizontal translation)
    #         ty (vertical translation)
    # Output: vectors x_coords (with x-coordinates) and 
    #                 y_coords (with y-coordinates)
    #======================================================================    
    
    x_coords = x + tx
    
    y_coords = y + ty
    
    return (x_coords, y_coords)
#------------------------------------------------------------------------------
#
#    
#------------------------------------------------------------------------------    
def rotation(x,y,angle):

    #======================================================================
    # Purpose:rotate x y coordinates with the angle of rotation (in radians)
    # Input : x y   (coordinate vectors)
    #         angle (rotation angle in radians) if positive, anti-clockwise
    #                                           if negative, clock-wise
    # Output: vectors x_coords (with x-coordinates) and 
    #                 y_coords (with y-coordinates)
    #======================================================================
    
    # initialize rotated coordinate vectors
    x_rotated = n.zeros((x.shape[0]),dtype=float)
    y_rotated = n.zeros((y.shape[0]),dtype=float)    
    
    # create rotation matrix
    R = n.zeros((2,2),dtype=float)
    
    R[0,0] = n.cos(angle)
    R[0,1] = -n.sin(angle)
    R[1,0] = n.sin(angle)
    R[1,1] = n.cos(angle)
    
    # apply rotation to each pair x,y
    
    vect = n.zeros((2),dtype=float)
    
    for ind in range(0,x.shape[0]):
        
        vect[0] = x[ind]
        vect[1] = y[ind]
        
        res = n.dot(R,vect)
        
        x_rotated[ind] = res[0]
        y_rotated[ind] = res[1]
        
    return (x_rotated,y_rotated)
#------------------------------------------------------------------------------
#
# 
#------------------------------------------------------------------------------
def wavestar_config_star_linear_aggregation(nunits, nfloaters, dim_floater, dx, dy, D):    
    
    #======================================================================
    # Purpose:generation of the vectors with x and y coordinates of the body 
    #         centers in the array for a wavestar with 3 branches
    # Input : D : separation distance between floters in extreme branches
    #         nunits: number of wavestar star units
    #         nfloaters (number of floaters per unit)
    #         characteristic dimension of the floater (diameter)
    #         dx (horizontal spacing)
    #         dy (vertical spacing)
    # Note:   origin of coordinates (first body) at (0,0)
    # Output: vectors x_coords (with x-coordinates) and 
    #                 y_coords (with y-coordinates)
    #======================================================================
    
    (x_first, y_first, r_first) = wavestar_config_star(nfloaters, dim_floater, dx, dy)
    
    span = n.max(y_first) - n.min(y_first)
    
    x_coords = []
    y_coords = []
    r_coords = []
    
    x_coords.append(x_first)
    y_coords.append(y_first)
    r_coords.append(r_first)
    
    for ind_unit in range(1, nunits):
        
        x_units = x_first
        y_units = y_first - (span + D) * ind_unit
        r_units = r_first
    
        x_coords.append(x_units)
        y_coords.append(y_units)
        r_coords.append(r_units)
        
    x_global = n.concatenate(n.asarray(x_coords))
    y_global = n.concatenate(n.asarray(y_coords))
    r_global = n.concatenate(n.asarray(r_coords))
    
    return(x_global, y_global, r_global)
#------------------------------------------------------------------------------
#
# 
#------------------------------------------------------------------------------
def wavestar_config_star_star_aggregation_3units(nfloaters, dim_floater, dx, dy, D):
    
      
    #======================================================================
    # Purpose:generation of the vectors with x and y coordinates of the body 
    #         centers in the array for a wavestar with 3 branches
    # Input : D : separation distance between floters in extreme branches
    #         nunits: number of wavestar star units
    #         nfloaters (number of floaters per unit)
    #         characteristic dimension of the floater (diameter)
    #         dx (horizontal spacing)
    #         dy (vertical spacing)
    # Note:   origin of coordinates (first body) at (0,0)
    # Output: vectors x_coords (with x-coordinates) and 
    #                 y_coords (with y-coordinates)
    #======================================================================
        
    (x_first, y_first, r_first) = wavestar_config_star(nfloaters, dim_floater, dx, dy)  
    
    angle = 60 * n.pi / 180.
    
    (x_rot, y_rot) = rotation(x_first, y_first, angle)  
    
    #First rotated unit
    y_span_rot = abs(y_rot[35] - y_rot[0])
    
    y_span_lin = abs(y_first[17] - y_first[0])

    vert_distance = y_span_rot + y_span_lin + dy/2.*n.cos(angle) + D * n.sin(angle)

    y_second = y_rot - vert_distance  
    
    x_span_rot = x_first[17] - x_rot[35] + D * n.cos(angle)
    
    x_second = x_rot + x_span_rot
    
    r_second = r_first + 60
    
    #third branch
    y_span_final = abs(y_first[29] - y_second[0])
    
    x_span_final = abs(-x_second[0] + x_first[29])
    
    x_third = x_first + x_span_final - D * n.cos(angle)
    
    y_third = y_first - y_span_final - D * n.sin(angle)
    
    r_third = r_first
    
    return(n.concatenate((x_first,x_second,x_third)), n.concatenate((y_first,y_second,y_third)), n.concatenate((r_first,r_second,r_third)))
        
        
        
        
        