#%% MODULES IMPORT

import numpy as n
#
#
#CLASS DEFINING THE PTO 
#------------------------------------------------------------------------------
class PTO:
    
    # Class Initialization
    #--------------------------------------------------------------------------
    def __init__(self, name, farm, K_PTO, B_PTO):
        
        if (K_PTO == None):
            
            dof_body = farm.dict_bodies[name].dof_body
            
            self.K = n.zeros((dof_body, dof_body), n.float)
            
        else:
            
            diag_values_k = n.asarray(K_PTO)
            self.K = n.diag(diag_values_k)
            
            
        if (B_PTO == None):
            
            dof_body = farm.dict_bodies[name].dof_body
            
            self.B = n.zeros((dof_body, dof_body), n.float)
            
        else:
            
            diag_values_b = n.asarray(B_PTO)
            self.B = n.diag(diag_values_b)
    #--------------------------------------------------------------------------  
#------------------------------------------------------------------------------        
#
#
#CLASS DEFINING THE MOORING 
#------------------------------------------------------------------------------
class Mooring:
    
    # Class Initialization
    #--------------------------------------------------------------------------
    def __init__(self, name, farm, K_Mooring):
        
        if (K_Mooring == None):
            
            dof_body = farm.dict_bodies[name].dof_body
            
            self.K = n.zeros((dof_body, dof_body), n.float)
            
        else:
            
            diag_values_k = n.asarray(K_Mooring)
            self.K = n.diag(diag_values_k)
    #--------------------------------------------------------------------------
#------------------------------------------------------------------------------