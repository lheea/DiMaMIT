from env import Environment
from farm import Farm
import pickle
from classes_farm_mechanics import *
from class_Mechanics import Mechanics
from class_sim import Simulation
from layout_generation import *
from class_body import *

# Import python tools
#------------------------------------------------------------------------------
import numpy as n



# Constant BPTO for each body of the array (no KPTO) one position
def power_seastate_calculation(BPTO, ind_seastate, mySim, myenv, myMechanics):
    
    # PTO definition
    #--------------------------------------------------------------------------
    # diag components of each freedom deg (use none if no k_pto or b_pto)
    k_pto = [0, 0, 0, 0, 0, 0] #units!!
    b_pto = [0.0, 0.0, 0.0, BPTO, 0.0, 0.0]
    
    # PTO used for the WECs
    BPTO_array = n.diag(b_pto)
    
    KPTO_array = n.diag(k_pto)
    
    b_pto_layout = [BPTO_array] * len(myMechanics.M_layout)
    
    k_pto_layout = [KPTO_array] * len(myMechanics.M_layout)
    #--------------------------------------------------------------------------

    Hs = mySim.Hs[ind_seastate] 
    Tp = mySim.Tp[ind_seastate]
    
    wave_amplitudes_array = myenv.bretschneider_spectrum_angular_spreading(Hs, Tp, myenv.frequency, mySim.ndir, mySim.m, mySim.theta_principal)[0]
    
    power_seastate_bodies = myMechanics.Power_bodies_infarm_seastate(myenv, wave_amplitudes_array, k_pto_layout, b_pto_layout)
    
    # Compute total power (minus sign for the optimization routine)
    power_seastate_total = - n.sum(power_seastate_bodies)
    
    return power_seastate_total
#------------------------------------------------------------------------------
#
#     
# constant BPTO all bodies and seastates and variable position of floaters    
def power_anual_calculation_fx_fbpto(x, nfloaters, cyl_rad, dict_bodies, mySim, myenv):
    
    BPTO = x[0]
    dx   = x[1]
    
    print ('BPTO value' + ' ' + str(BPTO))
    
    # wavestar characteristics
    # -----------------------------------------------------------------------------
    cyl_diameter = 2 * cyl_rad
    
    dy = 4 * cyl_diameter 
    
    name = 'hemisphere'
    
    #nfloaters = 60 
    # -----------------------------------------------------------------------------
    
    # layout definition
    # -----------------------------------------------------------------------------
    #dx = 2.4 * cyl_rad #from forehand paper (7.2m separation with 6m diameter)
    
    layout    = []
    
    #(x,y,r) = wavestar_config_lin(nfloaters, dx, dy)
    #plot_floaters_wavestar_lin(cyl_rad,nfloaters,x,y,dy)
    
    (x,y,r) = wavestar_config_star(nfloaters, cyl_rad*2, dx, dy)
    
    #plot_floaters_wavestar_star(cyl_rad,nfloaters,x,y,dy)
    
    for ind in range(0,nfloaters):
        
        print(str(ind))
        
        layout.append(Body(name, x[ind], y[ind], r[ind]))
    # -----------------------------------------------------------------------------
        
    # Initialization of the farm
    # -----------------------------------------------------------------------------
    myfarm = Farm(layout, myenv, dict_bodies)
    
    exc_forces = myfarm.gen_excitation_forces_farm(myenv,mySim.beta_array)

    (AM_coeffs, DAMP_coeffs) = myfarm.gen_hydrodynamic_coefficients_farm(myenv,mySim.dof_array)
    
    # Mooring definition
    #------------------------------------------------------------------------------
    # diag components of each freedom deg (use none if no k_mooring)
    k_mooring = [0,0,0,0,0,0] #units!!
    name = 'hemisphere' #name of the WEC to which the mooring will be added
    
    # Mooring used for the WECs
    Mooring_1 = Mooring(name, myfarm, k_mooring)
    
    Mooring_layout = [Mooring_1] * nfloaters
    #------------------------------------------------------------------------------
    
    # Initialization of the mechanical simulation
    #------------------------------------------------------------------------------
    myMechanics = Mechanics(mySim, myenv, myfarm, Mooring_layout)
    
    # PTO definition
    #--------------------------------------------------------------------------
    # diag components of each freedom deg (use none if no k_pto or b_pto)
    k_pto = [0, 0, 0, 0, 0, 0] #units!!
    b_pto = [0.0, 0.0, 0.0, BPTO, 0.0, 0.0]
    
    # PTO used for the WECs
    BPTO_array = n.diag(b_pto)
    
    KPTO_array = n.diag(k_pto)
    
    b_pto_layout = [BPTO_array] * len(myMechanics.M_layout)
    
    k_pto_layout = [KPTO_array] * len(myMechanics.M_layout)
    #--------------------------------------------------------------------------
    
    power_anual = 0.0
    
    for ind_seastate in range(0, len(mySim.Hs)):

        Hs = mySim.Hs[ind_seastate] 
        Tp = mySim.Tp[ind_seastate]
        
        wave_amplitudes_array = myenv.bretschneider_spectrum_angular_spreading(Hs, Tp, myenv.frequency, mySim.ndir, mySim.m, mySim.theta_principal)[0]
        
        power_seastate_bodies = myMechanics.Power_bodies_infarm_seastate(myenv, wave_amplitudes_array, k_pto_layout, b_pto_layout)
        
        # Compute total power (minus sign for the optimization routine)
        power_seastate_total = - n.sum(power_seastate_bodies)
        
        power_anual = power_seastate_total + power_anual
        
        
    return power_anual
#------------------------------------------------------------------------------
#
#    
# constant BPTO all bodies and seastates   
def power_anual_calculation_fbpto(BPTO, mySim, myenv, myMechanics):
    
    print ('BPTO value' + ' ' + str(BPTO))
    
    # PTO definition
    #--------------------------------------------------------------------------
    # diag components of each freedom deg (use none if no k_pto or b_pto)
    k_pto = [0, 0, 0, 0, 0, 0] #units!!
    b_pto = [0.0, 0.0, 0.0, BPTO, 0.0, 0.0]
    
    # PTO used for the WECs
    BPTO_array = n.diag(b_pto)
    
    KPTO_array = n.diag(k_pto)
    
    b_pto_layout = [BPTO_array] * len(myMechanics.M_layout)
    
    k_pto_layout = [KPTO_array] * len(myMechanics.M_layout)
    #--------------------------------------------------------------------------
    
    power_anual = 0.0
    
    for ind_seastate in range(0, len(mySim.Hs)):

        Hs = mySim.Hs[ind_seastate] 
        Tp = mySim.Tp[ind_seastate]
        Prob = mySim.Prob[ind_seastate]
        
        wave_amplitudes_array = myenv.bretschneider_spectrum_angular_spreading(Hs, Tp, myenv.frequency, mySim.ndir, mySim.m, mySim.theta_principal)[0]
        
        power_seastate_bodies = myMechanics.Power_bodies_infarm_seastate(myenv, wave_amplitudes_array, k_pto_layout, b_pto_layout)
        
        # Compute total power (minus sign for the optimization routine)
        power_seastate_total = - n.sum(power_seastate_bodies)
        
        power_anual = power_seastate_total*Prob + power_anual
        
        
    return power_anual
#------------------------------------------------------------------------------
#
#     
# constant BPTO all bodies and seastates and variable position of floaters    
def power_anual_calculation_fx(dx, BPTO, nfloaters, cyl_rad, dict_bodies, mySim, myenv):
    
    print ('dx value' + ' ' + str(dx))
    
    # wavestar characteristics
    # -----------------------------------------------------------------------------
    cyl_diameter = 2 * cyl_rad
    
    dy = 4 * cyl_diameter 
    
    name = 'hemisphere'
    
    #nfloaters = 60 
    # -----------------------------------------------------------------------------
    
    # layout definition
    # -----------------------------------------------------------------------------
    #dx = 2.4 * cyl_rad #from forehand paper (7.2m separation with 6m diameter)
    
    layout    = []
    
    #(x,y,r) = wavestar_config_lin(nfloaters, dx, dy)
    #plot_floaters_wavestar_lin(cyl_rad,nfloaters,x,y,dy)
    
    (x,y,r) = wavestar_config_star(nfloaters, cyl_rad*2, dx, dy)
    
    #plot_floaters_wavestar_star(cyl_rad,nfloaters,x,y,dy)
    
    for ind in range(0,nfloaters):
        
        print(str(ind))
        
        layout.append(Body(name, x[ind], y[ind], r[ind]))
    # -----------------------------------------------------------------------------
        
    # Initialization of the farm
    # -----------------------------------------------------------------------------
    myfarm = Farm(layout, myenv, dict_bodies)
    
    exc_forces = myfarm.gen_excitation_forces_farm(myenv,mySim.beta_array)

    (AM_coeffs, DAMP_coeffs) = myfarm.gen_hydrodynamic_coefficients_farm(myenv,mySim.dof_array)
    
    # Mooring definition
    #------------------------------------------------------------------------------
    # diag components of each freedom deg (use none if no k_mooring)
    k_mooring = [0,0,0,0,0,0] #units!!
    name = 'hemisphere' #name of the WEC to which the mooring will be added
    
    # Mooring used for the WECs
    Mooring_1 = Mooring(name, myfarm, k_mooring)
    
    Mooring_layout = [Mooring_1] * nfloaters
    #------------------------------------------------------------------------------
    
    # Initialization of the mechanical simulation
    #------------------------------------------------------------------------------
    myMechanics = Mechanics(mySim, myenv, myfarm, Mooring_layout)
    
    # PTO definition
    #--------------------------------------------------------------------------
    # diag components of each freedom deg (use none if no k_pto or b_pto)
    k_pto = [0, 0, 0, 0, 0, 0] #units!!
    b_pto = [0.0, 0.0, 0.0, BPTO, 0.0, 0.0]
    
    # PTO used for the WECs
    BPTO_array = n.diag(b_pto)
    
    KPTO_array = n.diag(k_pto)
    
    b_pto_layout = [BPTO_array] * len(myMechanics.M_layout)
    
    k_pto_layout = [KPTO_array] * len(myMechanics.M_layout)
    #--------------------------------------------------------------------------
    
    power_anual = 0.0
    
    for ind_seastate in range(0, len(mySim.Hs)):

        Hs = mySim.Hs[ind_seastate] 
        Tp = mySim.Tp[ind_seastate]
        Prob = mySim.Prob[ind_seastate]
        
        wave_amplitudes_array = myenv.bretschneider_spectrum_angular_spreading(Hs, Tp, myenv.frequency, mySim.ndir, mySim.m, mySim.theta_principal)[0]
        
        power_seastate_bodies = myMechanics.Power_bodies_infarm_seastate(myenv, wave_amplitudes_array, k_pto_layout, b_pto_layout)
        
        # Compute total power (minus sign for the optimization routine)
        power_seastate_total = - n.sum(power_seastate_bodies)
        
        power_anual = power_seastate_total*Prob + power_anual
        
        
    return power_anual
#------------------------------------------------------------------------------
#
#
# constant BPTO all bodies and seastates and variable position of floaters    
def power_anual_calculation_fx_linear_aggregation(D, dx, nunits, BPTO, nfloaters, cyl_rad, dict_bodies, mySim, myenv):
    
    print ('dx value' + ' ' + str(D))
    
    # wavestar characteristics
    # -----------------------------------------------------------------------------
    cyl_diameter = 2 * cyl_rad
    
    dy = 4 * cyl_diameter 
    
    name = dict_bodies.keys()[0]
    # ----------------------------------------------------------------------------
    
    layout    = []
    
    (x,y,r) = wavestar_config_star_linear_aggregation(nunits, nfloaters, cyl_rad*2, dx, dy, D)
    
    for ind in range(0,nfloaters):
        
        print(str(ind))
        
        layout.append(Body(name, x[ind], y[ind], r[ind]))
    # -----------------------------------------------------------------------------
        
    # Initialization of the farm
    # -----------------------------------------------------------------------------
    myfarm = Farm(layout, myenv, dict_bodies)
    
    exc_forces = myfarm.gen_excitation_forces_farm(myenv,mySim.beta_array)

    (AM_coeffs, DAMP_coeffs) = myfarm.gen_hydrodynamic_coefficients_farm(myenv,mySim.dof_array)
    
    # Mooring definition
    #------------------------------------------------------------------------------
    # diag components of each freedom deg (use none if no k_mooring)
    k_mooring = [0,0,0,0,0,0] #units!!
    
    # Mooring used for the WECs
    Mooring_1 = Mooring(name, myfarm, k_mooring)
    
    Mooring_layout = [Mooring_1] * nfloaters
    #------------------------------------------------------------------------------
    
    # Initialization of the mechanical simulation
    #------------------------------------------------------------------------------
    myMechanics = Mechanics(mySim, myenv, myfarm, Mooring_layout)
    
    # PTO definition
    #--------------------------------------------------------------------------
    # diag components of each freedom deg (use none if no k_pto or b_pto)
    k_pto = [0, 0, 0, 0, 0, 0] #units!!
    b_pto = [0.0, 0.0, 0.0, BPTO, 0.0, 0.0]
    
    # PTO used for the WECs
    BPTO_array = n.diag(b_pto)
    
    KPTO_array = n.diag(k_pto)
    
    b_pto_layout = [BPTO_array] * len(myMechanics.M_layout)
    
    k_pto_layout = [KPTO_array] * len(myMechanics.M_layout)
    #--------------------------------------------------------------------------
    
    power_anual = 0.0
    
    for ind_seastate in range(0, len(mySim.Hs)):

        Hs = mySim.Hs[ind_seastate] 
        Tp = mySim.Tp[ind_seastate]
        Prob = mySim.Prob[ind_seastate]
        
        wave_amplitudes_array = myenv.bretschneider_spectrum_angular_spreading(Hs, Tp, myenv.frequency, mySim.ndir, mySim.m, mySim.theta_principal)[0]
        
        power_seastate_bodies = myMechanics.Power_bodies_infarm_seastate(myenv, wave_amplitudes_array, k_pto_layout, b_pto_layout)
        
        # Compute total power (minus sign for the optimization routine)
        power_seastate_total = - n.sum(power_seastate_bodies)
        
        power_anual = power_seastate_total*Prob + power_anual
        
        
    return power_anual
#------------------------------------------------------------------------------
#
#
# constant BPTO all bodies and seastates and variable position of floaters    
def power_anual_calculation_fx_star_aggregation(D, dx, BPTO, nfloaters, cyl_rad, dict_bodies, mySim, myenv):
    
    print ('dx value' + ' ' + str(D))
    
    # wavestar characteristics
    # -----------------------------------------------------------------------------
    cyl_diameter = 2 * cyl_rad
    
    dy = 4 * cyl_diameter 
    
    name = dict_bodies.keys()[0]
    # ----------------------------------------------------------------------------
    
    layout    = []
    
    (x,y,r) = wavestar_config_star_star_aggregation_3units(nfloaters, 2 * cyl_rad, dx, dy, D)
    
    #plot_floaters_wavestar_star(cyl_rad,nfloaters,x,y,dy)
    
    for ind in range(0,nfloaters):
        
        print(str(ind))
        
        layout.append(Body(name, x[ind], y[ind], r[ind]))
    # -----------------------------------------------------------------------------
        
    # Initialization of the farm
    # -----------------------------------------------------------------------------
    myfarm = Farm(layout, myenv, dict_bodies)
    
    exc_forces = myfarm.gen_excitation_forces_farm(myenv,mySim.beta_array)

    (AM_coeffs, DAMP_coeffs) = myfarm.gen_hydrodynamic_coefficients_farm(myenv,mySim.dof_array)
    
    # Mooring definition
    #------------------------------------------------------------------------------
    # diag components of each freedom deg (use none if no k_mooring)
    k_mooring = [0,0,0,0,0,0] #units!!
    
    # Mooring used for the WECs
    Mooring_1 = Mooring(name, myfarm, k_mooring)
    
    Mooring_layout = [Mooring_1] * nfloaters
    #------------------------------------------------------------------------------
    
    # Initialization of the mechanical simulation
    #------------------------------------------------------------------------------
    myMechanics = Mechanics(mySim, myenv, myfarm, Mooring_layout)
    
    # PTO definition
    #--------------------------------------------------------------------------
    # diag components of each freedom deg (use none if no k_pto or b_pto)
    k_pto = [0, 0, 0, 0, 0, 0] #units!!
    b_pto = [0.0, 0.0, 0.0, BPTO, 0.0, 0.0]
    
    # PTO used for the WECs
    BPTO_array = n.diag(b_pto)
    
    KPTO_array = n.diag(k_pto)
    
    b_pto_layout = [BPTO_array] * len(myMechanics.M_layout)
    
    k_pto_layout = [KPTO_array] * len(myMechanics.M_layout)
    #--------------------------------------------------------------------------
    
    power_anual = 0.0
    
    for ind_seastate in range(0, len(mySim.Hs)):

        Hs = mySim.Hs[ind_seastate] 
        Tp = mySim.Tp[ind_seastate]
        Prob = mySim.Prob[ind_seastate]
        
        wave_amplitudes_array = myenv.bretschneider_spectrum_angular_spreading(Hs, Tp, myenv.frequency, mySim.ndir, mySim.m, mySim.theta_principal)[0]
        
        power_seastate_bodies = myMechanics.Power_bodies_infarm_seastate(myenv, wave_amplitudes_array, k_pto_layout, b_pto_layout)
        
        # Compute total power (minus sign for the optimization routine)
        power_seastate_total = - n.sum(power_seastate_bodies)
        
        power_anual = power_seastate_total*Prob + power_anual
        
        
    return power_anual
#------------------------------------------------------------------------------
#
#
#------------------------------------------------------------------------------    
def power_anual_calculation_fx_staggered_grid(dx, BPTO, KPTO, nrows, ncols, dict_bodies, mySim, myenv, name, mode):
    
    print ('dx value' + ' ' + str(dx))
    
    # grid characteristics
    # -------------------------------------------------------------------------
    dy = dx

    # Layout definition
    # -------------------------------------------------------------------------
    layout    = []
    
    (x,y) = staggered_square_grid_generation(nrows, ncols, dx, dy)
    
    nfloaters = nrows * ncols
    
    r = n.zeros(nfloaters, dtype = float)
    
    for ind in range(0,nfloaters):
        
        print(str(ind))
        
        layout.append(Body(name, x[ind], y[ind], r[ind]))
    # -------------------------------------------------------------------------
        
    # Initialization of the farm
    # -------------------------------------------------------------------------
    myfarm = Farm(layout, myenv, dict_bodies)
    
    exc_forces = myfarm.gen_excitation_forces_farm(myenv,mySim.beta_array)

    (AM_coeffs, DAMP_coeffs) = myfarm.gen_hydrodynamic_coefficients_farm(myenv,mySim.dof_array)
    
    # Mooring definition
    #--------------------------------------------------------------------------
    # diag components of each freedom deg (use none if no k_mooring)
    k_mooring = [0,0,0,0,0,0] #units!!
    
    # Mooring used for the WECs
    Mooring_1 = Mooring(name, myfarm, k_mooring)
    
    Mooring_layout = [Mooring_1] * nfloaters
    #--------------------------------------------------------------------------
    
    # Initialization of the mechanical simulation
    #------------------------------------------------------------------------------
    myMechanics = Mechanics(mySim, myenv, myfarm, Mooring_layout)
    
    # PTO definition
    #--------------------------------------------------------------------------
    # diag components of each freedom deg (use none if no k_pto or b_pto)
    k_pto = [0, 0, 0, 0, 0, 0] #units!!
    if (mode==0):
        k_pto[mode] = KPTO
    else:
        k_pto[mode] = 0
    b_pto = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    b_pto[mode] = BPTO
    
    # PTO used for the WECs
    BPTO_array = n.diag(b_pto)
    
    KPTO_array = n.diag(k_pto)
    
    b_pto_layout = [BPTO_array] * len(myMechanics.M_layout)
    
    k_pto_layout = [KPTO_array] * len(myMechanics.M_layout)
    #--------------------------------------------------------------------------
    
    power_anual = 0.0
    
    for ind_seastate in range(0, len(mySim.Hs)):
        
        print ('ind_seastate' + ' ' + str(ind_seastate))

        Hs = mySim.Hs[ind_seastate] 
        Tp = mySim.Tp[ind_seastate]
        Prob = mySim.Prob[ind_seastate]
        
        wave_amplitudes_array = myenv.bretschneider_spectrum_angular_spreading(Hs, Tp, myenv.frequency, mySim.ndir, mySim.m, mySim.theta_principal)[0]
        
        power_seastate_bodies = myMechanics.Power_bodies_infarm_seastate(myenv, wave_amplitudes_array, k_pto_layout, b_pto_layout)
        
        # Compute total power (minus sign for the optimization routine)
        power_seastate_total = - n.sum(power_seastate_bodies)
        
        power_anual = power_seastate_total*Prob + power_anual
        
        
    return power_anual
#------------------------------------------------------------------------------
#
#    
#------------------------------------------------------------------------------    
def minimization_power_anual_calculation_fx_staggered_grid(dx, BPTO, KPTO, nrows, ncols, dict_bodies, mySim, myenv, name, mode):
    
    print ('dx value' + ' ' + str(dx))
    
    # grid characteristics
    # -------------------------------------------------------------------------
    dy = dx

    # Layout definition
    # -------------------------------------------------------------------------
    layout    = []
    
    (x,y) = staggered_square_grid_generation(nrows, ncols, dx, dy)
    
    nfloaters = nrows * ncols
    
    r = n.zeros(nfloaters, dtype = float)
    
    for ind in range(0,nfloaters):
        
        print(str(ind))
        
        layout.append(Body(name, x[ind], y[ind], r[ind]))
    # -------------------------------------------------------------------------
        
    # Initialization of the farm
    # -------------------------------------------------------------------------
    myfarm = Farm(layout, myenv, dict_bodies)
    
    exc_forces = myfarm.gen_excitation_forces_farm(myenv,mySim.beta_array)

    (AM_coeffs, DAMP_coeffs) = myfarm.gen_hydrodynamic_coefficients_farm(myenv,mySim.dof_array)
    
    # Mooring definition
    #--------------------------------------------------------------------------
    # diag components of each freedom deg (use none if no k_mooring)
    k_mooring = [0,0,0,0,0,0] #units!!
    
    # Mooring used for the WECs
    Mooring_1 = Mooring(name, myfarm, k_mooring)
    
    Mooring_layout = [Mooring_1] * nfloaters
    #--------------------------------------------------------------------------
    
    # Initialization of the mechanical simulation
    #------------------------------------------------------------------------------
    myMechanics = Mechanics(mySim, myenv, myfarm, Mooring_layout)
    
    # PTO definition
    #--------------------------------------------------------------------------
    # diag components of each freedom deg (use none if no k_pto or b_pto)
    k_pto = [0, 0, 0, 0, 0, 0] #units!!
    if (mode==0):
        k_pto[mode] = KPTO
    else:
        k_pto[mode] = 0
    b_pto = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    b_pto[mode] = BPTO
    
    # PTO used for the WECs
    BPTO_array = n.diag(b_pto)
    
    KPTO_array = n.diag(k_pto)
    
    b_pto_layout = [BPTO_array] * len(myMechanics.M_layout)
    
    k_pto_layout = [KPTO_array] * len(myMechanics.M_layout)
    #--------------------------------------------------------------------------
    
    power_anual = 0.0
    
    for ind_seastate in range(0, len(mySim.Hs)):
        
        print ('ind_seastate' + ' ' + str(ind_seastate))

        Hs = mySim.Hs[ind_seastate] 
        Tp = mySim.Tp[ind_seastate]
        Prob = mySim.Prob[ind_seastate]
        
        wave_amplitudes_array = myenv.bretschneider_spectrum_angular_spreading(Hs, Tp, myenv.frequency, mySim.ndir, mySim.m, mySim.theta_principal)[0]
        
        power_seastate_bodies = myMechanics.Power_bodies_infarm_seastate(myenv, wave_amplitudes_array, k_pto_layout, b_pto_layout)
        
        # Compute total power (plus sign for the optimization routine)
        power_seastate_total =  n.sum(power_seastate_bodies)
        
        power_anual = power_seastate_total*Prob + power_anual
        
        
    return power_anual
#------------------------------------------------------------------------------
#
#
#------------------------------------------------------------------------------    
def qfactor_waveclimate_calculation_fx_staggered_grid(dx, BPTO, KPTO, nrows, ncols, dict_bodies, mySim, myenv, name, mode):
    
    print ('dx value' + ' ' + str(dx))
    
    # grid characteristics
    # -------------------------------------------------------------------------
    dy = dx

    # Layout definition
    # -------------------------------------------------------------------------
    layout    = []
    
    (x,y) = staggered_square_grid_generation(nrows, ncols, dx, dy)
    
    nfloaters = nrows * ncols
    
    r = n.zeros(nfloaters, dtype = float)
    
    for ind in range(0,nfloaters):
        
        print(str(ind))
        
        layout.append(Body(name, x[ind], y[ind], r[ind]))
    # -------------------------------------------------------------------------
        
    # Initialization of the farm
    # -------------------------------------------------------------------------
    myfarm = Farm(layout, myenv, dict_bodies)
    
    exc_forces = myfarm.gen_excitation_forces_farm(myenv,mySim.beta_array)

    (AM_coeffs, DAMP_coeffs) = myfarm.gen_hydrodynamic_coefficients_farm(myenv,mySim.dof_array)
    
    # Mooring definition
    #--------------------------------------------------------------------------
    # diag components of each freedom deg (use none if no k_mooring)
    k_mooring = [0,0,0,0,0,0] #units!!
    
    # Mooring used for the WECs
    Mooring_1 = Mooring(name, myfarm, k_mooring)
    
    Mooring_layout = [Mooring_1] * nfloaters
    #--------------------------------------------------------------------------
    
    # Initialization of the mechanical simulation
    #------------------------------------------------------------------------------
    myMechanics = Mechanics(mySim, myenv, myfarm, Mooring_layout)
    
    # PTO definition
    #--------------------------------------------------------------------------
    # diag components of each freedom deg (use none if no k_pto or b_pto)
    k_pto = [0, 0, 0, 0, 0, 0] #units!!
    if (mode==0):
        k_pto[mode] = KPTO
    else:
        k_pto[mode] = 0
    b_pto = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    b_pto[mode] = BPTO
    
    # PTO used for the WECs
    BPTO_array = n.diag(b_pto)
    
    KPTO_array = n.diag(k_pto)
    
    b_pto_layout = [BPTO_array] * len(myMechanics.M_layout)
    
    k_pto_layout = [KPTO_array] * len(myMechanics.M_layout)
    #--------------------------------------------------------------------------
    
    power_wc_bodies = n.zeros(len(myMechanics.M_layout), dtype=float)
    power_wc_bodies_isolated = n.zeros(len(myMechanics.M_layout), dtype=float)
    qfactors = n.zeros(len(myMechanics.M_layout), dtype=float)
    
    for ind_seastate in range(0, len(mySim.Hs)):
        
        print ('ind_seastate' + ' ' + str(ind_seastate))

        Hs = mySim.Hs[ind_seastate] 
        Tp = mySim.Tp[ind_seastate]
        Prob = mySim.Prob[ind_seastate]
        
        wave_amplitudes_array = myenv.bretschneider_spectrum_angular_spreading(Hs, Tp, myenv.frequency, mySim.ndir, mySim.m, mySim.theta_principal)[0]
        
        power_seastate_bodies = myMechanics.Power_bodies_infarm_seastate(myenv, wave_amplitudes_array, k_pto_layout, b_pto_layout)
        
        power_wc_bodies[:] = power_seastate_bodies[:] * Prob + power_wc_bodies[:]
        
        power_isolseastate_bodies = myMechanics.Power_bodies_isolated_seastate(myenv, wave_amplitudes_array, k_pto_layout, b_pto_layout)
        
        power_wc_bodies_isolated[:] = power_isolseastate_bodies[:] * Prob + power_wc_bodies_isolated[:]
        
    qfactors = power_wc_bodies / power_wc_bodies_isolated
    
    return qfactors
#------------------------------------------------------------------------------
#
#
# constant BPTO for a single body
def power_anual_calculation_fbpto_indivbody(BPTO, mySim, myenv, myMechanics):
    
    print ('BPTO value' + ' ' + str(BPTO))
    
    # PTO definition
    #--------------------------------------------------------------------------
    # diag components of each freedom deg (use none if no k_pto or b_pto)
    k_pto = [0, 0, 0, 0, 0, 0] #units!!
    b_pto = [0.0, 0.0, BPTO, 0.0, 0.0, 0.0] # Simulation in HEAVE!!
    
    # PTO used for the WECs
    BPTO_array = n.diag(b_pto)
    
    KPTO_array = n.diag(k_pto)
    
    b_pto_layout = [BPTO_array] 
    
    k_pto_layout = [KPTO_array] 
    #--------------------------------------------------------------------------
    
    power_anual = 0.0
    
    for ind_seastate in range(0, len(mySim.Hs)):

        Hs = mySim.Hs[ind_seastate] 
        Tp = mySim.Tp[ind_seastate]
        Prob = mySim.Prob[ind_seastate]
        
        wave_amplitudes_array = myenv.bretschneider_spectrum_angular_spreading(Hs, Tp, myenv.frequency, mySim.ndir, mySim.m, mySim.theta_principal)[0]

        power_seastate_bodies = myMechanics.Power_bodies_isolated_seastate(myenv, wave_amplitudes_array, k_pto_layout, b_pto_layout)
        
        # Compute total power (minus sign for the optimization routine)
        power_seastate_total = - n.sum(power_seastate_bodies) 
        
        power_anual = power_seastate_total*Prob + power_anual
        
        
    return power_anual
#------------------------------------------------------------------------------
#
#
# constant BPTO for a single body
def power_anual_and_seastate_calculation_fbpto_indivbody(BPTO, KPTO, mySim, myenv, myMechanics, mode):
    
    print ('BPTO value' + ' ' + str(BPTO))
    
    # PTO definition
    #--------------------------------------------------------------------------
    # diag components of each freedom deg (use none if no k_pto or b_pto)
    k_pto = [0, 0, 0, 0, 0, 0] #units!!
    if (mode == 0):
        k_pto[mode] = KPTO
    else:
        k_pto[mode] = 0.0
        
    b_pto = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0] # Simulation in HEAVE!!

    b_pto[mode] = BPTO
    
    
    # PTO used for the WECs
    BPTO_array = n.diag(b_pto)
    
    KPTO_array = n.diag(k_pto)
    
    b_pto_layout = [BPTO_array] 
    
    k_pto_layout = [KPTO_array] 
    #--------------------------------------------------------------------------
    
    power_anual = 0.0
    
    power_seastates = []
    
    for ind_seastate in range(0, len(mySim.Hs)):

        Hs = mySim.Hs[ind_seastate] 
        Tp = mySim.Tp[ind_seastate]
        Prob = mySim.Prob[ind_seastate]
        
        wave_amplitudes_array = myenv.bretschneider_spectrum_angular_spreading(Hs, Tp, myenv.frequency, mySim.ndir, mySim.m, mySim.theta_principal)[0]

        power_seastate_bodies = myMechanics.Power_bodies_isolated_seastate(myenv, wave_amplitudes_array, k_pto_layout, b_pto_layout)

        # Compute total power (minus sign for the optimization routine)
        power_seastate_total = - n.sum(power_seastate_bodies) 
        
        power_seastates.append(power_seastate_total)
        
        power_anual = power_seastate_total*Prob + power_anual
        
        
    return (power_anual,power_seastates)
#------------------------------------------------------------------------------
#
#
# constant BPTO for a single body
def power_anual_calculation_fbpto_indivbody_surge(BPTO, KPTO, mySim, myenv, myMechanics):
    
    print ('BPTO value' + ' ' + str(BPTO))
    
    # PTO definition
    #--------------------------------------------------------------------------
    # diag components of each freedom deg (use none if no k_pto or b_pto)
    k_pto = [KPTO, 0, 0, 0, 0, 0] #units!!
    b_pto = [BPTO, 0.0, 0.0, 0.0, 0.0, 0.0] # Simulation in HEAVE!!
    
    # PTO used for the WECs
    BPTO_array = n.diag(b_pto)
    
    KPTO_array = n.diag(k_pto)
    
    b_pto_layout = [BPTO_array] 
    
    k_pto_layout = [KPTO_array] 
    #--------------------------------------------------------------------------
    
    power_anual = 0.0
    
    for ind_seastate in range(0, len(mySim.Hs)):

        Hs = mySim.Hs[ind_seastate] 
        Tp = mySim.Tp[ind_seastate]
        Prob = mySim.Prob[ind_seastate]
        
        wave_amplitudes_array = myenv.bretschneider_spectrum_angular_spreading(Hs, Tp, myenv.frequency, mySim.ndir, mySim.m, mySim.theta_principal)[0]

        power_seastate_bodies = myMechanics.Power_bodies_isolated_seastate(myenv, wave_amplitudes_array, k_pto_layout, b_pto_layout)
        
        # Compute total power (minus sign for the optimization routine)
        power_seastate_total = - n.sum(power_seastate_bodies) 
        
        power_anual = power_seastate_total*Prob + power_anual
        
        
    return power_anual
#------------------------------------------------------------------------------
#
#
#------------------------------------------------------------------------------    
def power_anual_seastate_calculation_fx_staggered_grid(dx, BPTO, KPTO, nrows, ncols, dict_bodies, mySim, myenv, name, mode):
    
    print ('dx value' + ' ' + str(dx))
    
    # grid characteristics
    # -------------------------------------------------------------------------
    dy = dx


    # Layout definition
    # -------------------------------------------------------------------------
    layout    = []
    
    (x,y) = staggered_square_grid_generation(nrows, ncols, dx, dy)
    
    nfloaters = nrows * ncols
    
    r = n.zeros(nfloaters, dtype = float)
    
    for ind in range(0,nfloaters):
        
        print(str(ind))
        
        layout.append(Body(name, x[ind], y[ind], r[ind]))
    # -------------------------------------------------------------------------
        
    # Initialization of the farm
    # -------------------------------------------------------------------------
    myfarm = Farm(layout, myenv, dict_bodies)
    
    exc_forces = myfarm.gen_excitation_forces_farm(myenv,mySim.beta_array)

    (AM_coeffs, DAMP_coeffs) = myfarm.gen_hydrodynamic_coefficients_farm(myenv,mySim.dof_array)
    
    # Mooring definition
    #--------------------------------------------------------------------------
    # diag components of each freedom deg (use none if no k_mooring)
    k_mooring = [0,0,0,0,0,0] #units!! 
    
    # Mooring used for the WECs
    Mooring_1 = Mooring(name, myfarm, k_mooring)
    
    Mooring_layout = [Mooring_1] * nfloaters
    #--------------------------------------------------------------------------
    
    # Initialization of the mechanical simulation
    #------------------------------------------------------------------------------
    myMechanics = Mechanics(mySim, myenv, myfarm, Mooring_layout)
    
    # PTO definition
    #--------------------------------------------------------------------------
    # diag components of each freedom deg (use none if no k_pto or b_pto)
    k_pto = [0, 0, 0, 0, 0, 0] #units!!
    if (mode == 0):
        k_pto[mode] = KPTO
    else:
        k_pto[mode] = 0.0
    b_pto = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0] 
    b_pto[mode] = BPTO
    
    # PTO used for the WECs
    BPTO_array = n.diag(b_pto)
    
    KPTO_array = n.diag(k_pto)
    
    b_pto_layout = [BPTO_array] * len(myMechanics.M_layout)
    
    k_pto_layout = [KPTO_array] * len(myMechanics.M_layout)
    #--------------------------------------------------------------------------
    
    power_anual = 0.0
    
    power_seastates = []
    
    for ind_seastate in range(0, len(mySim.Hs)):
        
        print ('ind_seastate' + ' ' + str(ind_seastate))

        Hs = mySim.Hs[ind_seastate] 
        Tp = mySim.Tp[ind_seastate]
        Prob = mySim.Prob[ind_seastate]
        
        wave_amplitudes_array = myenv.bretschneider_spectrum_angular_spreading(Hs, Tp, myenv.frequency, mySim.ndir, mySim.m, mySim.theta_principal)[0]
        
        power_seastate_bodies = myMechanics.Power_bodies_infarm_seastate(myenv, wave_amplitudes_array, k_pto_layout, b_pto_layout)
        
        # Compute total power (minus sign for the optimization routine)
        power_seastate_total = - n.sum(power_seastate_bodies)
        
        power_seastates.append(power_seastate_total)
        
        power_anual = power_seastate_total*Prob + power_anual
        
        
    return (power_anual, power_seastates)
#------------------------------------------------------------------------------